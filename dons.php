<?php
/***************************************************************************
 *                                  dons.php
 ***************************************************************************/

/***************************************************************************
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

define('IN_PHPBB', true);
$phpbb_root_path = './';
include($phpbb_root_path . 'extension.inc');
include($phpbb_root_path . 'common.'.$phpEx);

global $show_ads;
$show_ads = false;

//
// Start session management
//
$userdata = session_pagestart($user_ip, PAGE_INDEX);
init_userprefs($userdata);
//
// End session management
//

//
// Lets build a page ...
//
$__g_alt_css = true;
include($phpbb_root_path . 'includes/page_header.'.$phpEx);

$template->set_filenames(array(
	'body' => 'dons_body.tpl')
);
make_jumpbox('viewforum.'.$phpEx);

//$template->assign_vars(array());

$template->pparse('body');

include($phpbb_root_path . 'includes/page_tail.'.$phpEx);

?>
