<?php
/***************************************************************************
 *                               viewtopic.php
 *                            -------------------
 *   begin                : Saturday, Feb 13, 2001
 *   copyright            : (C) 2001 The phpBB Group
 *   email                : support@phpbb.com
 *
 *   $Id: viewtopic.php,v 1.186.2.45 2005/10/05 17:42:04 grahamje Exp $
 *
 *
 ***************************************************************************/

/***************************************************************************
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

define('IN_PHPBB', true);
$phpbb_root_path = './';
include($phpbb_root_path . 'extension.inc');
include($phpbb_root_path . 'common.'.$phpEx);

header("Cache-Control: no-cache, must-revalidate");

//
// Start session management
//
$userdata = session_pagestart($user_ip, PAGE_PROFILE);
//init_userprefs($userdata);
//
// End session management
//

/**/
// Debugging
echo "<pre>\n";

print_r($userdata);

/**/

//
// Start auth check
//
if ( $userdata['session_logged_in'] )
{
	echo 'Logged in';	// Debugging
}

echo '</pre>';	// Debugging

?>