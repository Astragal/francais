<?php

define('IN_PHPBB', true);
$phpbb_root_path = './../';
include($phpbb_root_path . 'extension.inc');
include($phpbb_root_path . 'common.php');

// session management
$userdata = session_pagestart($user_ip, PAGE_INDEX);
init_userprefs($userdata);

// if user is not a moderator
if ($userdata['session_logged_in'] != 1 || ($userdata['user_level'] != ADMIN && $userdata['user_level'] != MOD))
{
	redirect("/index.php");
}

include_once('includes/user.php');

$_GET = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
$mode = '';

if (isset($_GET['mode']))
{
	$mode = $_GET['mode'];
}
elseif (isset($_POST['mode']))
{
	$mode = $_POST['mode'];
}
else
{
	redirect("/index.php");
}

switch ($mode)
{
	case 'user_toggleApproved':
		if (isset($_POST['user_id']))
		{
			echo user_toggleApproved($_POST['user_id']);
		}
		break;
	case 'setModeratorsApproved':
		setModeratorsApproved();
		break;
}

?>
