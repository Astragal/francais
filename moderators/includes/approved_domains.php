<?php

$approved_domains = array();

$approved_domains[] = '/\b \Q' . preg_replace('/\w+\.\w+$/', '\0', $_SERVER['HTTP_HOST']) . '\E \z/ix';

$approved_domains[] = '/\b \Qgoogle.\E [a-z]+\z/ix';

$approved_domains[] = '/\b \Qcnrtl.fr\E \z/ix';
$approved_domains[] = '/\b \Qlarousse.fr\E \z/ix';
$approved_domains[] = '/\b \Qacademie-francaise.fr\E \z/ix';
$approved_domains[] = '/\b \Qlittre.org\E \z/ix';
$approved_domains[] = '/\b \Qlexilogos.com\E \z/ix';
$approved_domains[] = '/\b \Qwikipedia.org\E \z/ix';
$approved_domains[] = '/\b \Qwiktionary.org\E \z/ix';
$approved_domains[] = '/\b \Qwikiquote.org\E \z/ix';
$approved_domains[] = '/\b \Qwikibooks.org\E \z/ix';
$approved_domains[] = '/\b \Qpersee.fr\E \z/ix';
