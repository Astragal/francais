<?php

function user_setApproved($user_id, $approved = 1)
{
	global $db;

	checkUserID($user_id);

	$approved = intval($approved);
	$sql = "UPDATE " . USERS_TABLE . "
		SET user_approved = " . $approved . "
		WHERE user_id = $user_id;";

	if (!$result = $db->sql_query($sql))
	{
		message_die(CRITICAL_ERROR, "Could not set user config information", "", __LINE__, __FILE__, $sql);
	}

	return $approved;
}

function user_getApproved($user_id)
{
	global $db;

	checkUserID($user_id);

	$sql = "SELECT user_approved
		FROM " . USERS_TABLE . "
		WHERE user_id = $user_id;";

	if (!$result = $db->sql_query($sql))
	{
		message_die(CRITICAL_ERROR, "Could not query user config information", "", __LINE__, __FILE__, $sql);
	}

	$row = $db->sql_fetchrow($result);
	return $row['user_approved'];
}

function user_toggleApproved($user_id)
{
	checkUserID($user_id);

	$approved = intval(!user_getApproved($user_id));
	return user_setApproved($user_id, $approved);
}

function isValidUserID($user_id)
{
	return preg_match('/^-?\d{1,7}$/', $user_id);
}

function checkUserID($user_id)
{
	if (!isValidUserID($user_id))
	{
		message_die(GENERAL_ERROR, "L'user_id suivant est invalide : '$user_id'.", "", __LINE__, __FILE__);
	}
}

function setModeratorsApproved()
{
	global $db;

	$sql = "UPDATE " . USERS_TABLE . "
		SET user_approved = 1
		WHERE user_level = " . ADMIN . " OR user_level = " . MOD . ";";

	if (!$result = $db->sql_query($sql))
	{
		message_die(CRITICAL_ERROR, "Could not set user config information", "", __LINE__, __FILE__, $sql);
	}

	echo $result;
}

?>
