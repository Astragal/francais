<?php
/***************************************************************************
                               mail_digest.php
                             -------------------
    begin                : Friday, August 17, 2007
    copyright            : (c) Mark D. Hamill
    email                : mhamill@computer.org

    $Id: $

 ***************************************************************************/

/***************************************************************************
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

// Written by Mark D. Hamill, mhamill@computer.org
// This software is designed to work with phpBB Version 2.0.22

// ----------------------------------------- WARNING ---------------------------------------------- //
// THIS PROGRAM SHOULD BE INVOKED TO RUN AUTOMATICALLY EVERY HOUR BY THE OPERATING SYSTEM USING AN 
// OPERATING SYSTEM FEATURE LIKE CRONTAB. SEE THE INSTALLATION INSTRUCTIONS.
// ----------------------------------------- WARNING ---------------------------------------------- //

define('IN_PHPBB', true);

//
// You may need to change the $phpbb_root_path to absolute path to phpBB on your server. An example is shown. See the Digest Installation page for more guidance.
// $phpbb_root_path = '/home/username/public_html/phpBB2/';
//
$phpbb_root_path = './';

include($phpbb_root_path . 'extension.inc');
include($phpbb_root_path . 'common.' . $phpEx);

$userdata = session_pagestart($user_ip, PAGE_INDEX);
init_userprefs($userdata);

include($phpbb_root_path . 'includes/emailer.' . $phpEx);
include($phpbb_root_path . 'includes/digest_functions.' . $phpEx);
include($phpbb_root_path . 'includes/bbcode.' . $phpEx);
include($phpbb_root_path . 'language/lang_' . $board_config['default_lang'] . '/lang_digests.' . $phpEx);

$settings = get_digest_settings();	// This will retrieve the current global settings

// Do some security checks, if $settings['require_send_key'] is true
if (($settings['require_send_key'] == 'YES') && (htmlspecialchars($HTTP_GET_VARS['key'], ENT_COMPAT, 'ISO-8859-1') == ''))
{
	die($lang['digest_no_key']);
}
if (($settings['require_send_key'] == 'YES') && (htmlspecialchars($HTTP_GET_VARS['key'], ENT_COMPAT, 'ISO-8859-1') <> $settings['send_key']))
{
	die($lang['digest_invalid_key']);
}

// Set some needed variables
$link_tag = '';
$link_tag_unset = true;
$break_type = ($settings['summary_type'] == 'HTML') ? "<br />\n" : "\n";
$digest_log_entry = '';

// Is this program being run in test mode? If there is an argument test=1 then programmed recipients will
// not receive a digest for the given hour. The processing will occur but the digests will not actually be emailed.
// However, you will see statistical information, providing $settings['show_summary'] = 'YES'
$test_mode = (htmlspecialchars($HTTP_GET_VARS['test'], ENT_COMPAT, 'ISO-8859-1') == '1') ? true : false;

// Is this program in simulate user mode? If simulate=1 then a digest will be mailed to the $lang['digest_from_email_address']
// instead of the intended recipient for the given hour.
$simulate_mode = (htmlspecialchars($HTTP_GET_VARS['simulate'], ENT_COMPAT, 'ISO-8859-1') == '1') ? true : false;

$test_mode2 = (htmlspecialchars($HTTP_GET_VARS['test2'], ENT_COMPAT, 'ISO-8859-1') == '1') ? true : false;

// Is today the day to run the weekly digest?
$today = getdate();
$wday = $today['wday'];
$current_hour = $today['hours'];
$weekly_digest_text = ($wday == $settings['weekly_digest_day'] ) ? " or (digest_type = 'WEEK' and send_hour = " . $current_hour . ')' : '';

// Get a list of all users to receive digests for this hour and day
$sql = "SELECT s.user_id, u.username, u.user_email, u.user_lastvisit, u.user_level, u.user_timezone, u.user_sig_bbcode_uid, u.user_last_privmsg,
	digest_type, format, show_text, show_mine, new_only, send_on_no_messages, send_hour, text_length, u.user_lang, s.show_pms  
	FROM " . DIGEST_SUBSCRIPTIONS_TABLE . ' s, ' . USERS_TABLE . " u 
	WHERE s.user_id = u.user_id AND ((digest_type = 'DAY' AND send_hour = " . $current_hour . 
	')' . $weekly_digest_text . ') AND u.user_active = 1 ORDER BY user_lang'; 
if ($test_mode2)
{
	$sql = "SELECT s.user_id, u.username, u.user_email, u.user_lastvisit, u.user_level, u.user_timezone, u.user_sig_bbcode_uid, u.user_last_privmsg,
	digest_type, format, show_text, show_mine, new_only, send_on_no_messages, send_hour, text_length, u.user_lang, s.show_pms  
	FROM phpbb_mod_subscriptions s, phpbb_users u 
	WHERE s.user_id = u.user_id AND u.user_active = 1 ORDER BY user_lang";
}

if ( !($result = $db->sql_query($sql)))
{
	message_die(GENERAL_ERROR, 'Unable to retrieve list of users wanting a digest', '', __LINE__, __FILE__, $sql);
}

// Retrieve a list of forum_ids that all registered users can access. Since digests go only to registered
// users it's important to include those forums not accessible to the general public but accessible to users.
$sql2 = 'SELECT forum_id FROM ' . FORUMS_TABLE . ' WHERE auth_read IN (' . AUTH_ALL . ', ' . AUTH_REG . ')';

if ( !($result2 = $db->sql_query($sql2)))
{
	message_die(GENERAL_ERROR, 'Unable to retrieve list of forum_ids all members can access', '', __LINE__, __FILE__, $sql);
}
$i = 0;
$valid_forums = array();
while ($row2 = $db->sql_fetchrow($result2)) 
{
	$valid_forums [$i] = $row2['forum_id'];
	$i++;
}

// Retrieve a list of forum_ids that all moderators can access. 
$sql2 = 'SELECT forum_id FROM ' . FORUMS_TABLE . ' WHERE auth_read IN (' . AUTH_ALL . ', ' . AUTH_REG . ', ' . AUTH_MOD . ')';

if ( !($result2 = $db->sql_query($sql2)))
{
	message_die(GENERAL_ERROR, 'Unable to retrieve list of forum_ids all members can access', '', __LINE__, __FILE__, $sql);
}
$i = 0;
$moderator_forums = array();
while ($row2 = $db->sql_fetchrow($result2)) 
{
	$moderator_forums [$i] = $row2['forum_id'];
	$i++;
}

// Retrieve a list of forum_ids that administrators can access, which is everything
$sql2 = 'SELECT forum_id FROM ' . FORUMS_TABLE . ' order by 1';

if ( !($result2 = $db->sql_query($sql2)))
{
	message_die(GENERAL_ERROR, 'Unable to retrieve list of forum_ids all members can access', '', __LINE__, __FILE__, $sql);
}
$i = 0;
$admin_forums = array();
while ($row2 = $db->sql_fetchrow($result2)) 
{
	$admin_forums [$i] = $row2['forum_id'];
	$i++;
}

// With each pass through the loop one user will receive a customized digest.

$digests_sent = 0;
while ($row = $db->sql_fetchrow($result)) 
{

	// This logic ensures that the ancillary text in the digest matches the user's language preference
	if($current_lang != $row['user_lang'])
	{
		$current_lang = $row['user_lang'];
		include($phpbb_root_path . 'language/lang_' . $current_lang . '/lang_digests.' . $phpEx);
	}
	
	// This logic ensures the hour the user wanted to receive the digest is reported correctly
	// in the digest.
	$board_timezone = date('Z')/3600;
	$user_timezone = (float) $row['user_timezone'];
	$offset = $board_timezone - $user_timezone;
	$send_hour = (float) $row['send_hour'] - $offset;
	$send_hour = ($send_hour < 0) ? $send_hour + 24 : $send_hour;
	$send_hour = ($send_hour >= 24) ? $send_hour - 24 : $send_hour;

	if ($row['new_only'] == 'TRUE') 
	
	{

		// To filter out any possible messages a user may have seen we need to examine a number of 
		// possibilities, including last user message date/time, date/time of last session, if it exists, and
		// of course, the last access date/time in the USERS table. Of these 3 possibilities, whichever is
		// the greatest value is the actual last accessed date, and we may need to filter out messages
		// prior to this date and time. My experience is phpBB doesn't always get it right.

		$sql3 = 'SELECT max(post_time) AS last_post_date FROM ' . POSTS_TABLE . 
			' WHERE poster_id = ' . $row['user_id'];

		if ( !($result3 = $db->sql_query($sql3)))
		{
			message_die(GENERAL_ERROR, 'Unable to select last post date for user.', '', __LINE__, __FILE__, $sql);
		}
		$row3 = $db->sql_fetchrow($result3);
		$last_post_date = ($row3['last_post_date'] <> '') ? $row3['last_post_date'] : 0;

		// When did the user's last session accessed?
		$sql3 = 'SELECT max(session_time) AS last_session_date 
			FROM ' . SESSIONS_TABLE .
			' WHERE session_user_id = ' . $row['user_id'];

		if ( !($result3 = $db->sql_query($sql3)))
		{
			message_die(GENERAL_ERROR, 'Unable to get last session date for user', '', __LINE__, __FILE__, $sql);
		}
		$row3 = $db->sql_fetchrow($result3);
		$last_session_date = ($row3['last_session_date'] <> '') ? $row3['last_session_date'] : 0;

		$last_visited_date = $row['user_lastvisit'];
		if ($last_visited_date == '')
		{
			$last_visited_date = 0;
		}

		// The true last visit date is the greatest of: last_visited_date, last message posted, and last session date
		$last_visited_date = max($last_post_date, $last_session_date, $last_visited_date);

	}

	// Get a list of forums that can only be read if user has been granted explicit permission
	$i = 0;	
	$elected_forums = array();
	$sql3 = 'SELECT distinct a.forum_id
		FROM ' . AUTH_ACCESS_TABLE . ' a, ' . USER_GROUP_TABLE . ' ug, ' . GROUPS_TABLE . ' g
		WHERE ug.group_id = g.group_id AND ug.user_id = ' . $row['user_id']
		. ' AND ug.user_pending = 0 
		AND a.group_id = ug.group_id';

	if ( !($result3 = $db->sql_query($sql3)))
	{
		message_die(GENERAL_ERROR, 'Unable to retrieve list of elected forums', '', __LINE__, __FILE__, $sql);
	}
	while ($row3 = $db->sql_fetchrow($result3)) 
	{
		$elected_forums [$i] = $row3['forum_id'];
		$i++;
	}

	// Get the union of the valid/moderator/admin_forums array (whichever applies) and the elected_formus array. The  
	// resulting elements are the forums that could be queried. This is necessary because MySQL 3.x doesn't support SQL Unions.
	switch ($row['user_level'])
	{
		case MOD:
			$queried_forums = array_merge($moderator_forums, $elected_forums);
			break;
		case ADMIN:
			$queried_forums = $admin_forums; // Administrators see all forums
			break;
		default:
			$queried_forums = array_merge($valid_forums, $elected_forums);
			break;
	}
	$queried_forums = array_unique($queried_forums);

	// Further filter the number of messages sent by including only forums from which the user
	// specifically wants to get emails. If there are none, assume all.
	$i = 0;
	unset ($subscribed_forums);
	$subscribed_forums = array();
	
	$sql3 = 'SELECT forum_id
		FROM ' . DIGEST_SUBSCRIBED_FORUMS_TABLE .
		' WHERE user_id = ' . $row['user_id'];

	if ( !($result3 = $db->sql_query($sql3)))
	{
		message_die(GENERAL_ERROR, 'Unable to retrieve list of subscribed forums', '', __LINE__, __FILE__, $sql);
	}

	while ($row3 = $db->sql_fetchrow($result3)) 
	{
		$subscribed_forums [$i] = $row3['forum_id'];
		$i++; 
	}

	// If there are subscribed forums, we only want to see messages for these forums.
	if ($i <> 0) 
	{ 
		$queried_forums = array_intersect($queried_forums, $subscribed_forums);
	}

	// Create a list of forums to be queried from the database. This is a comma delimited list of all forums
	// the user is allowed to read that can be used with the SQL IN operation.
	$forum_list = implode(',',$queried_forums);

	// Format sender's email address (SMTP seems to have a problem with adding username)
	$send_to = ($simulate_mode) ? $lang['digest_from_email_address'] : $row['user_email'];
	$to = ($board_config['smtp_delivery']) ? $send_to : $row['username'] . ' <' . $send_to  . '>';

	// Show the text of the message?
	$show_text = ($row['show_text'] == 'YES') ? true: false; 

	// Show messages written by this user?
	$show_mine = ($row['show_mine'] == 'YES') ? true: false; 

	// Show private messages?
	$show_pms = ($row['show_pms'] == 'YES') ? true: false; 

	// Prepare to get digest type
	if(trim($row['digest_type']) == 'DAY') 
	{
		$msg_period = $lang['digest_period_24_hrs'];
		$period = time() - (24 * 60 * 60);
	}
	else 
	{
		$msg_period = $lang['digest_period_1_week'];
		$period = time() - (7 * 24 * 60 * 60);
	}

	// Format differently if HTML requests
	if(($row['format'] == 'HTML') || ($row['format'] == 'PHTM')) 
	{ 
		$html = true;
		$enhanced_style = ($row['format'] == 'HTML') ? true : false;
		$parastart = '<p>';
		$paraend = "</p>\n";
	}
	else 
	{
		$html = false;
		$parastart = '';
		$paraend = "\n";
	}

	// Set part of SQL needed to retrieve new only, or messages through the selected period
	if ($row['new_only'] == 'TRUE') 
	{
		$code = max($period, $last_visited_date);
	}        
	else 
	{
		$code = $period; 
	}

	// Filter out user's own postings, if they so elected
	if ($show_mine == false)
	{
		$code .= ' and p.poster_id <> ' . $row['user_id'];
	}

	// The emailer class does not have the equivalent of the assign_block_vars operation, so the
	// entire digest must be placed inside a variable.
	$msg = '';

	// If there are any unread private messages, display each message as an item in the newsfeed
	
	if ($show_pms)

	{

		$unread_pms = 0;  // The number of actual private messages meeting the selection criteria may less than the total unread private messages
		$pm_headers_printed = false;
		
		$sql2 = 'SELECT u.username AS username_1, u.user_id AS user_id_1, u2.username AS username_2, u2.user_id AS user_id_2, u.user_sig_bbcode_uid,
					u.user_posts, u.user_from, u.user_website, u.user_email, u.user_icq, u.user_aim, u.user_yim, u.user_regdate, u.user_msnm, u.user_viewemail,
					u.user_rank, u.user_sig, u.user_avatar, u.user_allowhtml, u.user_allowsmile, pm.*, pmt.privmsgs_bbcode_uid, pmt.privmsgs_text
				FROM ' . PRIVMSGS_TABLE . ' pm, ' . PRIVMSGS_TEXT_TABLE . ' pmt, ' . USERS_TABLE . ' u, ' . USERS_TABLE . ' u2 
				WHERE pmt.privmsgs_text_id = pm.privmsgs_id 
				AND u.user_id = pm.privmsgs_from_userid 
				AND u2.user_id = pm.privmsgs_to_userid
				AND pm.privmsgs_to_userid = ' . $row['user_id'] . ' AND privmsgs_date > ' . $row['user_lastvisit'] . ' AND privmsgs_type IN (' . PRIVMSGS_NEW_MAIL . ', ' . PRIVMSGS_UNREAD_MAIL . ')';

		if ( !($result2 = $db->sql_query($sql2)))
		{
			message_die(GENERAL_ERROR, $lang['smartfeed_pm_retrieve_error'], '', __LINE__, __FILE__, $sql2);
		}
		else
		{

			while ($row2 = $db->sql_fetchrow ($result2))
			{

				$unread_pms++;
		
				// Print private message headers
				if (!$pm_headers_printed)
				{
					if ($html) 
					{
						$msg .= '<h2><a href="' . $settings['site_url'] . 'privmsg.php?folder=inbox">' . $lang['digest_unread_private_messages'] .  "</a></h2>\n";
						$msg .= '<p><span class="postbody">' . $lang['digest_pm_explanation_html'] . "</span></p>\n";
					}
					else
					{
						$msg .= "\n<<<< " . $lang['digest_unread_private_messages'] . " >>>>\n";
						$msg .= "\n" . $lang['digest_pm_explanation_text'] . "\n";
					}
					$pm_headers_printed = true;
				}
			
				// Calculate Display Time. Show in local time based on timezone in user's profile.
				$display_time = date($settings['date_format'], ($row2['privmsgs_date'] - (3600 * $offset)));
				$private_message = $row2['privmsgs_text'];
				// If directed, limit length of Post Text
				$private_message = (strlen($private_message) <= $row['text_length']) ? $private_message : substr($private_message, 0, $row['text_length']) . '...';
				
				if ($html) 
				{
				
					$msg .= "<table border=\"1\">\n";
					$msg .= "<tr>\n";
					if ($show_text)
					{
						$msg .= '<th>' . $lang['digest_link'] . '</th><th>' . $lang['digest_post_time'] . '</th><th>' . $lang['digest_author'] . 
							'</th><th>' . $lang['digest_subject'] . '</th><th>' . $lang['digest_message_excerpt'] . "</th>\n";
					}
					else
					{
						$msg .= '<th>' . $lang['digest_link'] . '</th><th>' . $lang['digest_post_time'] . '</th><th>' . $lang['digest_author'] . 
							'</th><th>' . $lang['digest_subject'] . "</th>\n";
					}
	
					$msg .= "</tr>\n"; 

					$msg .= "<tr>\n";
					
					// Link
					$msg .= '<td><span class="postbody"><a href="' . $settings['site_url'] . "privmsg.$phpEx?folder=inbox&mode=read&" . POST_POST_URL . '=' . $row2['privmsgs_id'] . '">' . $row2['privmsgs_id'] . "</span></td>\n";
					
					// Message Time
					$msg .= '<td><span class="postbody">' . $display_time . "</span></td>\n";
					
					// Sender
					$msg .= '<td><span class="postbody">' . $row2['username_1'] . "</span></td>\n";
					
					// Subject
					$msg .= '<td><span class="postbody">' . $row2['privmsgs_subject'] . "</span></td>\n";
					
					if ($show_text)
					{
						// Message Text
						
						if ($enhanced_style)
						{
						
							// Begin: The following code minimally modified from viewtopic.php (2.0.21). That's why it works!
							
							$bbcode_uid = $row2['privmsgs_bbcode_uid'];
						
							$user_sig = ( $row2['privmsgs_attach_sig'] && $row2['user_sig'] != '' && $board_config['allow_sig'] ) ? $row2['user_sig'] : '';
							$user_sig_bbcode_uid = $row2['user_sig_bbcode_uid'];
						
							//
							// Note! The order used for parsing the message _is_ important, moving things around could break any
							// output
							//
						
							//
							// If the board has HTML off but the post has HTML
							// on then we process it, else leave it alone
							//
							if ( !$board_config['allow_html'] || !$row2['user_allowhtml'])
							{
								if ( $user_sig != '' )
								{
									$user_sig = preg_replace('#(<)([\/]?.*?)(>)#is', "&lt;\\2&gt;", $user_sig);
								}
						
								if ( $row2['privmsgs_enable_html'] )
								{
									$private_message = preg_replace('#(<)([\/]?.*?)(>)#is', "&lt;\\2&gt;", $private_message);
								}
							}
						
							//
							// Parse message and/or sig for BBCode if reqd
							//
							
							if ($user_sig != '' && $user_sig_bbcode_uid != '')
							{
								$user_sig = ($board_config['allow_bbcode']) ? bbencode_second_pass($user_sig, $user_sig_bbcode_uid) : preg_replace("/\:$user_sig_bbcode_uid/si", '', $user_sig);
							}
			
							if ($bbcode_uid != '')
							{
								$private_message = ($board_config['allow_bbcode']) ? bbencode_second_pass($private_message, $bbcode_uid) : preg_replace("/\:$bbcode_uid/si", '', $private_message);
								if (strlen($private_message) <= $row['text_length'])
								{
									if (strstr($private_message,'<table width="90%" cellspacing="1" cellpadding="3" border="0" align="center">') != FALSE)
									{
										$private_message = $private_message . '</td></tr></table>';
									}
								}
							}
						
							if ( $user_sig != '' )
							{
								$user_sig = '<br />_________________<br />' . make_clickable($user_sig);
							}
							
							//
							// Parse smilies
							//
							if ( $board_config['allow_smilies'] )
							{
								if ( $row2['user_allowsmile'] && $user_sig != '' )
								{
									$user_sig = smilies_pass_digest($user_sig);
								}
						
								if ( $row2['privmsgs_enable_smilies'] )
								{
									$private_message = smilies_pass_digest($private_message);
								}
							}
							$private_message = make_clickable($private_message) . $user_sig;
							
							// End: The following code minimally modified from viewtopic.php
						
						}
						else
						{
							// Provides a more textual style, stripping out all BBCode (which includes images, URLs, etc) but reducing digest size. This is the classic 
							// presentation method.

							$private_message = smilies_pass_digest($private_message);
							$private_message = preg_replace("/\[.+\]/iU",'',$private_message); 
							$private_message = strip_tags($private_message);
							$private_message = trim( ereg_replace("[a-zA-Z]+://([-]*[.]?[a-zA-Z0-9_/-?&%])*", "", $private_message ) );
							$private_message = preg_replace("! +!",' ',$private_message);
							$msg .= $this_msg . "\n";

						}
						$private_message = preg_replace('/\\n/', '<br />', $private_message);
						$msg .= '<td><span class="postbody">' . $private_message . "</span></td>\n";
					}
					
					$msg .= "</tr>\n";
				}
				else
				{
					// Private messages as text
					
					$msg .= "\n<< " . $lang['digest_subject'] . ': ' . $row2['privmsgs_subject'] . " >>\n\n";

					$msg .= $lang['digest_posted_by'] . ' ' . $row2['username_1'] . ' ' . $lang['digest_posted_at'] . ' ' . $display_time . ', ' . $settings['site_url'] . "privmsg.$phpEx?folder=inbox&mode=read&" . POST_POST_URL . '=' . $row2['privmsgs_id'] . "\n";
					
					// If requested to show the message text
					if ($show_text) 
					{
						// Remove BBCode, makes for nicer presentation

						$private_message = smilies_pass_digest($private_message);
     					$private_message = preg_replace("/\[.+\]/iU",'',$private_message); 
     					$private_message = strip_tags($private_message);
     					$private_message = trim( ereg_replace("[a-zA-Z]+://([-]*[.]?[a-zA-Z0-9_/-?&%])*", "", $private_message ) );
						$private_message = preg_replace("! +!",' ',$private_message);
						$msg .= "\n" . $lang['digest_message_excerpt'] . ":\n\n" . $private_message . "\n";
						$msg .= "\n------------------------------\n"; 
					}
					else
					{
						$msg .= "\n------------------------------\n";			
					}
				}
		
			}

			if ($html && ($unread_pms > 0)) 
			{
				$msg.= "</table>\n<br />\n<hr />\n";
			}
			 
		}
		
	}

	// Create a list of messages for this user. Filter out unauthorized forums.
	$sql2 = "SELECT c.cat_id, f.forum_name, t.topic_title, u.username AS Posted_by, p.post_username, u.user_sig, u.user_allowhtml, p.post_time, u.user_allowsmile, 
		u.user_sig_bbcode_uid, pt.post_text, pt.bbcode_uid, p.post_id, t.topic_id, f.forum_id, p.enable_sig, p.enable_html, p.enable_smilies   
		FROM " . POSTS_TABLE . ' p, ' . TOPICS_TABLE . ' t, ' . FORUMS_TABLE . ' f, ' . USERS_TABLE . ' u, ' . 
		CATEGORIES_TABLE . ' c, ' . POSTS_TEXT_TABLE . ' pt 
		WHERE p.topic_id = t.topic_id AND t.forum_id = f.forum_id AND p.poster_id = u.user_id AND 
		f.cat_id = c.cat_id AND p.post_id = pt.post_id AND 
		post_time > ' . $code . ' AND f.forum_id IN (' . $forum_list . ') 
		ORDER BY c.cat_order, f.forum_order, t.topic_title, post_time'; 

	if ( !($result2 = $db->sql_query($sql2)))
	{
		message_die(GENERAL_ERROR, 'Unable to execute retrieve message summary for user', '', __LINE__, __FILE__, $sql);
	}

	// Format all the mail for this user

	$last_forum = '';
	$last_topic = '';
	$last_cat_id = -1;
	$msg_count = 0;

	while ($row2 = $db->sql_fetchrow($result2)) 
	{

		// Calculate Display Time. Show in local time based on timezone in user's profile.
		$display_time = date($settings['date_format'], ($row2['post_time'] - (3600 * $offset)));
		
		// If directed, limit length of Post Text
		$post_text = (strlen($row2['post_text']) <= $row['text_length']) ? $row2['post_text'] : substr($row2['post_text'], 0, $row['text_length']) . '...';
		
		// Show name of topic only if it changes
		if (($row2['topic_title'] <> $last_topic) || ($row2['cat_id'] <> $last_cat_id)) 
		{
			if ($html) 
			{
				if ($last_topic <> '')
				{
					$msg .= "</table>\n";
				}
				
				// Check for change in forum name
				if ($row2['forum_name'] <> $last_forum) 
				{ 
					$msg .= '<h2>' . $lang['digest_forum'] . '<a href="' . $settings['site_url'] . 'viewforum.' . $phpEx . '?' . POST_FORUM_URL . '=' . 
						$row2['forum_id'] . '">' . $row2['forum_name'] . "</a></h2>\n";
					$last_forum = $row2['forum_name'];
				}
						
				$msg .= '<h3>' . $lang['digest_topic'] . '<a href="' . $settings['site_url'] . 'viewtopic.' . $phpEx . '?' . POST_TOPIC_URL . '=' . 
					$row2['topic_id'] . '">' . $row2['topic_title'] . "</a></h3>\n";
				$msg .= "<table border=\"1\">\n";
				$msg .= "<tr>\n";
				if ($show_text)
				{
					$msg .= '<th>' . $lang['digest_link'] . '</th><th>' . $lang['digest_post_time'] . '</th><th>' . $lang['digest_author'] . 
						'</th><th>' . $lang['digest_message_excerpt'] . "</th>\n";
				}
				else
				{
					$msg .= '<th>' . $lang['digest_link'] . '</th><th>' . $lang['digest_post_time'] . '</th><th>' . $lang['digest_author'] . "</th>\n";
				}

				$msg .= "</tr>\n"; 
			}
			else
			{ 
				// Check for change in forum name
				if ($row2['forum_name'] <> $last_forum) 
				{ 
					$msg .= "\n<<<< " . $lang['digest_forum'] . $row2['forum_name'] . 
						', ' . $settings['site_url'] . 'viewforum.' . $phpEx . '?' . POST_FORUM_URL . '=' . $row2['forum_id'] . " >>>>\n";
					$last_forum = $row2['forum_name'];
				}
 
				$msg .= "\n<< " . $lang['digest_topic'] . $row2['topic_title'] . 
					', ' . $settings['site_url'] . 'viewtopic.' . $phpEx. '?' . POST_TOPIC_URL . '=' . $row2['topic_id'] . " >>\n\n";
			}
			$last_topic = $row2['topic_title'];
			$last_cat_id = $row2['cat_id'];
		}

		// Show message information
		if ($html) 
		{
			$msg .= "<tr>\n"; 

			$msg .= '<td><span class="postbody"><a href="' . $settings['site_url'] . 'viewtopic.' . $phpEx . '?' . POST_POST_URL . '=' . $row2['post_id'] . '#' . 
				$row2['post_id'] . '">' . $row2['post_id'] . '</a></span></td>';
			$msg .= '<td><span class="postbody">' . $display_time . '</span></td>';
			$row2['Posted_by'] = ($row2['user_id'] == ANONYMOUS) ? $row2['post_username'] : $row2['Posted_by'];
			$row2['Posted_by'] = ($row2['Posted_by'] == '') ? $lang['Guest'] : $row2['Posted_by'];
			$msg .= '<td><span class="postbody">' . $row2['Posted_by'] . '</span></td>';
			if ($show_text)
			{
				if ($enhanced_style)
				{
				
					// This block provides a fancy phpBB look and feel to each message in a HTML digest. However, some email clients may not be able to handle its
					// richness and many users may prefer a more textual style. If so the user can select a plain HTML style using digests.php.
				
					// Begin: The following code minimally modified from viewtopic.php (2.0.21). That's why it works!
					
					$bbcode_uid = $row2['bbcode_uid'];
				
					$user_sig = ( $row2['enable_sig'] && $row2['user_sig'] != '' && $board_config['allow_sig'] ) ? $row2['user_sig'] : '';
					$user_sig_bbcode_uid = $row2['user_sig_bbcode_uid'];
				
					//
					// Note! The order used for parsing the message _is_ important, moving things around could break any
					// output
					//
				
					//
					// If the board has HTML off but the post has HTML
					// on then we process it, else leave it alone
					//
					if ( !$board_config['allow_html'] || !$row2['user_allowhtml'])
					{
						if ( $user_sig != '' )
						{
							$user_sig = preg_replace('#(<)([\/]?.*?)(>)#is', "&lt;\\2&gt;", $user_sig);
						}
				
						if ( $row2['enable_html'] )
						{
							$post_text = preg_replace('#(<)([\/]?.*?)(>)#is', "&lt;\\2&gt;", $post_text);
						}
					}
				
					//
					// Parse message and/or sig for BBCode if reqd
					//
					
					if ($user_sig != '' && $user_sig_bbcode_uid != '')
					{
						$user_sig = ($board_config['allow_bbcode']) ? bbencode_second_pass($user_sig, $user_sig_bbcode_uid) : preg_replace("/\:$user_sig_bbcode_uid/si", '', $user_sig);
					}
	
					if ($bbcode_uid != '')
					{
						$post_text = ($board_config['allow_bbcode']) ? bbencode_second_pass($post_text, $bbcode_uid) : preg_replace("/\:$bbcode_uid/si", '', $post_text);
						if (strlen($row2['post_text']) <= $post_text)
						{
							if (strstr($post_text,'<table width="90%" cellspacing="1" cellpadding="3" border="0" align="center">') != FALSE)
							{
								$post_text = $post_text . '</td></tr></table>';
							}
						}
					}
				
					if ( $user_sig != '' )
					{
						$user_sig = '<br />_________________<br />' . make_clickable($user_sig);
					}
					
					//
					// Parse smilies
					//
					if ( $board_config['allow_smilies'] )
					{
						if ( $row2['user_allowsmile'] && $user_sig != '' )
						{
							$user_sig = smilies_pass_digest($user_sig);
						}
				
						if ( $row2['enable_smilies'] )
						{
							$post_text = smilies_pass_digest($post_text);
						}
					}
					$post_text = make_clickable($post_text) . $user_sig;
					
					// End: The following code minimally modified from viewtopic.php
					$post_text = preg_replace('/\\n/', '<br />', $post_text);
					$msg .= '<td><span class="postbody">' . $post_text . '</span></td>';
			
				}
				else
				{
					// Provides a more textual style, stripping out all BBCode (which includes images, URLs, etc) but reducing digest size. Tbis is the classic 
					// presentation method.

					$post_text = smilies_pass_digest($post_text);
					$post_text = preg_replace("/\[.+\]/iU",'',$post_text); 
					$post_text = strip_tags($post_text);
					$post_text = trim( ereg_replace("[a-zA-Z]+://([-]*[.]?[a-zA-Z0-9_/-?&%])*", "", $post_text ) );
					$post_text = preg_replace("! +!",' ',$post_text);
					$this_msg = '<td><span class="postbody">' . $post_text . '</span></td>';
					$this_msg = preg_replace('/\\n/', '<br />', $this_msg);
					$msg .= $this_msg . "\n";
					
				}
					
			}
			$msg .= "</tr>\n"; 
		}
		else 
		{

			$msg .= $lang['digest_posted_by'] . ' ' . $row2['Posted_by'] . ' ' . $lang['digest_posted_at'] . ' ' . $display_time . ', ' . $settings['site_url'] . 'viewtopic.' . $phpEx . '?' . POST_POST_URL . '=' . $row2['post_id'] . '#' . $row2['post_id'] . "\n\n";

			// If requested to show the message text
			if ($show_text) 
			{
				// Remove BBCode, makes for nicer presentation
				$post_text = smilies_pass_digest($post_text);
				$post_text = preg_replace("/\[.+\]/iU",'',$post_text); 
				$post_text = strip_tags($post_text);
				$post_text = trim( ereg_replace("[a-zA-Z]+://([-]*[.]?[a-zA-Z0-9_/-?&%])*", "", $post_text ) );
				$post_text = preg_replace("! +!",' ',$post_text);
				$msg .= $lang['digest_message_excerpt'] . ":\n\n" . $post_text . "\n";
				$msg .= "\n------------------------------\n\n"; 
			}
			else
			{
				$msg .= "\n------------------------------\n\n";			
			}

		}

		$msg_count++;
	}

	if ($html) 
	{
		$msg .= "</table>\n";
	}

	if ($msg_count == 0)
	{
		if ($html)
		{
			$msg .= $parastart . '<span class="postbody">' . $lang['digest_no_new_messages'] . '</span>' . $paraend;
		}
		else
		{
			$msg .= $parastart . "\n" . $lang['digest_no_new_messages'] . "\n\n------------------------------" . $paraend;
		}
	}

	// Send the email if there are messages or if user selected to send email anyhow
	if ($msg_count > 0 || $row['send_on_no_messages'] == 'YES') 
	
	{

		if (!(is_object($emailer)))
		{
			$emailer = new emailer($board_config['smtp_delivery']);
		}

		if ($html) 
		{
			$emailer->set_content_type('text/html');
			$emailer->use_template('mail_digests_html', $current_lang);

			// Apply a style sheet if requested for HTML digest. If no style sheet is wanted
			// then the link tag pointing to the style sheet is not displayed. A custom style
			// sheet gets first priority.

			if ($link_tag_unset) 
			{
				$stylesheet = '';
				if ($settings['use_custom_stylesheet'] == 'YES')
				{
					$stylesheet = $settings['custom_stylesheet_path'];
				}
				elseif ($settings['use_custom_stylesheet'] == 'NO') 
				{

					// Get the default style sheet to apply to the HTML email
					$sql2 = 'SELECT style_name, head_stylesheet
						FROM ' . THEMES_TABLE . '
						WHERE themes_id = ' . $board_config['default_style'];
					if ( !($result2 = $db->sql_query($sql2)) )
					{
						message_die(CRITICAL_ERROR, 'Could not query database for theme info');
					}
					$row2 = $db->sql_fetchrow($result2);
					$stylesheet = 'templates/' . $row2['style_name'] . '/' . $row2['head_stylesheet'];
				}

				if ($stylesheet <> '') 
				{
					$link_tag = '<link rel="stylesheet" type="text/css" href="' . $settings['site_url'] . $stylesheet . '" />';
				}

				$link_tag_unset = false;

			}

		}
		else 
		{
			$emailer->use_template('mail_digests_text',$row['user_lang']);
		}

		$emailer->from(mb_encode_mimeheader($lang['digest_from_text_name']) . ' <' . $lang['digest_from_email_address'] . ">\n");

		switch ($row['format'])
		{
			case 'HTML':
				$display_digest_format = $lang['digest_html_enhanced'];
				break;
			case 'PHTM':
				$display_digest_format = $lang['digest_html_plain'];
				break;
			case 'TEXT':
				$display_digest_format = $lang['digest_text'];
				break;
		}
				
		$emailer->email_address($to);
		$emailer->set_subject($lang['digest_subject_line']);
		$emailer->assign_vars(array(
			'BOARD_URL' => $settings['site_url'],
			'DIGEST_CONTENT' => $msg,
			'DISCLAIMER' => ($html) ? $lang['digest_disclaimer_html'] : $lang['digest_disclaimer_text'],
			'FORMAT' => $display_digest_format,
			'FREQUENCY' => ($row['digest_type'] == 'DAY') ? $lang['digest_daily'] : $lang['digest_weekly'],
			'L_DIGEST_OPTIONS' => $lang['digest_your_digest_options'],
			'L_FORMAT' => $lang['digest_format_footer'],
			'L_FREQUENCY' => $lang['digest_frequency'],
			'L_INTRODUCTION' => $lang['digest_introduction'],
			'L_MESSAGE_TEXT' => $lang['digest_show_message_text'],
			'L_MY_MESSAGES' => $lang['digest_show_my_messages_plain'],
			'L_NEW_MESSAGES' => $lang['digest_show_only_new_messages'],
			'L_POWERED_BY' => $lang['digest_powered_by'],
			'L_SALUTATION' => $lang['digest_salutation'],
			'L_SEND_DIGEST' => $lang['digest_send_if_no_new_messages'],
			'L_SEND_TIME' => $lang['digest_hour_to_send_footer'],
			'L_SITENAME' => $board_config['sitename'],
			'L_TEXT_LENGTH' => $lang['digest_message_size'],
			'L_TOTAL_PMS' => ($show_pms) ? $lang['digest_total_unread_private_messages'] : '',
			'L_TOTAL_POSTS' => $lang['digest_total_posts'],
			'L_VERSION' => $lang['digest_version_text'],
			'LINK' => $link_tag,
			'MESSAGE_TEXT' => ($row['show_text'] == 'YES') ? $lang['Yes']: $lang['No'], 
			'MY_MESSAGES' => ($row['show_mine'] == 'YES') ? $lang['Yes']: $lang['No'], 
			'NEW_MESSAGES' => ($row['new_only'] == 'TRUE') ? $lang['Yes']: $lang['No'], 
			'POWERED_BY' => ($html) ? '<a href="' . DIGEST_PAGE_URL . '">phpBB Digests</a>': 'phpBB Digests',
			'SALUTATION' => ($simulate_mode) ? $row['username'] . ' ' . $lang['digest_simulate_mode_salutation_text'] : $row['username'],
			'SEND_DIGEST' => ($row['send_on_no_messages'] == 'YES') ? $lang['Yes']: $lang['No'],
			'SEND_TIME' => date('g A', mktime($send_hour)),
			'TEXT_LENGTH' => $row['text_length'],
			'TOTAL_PMS' => ($show_pms) ? $unread_pms : '',
			'TOTAL_POSTS' => $msg_count,
			'VERSION' => $settings['version']));
		if ($test_mode <> true)
		{
			$emailer->send();
		}
		$emailer->reset();

		$digests_sent++;

	}

	// Normally this is run as a batch job, but it can be useful to get summary information of what
	// was sent and to whom.
	if ($settings['show_summary'] == 'YES')
	{
		$pms_string = ($show_pms) ? $lang['digest_and'] . ' ' . $unread_pms . ' ' . $lang['digest_private_messages'] . ' ' : '';
		$digest_log_entry .= $lang['digest_a_digest_containing'] . ' ' . $msg_count . ' ' . $lang['digest_posts'] . ' ' . $pms_string . $lang['digest_was_sent_to'] . ' ' . $row['user_email'] . $break_type;
	}
}

// Summary information normally not seen, but can be captured via command line to a file
if ($settings['show_summary'] == 'YES')
{
	if ($settings['summary_type'] == 'HTML')
	{
		echo "<html>\n";
		echo "<head>\n";
		echo '<title>' . $lang['digest_summary'] . "</title>\n";
		echo "</head>\n";
		echo "<body>\n";
		echo '<h1>' . $lang['digest_summary'] . "</h1>\n";
	}
	if ($test_mode)
	{
		echo $lang['digest_test_mode'] . $break_type . $break_type;
	}
	echo $digest_log_entry;
	if ($settings['summary_type'] == 'HTML')
	{
		echo "<hr />\n";
	}
	echo $lang['digest_a_total_of'] . ' ' . $digests_sent . ' ' . $lang['digest_were_emailed'] . $break_type;
	echo $lang['digest_server_date'] . ' ' . date($settings['server_display_date']) .  $break_type;
	echo $lang['digest_server_hour'] . ' ' . date('H') . $break_type;
	echo $lang['digest_server_time_zone'] . ' ' . date('Z')/3600 . ' ' . $lang['digest_or'] . ' ' . date('T') .  $break_type;
	if ($settings['summary_type'] == 'HTML')
	{
		echo "</body>\n";
		echo "</html>\n";
	}
}

function smilies_pass_digest($message)
{

	// This is a minimally modified version of smilies_pass from /includes/bbcode.php. The only modification is to 
	// insert the full path name for the smilie image. This code taken from phpBB version 2.0.21.
	
	static $orig, $repl;
	global $settings;

	if (!isset($orig))
	{
		global $db, $board_config;
		$orig = $repl = array();

		$sql = 'SELECT * FROM ' . SMILIES_TABLE;
		if( !$result = $db->sql_query($sql) )
		{
			message_die(GENERAL_ERROR, "Couldn't obtain smilies data", "", __LINE__, __FILE__, $sql);
		}
		$smilies = $db->sql_fetchrowset($result);

		if (count($smilies))
		{
			usort($smilies, 'smiley_sort');
		}

		for ($i = 0; $i < count($smilies); $i++)
		{
			$orig[] = "/(?<=.\W|\W.|^\W)" . preg_quote($smilies[$i]['code'], "/") . "(?=.\W|\W.|\W$)/";
			$repl[] = '<img src="'. $settings['site_url'] . $board_config['smilies_path'] . '/' . $smilies[$i]['smile_url'] . '" alt="' . $smilies[$i]['emoticon'] . '" border="0" />';
		}
	}

	if (count($orig))
	{
		$message = preg_replace($orig, $repl, ' ' . $message . ' ');
		$message = substr($message, 1, -1);
	}
	
	return $message;
}

?>
