<?php 
/***************************************************************************
                             digest_functions.php
                             --------------------
    begin                : Thurs Apr 19, 2007
    copyright            : (c) Mark D. Hamill
    email                : mhamill@computer.org

    $Id: $

 ***************************************************************************/

/***************************************************************************
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

// digest_functions.php
// Written by Mark D. Hamill, mhamill@computer.org
// This software is designed to work with phpBB Version 2.0.22

if ( !defined('IN_PHPBB') )
{
	die('Hacking attempt');
}

// Table names. Change if you prefer to use different table names. I used the mod_ prefix to make
// them stand out from other standard phpBB tables.

define('DIGEST_SUBSCRIPTIONS_TABLE', $table_prefix.'mod_subscriptions');
define('DIGEST_SUBSCRIBED_FORUMS_TABLE', $table_prefix.'mod_subscribed_forums');
define('DIGEST_SETTINGS_TABLE', $table_prefix.'mod_digest_settings');

// Do not change these values. They are only changed by the mod author
define('DIGEST_PAGE_URL','http://phpbb.potomactavern.org/digests/'); 

function save_digest_settings ($this_user_id, $action, $type, $format, $excerpt, $mine, $new, $only_if_msgs, $pms, $send_hour, $text_length)
{

	// This function inserts, updates or deletes the digest settings for one user.

	global $db;
	
	// Return the username that was modified or deleted
	$sql = 'SELECT username FROM ' . USERS_TABLE . ' WHERE user_id = ' . $this_user_id;
	if ( !($result = $db->sql_query($sql)))
	{
		message_die(GENERAL_ERROR, 'Could not select from ' . USERS_TABLE . ' table', '', __LINE__, __FILE__, $sql);
	}
	$row = $db->sql_fetchrow($result);
	$this_username = $row['username'];

	// Create the SQL
	if ($action == 'UPD') // update subscription
	{
		$sql = 'UPDATE ' . DIGEST_SUBSCRIPTIONS_TABLE . " SET digest_type = '" . $type . "', format = '" . $format . "', show_text = '" . $excerpt . "', show_mine = '" . $mine . "', new_only = '" . $new . "', send_on_no_messages = '" .  $only_if_msgs . "', show_pms = '" . $pms . "', send_hour = '" . $send_hour . "', text_length = " . $text_length . " WHERE user_id = " . $this_user_id;
		if ( !($result = $db->sql_query($sql)))
		{
			message_die(GENERAL_ERROR, 'Could not update ' . DIGEST_SUBSCRIPTIONS_TABLE . ' table', '', __LINE__, __FILE__, $sql);
		} 
	}
	else if ($action == 'DEL') // delete subscription
	{
		// first remove all individual forum subscriptions
		$sql = 'DELETE FROM ' . DIGEST_SUBSCRIBED_FORUMS_TABLE . ' WHERE user_id = ' . $this_user_id;
		if ( !($result = $db->sql_query($sql)))
		{
			message_die(GENERAL_ERROR, 'Could not delete from ' . DIGEST_SUBSCRIBED_FORUMS_TABLE . ' table', '', __LINE__, __FILE__, $sql);
		}
		
		// now remove the subscription
		$sql = 'DELETE FROM '. DIGEST_SUBSCRIPTIONS_TABLE . ' WHERE user_id = ' . $this_user_id;
		if ( !($result = $db->sql_query($sql)))
		{
			message_die(GENERAL_ERROR, 'Could not delete from ' . DIGEST_SUBSCRIPTIONS_TABLE . ' table', '', __LINE__, __FILE__, $sql);
		}
	}
	else if ($action == 'INS') // add subscription
	{
		if ($send_hour === 'R') // pick a random hour
		{
			$send_hour = rand(0,23);
		}
		$sql = 'INSERT INTO ' . DIGEST_SUBSCRIPTIONS_TABLE . " (user_id, digest_type, format, show_text, show_mine, new_only, send_on_no_messages, show_pms, send_hour, text_length) VALUES ($this_user_id, '$type', '$format', '$excerpt', '$mine', '$new', '$only_if_msgs', '$pms', '$send_hour', $text_length)";
		if ( !($result = $db->sql_query($sql)))
		{
			message_die(GENERAL_ERROR, 'Could not insert into ' . DIGEST_SUBSCRIPTIONS_TABLE . ' table', '', __LINE__, __FILE__, $sql);
		}
	}
	else
	{
		message_die(GENERAL_ERROR, 'Invalid action value of ' . $action, '', __LINE__, __FILE__, '');
	}
	
	return $this_username;

}

function get_digest_defaults ()
{
	// This function returns an array with the digest defaults. The defaults are specified in the phpbb_mod_subscriptions table where user_id = -1 (ANONYMOUS).
	// If it doesn't exist this function will create it. Note, if send_hour = -1, this is interpreted to mean that new digests should default to being sent at a random
	// hour from 0 to 23. This is useful to ensure an even distribution of the digest load, rather than trying to send all digests at the same hour. 
	
	global $db;
	
	$sql = 'SELECT count(*) AS default_count FROM ' . DIGEST_SUBSCRIPTIONS_TABLE . ' WHERE user_id = ' . ANONYMOUS;
	if ( !($result = $db->sql_query($sql)))
	{
		message_die(GENERAL_ERROR, 'Could not perform a count function from ' . DIGEST_SUBSCRIPTIONS_TABLE . ' table', '', __LINE__, __FILE__, $sql);
	}
	
	$row = $db->sql_fetchrow($result);
	if ($row['default_count'] == 0)
	{
		// Defaults do not exist, so create them
		$sql = 'INSERT INTO ' . DIGEST_SUBSCRIPTIONS_TABLE . ' (user_id, digest_type, format, show_text, show_mine, new_only, send_on_no_messages, show_pms, send_hour, text_length) VALUES (' . ANONYMOUS . ", 'DAY', 'HTML', 'YES', 'YES', 'TRUE', 'YES', 'YES', -1, 32000)";
		if ( !($result = $db->sql_query($sql)))
		{
			message_die(GENERAL_ERROR, 'Could not insert default values into ' . DIGEST_SUBSCRIPTIONS_TABLE . ' table', '', __LINE__, __FILE__, $sql);
		}
	}
	
	// Get the digest defaults
	$sql = 'SELECT * FROM ' . DIGEST_SUBSCRIPTIONS_TABLE . ' WHERE user_id = ' . ANONYMOUS;
	if ( !($result = $db->sql_query($sql)))
	{
		message_die(GENERAL_ERROR, 'Could not perform a select from ' . DIGEST_SUBSCRIPTIONS_TABLE . ' table', '', __LINE__, __FILE__, $sql);
	}
	$row = $db->sql_fetchrow($result);
	return $row;	
}

function get_digest_settings ()
{
	// This function returns an array with the digest settings. It is assumed that there is only one row with a table_key = 1 in the phpbb_mod_digest_settings table
	
	global $db;
	
	$sql = 'SELECT count(*) AS default_count FROM ' . DIGEST_SETTINGS_TABLE;
	if ( !($result = $db->sql_query($sql)))
	{
		message_die(GENERAL_ERROR, 'Could not perform a count function from ' . DIGEST_SETTINGS_TABLE . ' table', '', __LINE__, __FILE__, $sql);
	}
	
	$row = $db->sql_fetchrow($result);
	if ($row['default_count'] <> 1)
	{
		message_die(GENERAL_ERROR, 'Inconsistent database. There should only be a single row in the ' . DIGEST_SETTINGS_TABLE . ' table', '', __LINE__, __FILE__, $sql);
	}
	
	// Get the digest settings
	$sql = 'SELECT * FROM ' . DIGEST_SETTINGS_TABLE;
	if ( !($result = $db->sql_query($sql)))
	{
		message_die(GENERAL_ERROR, 'Could not perform select from ' . DIGEST_SETTINGS_TABLE . ' table', '', __LINE__, __FILE__, $sql);
	}
	$row = $db->sql_fetchrow($result);
	return $row;	
}

function autosubscribe_digest($user_id)
{

	// This function subscribes one user using the default digest settings, if the feature is enabled.

	global $db;

	$defaults = get_digest_defaults();
	$settings = get_digest_settings();
	if ($settings['autosubscribe_users'] == 'YES')
	{
	
		// Add only if user does not alraedy have a subscription
		$sql = 'SELECT count(*) AS this_user_count FROM ' . DIGEST_SUBSCRIPTIONS_TABLE . ' WHERE user_id = ' . $user_id;
		if ( !($result = $db->sql_query($sql)))
		{
			message_die(GENERAL_ERROR, 'Could not perform a count function from ' . DIGEST_SUBSCRIPTIONS_TABLE . ' table', '', __LINE__, __FILE__, $sql);
		}
		
		$row = $db->sql_fetchrow($result);
		if ($row['this_user_count'] == 0)
		{
			save_digest_settings ($user_id, 'INS', $defaults['digest_type'], $defaults['format'], $defaults['show_text'], $defaults['show_mine'], $defaults['new_only'], $defaults['send_on_no_messages'], $defaults['show_pms'], ($defaults['send_hour'] == -1) ? rand(0,23) : $defaults['send_hour'], $defaults['text_length']);
		}
		
	}

}

?>