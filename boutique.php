<?php
/***************************************************************************
 *                                  dons.php
 ***************************************************************************/

/***************************************************************************
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

define('IN_PHPBB', true);
$phpbb_root_path = './';
include($phpbb_root_path . 'extension.inc');
include($phpbb_root_path . 'common.' . $phpEx);

global $show_ads;
$show_ads = false;

// session management
$userdata = session_pagestart($user_ip, PAGE_INDEX);
init_userprefs($userdata);

$__g_alt_css = true;
$template->assign_vars(array(
        'META' => '<meta name="robots" content="noindex, nofollow">'
    )
);

include($phpbb_root_path . 'includes/page_header.' . $phpEx);

if (PHPBB_ENVIRONMENT === 'production') {
    $template->assign_block_vars('switch_production', array());
}

$template->set_filenames(array(
        'body' => 'boutique_body.tpl'
    )
);
$template->pparse('body');

include($phpbb_root_path . 'includes/page_tail.' . $phpEx);

?>
