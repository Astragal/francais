<?php
/***************************************************************************
                                digests.php
                                -----------
    begin                : Friday, August 17, 2007
    copyright            : (c) Mark D. Hamill
    email                : mhamill@computer.org

    $Id: $

***************************************************************************/

/***************************************************************************
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

// Written by Mark D. Hamill, mhamill@computer.org
// This software is designed to work with phpBB Version 2.0.22

// This is the user interface for the digest software. Users can use it to create and modify their digest 
// settings, or remove their digest subscription. 

// Warning: this was only tested with MySQL. I don't have access to other databases. Consequently, 
// the SQL may need tweaking for other relational databases.

define('IN_PHPBB', true);
$phpbb_root_path = './';
include($phpbb_root_path . 'extension.inc');
include($phpbb_root_path . 'common.'.$phpEx);

//
// Start session management
//

$userdata = session_pagestart($user_ip, PAGE_INDEX);
init_userprefs($userdata);

//
// End session management
//

if ( !$userdata['session_logged_in'] ) 
{ 
	redirect(append_sid("login.".$phpEx."?redirect=digests.".$phpEx, true)); 
	exit; 
} 

include($phpbb_root_path . 'includes/digest_functions.' . $phpEx); 
include($phpbb_root_path . 'language/lang_' . $board_config['default_lang'] . '/lang_digests.' . $phpEx);

$settings = get_digest_settings();	// This will retrieve the current global settings

$start = ( isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$start = ($start < 0) ? 0 : $start;

$page_title = $lang['digest_page_title'];
include($phpbb_root_path . 'includes/page_header.'.$phpEx);

// Get the server time zone. This is not necessarily what appears in $board_config['board_timezone']
$board_timezone = date('Z')/3600;

// Get current user's timezone
$user_timezone = (float) $userdata['user_timezone'];

// Offset the timezone information. We will store in the subscriptions table the 
// server time to send the digest, since mail_digests.php expects it this way.

$offset = $board_timezone - $user_timezone;

if ($HTTP_SERVER_VARS['REQUEST_METHOD'] == 'GET')
{

	if ( $userdata['session_logged_in'] )
	{

		$template->set_filenames(array('digests' => 'digests_body.tpl'));

		// get current subscription data for this user, if any
		$sql = 'SELECT count(*) AS count FROM ' . DIGEST_SUBSCRIPTIONS_TABLE . ' WHERE user_id = ' . $userdata['user_id'];
		if ( !($result = $db->sql_query($sql)))
		{
			message_die(GENERAL_ERROR, 'Could not get count from '. DIGEST_SUBSCRIPTIONS_TABLE . ' table', '', __LINE__, __FILE__, $sql);
		}
   		$row = $db->sql_fetchrow($result);
   		$create_new = ($row['count'] == 0) ? true: false;

		if ($create_new)
		{
			// Get the default values if no digest subscription for user
			$defaults = get_digest_defaults();
			
			$digest_type = trim($defaults['digest_type']);
			$format = trim($defaults['format']);
			$show_text = trim($defaults['show_text']);
			$show_mine = trim($defaults['show_mine']);
			$new_only = trim($defaults['new_only']);
			$send_on_no_messages = trim($defaults['send_on_no_messages']);
			$send_hour = (float) $defaults['send_hour'];
			if ($send_hour == -1)
			{
				$send_hour = rand(0,23); // If -1, this means pick a random hour of the day. This should even the digest processing load.
			}
			else
			{
				$send_hour = $send_hour - $offset;
				$send_hour = ($send_hour < 0) ? $send_hour + 24 : $send_hour;
				$send_hour = ($send_hour >= 24) ? $send_hour - 24 : $send_hour; 
			}
			$text_length = trim($defaults['text_length']);
			$show_pms = trim($defaults['show_pms']); 
			
		}
		else 
		{
			// read current digest options into local variables, because we have one inherent connection

			$sql = 'SELECT digest_type, format, show_text, show_mine, new_only, send_on_no_messages, send_hour, text_length, show_pms FROM ' . DIGEST_SUBSCRIPTIONS_TABLE . ' WHERE user_id = ' . $userdata['user_id'];

			if ( !($result = $db->sql_query($sql)))
			{
				message_die(GENERAL_ERROR, 'Could not get count from ' . DIGEST_SUBSCRIPTIONS_TABLE . ' table', '', __LINE__, __FILE__, $sql);
			}
			$row = $db->sql_fetchrow($result);

			$digest_type = trim($row['digest_type']);
			$format = trim($row['format']);
			$show_text = trim($row['show_text']);
			$show_mine = trim($row['show_mine']);
			$new_only = trim($row['new_only']);
			$send_on_no_messages = trim($row['send_on_no_messages']);
			$send_hour = (float) $row['send_hour'] - $offset;
			$send_hour = ($send_hour < 0) ? $send_hour + 24 : $send_hour;
			$send_hour = ($send_hour >= 24) ? $send_hour - 24 : $send_hour;
			$text_length = $row['text_length'];
			$show_pms = trim($row['show_pms']);
		}  
    
		// get current subscribed forums for this user, if any
		$sql = 'SELECT count(*) AS count FROM ' . DIGEST_SUBSCRIBED_FORUMS_TABLE . ' WHERE user_id = ' . $userdata['user_id'];
		if ( !($result = $db->sql_query($sql)))
		{
			message_die(GENERAL_ERROR, 'Could not get count from ' . DIGEST_SUBSCRIBED_FORUMS_TABLE . ' table', '', __LINE__, __FILE__, $sql);
		}
		$row = $db->sql_fetchrow($result);
		$all_forums_new = ($row['count'] == 0) ? true : false;

		// fill template with current digest options for user
		$template->assign_vars(array(
			'1AM_SELECTED' => ($send_hour=='1') ? 'selected="selected"' : '',
			'1PM_SELECTED' => ($send_hour=='13') ? 'selected="selected"' : '',
			'2AM_SELECTED' => ($send_hour=='2') ? 'selected="selected"' : '',
			'2PM_SELECTED' => ($send_hour=='14') ? 'selected="selected"' : '',
			'3AM_SELECTED' => ($send_hour=='3') ? 'selected="selected"' : '',
			'3PM_SELECTED' => ($send_hour=='15') ? 'selected="selected"' : '',
			'4AM_SELECTED' => ($send_hour=='4') ? 'selected="selected"' : '',
			'4PM_SELECTED' => ($send_hour=='16') ? 'selected="selected"' : '',
			'5AM_SELECTED' => ($send_hour=='5') ? 'selected="selected"' : '',
			'5PM_SELECTED' => ($send_hour=='17') ? 'selected="selected"' : '',
			'6AM_SELECTED' => ($send_hour=='6') ? 'selected="selected"' : '',
			'6PM_SELECTED' => ($send_hour=='18') ? 'selected="selected"' : '',
			'7AM_SELECTED' => ($send_hour=='7') ? 'selected="selected"' : '',
			'7PM_SELECTED' => ($send_hour=='19') ? 'selected="selected"' : '',
			'8AM_SELECTED' => ($send_hour=='8') ? 'selected="selected"' : '',
			'8PM_SELECTED' => ($send_hour=='20') ? 'selected="selected"' : '',
			'9AM_SELECTED' => ($send_hour=='9') ? 'selected="selected"' : '',
			'9PM_SELECTED' => ($send_hour=='21') ? 'selected="selected"' : '',
			'10AM_SELECTED' => ($send_hour=='10') ? 'selected="selected"' : '',
			'10PM_SELECTED' => ($send_hour=='22') ? 'selected="selected"' : '',
			'11AM_SELECTED' => ($send_hour=='11') ? 'selected="selected"' : '',
			'11PM_SELECTED' => ($send_hour=='23') ? 'selected="selected"' : '',
			'12PM_SELECTED' => ($send_hour=='12') ? 'selected="selected"' : '',
			'50_SELECTED' => ($text_length=='50') ? 'selected="selected"' : '',
			'100_SELECTED' => ($text_length=='100') ? 'selected="selected"' : '',
			'150_SELECTED' => ($text_length=='150') ? 'selected="selected"' : '',
			'300_SELECTED' => ($text_length=='300') ? 'selected="selected"' : '',
			'600_SELECTED' => ($text_length=='600') ? 'selected="selected"' : '',
			'ALL_FORUMS_CHECKED' => ($create_new || ((!($create_new)) && $all_forums_new)) ? 'checked="checked"' : '',
			'DAY_CHECKED' => ($digest_type=='DAY') ? 'checked="checked"' : '',
			'DIGEST_CREATE_NEW_VALUE' => ($create_new) ? '1' : '0',
			'DIGEST_VERSION' => $settings['version'],
			'HTML_CHECKED' => ($format=='HTML') ? 'checked="checked"' : '',
			'L_100' => $lang['digest_size_100'],
			'L_10AM' => $lang['digest_10am'],
			'L_10PM' => $lang['digest_10pm'],
			'L_11AM' => $lang['digest_11am'],
			'L_11PM' => $lang['digest_11pm'],
			'L_12PM' => $lang['digest_12pm'],
			'L_150' => $lang['digest_size_150'],
			'L_1AM' => $lang['digest_1am'],
			'L_1PM' => $lang['digest_1pm'],
			'L_2AM' => $lang['digest_2am'],
			'L_2PM' => $lang['digest_2pm'],
			'L_300' => $lang['digest_size_300'],
			'L_3AM' => $lang['digest_3am'],
			'L_3PM' => $lang['digest_3pm'],
			'L_4AM' => $lang['digest_4am'],
			'L_4PM' => $lang['digest_4pm'],
			'L_50' => $lang['digest_size_50'],
			'L_5AM' => $lang['digest_5am'],
			'L_5PM' => $lang['digest_5pm'],
			'L_600' => $lang['digest_size_600'],
			'L_6AM' => $lang['digest_6am'],
			'L_6PM' => $lang['digest_6pm'],
			'L_7AM' => $lang['digest_7am'],
			'L_7PM' => $lang['digest_7pm'],
			'L_8AM' => $lang['digest_8am'],
			'L_8PM' => $lang['digest_8pm'],
			'L_9AM' => $lang['digest_9am'],
			'L_9PM' => $lang['digest_9pm'],
			'L_ALL_SUBSCRIBED_FORUMS' => $lang['digest_all_forums'],
			'L_DAILY' => $lang['digest_daily'],
			'L_DIGEST_EXPLANATION' => $lang['digest_explanation'],
			'L_DIGEST_HEADER' => $lang['digest_page_title'],
			'L_DIGEST_SUBSCRIPTION_STATUS' => ($create_new) ? $lang['digest_not_subscribed'] : $lang['digest_subscribed'],
			'L_DIGEST_TYPE' => sprintf($lang['digest_wanted'], $lang['digest_weekday'][$settings['weekly_digest_day']]),
			'L_FORMAT' => $lang['digest_format'],
			'L_FORUM_SELECTION' => $lang['digest_select_forums'],
			'L_HTML_ENH' => $lang['digest_html_enhanced'],
			'L_HTML_PLAIN' => $lang['digest_html_plain'],
			'L_MAX' => $lang['digest_size_max'],
			'L_MIDNIGHT' => $lang['digest_midnight'],
			'L_NEW_ONLY' => $lang['digest_show_new_only'],
			'L_NO' => $lang['digest_no'],
			'L_NO_FORUMS_SELECTED' => $lang['digest_no_forums_selected'],
			'L_NONE' => $lang['digest_none'],
			'L_PRIVATE_MGS_IN_FEED' => $lang['digest_private_messages_in_digest'],
			'L_RESET' => $lang['digest_reset_text'],
			'L_SEND_HOUR' => $lang['digest_hour_to_send'],
			'L_SEND_ON_NO_MESSAGES' => $lang['digest_send_if_no_msgs'],
			'L_SHOW_MINE' => $lang['digest_show_my_messages'],
			'L_SHOW_TEXT' => $lang['digest_excerpt'],
			'L_SUBMIT' => $lang['digest_submit_text'],
			'L_TEXT' => $lang['digest_text'],
			'L_TEXT_LENGTH' => $lang['digest_size'],
			'L_WEEKLY' => $lang['digest_weekly'],
			'L_YES' => $lang['digest_yes'],
			'MAX_SELECTED' => ($text_length=='32000') ? 'selected="selected"' : '',
			'MIDNIGHT_SELECTED' => ($send_hour=='0') ? 'selected="selected"' : '',
			'NEW_ONLY_NO_CHECKED' => ($new_only=='FALSE') ? 'checked="checked"' : '',
			'NEW_ONLY_YES_CHECKED' => ($new_only=='TRUE') ? 'checked="checked"' : '',
			'PLAIN_HTML_CHECKED' => ($format=='PHTM') ? 'checked="checked"' : '',
			'S_POST_ACTION' => append_sid("digests.$phpEx"),
			'SEND_ON_NO_MESSAGES_NO_CHECKED' => ($send_on_no_messages=='NO') ? 'checked="checked"' : '',
			'SEND_ON_NO_MESSAGES_YES_CHECKED' => ($send_on_no_messages=='YES') ? 'checked="checked"' : '',
			'SHOW_MINE_NO_CHECKED' => ($show_mine=='NO') ? 'checked="checked"' : '',
			'SHOW_MINE_YES_CHECKED' => ($show_mine=='YES') ? 'checked="checked"' : '',
			'SHOW_PMS_NO_CHECKED' => ($show_pms=='NO') ? 'checked="checked"' : '',
			'SHOW_PMS_YES_CHECKED' => ($show_pms=='YES') ? 'checked="checked"' : '',
			'SHOW_TEXT_NO_CHECKED' => ($show_text=='NO') ? 'checked="checked"' : '',
			'SHOW_TEXT_YES_CHECKED' => ($show_text=='YES') ? 'checked="checked"' : '',
			'TEXT_CHECKED' => ($format=='TEXT') ? 'checked="checked"' : '',
			'U_DIGEST_PAGE_URL' => DIGEST_PAGE_URL,
			'WEEK_CHECKED' => ($digest_type=='WEEK') ? 'checked="checked"' : ''));
			
		// This logic ensures that those with special privileges (moderators, administrators) always see forums where they have read permissions
		switch($userdata['user_level'])
		{
			case USER:
				// Retrieve a list of forum_ids that all members can access
				$sql = 'SELECT f.forum_id, f.forum_name, c.cat_order, f.forum_order, f.auth_read
					FROM ' . FORUMS_TABLE . ' f, ' . CATEGORIES_TABLE . ' c
					WHERE f.cat_id = c.cat_id AND auth_read IN (' . AUTH_ALL . ',' . AUTH_REG .') 
					ORDER BY c.cat_order, f.forum_order';
				break;
			case MOD:
				$sql = 'SELECT f.forum_id, f.forum_name, c.cat_order, f.forum_order, f.auth_read
					FROM ' . FORUMS_TABLE . ' f, ' . CATEGORIES_TABLE . ' c
					WHERE f.cat_id = c.cat_id AND auth_read IN (' . AUTH_ALL . ',' . AUTH_REG . ',' . AUTH_MOD .') 
					ORDER BY c.cat_order, f.forum_order';
				break;
			case ADMIN:
				$sql = 'SELECT f.forum_id, f.forum_name, c.cat_order, f.forum_order, f.auth_read
					FROM ' . FORUMS_TABLE . ' f, ' . CATEGORIES_TABLE . ' c
					WHERE f.cat_id = c.cat_id 
					ORDER BY c.cat_order, f.forum_order';
				break;
		}

		if ( !($result = $db->sql_query($sql)))
		{
			message_die(GENERAL_ERROR, 'Could not query forum information', '', __LINE__, __FILE__, $sql);
		}

		// We have to do a lot of array processing mainly because MySQL 3.x can't handle unions or 
		// intersections. Basically we need to figure out: of all forums, which are those this 
		// user can potentially read? We only want to send digests for forums for which a user
		// has read privileges.
		$forum_ids = array();
		$forum_names = array();
		$cat_orders = array();
		$forum_orders = array();
		$auth_read = array();
      
		$i=0;
		while ($row = $db->sql_fetchrow ($result)) 
		{ 
			$forum_ids [$i] = $row['forum_id'];
			$forum_names [$i] = $row['forum_name'];
			$cat_orders [$i] = $row['cat_order'];
			$forum_orders [$i] = $row['forum_order'];
			$auth_read [$i] = $row['auth_read'];
			$i++;
		}

		// Now we need to add to our forums array other forums that may be private for which
		// the user has access.

		$sql = 'SELECT DISTINCT a.forum_id, f.forum_name, c.cat_order, f.forum_order, f.auth_read
			FROM ' . AUTH_ACCESS_TABLE . ' a, ' . USER_GROUP_TABLE . ' ug, ' . FORUMS_TABLE . ' f, ' . CATEGORIES_TABLE . ' c, ' . GROUPS_TABLE . ' g 
			WHERE ug.user_id = ' . $userdata['user_id']
			. ' AND ug.user_pending = 0 
			AND a.group_id = ug.group_id AND 
			a.forum_id = f.forum_id AND f.cat_id = c.cat_id AND ug.group_id = g.group_id';

		if ( !($result = $db->sql_query($sql)))
		{
			message_die(GENERAL_ERROR, 'Could not query forum information', '', __LINE__, __FILE__, $sql);
		}

		while ($row = $db->sql_fetchrow ($result)) 
		{ 
			$forum_ids [$i] = $row['forum_id'];
			$forum_names [$i] = $row['forum_name'];
			$cat_orders [$i] = $row['cat_order'];
			$forum_orders [$i] = $row['forum_order'];
			$auth_read [$i] = $row['auth_read'];
			$i++;
		}
		$i--;

		// Sort forums so they appear as they would appear on the main index. This makes for a more 
		// natural presentation.

		array_multisort($cat_orders, SORT_ASC, $forum_orders, SORT_ASC, $forum_ids, SORT_ASC, $forum_names, SORT_ASC, $auth_read, SORT_ASC);

		// now print the forums on the web page, each forum being a checkbox with appropriate label
		for ($j=0; $j<=$i; $j++) 
		{

			// Don't print if a duplicate

			if (!(($j>0) && ($cat_orders[$j] == $cat_orders[$j-1]) && ($forum_orders[$j] == $forum_orders[$j-1]) && ($forum_names[$j] == $forum_names[$j-1]))) 

			{
    
				switch($auth_read[$j])
				{
					case AUTH_REG:
						$auth_label = $lang['digest_auth_reg_text'];
						break;
					case AUTH_ACL:
						$auth_label = $lang['digest_auth_acl_text'];
						break;
					case AUTH_MOD:
						$auth_label = $lang['digest_auth_mod_text'];
						break;
					case AUTH_ADMIN:
						$auth_label = $lang['digest_auth_admin_text'];
						break;
					default:
						$auth_label = '';
				}
				
				// Is this forum currently subscribed? If so it needs to be checkmarked
				if (!($all_forums_new)) 
				{
					$sql = 'SELECT count(*) AS count FROM ' . DIGEST_SUBSCRIBED_FORUMS_TABLE . ' WHERE forum_id = ' . $forum_ids [$j] . ' AND user_id = ' . $userdata['user_id'];
					if ( !($result = $db->sql_query($sql)))
					{
						message_die(GENERAL_ERROR, 'Could not get count from ' . DIGEST_SUBSCRIBED_FORUMS_TABLE . ' table', '', __LINE__, __FILE__, $sql);
					}
					$row = $db->sql_fetchrow($result);
					if ($row['count'] == 0)
					{
						$forum_checked = false;
					}
					else
					{
						$forum_checked = true;
					}
				}
				else  
				{
					$forum_checked = true;               	
				}

				$template->assign_block_vars('forums', array( 
					'FORUM_NAME' => 'forum_' . $forum_ids [$j],
					'CHECKED' => ($forum_checked || $create_new) ? 'checked="checked"' : '',
					'FORUM_LABEL' => $forum_names[$j],
					'FORUM_AUTH' => $auth_label));
			}

		}

		$template->pparse('digests');

	}
}

else 
{

	// The user has submitted the form. This logic takes the necessary action to update the database
	// and gives an appropriate confirmation message.

	if ($HTTP_POST_VARS['digest_type'] == 'NONE') 
	{

		// user no longer wants a digest
		// first remove all individual forum subscriptions
		$sql = 'DELETE FROM ' . DIGEST_SUBSCRIBED_FORUMS_TABLE . ' WHERE user_id = ' . $userdata['user_id'];

		if ( !($result = $db->sql_query($sql)))
		{
			message_die(GENERAL_ERROR, 'Could not delete from ' . DIGEST_SUBSCRIBED_FORUMS_TABLE . ' table', '', __LINE__, __FILE__, $sql);
		}
    
		// remove subscription itself
		$sql = 'DELETE FROM ' . DIGEST_SUBSCRIPTIONS_TABLE . ' WHERE user_id = ' . $userdata['user_id'];

		if ( !($result = $db->sql_query($sql)))
		{
			message_die(GENERAL_ERROR, 'Could not delete from ' . DIGEST_SUBSCRIPTIONS_TABLE . ' table', '', __LINE__, __FILE__, $sql);
		}

		$update_type = 'unsubscribe';
    
	}

	else 
	{

		// In all other cases a digest has to be either created or updated

		// From the offset, calculate the real hour digest is wanted based on server time
		$send_hour = (float) $HTTP_POST_VARS['send_hour'] + $offset;
		$send_hour = ($send_hour < 0) ? $send_hour + 24 : $send_hour;
		$send_hour = ($send_hour >= 24) ? $send_hour - 24 : $send_hour;
		
		// first, create or update the subscription
		if ($HTTP_POST_VARS['create_new'] == '1')// new digest
		{
			$sql = 'INSERT INTO ' . DIGEST_SUBSCRIPTIONS_TABLE . ' (user_id, digest_type, format, show_text, show_mine, new_only, send_on_no_messages, send_hour, text_length, show_pms) VALUES (' .
				intval($userdata['user_id']) . ', ' .
				"'" . htmlspecialchars($HTTP_POST_VARS['digest_type']) . "', " .
				"'" . htmlspecialchars($HTTP_POST_VARS['format']) . "', " .
				"'" . htmlspecialchars($HTTP_POST_VARS['show_text']) . "', " .
				"'" . htmlspecialchars($HTTP_POST_VARS['show_mine']) . "', " .
				"'" . htmlspecialchars($HTTP_POST_VARS['new_only']) . "', " .
				"'" . htmlspecialchars($HTTP_POST_VARS['send_on_no_messages']) . "', " .
				"'" . intval($send_hour) . "', " .
				intval($HTTP_POST_VARS['text_length']) . ', ' .
				"'" . htmlspecialchars($HTTP_POST_VARS['show_pms']) . "')";
			$update_type = 'create';
		}
		else 
		{
			$sql = 'UPDATE ' . DIGEST_SUBSCRIPTIONS_TABLE . ' SET ' .
				"digest_type = '" . htmlspecialchars($HTTP_POST_VARS['digest_type']) . "', " .
				"format = '" . htmlspecialchars($HTTP_POST_VARS['format']) . "', " .
				"show_text = '" . htmlspecialchars($HTTP_POST_VARS['show_text']) . "', " .
				"show_mine = '" . htmlspecialchars($HTTP_POST_VARS['show_mine']) . "', " .
				"new_only = '" . htmlspecialchars($HTTP_POST_VARS['new_only']) . "', " .
				"send_on_no_messages = '" . htmlspecialchars($HTTP_POST_VARS['send_on_no_messages']) . "', " .
				"send_hour = '" . intval($send_hour) . "', " .
				'text_length = ' . intval($HTTP_POST_VARS['text_length']) . ', ' . 
				"show_pms = '" . htmlspecialchars($HTTP_POST_VARS['show_pms']) . "' " .
				'WHERE user_id = ' . intval($userdata['user_id']);
			$update_type = 'modify';
		}
		if ( !($result = $db->sql_query($sql)))
		{
			message_die(GENERAL_ERROR, 'Could not insert or update ' . DIGEST_SUBSCRIPTIONS_TABLE . ' table', '', __LINE__, __FILE__, $sql);
		}

		// next, if there are any individual forum subscriptions, remove the old ones and create the new ones

		$sql = 'DELETE FROM ' . DIGEST_SUBSCRIBED_FORUMS_TABLE . ' WHERE user_id = ' . $userdata['user_id'];
		if ( !($result = $db->sql_query($sql)))
		{
			message_die(GENERAL_ERROR, 'Could not delete from ' . DIGEST_SUBSCRIBED_FORUMS_TABLE . ' table', '', __LINE__, __FILE__, $sql);
		}

		// Note that if "all_forums" is checked, this is noted in the subscriptions table. It does not put
		// each forum in the subscribed_forums table. This conserves disk space. "all_forums" means all 
		// forums this user is allowed to access.

		if ($HTTP_POST_VARS['all_forums'] !== 'on') 
		{
			foreach ($HTTP_POST_VARS as $key => $value) 
			{
				if (substr($key, 0, 6) == 'forum_') 
				{
					$sql = 'INSERT INTO ' . DIGEST_SUBSCRIBED_FORUMS_TABLE . ' (user_id, forum_id) VALUES (' .
					$userdata['user_id'] . ', ' . htmlspecialchars(substr($key,6)) . ')';
					if ( !($result = $db->sql_query($sql)))
					{
						message_die(GENERAL_ERROR, 'Could not insert into ' . DIGEST_SUBSCRIBED_FORUMS_TABLE . ' table', '', __LINE__, __FILE__, $sql);
					}
				}
			}
		}

	}

	// Show appropriate confirmation message
	$display_message = sprintf($lang['digest_return_digests'], '<a href="' . append_sid("digests.$phpEx") . '">', '</a> ') . '<br /><br />';

	if ($update_type == 'unsubscribe')
	{
		$message = $lang['digest_unsubscribe'] . '<br /><br />' . $display_message . sprintf($lang['Click_return_index'], '<a href="' . append_sid("index.$phpEx") . '">', '</a> ');
	}
	else if ($update_type == 'create')
	{
		$message = $lang['digest_create'] . '<br /><br />' . $display_message . sprintf($lang['Click_return_index'], '<a href="' . append_sid("index.$phpEx") . '">', '</a> ');
	}
	else
	{
		$message = $lang['digest_modify'] . '<br /><br />' . $display_message . sprintf($lang['Click_return_index'], '<a href="' . append_sid("index.$phpEx") . '">', '</a> ');
	}
	
	message_die(GENERAL_MESSAGE, $message);

}

include($phpbb_root_path . 'includes/page_tail.'.$phpEx);

?>
