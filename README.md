## Syntaxe des messages de commit

    [+|!|$] [m] [(port�e)] sujet
    [+|!|$] [m] (port�e) [sujet]

`+` = ajout d'une nouvelle fonctionnalit�  
`!` = correction d'un bogue  
`$` = correction d'une faille de s�curit�  
`m` = modification mineure

Exemple :

    ! (RSS) corrige l'affichage des caract�res accentu�s
