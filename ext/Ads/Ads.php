<?php

class Ads
{
    private $doc, $xpath, $devMode;
    private $xmlValid = true;

    /**
     * Ads constructor.
     * @param string $xmlFile
     * @param string $xsdFile
     * @param bool $devMode
     *   false = production
     *   true = development
     */
    public function __construct($xmlFile, $xsdFile = null, $devMode = false)
    {
        $this->devMode = $devMode;

        $this->doc = new DOMDocument();
        $this->doc->preserveWhiteSpace = false;
        $this->doc->load($xmlFile, LIBXML_COMPACT);
        if ($xsdFile) {
            $this->xmlValid = $this->doc->schemaValidate($xsdFile, LIBXML_SCHEMA_CREATE);
        }
        $this->xpath = new DOMXPath($this->doc);
    }

    /**
     * @param string $id
     * @return string
     */
    public function getAd($id)
    {
        if (!$this->xmlValid) {
            return null;
        }
        $location = $this->getLocation($id);
        if (!$location) {
            return null;
        }

        $ads = $this->getAdsInLocation($location);
        $randomAd = $this->getRandomElement($ads);
        $advertiserRefId = $this->getAdvertiserRefId($randomAd);
        $adRefId = $this->getAdRefId($randomAd);
        $adSettings = $this->getAdSettings($advertiserRefId, $adRefId);
        if (!$adSettings) {
            return null;
        }

        if ($this->devMode) {
            $adCode = $this->getAdPlaceholder($advertiserRefId, $adSettings);
        } else {
            $adCode = $this->getAdCode($adSettings);
        }

        return utf8_decode(trim($adCode));
    }

    /**
     * @param string $id
     * @return DOMElement
     */
    private function getLocation($id)
    {
        return $this->xpath->query(sprintf('/*/locations/location[@id = "%s"][@active = "true"]',
            $id))->item(0);
    }

    /**
     * @param DOMElement $location
     * @return DOMNodeList
     */
    private function getAdsInLocation(DOMElement $location)
    {
        return $this->xpath->query('advertiser[@active = "true"]/ad[@active = "true"]', $location);
    }

    /**
     * @param DOMNodeList $elements
     * @return DOMElement
     */
    private function getRandomElement(DOMNodeList $elements)
    {
        return $elements->item(rand(0, $elements->length - 1));
    }

    /**
     * @param DOMElement $ad
     * @return string
     */
    private function getAdvertiserRefId(DOMElement $ad)
    {
        return $this->xpath->evaluate('string(parent::advertiser/@ref)', $ad);
    }

    /**
     * @param DOMElement $randomAd
     * @return string
     */
    private function getAdRefId(DOMElement $randomAd)
    {
        return $randomAd->textContent;
    }

    /**
     * @param string $advertiserId
     * @param string $adId
     * @return DOMElement
     */
    private function getAdSettings($advertiserId, $adId)
    {
        return $this->xpath->query(sprintf('/*/ads/advertiser[@id = "%s"]/ad[@id = "%s"]', $advertiserId,
            $adId))->item(0);
    }

    private function getAdPlaceholder($advertiserId, DOMElement $ad)
    {
        $isPlaceholderActive = true;
        if ($this->xpath->query('placeholder', $ad)->length) {
            $isPlaceholderActive = filter_var($this->xpath->evaluate('string(placeholder/@active)', $ad),
                FILTER_VALIDATE_BOOLEAN);
        }
        if (!$isPlaceholderActive) {
            return $this->getAdCode($ad);
        }

        $placeholder = $this->xpath->evaluate('string(placeholder)', $ad);
        if ($placeholder === "") {
            $placeholder = $this->replaceVars($this->getPlaceholder('default'), $advertiserId, $ad);
        } else {
            $placeholder = $this->replaceVars($placeholder, $advertiserId, $ad);
        }

        return $placeholder;
    }

    /**
     * @param DOMElement $ad
     * @return string
     */
    private function getAdCode($ad)
    {
        return $this->xpath->evaluate('string(code)', $ad);
    }

    /**
     * @param string $str
     * @param string $advertiserId
     * @param DOMElement $ad
     * @return string
     */
    private function replaceVars($str, $advertiserId, DOMElement $ad)
    {
        $id = $ad->getAttribute('id');
        $width = $this->xpath->evaluate('string(width)', $ad);
        $height = $this->xpath->evaluate('string(height)', $ad);

        $name = $this->xpath->evaluate('string(name)', $ad);

        $width = ($width) ? $width . 'px' : '100%';
        $height = ($height) ? $height . 'px' : '100%';

        $str = str_replace('{advertiserId}', $advertiserId, $str);
        $str = str_replace('{id}', $id, $str);
        $str = str_replace('{name}', $name, $str);
        $str = str_replace('{width}', $width, $str);
        $str = str_replace('{height}', $height, $str);

        return $str;
    }

    /**
     * @param string $name
     * @return string
     */
    private function getPlaceholder($name)
    {
        return $this->xpath->evaluate(sprintf('string(/*/placeholders/%s)', $name));
    }

    /**
     * @return bool
     */
    public function isXmlValid()
    {
        return $this->xmlValid;
    }
}
