<?php
/*
* Code written by Chris Monahan, portions copyright phpBB.
*
* Please make sure that you do not upload this file to your
* site if you have received it from an untrusted source.
* This script is provided as-is without any warranty. By
* using this script you agree that MessageForums.net and
* its owners will not be held responsible for any issues
* that may arise from using this script. USE AT YOUR OWN RISK.
*/

// you may be asked to turn this on if the script is not working properly
$debugMode = false;

// let others know about your forum so they can join?
$addThisForumToDirectory = true;

// alert iPhone, iPad and iPod touch users about TouchBB?
$promptUsersAboutTouchBB = true;

// ****************************************************************************** ///
// *** Please do not change anything below unless you know what you are doing *** ///
// ****************************************************************************** ///

// phpBB2 initialization
define('IN_PHPBB', true);
$phpbb_root_path = './';

// support for PHP-Nuke
$nuked = false;
if (@file_exists('../../mainfile.php')) {
  define('MODULE_FILE', true);
  chdir('../../');

  if (!isset($popup) OR ($popup != "1")) {
    $module_name = "Forums";
    $nuke_root_path = "modules.php?name=" . $module_name;
    $nuke_file_path = "modules.php?name=" . $module_name . "&file=";
    $phpbb_root_path = "modules/" . $module_name . "/";
    $phpbb_root_dir = "./../";

    @require_once("mainfile.php");
  } else {
    $phpbb_root_path = 'modules/Forums/';
  }

  @include('../../config.php');
  @include('../../mainfile.php');
  $nuked = true;
}

@include($phpbb_root_path . 'extension.inc');
@include($phpbb_root_path . 'common.' . $phpEx);

// setup variables
$appName = 'TouchBB';
$scriptVersion = '2.2';
$delims = array(chr(13), chr(10), chr(9));
$appURL = 'http://itunes.apple.com/it/app/touchbb-lite/id332458253?mt=8';
$newerVersion = (@version_compare($board_config['version'], '2.0.19', '>='))?true:false;

// requested variables
$get = ($HTTP_GET_VARS['get'])?$HTTP_GET_VARS['get']:(($HTTP_POST_VARS['get'])?$HTTP_POST_VARS['get']:'');
$id = intval(($HTTP_GET_VARS['id'])?$HTTP_GET_VARS['id']:0);
$username = utf8_decode($HTTP_POST_VARS['user']);
$password = utf8_decode($HTTP_POST_VARS['pass']);
$search = utf8_decode($HTTP_GET_VARS['search']);
$msgsPerPage = ($HTTP_GET_VARS['mpp'])?$HTTP_GET_VARS['mpp']:20;
$page = ((($HTTP_GET_VARS['page'])?$HTTP_GET_VARS['page']:1) * $msgsPerPage) - $msgsPerPage;
$slang = ($HTTP_GET_VARS['l'])?$HTTP_GET_VARS['l']:'en';

// legacy support; localization is now available in the app itself
$strings = array('Last post by %s, %s',
                 '%d post%s in %d topic%s',
                 'No activity',
                 'By %s, %s',
                 'No replies',
                 '%d repl%s from %d view%s',
                 '%d unread message%s',
                 '%d read message%s',
                 'Topics',
                 'Stickies',
                 'Announcements',
                 'Other',
                 'Active Topics',
                 'Unanswered Posts',
                 'My Posts',
                 'Matching Topics',
                 'Forum',
                 'Subforums',
);

// read in extra settings if specified
if (is_file('touchbb.ini')) {
  $tbbSettings = parse_ini_file('touchbb.ini', true);
}

// try turning this on if you are having login issues
$fixSettings = false;
if ($fixSettings) {
  $path = dirname($_SERVER['SCRIPT_NAME']);
  $board_config['script_path'] = $board_config['cookie_path'] = $path;
  $board_config['cookie_domain'] = $_SERVER['HTTP_HOST'];
}

// make sure these variables are clean
$output = '';

// turn off all error reporting
if (!$debugMode) {
  error_reporting(0);
}

// start session management
$userdata = session_pagestart($user_ip, $id);
init_userprefs($userdata);

// Explicity set the encoding for PHP >= 5.6
header('Content-Type: text/html; charset=' . $lang['ENCODING']);

// make sure the version string is correct
if ($board_config['version'][0] == '.') {
  $board_config['version'] = '2' . $board_config['version'];
}

// figure out what version of the software they are using
$appVersion = '1.0';
$UA = str_replace('TouchBBLite', 'TouchBB', $_SERVER['HTTP_USER_AGENT']);
if (substr_count($UA, $appName)) {
  if (@preg_match("/$appName\/(.*?)\s/", $UA, $match)) {
    $appVersion = $match[1];
  } elseif (@preg_match("/$appName(.*?)\s/", $UA, $match)) {
    $appVersion = $match[1];
  }
}

// a flag that signifies a newer protocol
$newProtocol = (@version_compare($appVersion, '2.0', '>='))?true:false;

// send a unix timestamp to newer clients
if ($newProtocol) {
  $userdata['user_timezone'] = $board_config['board_timezone'] = 0;
  $userdata['user_dateformat'] = $board_config['default_dateformat'] = 'U';
  $board_config['fuzzy_dates'] = 0;
} else {
  // shorten the date format a little
  $board_config['default_dateformat'] = str_replace('F', 'M', $board_config['default_dateformat']);
  if ($userdata['user_dateformat']) {
    $userdata['user_dateformat'] = str_replace('F', 'M', $userdata['user_dateformat']);
  }
}

// this is a list of forum IDs this user has permission to
$sql = "SELECT forum_id FROM " . FORUMS_TABLE;
$result = $db->sql_query($sql);
while ($row = $db->sql_fetchrow($result)) {
  // start auth check
  $is_auth = auth(AUTH_ALL, $row['forum_id'], $userdata);

  // make sure we can read and view
  if ($is_auth['auth_read'] && $is_auth['auth_view']) {
    $forumids[] = $row['forum_id'];
  }
}
$db->sql_freeresult($result);

// ****************************
// ********* READ PM **********
// ****************************
if ($get == 'pm') {
  // don't allow anonymous reading of PMs
  if ($userdata['session_logged_in']) {
    if (!$board_config['privmsg_disable']) {
      // grab the private message data
      $sql = "SELECT username,privmsgs_text,privmsgs_bbcode_uid,privmsgs_date" . (function_exists('display_post_attachments')?',privmsgs_attachment':'') . ',privmsgs_to_userid,privmsgs_from_userid FROM ' . PRIVMSGS_TABLE . ',' . PRIVMSGS_TEXT_TABLE . ',' . USERS_TABLE . " WHERE privmsgs_text_id=privmsgs_id AND user_id=privmsgs_from_userid AND privmsgs_id=" . quote($id);

      $result = $db->sql_query($sql);
      $row = $db->sql_fetchrow($result);
      $db->sql_freeresult($result);

      if ($row) {
        // allow user to read their sent PMs too
        if ($userdata['user_id'] == $row['privmsgs_to_userid'] || $userdata['user_id'] == $row['privmsgs_from_userid']) {
          $message = $row['privmsgs_text'];

          if ($row['privmsgs_attachment']) {
            $sql = "SELECT physical_filename FROM " . ATTACHMENTS_TABLE . "," . ATTACHMENTS_DESC_TABLE . " WHERE mimetype LIKE '%image/%' AND " . ATTACHMENTS_TABLE . ".attach_id=" . ATTACHMENTS_DESC_TABLE . ".attach_id AND privmsgs_id=" . quote($id);
            if (!($result = $db->sql_query($sql))) {
              touchbb_error('INTERNAL_ERROR');
            }

            $cntr = 0;
            while ($row2 = $db->sql_fetchrow($result)) {
              if (!$cntr++) {
                $message .= "<br><b>Attachments:</b><br>";
              }

              $message .= '[img]http://' . $board_config['server_name'] . (($nuked)?'/':$board_config['script_path']) . $attach_config['upload_dir'] . '/' . $row2['physical_filename'] . '[/img]<br>';
            }

            $db->sql_freeresult($result);
          }

          // display header ads
          if ($tbbSettings['pm_header_ads']) {
            $imgs = $urls = array();
            if ($tbbSettings['pm_header_ads']['random']) {
              $pos = rand(0, count($tbbSettings['pm_header_ads']['imgs']) - 1);
              $imgs[] = $tbbSettings['pm_header_ads']['imgs'][$pos];
              $urls[] = $tbbSettings['pm_header_ads']['urls'][$pos];
            } else {
              $imgs = array_reverse($tbbSettings['pm_header_ads']['imgs']);
              $urls = array_reverse($tbbSettings['pm_header_ads']['urls']);
            }

            for ($adc=0;$adc<count($imgs);$adc++) {
              $message = '<a href="' . $urls[$adc] . '"><img src="' . $imgs[$adc] . '" onload="resizeIMG(this);"></a><br><br>' . $message;
            }
          }

          // display footer ads
          if ($tbbSettings['pm_footer_ads']) {
            $imgs = $urls = array();
            if ($tbbSettings['pm_footer_ads']['random']) {
              $pos = rand(0, count($tbbSettings['pm_footer_ads']['imgs']) - 1);
              $imgs[] = $tbbSettings['pm_footer_ads']['imgs'][$pos];
              $urls[] = $tbbSettings['pm_footer_ads']['urls'][$pos];
            } else {
              $imgs = $tbbSettings['pm_footer_ads']['imgs'];
              $urls = $tbbSettings['pm_footer_ads']['urls'];
            }

            for ($adc=0;$adc<count($imgs);$adc++) {
              $message .= '<br><br><a href="' . $urls[$adc] . '"><img src="' . $imgs[$adc] . '" onload="resizeIMG(this);"></a>';
            }
          }

          $output .= clean($id) . $delims[1];
          $output .= clean($row['username']) . $delims[1];
          $output .= clean($message, $row['privmsgs_bbcode_uid']) . $delims[1];
          $output .= clean(create_date($board_config['default_dateformat'], $row['privmsgs_date'], $board_config['board_timezone']));

          // mark message as read if we didn't send it
          if ($HTTP_GET_VARS['mark'] == 'read') {
            $db->sql_query("UPDATE " . PRIVMSGS_TO_TABLE . " SET pm_unread=0 WHERE msg_id=" . quote($id));
            $db->sql_query("UPDATE " . PRIVMSGS_TABLE . " SET privmsgs_type=" . PRIVMSGS_READ_MAIL . " WHERE privmsgs_id=" . quote($id));

            // update PM unread status
            update_pm_counts();
          }
        } else {
          touchbb_error('NOT_YOUR_PM');
        }
      } else {
        touchbb_error('INVALID_PM');
      }
    } else {
      // private messages disabled
    }
  } else {
    // not logged in
  }
}

// ****************************
// ********* POST PM **********
// ****************************
if ($get == 'replypm' || $get == 'postpm') {
  // don't allow anonymous posting or spam bots will go crazy
  if ($userdata['session_logged_in']) {
    if (!$board_config['privmsg_disable']) {
      @include($phpbb_root_path . 'includes/bbcode.' . $phpEx);
      @include($phpbb_root_path . 'includes/functions_post.' . $phpEx);

      $html_on = true;
      $bbcode_on = true;
      $smilies_on = true;
      $bbcode_uid = true;
      $attach_sig = '0';

      if ($bbcode_on) {
        $bbcode_uid = make_bbcode_uid();
      }

      $privmsg_message = prepare_message($HTTP_POST_VARS['txt'], $html_on, $bbcode_on, $smilies_on, $bbcode_uid);
      $privmsg_subject = trim(htmlspecialchars($HTTP_POST_VARS['title'], ENT_COMPAT, $lang['ENCODING']));
      if (!$privmsg_subject) {
        $sql = "SELECT privmsgs_subject FROM " . PRIVMSGS_TABLE . " WHERE privmsgs_id=" . quote($HTTP_POST_VARS['rid']);
        $result = $db->sql_query($sql);
        $row = $db->sql_fetchrow($result);
        $db->sql_freeresult($result);
        $privmsg_subject = $row['privmsgs_subject'];
        if (!substr_count($privmsg_subject, 'Re:')) {
          $privmsg_subject = 'Re: ' . $privmsg_subject;
        }
      }

      $to = $_REQUEST['to'];
      for ($i=0;$i<count($to);$i++) {
        $to_username = touchbb_clean_username($to[$i]);

        $sql = "SELECT user_id, user_notify_pm, user_email, user_lang, user_active FROM " . USERS_TABLE . " WHERE username=" . quote($to_username) . " AND user_id <> " . ANONYMOUS;
        if (!($result = $db->sql_query($sql))) {
          touchbb_error('INTERNAL_ERROR');
        }

        if (!($to_userdata = $db->sql_fetchrow($result))) {
          touchbb_error('INTERNAL_ERROR');
        }

        // see if recipient is at their inbox limit
        $sql = "SELECT COUNT(privmsgs_id) AS inbox_items, MIN(privmsgs_date) AS oldest_post_time FROM " . PRIVMSGS_TABLE . " WHERE (privmsgs_type = " . PRIVMSGS_NEW_MAIL . " OR privmsgs_type = " . PRIVMSGS_READ_MAIL . "  OR privmsgs_type = " . PRIVMSGS_UNREAD_MAIL . ") AND privmsgs_to_userid = " . quote($to_userdata['user_id']);
        if (!($result = $db->sql_query($sql))) {
          touchbb_error('INTERNAL_ERROR');
        }

        if ($inbox_info = $db->sql_fetchrow($result)) {
          if ($board_config['max_inbox_privmsgs'] && $inbox_info['inbox_items'] >= $board_config['max_inbox_privmsgs']) {
            $sql = "SELECT privmsgs_id FROM " . PRIVMSGS_TABLE . " WHERE (privmsgs_type=" . PRIVMSGS_NEW_MAIL . " OR privmsgs_type=" . PRIVMSGS_READ_MAIL . " OR privmsgs_type=" . PRIVMSGS_UNREAD_MAIL . ") AND privmsgs_date=" . quote($inbox_info['oldest_post_time']) . " AND privmsgs_to_userid=" . quote($to_userdata['user_id']);
            if (!$result = $db->sql_query($sql)) {
              touchbb_error('INTERNAL_ERROR');
            }

            $old_privmsgs_id = $db->sql_fetchrow($result);
            $old_privmsgs_id = $old_privmsgs_id['privmsgs_id'];

            $sql = "DELETE FROM " . PRIVMSGS_TABLE . " WHERE privmsgs_id=" . quote($old_privmsgs_id);
            if (!$db->sql_query($sql)) {
              touchbb_error('INTERNAL_ERROR');
            }

            $sql = "DELETE FROM " . PRIVMSGS_TEXT_TABLE . " WHERE privmsgs_text_id=" . quote($old_privmsgs_id);
            if (!$db->sql_query($sql)) {
              touchbb_error('INTERNAL_ERROR');
            }
          }
        }

        $sql_info = 'INSERT INTO ' . PRIVMSGS_TABLE . ' (privmsgs_type, privmsgs_subject, privmsgs_from_userid, privmsgs_to_userid, privmsgs_date, privmsgs_ip, privmsgs_enable_html, privmsgs_enable_bbcode, privmsgs_enable_smilies, privmsgs_attach_sig) VALUES (' . PRIVMSGS_NEW_MAIL . ',' . quote($privmsg_subject) . ',' . quote($userdata['user_id']) . ',' . quote($to_userdata['user_id']) . ',' . quote(time(), false) . ',' . quote($user_ip) . ',' . quote($html_on) . ',' . quote($bbcode_on) . ',' . quote($smilies_on) . ',' . quote($attach_sig) . ')';

        if (!($result = $db->sql_query($sql_info, BEGIN_TRANSACTION))) {
          touchbb_error('INTERNAL_ERROR');
        }

        $privmsg_sent_id = $db->sql_nextid();

        $sql = "INSERT INTO " . PRIVMSGS_TEXT_TABLE . " (privmsgs_text_id, privmsgs_bbcode_uid, privmsgs_text) VALUES (" . quote($privmsg_sent_id) . ',' . quote($bbcode_uid) . ',' . quote($privmsg_message) . ')';

        if (!$db->sql_query($sql, END_TRANSACTION)) {
          touchbb_error('INTERNAL_ERROR');
        }

        // add to the users new pm counter
        $sql = "UPDATE " . USERS_TABLE . " SET user_new_privmsg=user_new_privmsg+1,user_last_privmsg=" . quote(time(), false) . " WHERE user_id=" . quote($to_userdata['user_id']);
        if (!$status = $db->sql_query($sql)) {
          touchbb_error('INTERNAL_ERROR');
        }

        // attach image to this post
        if (function_exists('display_post_attachments')) {
          $file_tmp = $_FILES['userfile']['tmp_name'];
          if ($file_tmp != 'none' && $file_tmp) {
            $file_type = $_FILES['userfile']['type'];
            $file_name = $_FILES['userfile']['name'];
            $file_error = $_FILES['userfile']['error'];
            $file_size = $_FILES['userfile']['size'];

            $poster_id = $userdata['user_id'];
            $phy_file = $poster_id . '_' . md5(time());
            $mime = 'image/jpeg';
            $ext = 'jpg';

            @copy($file_tmp, $phpbb_root_path . $attach_config['upload_dir'] . '/' . $phy_file);

            $sql = 'INSERT INTO ' . ATTACHMENTS_DESC_TABLE . " (physical_filename,real_filename,download_count,comment,extension,mimetype,filesize,filetime,thumbnail) VALUES (" . quote($phy_file) . ',' . quote($file_name) . ",0,''," . quote($ext) . ',' . quote($mime) . ',' . quote($file_size) . "," . quote(time(), false) . ",0)";
            $db->sql_query($sql);

            $attach_id = $db->sql_nextid();

            $sql = 'INSERT INTO ' . ATTACHMENTS_TABLE . ' (attach_id,post_id,privmsgs_id,user_id_1,user_id_2) VALUES (' . quote($attach_id) . ',0,' . quote($privmsg_sent_id) . ',' . quote($to_userdata['user_id']) . ',' . quote($userdata['user_id']) . ')';
            $db->sql_query($sql);

            $sql = 'UPDATE ' . PRIVMSGS_TABLE . " SET privmsgs_attachment=1 WHERE privmsgs_id=" . quote($privmsg_sent_id);
            $db->sql_query($sql);
          }
        }

        if ($to_userdata['user_notify_pm'] && !empty($to_userdata['user_email']) && $to_userdata['user_active']) {
          $script_name = preg_replace('/^\/?(.*?)\/?$/', "\\1", trim($board_config['script_path']));
          $script_name = ($script_name != '')?$script_name . '/privmsg.php':'privmsg.php';
          $server_name = trim($board_config['server_name']);
          $server_protocol = ($board_config['cookie_secure'])?'https://':'http://';
          $server_port = ($board_config['server_port'] <> 80)?':' . trim($board_config['server_port']) . '/':'/';

          @include($phpbb_root_path . 'includes/emailer.' . $phpEx);
          $emailer = new emailer($board_config['smtp_delivery']);

          $emailer->from($board_config['board_email']);
          $emailer->replyto($board_config['board_email']);

          $emailer->use_template('privmsg_notify', $to_userdata['user_lang']);
          $emailer->email_address($to_userdata['user_email']);
          $emailer->set_subject($lang['Notification_subject']);

          $emailer->assign_vars(array(
            'USERNAME' => stripslashes($to_username),
            'SITENAME' => $board_config['sitename'],
            'EMAIL_SIG' => (!empty($board_config['board_email_sig'])) ? str_replace('<br />', "\n", "-- \n" . $board_config['board_email_sig']) : '',
            'U_INBOX' => $server_protocol . $server_name . $server_port . $script_name . '?folder=inbox')
          );

          $emailer->send();
          $emailer->reset();
        }
      }

      // send success
      $output = clean('1');
    } else {
      // send failure
      $output = clean('0');
    }
  } else {
    // send failure
    $output = clean('0');
  }
}

// ***************************
// ********** LOGIN **********
// ***************************
if ($username && $password) {
  $username = touchbb_clean_username($username);

  // v1.1 added base64 encoding to passwords. Simple encoding is used and not encryption.
  // The problem with a publically available wrapper is that the key used for encryption
  // would be available to anyone who downloaded this script thus not secure. The moral of
  // the story is not to use the same passwords used for bank accounts and secure data as
  // on forums. If this statement really bothers you, consider this. Almost all forums are
  // operated on http:// (non secure) URLs, and your password is sent out via plain text
  // anyway. If anyone has a solution to this that would work on multiple server configs
  // out there, please let me know.
  $password = base64_decode($password);

  $sql = "SELECT user_id, username, user_password, user_active, user_level" . (($newerVersion)?", user_login_tries, user_last_login_try":'') . " FROM " . USERS_TABLE . " WHERE username=" . quote($username);

  if (!($result = $db->sql_query($sql))) {
    touchbb_error('INTERNAL_ERROR');
  }

  if ($row = $db->sql_fetchrow($result)) {
    if ($row['user_level'] != ADMIN && $board_config['board_disable']) {
      touchbb_error('INTERNAL_ERROR');
    } else {
      if ($newerVersion) {
        // if the last login is more than x minutes ago, then reset the login tries/time
        if ($row['user_last_login_try'] && $board_config['login_reset_time'] && $row['user_last_login_try'] < (time() - ($board_config['login_reset_time'] * 60)) ) {
          $db->sql_query('UPDATE ' . USERS_TABLE . ' SET user_login_tries=0,user_last_login_try=0 WHERE user_id=' . quote($row['user_id']));
          $row['user_last_login_try'] = $row['user_login_tries'] = 0;
        }

        // check to see if user is allowed to login again... if his tries are exceeded
        if ($row['user_last_login_try'] && $board_config['login_reset_time'] && $board_config['max_login_attempts'] && $row['user_last_login_try'] >= (time() - ($board_config['login_reset_time'] * 60)) && $row['user_login_tries'] >= $board_config['max_login_attempts'] && $userdata['user_level'] != ADMIN) {
          touchbb_error('INTERNAL_ERROR');
        }
      }

      // did the user specify the right password?
      if (md5($password) == $row['user_password'] && $row['user_active']) {
        $session_id = session_begin($row['user_id'], $user_ip, PAGE_INDEX, FALSE, 0, 0);

        // reset login tries
        if ($newerVersion) {
          $db->sql_query('UPDATE ' . USERS_TABLE . ' SET user_login_tries=0,user_last_login_try=0 WHERE user_id=' . quote($row['user_id']));
        }

        if ($session_id) {
          // logged in!
          $userdata['session_logged_in'] = 1;
        } else {
          touchbb_error('INTERNAL_ERROR');
        }
      } elseif ($row['user_active'] && $newerVersion) {
        // only store a failed login attempt for an active user - inactive users can't login even with a correct password
        if ($row['user_id'] != ANONYMOUS) {
          $sql = 'UPDATE ' . USERS_TABLE . ' SET user_login_tries=user_login_tries+1,user_last_login_try=' . quote(time(), false) . ' WHERE user_id=' . quote($row['user_id']);
          $db->sql_query($sql);
        }
      }

      if (!$session_id) {
        touchbb_error('INTERNAL_ERROR');
      }
    }
  } else {
    touchbb_error('INTERNAL_ERROR');
  }
}

// ****************************
// ********** LOGOUT **********
// ****************************
if ($get == 'logout') {
  if ($userdata['session_logged_in']) {
    session_end($userdata['session_id'], $userdata['user_id']);
  }

  if ($userdata['session_logged_in']) {
    $output = 0;
  } else {
    $output = 1;
  }
}

// ****************************
// ********* ACTIVE ***********
// ****************************
if ($get == 'active') {
  // grab all the basic data (all topics except announcements) for this forum
  $sql = "SELECT t.*, u.username, u.user_id, u2.username as user2, u2.user_id as id2, p.post_username, p2.post_username AS post_username2, p2.post_time FROM " . TOPICS_TABLE . " t, " . USERS_TABLE . " u, " . POSTS_TABLE . " p, " . POSTS_TABLE . " p2, " . USERS_TABLE . " u2 WHERE (t.forum_id='" . implode("' OR t.forum_id='", $forumids) . "') AND t.topic_poster=u.user_id AND p.post_id=t.topic_first_post_id AND p2.post_id=t.topic_last_post_id AND u2.user_id=p2.poster_id AND t.topic_type <> " . POST_ANNOUNCE . " ORDER BY t.topic_last_post_id DESC LIMIT " . quote($page, false) . ',' . quote($msgsPerPage, false);
  if (!($result = $db->sql_query($sql))) {
    touchbb_error('INTERNAL_ERROR');
  }

  $topic_rowset = array();
  while ($row = $db->sql_fetchrow($result)) {
    $topic_rowset[] = $row;
  }

  $db->sql_freeresult($result);

  // define censored word matches
  $orig_word = array();
  $replacement_word = array();
  obtain_word_list($orig_word, $replacement_word);

  // okay, lets dump out the page ...
  for ($i = 0; $i < count($topic_rowset); $i++) {
    $forum_id = $topic_rowset[$i]['forum_id'];

    $topic_id = $topic_rowset[$i]['topic_id'];
    $topic_title = (count($orig_word)) ? preg_replace($orig_word, $replacement_word, $topic_rowset[$i]['topic_title']) : $topic_rowset[$i]['topic_title'];
    $replies = $topic_rowset[$i]['topic_replies'];

    if ($topic_rowset[$i]['topic_status'] == TOPIC_MOVED) {
      $topic_id = $topic_rowset[$i]['topic_moved_id'];
    }

    $topic_author = ($topic_rowset[$i]['user_id'] != ANONYMOUS) ? $topic_rowset[$i]['username'] : (($topic_rowset[$i]['post_username'] != '') ? $topic_rowset[$i]['post_username'] : $lang['Guest']);
    $first_post_time = create_date($board_config['default_dateformat'], $topic_rowset[$i]['topic_time'], $board_config['board_timezone']);
    $last_post_time = create_date($board_config['default_dateformat'], $topic_rowset[$i]['post_time'], $board_config['board_timezone']);
    $last_post_author = ($topic_rowset[$i]['id2'] == ANONYMOUS) ? (($topic_rowset[$i]['post_username2'] != '') ? $topic_rowset[$i]['post_username2'] . ' ' : $lang['Guest'] . ' ') : ''. $topic_rowset[$i]['user2'];

    $views = $topic_rowset[$i]['topic_views'];

    if ($output) {
      $output .= $delims[0];
    }

    // tell the application that this is a topic
    $output .= clean(1) . $delims[1];
    $output .= clean($topic_id) . $delims[1];
    $output .= ((!$i&&!$newProtocol)?clean($strings[12]):'') . $delims[1];
    $output .= html_to_straight_quotes($topic_title) . $delims[1];

    if ($newProtocol) {
      if ($replies > 0) {
        $output .= clean($last_post_author);
      } else {
        $output .= clean($topic_author);
      }
    } else {
      if ($replies > 0) {
        $output .= clean(sprintf($strings[0], $last_post_author, $last_post_time));
      } else {
        $output .= clean(sprintf($strings[3], $topic_author, $first_post_time));
      }
    }

    $output .= $delims[1];

    if ($newProtocol) {
      $output .= clean($views);
    } else {
      if ($views > 0 && $replies > 0) {
        $output .= clean(sprintf($strings[5], $replies, ($replies!=1)?'ies':'y', $views, ($views!=1)?'s':''));
      } else {
        $output .= clean($strings[4]);
      }
    }

    $output .= $delims[1];

    $output .= clean($forum_id) . $delims[1];
    $output .= (($userdata['session_logged_in'] && $topic_rowset[$i]['post_time'] > $userdata['user_lastvisit'])?1:0) . $delims[1];
    $output .= ($replies + 1);

    if ($newProtocol) {
      $output .= $delims[1];

      if ($replies > 0) {
        $output .= clean($last_post_time);
      } else {
        $output .= clean($first_post_time);
      }
    }
  }
}

// ****************************
// ******* UNANSWERED *********
// ****************************
if ($get == 'unanswered') {
  // grab all the basic data (all topics except announcements) for this forum
  $sql = "SELECT t.*, u.username, u.user_id, u2.username as user2, u2.user_id as id2, p.post_username, p2.post_username AS post_username2, p2.post_time FROM " . TOPICS_TABLE . " t, " . USERS_TABLE . " u, " . POSTS_TABLE . " p, " . POSTS_TABLE . " p2, " . USERS_TABLE . " u2 WHERE (t.forum_id='" . implode("' OR t.forum_id='", $forumids) . "') AND t.topic_replies=0 AND t.topic_poster=u.user_id AND p.post_id=t.topic_first_post_id AND p2.post_id=t.topic_last_post_id AND u2.user_id=p2.poster_id AND t.topic_type <> " . POST_ANNOUNCE . " ORDER BY t.topic_last_post_id DESC LIMIT " . quote($page, false) . ',' . quote($msgsPerPage, false);
  if (!($result = $db->sql_query($sql))) {
    touchbb_error('INTERNAL_ERROR');
  }

  $topic_rowset = array();
  while ($row = $db->sql_fetchrow($result)) {
    $topic_rowset[] = $row;
  }

  $db->sql_freeresult($result);

  // define censored word matches
  $orig_word = array();
  $replacement_word = array();
  obtain_word_list($orig_word, $replacement_word);

  // okay, lets dump out the page ...
  for ($i = 0; $i < count($topic_rowset); $i++) {
    $forum_id = $topic_rowset[$i]['forum_id'];

    $topic_id = $topic_rowset[$i]['topic_id'];
    $topic_title = (count($orig_word)) ? preg_replace($orig_word, $replacement_word, $topic_rowset[$i]['topic_title']) : $topic_rowset[$i]['topic_title'];
    $replies = $topic_rowset[$i]['topic_replies'];

    if ($topic_rowset[$i]['topic_status'] == TOPIC_MOVED) {
      $topic_id = $topic_rowset[$i]['topic_moved_id'];
    }

    $topic_author = ($topic_rowset[$i]['user_id'] != ANONYMOUS) ? $topic_rowset[$i]['username'] : (($topic_rowset[$i]['post_username'] != '') ? $topic_rowset[$i]['post_username'] : $lang['Guest']);
    $first_post_time = create_date($board_config['default_dateformat'], $topic_rowset[$i]['topic_time'], $board_config['board_timezone']);
    $last_post_time = create_date($board_config['default_dateformat'], $topic_rowset[$i]['post_time'], $board_config['board_timezone']);
    $last_post_author = ($topic_rowset[$i]['id2'] == ANONYMOUS) ? (($topic_rowset[$i]['post_username2'] != '') ? $topic_rowset[$i]['post_username2'] . ' ' : $lang['Guest'] . ' ') : ''. $topic_rowset[$i]['user2'];

    $views = $topic_rowset[$i]['topic_views'];

    if ($output) {
      $output .= $delims[0];
    }

    // tell the application that this is a topic
    $output .= clean(1) . $delims[1];
    $output .= clean($topic_id) . $delims[1];
    $output .= ((!$i&&!$newProtocol)?clean($strings[13]):'') . $delims[1];
    $output .= html_to_straight_quotes($topic_title) . $delims[1];

    if ($newProtocol) {
      if ($replies > 0) {
        $output .= clean($last_post_author);
      } else {
        $output .= clean($topic_author);
      }
    } else {
      if ($replies > 0) {
        $output .= clean(sprintf($strings[0], $last_post_author, $last_post_time));
      } else {
        $output .= clean(sprintf($strings[3], $topic_author, $first_post_time));
      }
    }

    $output .= $delims[1];

    if ($newProtocol) {
      $output .= clean($views);
    } else {
      if ($views > 0 && $replies > 0) {
        $output .= clean(sprintf($strings[5], $replies, ($replies!=1)?'ies':'y', $views, ($views!=1)?'s':''));
      } else {
        $output .= clean($strings[4]);
      }
    }

    $output .= $delims[1];

    $output .= clean($forum_id) . $delims[1];
    $output .= (($userdata['session_logged_in'] && $topic_rowset[$i]['post_time'] > $userdata['user_lastvisit'])?1:0) . $delims[1];
    $output .= ($replies + 1);

    if ($newProtocol) {
      $output .= $delims[1];

      if ($replies > 0) {
        $output .= clean($last_post_time);
      } else {
        $output .= clean($first_post_time);
      }
    }
  }
}

// ****************************
// ********* MY POSTS *********
// ****************************
if ($get == 'myposts') {
  // grab all the basic data (all topics except announcements) for this forum
  $sql = "SELECT t.*, u.username, u.user_id, u2.username as user2, u2.user_id as id2, p.post_username, p2.post_username AS post_username2, p2.post_time FROM " . TOPICS_TABLE . " t, " . USERS_TABLE . " u, " . POSTS_TABLE . " p, " . POSTS_TABLE . " p2, " . USERS_TABLE . " u2 WHERE (t.forum_id='" . implode("' OR t.forum_id='", $forumids) . "') AND u.user_id=" . quote($userdata['user_id']) . " AND t.topic_poster=u.user_id AND p.post_id=t.topic_first_post_id AND p2.post_id=t.topic_last_post_id AND u2.user_id=p2.poster_id AND t.topic_type <> " . POST_ANNOUNCE . " ORDER BY t.topic_type DESC, t.topic_last_post_id DESC LIMIT " . quote($page, false) . ',' . quote($msgsPerPage, false);
  if (!($result = $db->sql_query($sql))) {
    touchbb_error('INTERNAL_ERROR');
  }

  $topic_rowset = array();
  while ($row = $db->sql_fetchrow($result)) {
    $topic_rowset[] = $row;
  }

  $db->sql_freeresult($result);

  // define censored word matches
  $orig_word = array();
  $replacement_word = array();
  obtain_word_list($orig_word, $replacement_word);

  // okay, lets dump out the page ...
  for ($i = 0; $i < count($topic_rowset); $i++) {
    $forum_id = $topic_rowset[$i]['forum_id'];

    $topic_id = $topic_rowset[$i]['topic_id'];
    $topic_title = (count($orig_word)) ? preg_replace($orig_word, $replacement_word, $topic_rowset[$i]['topic_title']) : $topic_rowset[$i]['topic_title'];
    $replies = $topic_rowset[$i]['topic_replies'];

    if ($topic_rowset[$i]['topic_status'] == TOPIC_MOVED) {
      $topic_id = $topic_rowset[$i]['topic_moved_id'];
    }

    $topic_author = ($topic_rowset[$i]['user_id'] != ANONYMOUS) ? $topic_rowset[$i]['username'] : (($topic_rowset[$i]['post_username'] != '') ? $topic_rowset[$i]['post_username'] : $lang['Guest']);
    $first_post_time = create_date($board_config['default_dateformat'], $topic_rowset[$i]['topic_time'], $board_config['board_timezone']);
    $last_post_time = create_date($board_config['default_dateformat'], $topic_rowset[$i]['post_time'], $board_config['board_timezone']);
    $last_post_author = ($topic_rowset[$i]['id2'] == ANONYMOUS) ? (($topic_rowset[$i]['post_username2'] != '') ? $topic_rowset[$i]['post_username2'] . ' ' : $lang['Guest'] . ' ') : ''. $topic_rowset[$i]['user2'];

    $views = $topic_rowset[$i]['topic_views'];

    if ($output) {
      $output .= $delims[0];
    }

    // tell the application that this is a topic
    $output .= clean(1) . $delims[1];
    $output .= clean($topic_id) . $delims[1];
    $output .= ((!$i&&!$newProtocol)?clean($strings[14]):'') . $delims[1];
	$output .= html_to_straight_quotes($topic_title) . $delims[1];

    if ($newProtocol) {
      if ($replies > 0) {
        $output .= clean($last_post_author);
      } else {
        $output .= clean($topic_author);
      }
    } else {
      if ($replies > 0) {
        $output .= clean(sprintf($strings[0], $last_post_author, $last_post_time));
      } else {
        $output .= clean(sprintf($strings[3], $topic_author, $first_post_time));
      }
    }

    $output .= $delims[1];

    if ($newProtocol) {
      $output .= clean($views);
    } else {
      if ($views > 0 && $replies > 0) {
        $output .= clean(sprintf($strings[5], $replies, ($replies!=1)?'ies':'y', $views, ($views!=1)?'s':''));
      } else {
        $output .= clean($strings[4]);
      }
    }

    $output .= $delims[1];

    $output .= clean($forum_id) . $delims[1];
    $output .= (($userdata['session_logged_in'] && $topic_rowset[$i]['post_time'] > $userdata['user_lastvisit'])?1:0) . $delims[1];
    $output .= ($replies + 1);

    if ($newProtocol) {
      $output .= $delims[1];

      if ($replies > 0) {
        $output .= clean($last_post_time);
      } else {
        $output .= clean($first_post_time);
      }
    }
  }
}

// **************************
// ********* ONLINE *********
// **************************
if ($get == 'online') {
  $sql = "SELECT u.user_id, u.username, u.user_allow_viewonline FROM " . USERS_TABLE . " u, " . SESSIONS_TABLE . " s WHERE u.user_id!=-1 AND u.user_id=s.session_user_id AND s.session_time>=" . quote(time() - 300, false) . " GROUP BY u.user_id ORDER BY u.username ASC, s.session_ip ASC";
  if (!($result = $db->sql_query($sql))) {
    touchbb_error('INTERNAL_ERROR');
  }

  // loop through users list
  while ($row = $db->sql_fetchrow($result)) {
    if ($row['user_allow_viewonline']) {
      if ($output) {
        $output .= $delims[0];
      }

      $output .= clean($row['user_id']) . $delims[1];
      $output .= clean($row['username']);
    }
  }

  $db->sql_freeresult($result);
}

// ***************************
// ********* MEMBERS *********
// ***************************
if ($get == 'members') {
  $sql = "SELECT user_id,username FROM " . USERS_TABLE . " WHERE " . USERS_TABLE . ".user_id!=-1 ORDER BY LOWER(username)";
  $result = $db->sql_query($sql);
  while ($row = $db->sql_fetchrow($result)) {
    if ($output) {
      $output .= $delims[0];
    }

    $output .= clean($row['user_id']) . $delims[1];
    $output .= clean($row['username']);
  }

  $db->sql_freeresult($result);
}

// ****************************
// *********** SENT ***********
// ****************************
if ($get == 'sent') {
  // don't allow anonymous reading of PMs
  if ($userdata['session_logged_in']) {
    if (!$board_config['privmsg_disable']) {
      // update PM unread status
      update_pm_counts();

      $sql = "SELECT pm.privmsgs_type, pm.privmsgs_id, pm.privmsgs_date, pm.privmsgs_subject, u.user_id, u.username FROM " . PRIVMSGS_TABLE . " pm, " . USERS_TABLE . " u WHERE u.user_id=pm.privmsgs_to_userid AND pm.privmsgs_from_userid=" . quote($userdata['user_id']) . " AND (pm.privmsgs_type=" . PRIVMSGS_NEW_MAIL . " OR pm.privmsgs_type=" . PRIVMSGS_UNREAD_MAIL . " OR pm.privmsgs_type=" . PRIVMSGS_SENT_MAIL . " OR pm.privmsgs_type=" . PRIVMSGS_READ_MAIL . ") ORDER BY privmsgs_date DESC";

      if (!($result = $db->sql_query($sql))) {
        touchbb_error('INTERNAL_ERROR');
      }

      while ($row = $db->sql_fetchrow($result)) {
        if ($row['privmsgs_type'] == PRIVMSGS_NEW_MAIL || $row['privmsgs_type'] == PRIVMSGS_UNREAD_MAIL) {
          $outbox[] = $row;
        } else {
          $sent[] = $row;
        }
      }

      $db->sql_freeresult($result);

      // send outbox messages
      for ($i=0;$i<count($outbox);$i++) {
        if ($output) {
          $output .= $delims[0];
        }

        // grab the current message
        $msg = $outbox[$i];

        // format output
        $output .= clean($msg['privmsgs_id']) . $delims[1];

        if ($newProtocol) {
          $output .= ((!$i)?clean(count($outbox)):'') . $delims[1];
        } else {
          $output .= ((!$i)?clean(sprintf($strings[6], count($outbox), (count($outbox)==1)?'':'s')):'') . $delims[1];
        }

        $output .= clean($msg['privmsgs_subject']) . $delims[1];
        $output .= clean($msg['username']) . $delims[1];
        $output .= clean(create_date($board_config['default_dateformat'], $msg['privmsgs_date'], $board_config['board_timezone']));

        if ($newProtocol) {
          $output .= $delims[1] . ((!$i)?'1':'');
        }
      }

      // send sent messages
      for ($i=0;$i<count($sent);$i++) {
        if ($output) {
          $output .= $delims[0];
        }

        // grab the current message
        $msg = $sent[$i];

        // format output
        $output .= clean($msg['privmsgs_id']) . $delims[1];

        if ($newProtocol) {
          $output .= ((!$i)?clean(count($sent)):'') . $delims[1];
        } else {
          $output .= ((!$i)?clean(sprintf($strings[7], count($sent), (count($sent)==1)?'':'s')):'') . $delims[1];
        }

        $output .= clean($msg['privmsgs_subject']) . $delims[1];
        $output .= clean($msg['username']) . $delims[1];
        $output .= clean(create_date($board_config['default_dateformat'], $msg['privmsgs_date'], $board_config['board_timezone']));

        if ($newProtocol) {
          $output .= $delims[1] . ((!$i)?'2':'');
        }
      }
    } else {
      // private messages disabled
    }
  } else {
    // not logged in
  }
}

// ****************************
// ********** INBOX ***********
// ****************************
if ($get == 'inbox') {
  // don't allow anonymous reading of PMs
  if ($userdata['session_logged_in']) {
    if (!$board_config['privmsg_disable']) {
      // update PM unread status
      update_pm_counts();

      $sql = "UPDATE " . PRIVMSGS_TABLE . " SET privmsgs_type=" . PRIVMSGS_UNREAD_MAIL . " WHERE privmsgs_type=" . PRIVMSGS_NEW_MAIL . " AND privmsgs_to_userid=" . quote($userdata['user_id']);
      if (!$db->sql_query($sql)) {
        touchbb_error('INTERNAL_ERROR');
      }

      $sql = "SELECT pm.privmsgs_type, pm.privmsgs_id, pm.privmsgs_date, pm.privmsgs_subject, u.user_id, u.username FROM " . PRIVMSGS_TABLE . " pm, " . USERS_TABLE . " u WHERE pm.privmsgs_to_userid=" . quote($userdata['user_id']) . " AND u.user_id=pm.privmsgs_from_userid AND (pm.privmsgs_type=" . PRIVMSGS_NEW_MAIL . " OR pm.privmsgs_type=" . PRIVMSGS_READ_MAIL . " OR privmsgs_type=" . PRIVMSGS_UNREAD_MAIL . ") ORDER BY privmsgs_date DESC";

      if (!($result = $db->sql_query($sql))) {
        touchbb_error('INTERNAL_ERROR');
      }

      while ($row = $db->sql_fetchrow($result)) {
        if ($row['privmsgs_type'] == PRIVMSGS_NEW_MAIL || $row['privmsgs_type'] == PRIVMSGS_UNREAD_MAIL) {
          $unread[] = $row;
        } else {
          $read[] = $row;
        }
      }

      $db->sql_freeresult($result);

      // send unread messages
      for ($i=0;$i<count($unread);$i++) {
        if ($output) {
          $output .= $delims[0];
        }

        // grab the current message
        $msg = $unread[$i];

        // format output
        $output .= clean($msg['privmsgs_id']) . $delims[1];

        if ($newProtocol) {
          $output .= ((!$i)?clean(count($unread)):'') . $delims[1];
        } else {
          $output .= ((!$i)?clean(sprintf($strings[6], count($unread), (count($unread)==1)?'':'s')):'') . $delims[1];
        }

        $output .= clean($msg['privmsgs_subject']) . $delims[1];
        $output .= clean($msg['username']) . $delims[1];
        $output .= clean(create_date($board_config['default_dateformat'], $msg['privmsgs_date'], $board_config['board_timezone']));

        if ($newProtocol) {
          $output .= $delims[1] . ((!$i)?'1':'');
        }
      }

      // send read messages
      for ($i=0;$i<count($read);$i++) {
        if ($output) {
          $output .= $delims[0];
        }

        // grab the current message
        $msg = $read[$i];

        // format output
        $output .= clean($msg['privmsgs_id']) . $delims[1];

        if ($newProtocol) {
          $output .= ((!$i)?clean(count($read)):'') . $delims[1];
        } else {
          $output .= ((!$i)?clean(sprintf($strings[7], count($read), (count($read)==1)?'':'s')):'') . $delims[1];
        }

        $output .= clean($msg['privmsgs_subject']) . $delims[1];
        $output .= clean($msg['username']) . $delims[1];
        $output .= clean(create_date($board_config['default_dateformat'], $msg['privmsgs_date'], $board_config['board_timezone']));

        if ($newProtocol) {
          $output .= $delims[1] . ((!$i)?'2':'');
        }
      }
    } else {
      // private messages disabled
    }
  } else {
    // not logged in
  }
}

// ****************************
// ****** REPLY and POST ******
// ****************************
if ($get == 'reply' || $get == 'post') {
  @include($phpbb_root_path . 'includes/bbcode.' . $phpEx);
  @include($phpbb_root_path . 'includes/functions_post.' . $phpEx);

  if (@file_exists($phpbb_root_path . 'includes/functions_points.' . $phpEx)) {
    @include($phpbb_root_path . 'includes/functions_points.' . $phpEx);
  }

  // don't allow anonymous posting so spam bots won't go crazy
  if ($userdata['session_logged_in']) {
    $subject = (!empty($HTTP_POST_VARS['title'])) ? utf8_decode(trim($HTTP_POST_VARS['title'])) : '';
    $message = (!empty($HTTP_POST_VARS['txt'])) ? utf8_decode($HTTP_POST_VARS['txt']) : '';
    $topic_id = (!empty($HTTP_POST_VARS['tid'])) ? $HTTP_POST_VARS['tid'] : '';
    $forum_id = (!empty($HTTP_POST_VARS['fid'])) ? $HTTP_POST_VARS['fid'] : '';
    $bbcode_uid = '';

    $mode = ($get=='post')?'newtopic':'reply';
    $bbcode_on = 1;
    $html_on = 0;
    $smilies_on = 0;

    if ($nuked) {
      prepare_post($mode, $post_data, $bbcode_on, $html_on, $smilies_on, $error_msg, $username, $bbcode_uid, $subject, $message, $null, $null, $null, $null);
    } else {
      prepare_post($mode, $post_data, $bbcode_on, $html_on, $smilies_on, $error_msg, $username, $bbcode_uid, $subject, $message, $null, $null, $null);
    }

    if ($error_msg == '') {
      $topic_type = POST_NORMAL;
      $attach_sig = 1;

      if ($nuked) {
        submit_post($mode, $post_data, $return_message, $return_meta, $forum_id, $topic_id, $post_id, $null, $topic_type, $bbcode_on, $html_on, $smilies_on, $attach_sig, $bbcode_uid, str_replace("\'", "''", $username), str_replace("\'", "''", $subject), str_replace("\'", "''", $message), $null, $null, $null, $null);
      } else {
        submit_post($mode, $post_data, $return_message, $return_meta, $forum_id, $topic_id, $post_id, $null, $topic_type, $bbcode_on, $html_on, $smilies_on, $attach_sig, $bbcode_uid, str_replace("\'", "''", $username), str_replace("\'", "''", $subject), str_replace("\'", "''", $message), $null, $null, $null);
      }

      // attach image to this post
      if (function_exists('display_post_attachments')) {
        $file_tmp = $_FILES['userfile']['tmp_name'];
        if ($file_tmp != 'none' && $file_tmp) {
          $file_type = $_FILES['userfile']['type'];
          $file_name = $_FILES['userfile']['name'];
          $file_error = $_FILES['userfile']['error'];
          $file_size = $_FILES['userfile']['size'];

          $poster_id = $userdata['user_id'];
          $phy_file = $poster_id . '_' . md5(time());
          $mime = 'image/jpeg';
          $ext = 'jpg';

          @copy($file_tmp, $upload_dir . '/' . $phy_file);

          $sql = 'INSERT INTO ' . ATTACHMENTS_DESC_TABLE . " (physical_filename,real_filename,download_count,comment,extension,mimetype,filesize,filetime,thumbnail) VALUES (" . quote($phy_file) . ',' . quote($file_name) . ",0,''," . quote($ext) . ',' . quote($mime) . ',' . quote($file_size) . "," . quote(time(), false) . ",0)";
          $db->sql_query($sql);

          $attach_id = $db->sql_nextid();

          $sql = 'INSERT INTO ' . ATTACHMENTS_TABLE . ' (attach_id,post_id,privmsgs_id,user_id_1,user_id_2) VALUES (' . quote($attach_id) . ',' . quote($post_id) . ',0,' . quote($userdata['user_id']) . ',0)';
          $db->sql_query($sql);

          $sql = 'UPDATE ' . POSTS_TABLE . " SET post_attachment=1 WHERE post_id=" . quote($post_id);
          $db->sql_query($sql);
        }
      }
    }

    if ($error_msg == '') {
      update_post_stats($mode, $post_data, $forum_id, $topic_id, $post_id, $userdata['user_id']);

      if ($error_msg == '') {
        user_notification($mode, $post_data, $post_info['topic_title'], $forum_id, $topic_id, $post_id, $notify_user);
      }
    }

    // return updated forum content below
    $get = 'forum';
    $id = $forum_id;
  }
}

// ***************************
// ********* SEARCH **********
// ***************************
if ($search) {
  // grab all the basic data (all topics except announcements) for this forum
  $sql = "SELECT t.*, u.username, u.user_id, u2.username as user2, u2.user_id as id2, p.post_username, p2.post_username AS post_username2, p2.post_time FROM " . TOPICS_TABLE . " t, " . USERS_TABLE . " u, " . POSTS_TABLE . " p, " . POSTS_TABLE . " p2, " . USERS_TABLE . " u2 WHERE (t.forum_id='" . implode("' OR t.forum_id='", $forumids) . "') AND t.topic_title LIKE '%" . quote($search, false) . "%' AND t.topic_poster=u.user_id AND p.post_id=t.topic_first_post_id AND p2.post_id=t.topic_last_post_id AND u2.user_id=p2.poster_id AND t.topic_type <> " . POST_ANNOUNCE . " ORDER BY t.topic_type DESC, t.topic_last_post_id DESC LIMIT " . quote($page, false) . ',' . quote($msgsPerPage, false);
  if (!($result = $db->sql_query($sql))) {
    touchbb_error('INTERNAL_ERROR');
  }

  $topic_rowset = array();
  while ($row = $db->sql_fetchrow($result)) {
    $topic_rowset[] = $row;
  }

  $db->sql_freeresult($result);

  // define censored word matches
  $orig_word = array();
  $replacement_word = array();
  obtain_word_list($orig_word, $replacement_word);

  // okay, lets dump out the page ...
  for ($i = 0; $i < count($topic_rowset); $i++) {
    $forum_id = $topic_rowset[$i]['forum_id'];
    $topic_id = $topic_rowset[$i]['topic_id'];
    $topic_title = (count($orig_word)) ? preg_replace($orig_word, $replacement_word, $topic_rowset[$i]['topic_title']) : $topic_rowset[$i]['topic_title'];
    $replies = $topic_rowset[$i]['topic_replies'];

    if ($topic_rowset[$i]['topic_status'] == TOPIC_MOVED) {
      $topic_id = $topic_rowset[$i]['topic_moved_id'];
    }

    $topic_author = ($topic_rowset[$i]['user_id'] != ANONYMOUS) ? $topic_rowset[$i]['username'] : (($topic_rowset[$i]['post_username'] != '') ? $topic_rowset[$i]['post_username'] : $lang['Guest']);
    $first_post_time = create_date($board_config['default_dateformat'], $topic_rowset[$i]['topic_time'], $board_config['board_timezone']);
    $last_post_time = create_date($board_config['default_dateformat'], $topic_rowset[$i]['post_time'], $board_config['board_timezone']);
    $last_post_author = ($topic_rowset[$i]['id2'] == ANONYMOUS) ? (($topic_rowset[$i]['post_username2'] != '') ? $topic_rowset[$i]['post_username2'] . ' ' : $lang['Guest'] . ' ') : ''. $topic_rowset[$i]['user2'];

    $views = $topic_rowset[$i]['topic_views'];

    if ($output) {
      $output .= $delims[0];
    }

    // tell the application that this is a topic
    $output .= clean(1) . $delims[1];
    $output .= clean($topic_id) . $delims[1];
    $output .= ((!$i&&!$newProtocol)?clean($strings[15]):'') . $delims[1];
    $output .= html_to_straight_quotes($topic_title) . $delims[1];

    if ($newProtocol) {
      if ($replies > 0) {
        $output .= clean($last_post_author);
      } else {
        $output .= clean($topic_author);
      }
    } else {
      if ($replies > 0) {
        $output .= clean(sprintf($strings[0], $last_post_author, $last_post_time));
      } else {
        $output .= clean(sprintf($strings[3], $topic_author, $first_post_time));
      }
    }

    $output .= $delims[1];

    if ($newProtocol) {
      $output .= clean($views);
    } else {
      if ($views > 0 && $replies > 0) {
        $output .= clean(sprintf($strings[5], $replies, ($replies!=1)?'ies':'y', $views, ($views!=1)?'s':''));
      } else {
        $output .= clean($strings[4]);
      }
    }

    $output .= $delims[1];

    $output .= clean($forum_id) . $delims[1];
    $output .= (($userdata['session_logged_in'] && $topic_rowset[$i]['post_time'] > $userdata['user_lastvisit'])?1:0) . $delims[1];
    $output .= ($replies + 1);

    if ($newProtocol) {
      $output .= $delims[1];

      if ($replies > 0) {
        $output .= clean($last_post_time);
      } else {
        $output .= clean($first_post_time);
      }
    }
  }
}

// ***************************
// ********** TOPIC **********
// ***************************
if ($get == 'topic') {
  @include($phpbb_root_path . 'includes/bbcode.' . $phpEx);
  $showUserSig = false;

  if (!$id) {
    touchbb_error('INTERNAL_ERROR');
  }

  // go ahead and allow notifications of this topic again
  if (defined('TOPICS_WATCH_TABLE')) {
    $sql = "UPDATE " . TOPICS_WATCH_TABLE . " SET notify_status=0 WHERE topic_id=" . quote($id) . " AND user_id=" . quote($userdata['user_id']);
    if (!($result = $db->sql_query($sql))) {
      message_die(GENERAL_ERROR, "Could not update topic watch information", '', __LINE__, __FILE__, $sql);
    }
  }

  // get the topic data
  $sql = "SELECT t.topic_id, t.topic_title, t.topic_status, t.topic_replies, t.topic_time, t.topic_type, t.topic_vote, t.topic_last_post_id, f.forum_name, f.forum_status, f.forum_id, f.auth_view, f.auth_read, f.auth_post, f.auth_reply, f.auth_edit, f.auth_delete, f.auth_sticky, f.auth_announce, f.auth_pollcreate, f.auth_vote FROM " . TOPICS_TABLE . " t, " . FORUMS_TABLE . " f WHERE t.topic_id=" . quote($id) . " AND f.forum_id=t.forum_id";

  if (!($result = $db->sql_query($sql))) {
    touchbb_error('INTERNAL_ERROR');
  }

  if (!($forum_topic_data = $db->sql_fetchrow($result))) {
    touchbb_error('INTERNAL_ERROR');
  }

  $forum_id = intval($forum_topic_data['forum_id']);

  // start auth check
  $is_auth = auth(AUTH_ALL, $forum_id, $userdata, $forum_topic_data);

  if (!$is_auth['auth_view'] || !$is_auth['auth_read']) {
    touchbb_error('INTERNAL_ERROR');
  }

  // go ahead and pull all data for this topic
  $sql = "SELECT u.username, u.user_id, u.user_posts, u.user_from, u.user_website, u.user_email, u.user_regdate, u.user_msnm, u.user_viewemail, u.user_rank, u.user_sig, u.user_sig_bbcode_uid, u.user_avatar, u.user_avatar_type, u.user_allowavatar, u.user_allowsmile, p.*, pt.post_text, pt.post_subject, pt.bbcode_uid, s.session_start FROM (" . POSTS_TABLE . " p, " . USERS_TABLE . " u, " . POSTS_TEXT_TABLE . " pt) LEFT JOIN " . SESSIONS_TABLE . " s ON s.session_user_id=u.user_id AND s.session_time>=" . quote(time() - 300, false) . " WHERE p.topic_id=" . quote($id) . " AND pt.post_id=p.post_id AND u.user_id=p.poster_id GROUP BY p.post_id ORDER BY p.post_time ASC";

  if (@version_compare($appVersion, '1.4', '>=')) {
    $sql .= " LIMIT $page, $msgsPerPage";
  }

  if (!($result = $db->sql_query($sql))) {
    touchbb_error('INTERNAL_ERROR');
  }

  $postrow = array();
  if ($row = $db->sql_fetchrow($result)) {
    do {
      $postrow[] = $row;
    }
    while ($row = $db->sql_fetchrow($result));
    $db->sql_freeresult($result);
  } else {
     @include($phpbb_root_path . 'includes/functions_admin.' . $phpEx);
     sync('topic', $id);

    touchbb_error('INTERNAL_ERROR');
  }

  // define censored word matches
  $orig_word = array();
  $replacement_word = array();
  obtain_word_list($orig_word, $replacement_word);

  // was a highlight request part of the URI?
  $highlight_match = $highlight = '';
  if (isset($HTTP_GET_VARS['hl'])) {
    // Split words and phrases
    $words = explode(' ', trim(htmlspecialchars($HTTP_GET_VARS['hl'], ENT_COMPAT, 'ISO-8859-1')));

    for ($i = 0; $i < sizeof($words); $i++) {
      if (trim($words[$i]) != '') {
        $highlight_match .= (($highlight_match != '') ? '|' : '') . str_replace('*', '\w*', preg_quote($words[$i], '#'));
      }
    }
    unset($words);

    $highlight = urlencode($HTTP_GET_VARS['hl']);
    $highlight_match = phpbb_rtrim($highlight_match, "\\");
  }

  // update the topic view counter
  if (($postrow[0]['user_id'] != $userdata['user_id'])) {
    $sql = "UPDATE " . TOPICS_TABLE . " SET topic_views=topic_views+1 WHERE topic_id=" . quote($id);
    if (!$db->sql_query($sql)) {
      touchbb_error('INTERNAL_ERROR');
    }
  }

  // loop through posts and display them
  $total_posts = count($postrow);
  for ($i=0; $i<$total_posts; $i++) {
    $poster_id = $postrow[$i]['user_id'];
    $poster = ($poster_id == ANONYMOUS)?$lang['Guest']:$postrow[$i]['username'];
    $post_date = create_date($board_config['default_dateformat'], $postrow[$i]['post_time'], $board_config['board_timezone']);
    $message = $postrow[$i]['post_text'];
    $bbcode_uid = $postrow[$i]['bbcode_uid'];

    if ($postrow[$i]['user_id']) {
      $user_sig = ($postrow[$i]['enable_sig'] && $postrow[$i]['user_sig'] != '' && $board_config['allow_sig']) ? $postrow[$i]['user_sig'] : '';
      $user_sig_bbcode_uid = $postrow[$i]['user_sig_bbcode_uid'];

      // If the board has HTML off but the post has HTML on then we process it, else leave it alone
      if (!$board_config['allow_html'] || !$userdata['user_allowhtml']) {
        if ($user_sig != '') {
          $user_sig = preg_replace('#(<)([\/]?.*?)(>)#is', "&lt;\\2&gt;", $user_sig);
        }

        if ($postrow[$i]['enable_html']) {
          $message = preg_replace('#(<)([\/]?.*?)(>)#is', "&lt;\\2&gt;", $message);
        }
      }

      // parse message and/or sig for BBCode if reqd
      if ($user_sig && $user_sig_bbcode_uid) {
        $user_sig = ($board_config['allow_bbcode'])?clean($user_sig, $user_sig_bbcode_uid):preg_replace("/\:$user_sig_bbcode_uid/si", '', $user_sig);
      }

      if ($user_sig) {
        $user_sig = make_clickable($user_sig);
      }

      $message = make_clickable($message);

      // resize images on device
      $user_sig = str_replace('<img ', '<img onload="resizeIMG(this);" ', $user_sig);
      $message = str_replace('<img ', '<img onload="resizeIMG(this);" ', $message);

      // highlight active words (primarily for search)
      if ($highlight_match) {
        $message = preg_replace('#(?!<.*)(?<!\w)(' . $highlight_match . ')(?!\w|[^<>]*>)#i', '<b style="color:#' . $theme['fontcolor3'] . '">\1</b>', $message);
      }

      // replace naughty words
      if (count($orig_word)) {
        if ($user_sig != '') {
          $user_sig = str_replace('\"', '"', substr(@preg_replace('#(\>(((?>([^><]+|(?R)))*)\<))#se', "@preg_replace(\$orig_word, \$replacement_word, '\\0')", '>' . $user_sig . '<'), 1, -1));
        }

        $message = str_replace('\"', '"', substr(@preg_replace('#(\>(((?>([^><]+|(?R)))*)\<))#se', "@preg_replace(\$orig_word, \$replacement_word, '\\0')", '>' . $message . '<'), 1, -1));
      }

      // replace newlines (we use this rather than nl2br because till recently it wasn't XHTML compliant)
      if ($user_sig != '') {
        $user_sig = '<hr>' . $user_sig;
      }
    }

    if (!$postrow[$i]['user_id']) {
      $user_sig = '';
    }

    if ($showUserSig && $user_sig) {
      $message .= $user_sig;
    }

    if ($output) {
      $output .= $delims[0];
    }

    if (function_exists('display_post_attachments') && $postrow[$i]['post_attachment']) {
      $sql = "SELECT physical_filename FROM " . ATTACHMENTS_TABLE . "," . ATTACHMENTS_DESC_TABLE . " WHERE mimetype LIKE '%image/%' AND " . ATTACHMENTS_TABLE . ".attach_id=" . ATTACHMENTS_DESC_TABLE . ".attach_id AND post_id=" . quote($postrow[$i]['post_id']);
      if (!($result = $db->sql_query($sql))) {
        touchbb_error('INTERNAL_ERROR');
      }

      $cntr = 0;
      while ($row = $db->sql_fetchrow($result)) {
        if (!$cntr++) {
          $message .= "<br><b>Attachments:</b><br>";
        }

        $message .= '[img]http://' . $board_config['server_name'] . (($nuked)?'/':$board_config['script_path']) . $attach_config['upload_dir'] . '/' . $row['physical_filename'] . '[/img]<br>';
      }

      $db->sql_freeresult($result);
    }

    // display header ads
    if (!$i) {
      if ($tbbSettings['topic_header_ads']) {
        $imgs = $urls = array();
        if ($tbbSettings['topic_header_ads']['random']) {
          $pos = rand(0, count($tbbSettings['topic_header_ads']['imgs']) - 1);
          $imgs[] = $tbbSettings['topic_header_ads']['imgs'][$pos];
          $urls[] = $tbbSettings['topic_header_ads']['urls'][$pos];
        } else {
          $imgs = array_reverse($tbbSettings['topic_header_ads']['imgs']);
          $urls = array_reverse($tbbSettings['topic_header_ads']['urls']);
        }

        for ($adc=0;$adc<count($imgs);$adc++) {
          $message = '<a href="' . $urls[$adc] . '"><img src="' . $imgs[$adc] . '" onload="resizeIMG(this);"></a><br><br>' . $message;
        }
      }
    }

    // display footer ads
    if ($i == $total_posts - 1 /*&& $i*/) {
      if ($tbbSettings['topic_footer_ads']) {
        $imgs = $urls = array();
        if ($tbbSettings['topic_footer_ads']['random']) {
          $pos = rand(0, count($tbbSettings['topic_footer_ads']['imgs']) - 1);
          $imgs[] = $tbbSettings['topic_footer_ads']['imgs'][$pos];
          $urls[] = $tbbSettings['topic_footer_ads']['urls'][$pos];
        } else {
          $imgs = $tbbSettings['topic_footer_ads']['imgs'];
          $urls = $tbbSettings['topic_footer_ads']['urls'];
        }

        for ($adc=0;$adc<count($imgs);$adc++) {
          $message .= '<br><br><a href="' . $urls[$adc] . '"><img src="' . $imgs[$adc] . '" onload="resizeIMG(this);"></a>';
        }
      }
    }

    $output .= clean($postrow[$i]['post_id']) . $delims[1];
    $output .= clean($poster) . $delims[1];
    $output .= clean($message, $bbcode_uid) . $delims[1];
    $output .= clean('') . $delims[1]; // empty for now
    $output .= clean($post_date);
  }
}

// ***************************
// ********** FORUM **********
// ***************************
if ($get == 'forum') {
  // check if the user has actually sent a forum ID with his/her request, if not give them a nice error page
  if (!empty($id)) {
    $sql = "SELECT * FROM " . FORUMS_TABLE . " WHERE forum_id=" . quote($id);
    if (!($result = $db->sql_query($sql))) {
      touchbb_error('INTERNAL_ERROR');
    }
  } else {
    touchbb_error('INTERNAL_ERROR');
  }

  // if the query doesn't return any rows this isn't a valid forum. Inform the user
  if (!($forum_row = $db->sql_fetchrow($result))) {
    touchbb_error('INTERNAL_ERROR');
  }

  // start auth check
  $is_auth = auth(AUTH_ALL, $id, $userdata, $forum_row);

  if (!$is_auth['auth_read'] || !$is_auth['auth_view']) {
    // the user is not authed to read this forum ...
    touchbb_error('INTERNAL_ERROR');
  }

  // grab all the basic data (all topics except announcements) for this forum
  $sql = "SELECT t.*, u.username, u.user_id, u2.username as user2, u2.user_id as id2, p.post_username, p2.post_username AS post_username2, p2.post_time FROM " . TOPICS_TABLE . " t, " . USERS_TABLE . " u, " . POSTS_TABLE . " p, " . POSTS_TABLE . " p2, " . USERS_TABLE . " u2 WHERE t.forum_id=" . quote($id) . " AND t.topic_poster=u.user_id AND p.post_id=t.topic_first_post_id AND p2.post_id=t.topic_last_post_id AND u2.user_id=p2.poster_id ORDER BY t.topic_type DESC, t.topic_last_post_id DESC LIMIT " . quote($page, false) . ',' . quote($msgsPerPage, false);
  if (!($result = $db->sql_query($sql))) {
    touchbb_error('INTERNAL_ERROR');
  }

  while ($row = $db->sql_fetchrow($result)) {
    $topic_rowset[] = $row;
  }

  $db->sql_freeresult($result);

  // define censored word matches
  $orig_word = array();
  $replacement_word = array();
  obtain_word_list($orig_word, $replacement_word);

  // okay, lets dump out the page ...
  for ($i = 0; $i < count($topic_rowset); $i++) {
    $topic_id = $topic_rowset[$i]['topic_id'];
    $topic_title = (count($orig_word)) ? preg_replace($orig_word, $replacement_word, $topic_rowset[$i]['topic_title']) : $topic_rowset[$i]['topic_title'];
    $replies = $topic_rowset[$i]['topic_replies'];
    $topic_type = $topic_rowset[$i]['topic_type'];

    if ($topic_type == POST_ANNOUNCE) {
      $topic_type = ($newProtocol)?3:$strings[10];
    } else if ($topic_type == POST_STICKY) {
      $topic_type = ($newProtocol)?2:$strings[9];
    } else {
      $topic_type = ($newProtocol)?1:$strings[8];
    }

    if ($topic_rowset[$i]['topic_status'] == TOPIC_MOVED) {
      $topic_id = $topic_rowset[$i]['topic_moved_id'];
    }

    $topic_author = ($topic_rowset[$i]['user_id'] != ANONYMOUS) ? $topic_rowset[$i]['username'] : (($topic_rowset[$i]['post_username'] != '') ? $topic_rowset[$i]['post_username'] : $lang['Guest']);
    $first_post_time = create_date($board_config['default_dateformat'], $topic_rowset[$i]['topic_time'], $board_config['board_timezone']);
    $last_post_time = create_date($board_config['default_dateformat'], $topic_rowset[$i]['post_time'], $board_config['board_timezone']);
    $last_post_author = ($topic_rowset[$i]['id2'] == ANONYMOUS) ? (($topic_rowset[$i]['post_username2'] != '') ? $topic_rowset[$i]['post_username2'] . ' ' : $lang['Guest'] . ' ') : ''. $topic_rowset[$i]['user2'];

    $views = $topic_rowset[$i]['topic_views'];

    if ($output) {
      $output .= $delims[0];
    }

    // tell the application that this is a topic
    $output .= clean(1) . $delims[1];

    $output .= clean($topic_id) . $delims[1];

    if ($oldCategory != $topic_type) {
      $output .= clean($topic_type) . $delims[1];
    } else {
      $output .= $delims[1];
    }

    $output .= html_to_straight_quotes($topic_title) . $delims[1];

    if ($newProtocol) {
      if ($replies > 0) {
        $output .= clean($last_post_author);
      } else {
        $output .= clean($topic_author);
      }
    } else {
      if ($replies > 0) {
        $output .= clean(sprintf($strings[0], $last_post_author, $last_post_time));
      } else {
        $output .= clean(sprintf($strings[3], $topic_author, $first_post_time));
      }
    }

    $output .= $delims[1];

    if ($newProtocol) {
      $output .= clean($views);
    } else {
      if ($views > 0 || $replies > 0) {
        $output .= clean(sprintf($strings[5], $replies, ($replies!=1)?'ies':'y', $views, ($views!=1)?'s':''));
      } else {
        $output .= clean($strings[4]);
      }
    }

    $output .= $delims[1];

    $output .= (($userdata['session_logged_in'] && $topic_rowset[$i]['post_time'] > $userdata['user_lastvisit'])?1:0) . $delims[1];
    $output .= ($replies + 1);

    if ($newProtocol) {
      $output .= $delims[1];

      if ($replies > 0) {
        $output .= clean($last_post_time);
      } else {
        $output .= clean($first_post_time);
      }
    }

    // save old topic id so we don't waste bandwidth repeating it
    $oldCategory = $topic_type;
  }
}

// ***************************
// ******* FORUM INDEX *******
// ***************************
if ($get == 'index') {
  $sql = "SELECT c.cat_id, c.cat_title, c.cat_order FROM " . CATEGORIES_TABLE . " c ORDER BY c.cat_order";
  if (!($result = $db->sql_query($sql))) {
    touchbb_error('INTERNAL_ERROR');
  }

  $category_rows = array();
  while ($row = $db->sql_fetchrow($result)) {
    $category_rows[] = $row;
  }
  $db->sql_freeresult($result);

  if ($total_categories = count($category_rows)) {
    $sql = "SELECT f.*, p.post_time, p.post_username, u.username, u.user_id, p.topic_id, t.topic_title, t.topic_replies FROM ((" . FORUMS_TABLE . " f LEFT JOIN " . POSTS_TABLE . " p ON p.post_id = f.forum_last_post_id) LEFT JOIN " . USERS_TABLE . " u ON u.user_id=p.poster_id LEFT JOIN " . TOPICS_TABLE . " t ON p.topic_id=t.topic_id) ORDER BY f.cat_id, f.forum_order";
    if (!($result = $db->sql_query($sql))) {
      touchbb_error('INTERNAL_ERROR');
    }

    $forum_data = array();
    while ($row = $db->sql_fetchrow($result)) {
      $forum_data[] = $row;
    }

    $db->sql_freeresult($result);

    if (!($total_forums = count($forum_data))) {
      touchbb_error('INTERNAL_ERROR');
    }

    // find which forums are visible for this user
    $is_auth_ary = auth(AUTH_VIEW, AUTH_LIST_ALL, $userdata, $forum_data);

    // let's decide which categories we should display
    $display_categories = array();

    for ($i = 0; $i < $total_forums; $i++) {
      if ($is_auth_ary[$forum_data[$i]['forum_id']]['auth_view']) {
        $display_categories[$forum_data[$i]['cat_id']] = true;
      }
    }

    for ($i = 0; $i < $total_categories; $i++) {
      $cat_id = $category_rows[$i]['cat_id'];

      // yes, we should, so first dump out the category title, then, if appropriate the forum list
      if (isset($display_categories[$cat_id]) && $display_categories[$cat_id]) {
        for ($j = 0; $j < $total_forums; $j++) {
          if ($forum_data[$j]['cat_id'] == $cat_id) {
            $forum_id = $forum_data[$j]['forum_id'];

            if ($is_auth_ary[$forum_id]['auth_view']) {
              $posts = $forum_data[$j]['forum_posts'];
              $topics = $forum_data[$j]['forum_topics'];
              $post_time = create_date($board_config['default_dateformat'], $forum_data[$j]['post_time'], $board_config['board_timezone']);

              if ($output) {
                $output .= $delims[0];
              }

              $output .= clean($forum_data[$j]['forum_id']) . $delims[1];

              if ($oldCategory != $category_rows[$i]['cat_title']) {
                $output .= html_to_straight_quotes($category_rows[$i]['cat_title']) . $delims[1];
              } else {
                $output .= $delims[1];
              }

              $output .= html_to_straight_quotes($forum_data[$j]['forum_name']) . $delims[1];

              if ($newProtocol) {
                $output .= clean($forum_data[$j]['username']) . $delims[1];
                $output .= clean($posts);
              } else {
                if ($posts > 0 && $topics > 0) {
                  $output .= clean(sprintf($strings[0], $forum_data[$j]['username'], $post_time)) . $delims[1];
                  $output .= clean(sprintf($strings[1], $posts, ($posts!=1)?'s':'', $topics, ($topics!=1)?'s':''));
                } else {
                  $output .= clean($strings[2]) . $delims[1];
                  $output .= '';
                }
              }

              if ($newProtocol) {
                $output .= $delims[1];

                $output .= clean($post_time) . $delims[1];
                $output .= clean($topics) . $delims[1];
              }

              $oldCategory = $category_rows[$i]['cat_title'];
            }
          }
        }
      }
    } // for ... categories
  } else { // if ... total_categories
    // no forums
  }
}

// ***************************
// ********* VALIDATE ********
// ***************************
if ($get == 'status') {
  $output .= 'OK' . $delims[1];
  $output .= clean($board_config['sitename']) . $delims[1];
  $output .= clean($msgsPerPage) . $delims[1];
  $output .= ($newProtocol)?2:1;

  // does the forum owner want to add their forum to the public directory?
  if ($addThisForumToDirectory && function_exists('get_db_stat')) {
    // get the last time stamp we contacted the server
    $lastUpdated = @filemtime(__FILE__);

    // only run if we have a valid date returned
    if ($lastUpdated) {
      // send the details every 5 days
      if (($lastUpdated + (86400 * 5)) < time()) {
        // notify the server only if we can update the timestamp
        if (@touch(__FILE__)) {
          // send the details
          $fp = @fopen('http://www.messageforums.net/touchbb.php?get=activeforum&name=' . urlencode($board_config['sitename']) . '&forum=' . urlencode($board_config['server_name'] . $board_config['script_path']) . '&posts=' . urlencode(get_db_stat('postcount')) . '&users=' . urlencode(get_db_stat('usercount')) . '&topics=' . urlencode(get_db_stat('topiccount')), "r");

          if ($fp) {
            @fclose($fp);
          }
        }
      }
    }
  }

  // does the forum owner want to alert the user about TouchBB?
  if ($promptUsersAboutTouchBB) {
    $styles = array($board_config['default_style']);

    if (!$board_config['override_user_style']) {
      $sql = "SELECT user_style FROM " . USERS_TABLE . " WHERE user_style!=" . quote($board_config['default_style']);
      if (!($result = $db->sql_query($sql))) {
        touchbb_error('INTERNAL_ERROR');
      }

      while ($row = $db->sql_fetchrow($result)) {
        if ($row['user_style'] && !@in_array($row['user_style'], $styles)) {
          $styles[] = $row['user_style'];
        }
      }

      $db->sql_freeresult($result);
    }

    if ($styles) {
      // use random names for cookie and values
      $cookieName = randomString(rand(4, 6));
      $cookieAsked = randomString(rand(4, 6));
      $aProds = array('iPhone', 'iPod');

      // assemble the footer
      $header = $footer = "<!-- TouchBB Alert -->\n<script type=\"text/javascript\">\nvar aProds=new Array('" . implode("','", $aProds) . "');for(i=0;i<" . (count($aProds)) . ";i++){if(navigator.userAgent.indexOf(aProds[i]+';')!=-1){";
      $footer .= "var cValue='';if(document.cookie.length>0){cStart=document.cookie.indexOf('$cookieName=');if(cStart!=-1){cStart+=" . (strlen($cookieName) + 1) . ";cEnd=document.cookie.indexOf(';',cStart);if(cEnd==-1)cEnd=document.cookie.length;cValue=document.cookie.substring(cStart,cEnd)}}if(cValue!='$cookieAsked'){var exdate=new Date();exdate.setDate(exdate.getDate()+3);document.cookie='$cookieName=$cookieAsked;expires='+exdate.toUTCString();if(confirm('Abbiamo rilevato che stai usando un '+aProds[i]+'. Vuoi provare un\'applicazione che ti permetta di accedere pi\xfa agevolmente al forum?'))location.href='" . $appURL . "';}break}}";
      $footer .= "\n</script>\n";

      // loop through themes to change
      for ($i=0;$i<count($styles);$i++) {
        $sql = 'SELECT template_name FROM ' . THEMES_TABLE . " WHERE themes_id=" . quote($styles[$i]);
        $result = $db->sql_query($sql);
        $row = $db->sql_fetchrow($result);
        $db->sql_freeresult($result);

        // see if this theme has a standard footer file
        $file = $phpbb_root_path . 'templates/' . $row['template_name'] . '/overall_footer.tpl';
        if (@is_file($file)) {
          // read cached footer file
          $fp = @fopen($file, "r");
          $contents = @fread($fp, @filesize($file));
          @fclose($fp);

          // does this footer have our prompt?
          if (!substr_count($contents, $header)) {
            // add our prompt to the footer
            $contents = $footer . $contents;

            // save cache file
            $fp = @fopen($file, 'w');
            @fwrite($fp, $contents);
            @fclose($fp);
          }
        }
      }
    }
  }
}

// has the script been run with parameters?
if ($get || $search || ($username && $password)) {
  // tell TouchBB info about this board and user
  $header = '';

  // send logged in status
  $header .= clean(($userdata['session_logged_in'])?1:0) . $delims[1];

  // send new private message count
  $header .= clean($userdata['user_unread_privmsg']);

  // insert this into the output
  $output = $header . $delims[2] . $output;

  // send the output to TouchBB
  echo $appName;

  // send delims with status
  if ($get == 'status') {
    $output = count($delims) . implode($delims, '') . $output;
  }

  // make sure we are UTF8 compliant
  $output = utf8_encode($output);

  echo $output . $delims[1] . md5($output);
} else {
  displayStatus();
}

// ***************************
// ******** FUNCTIONS ********
// ***************************
function clean($str, $uid = '') {
  global $db, $board_config, $user, $phpbb_root_path, $delims, $smilies, $nuked;

  $str = stripslashes(convert_smart_quotes($str, $plain));
  $str = replace_all("\r\n", "\n", $str);

  if ($uid) {
    $str = str_replace("\n", '<br>', $str);

    if ($board_config['allow_bbcode']) {
      if ($board_config['allow_smilies']) {
        if (!$smilies) {
          $sql = 'SELECT * FROM ' . SMILIES_TABLE;
          if (!$result = $db->sql_query($sql)) {
            touchbb_error('INTERNAL_ERROR');
          }

          $smilies = $db->sql_fetchrowset($result);

          if (count($smilies)) {
            usort($smilies, 'touchbb_sort');
          }
        }

        // convert bbcode smilies to img sources, checking image width to see if it will fit on the phone or not
        $orig = $repl = array();

        for ($i=0;$i<count($smilies);$i++) {
          $resize = true;
          $local = $phpbb_root_path . (($nuked)?'/':$board_config['script_path']) . $board_config['smilies_path'] . '/' . $smilies[$i]['smile_url'];
          if ($imagedata = @getimagesize($local)) {
            if ($imagedata[0] < 320) {
              $resize = false;
            }
          }

          $orig[] = "/(?<=.\W|\W.|^\W)" . preg_quote($smilies[$i]['code'], "/") . "(?=.\W|\W.|\W$)/";
          $repl[] = '<img src="http://' . $board_config['server_name'] . (($nuked)?'/':$board_config['script_path']) . $board_config['smilies_path'] . '/' . $smilies[$i]['smile_url'] . '"' . (($resize)?' onload="resizeIMG(this);"':'') . ' alt="' . $smilies[$i]['emoticon'] . '" border="0" />';
        }

        if (count($orig)) {
          $str = preg_replace($orig, $repl, ' ' . $str . ' ');
        }
      }

      // our list of bbcodes to change
      $bbcode = array('/\[i(|:' . $uid . ')\](.*?)\[\/i(|:' . $uid . ')\]/is' => '<i>$2</i>',
                      '/\[b(|:' . $uid . ')\](.*?)\[\/b(|:' . $uid . ')\]/is' => '<b>$2</b>',
                      '/\[u(|:' . $uid . ')\](.*?)\[\/u(|:' . $uid . ')\]/is' => '<u>$2</u>',
                      '/\[color=(.*?)(|:' . $uid . ')\](.*?)\[\/color(|:' . $uid . ')\]/is' => '<font color="$1">$3</font>',
                      '/\[size=(.*?)(|:' . $uid . ')\](.*?)\[\/size(|:' . $uid . ')\]/is' => '<span style="font-size:$1px;line-height:normal">$3</span>',
                      '/\[center(|:' . $uid . ')\](.*?)\[\/center(|:' . $uid . ')\]/is' => '<center>$2</center>',
                      '/\[hr(|:' . $uid . ')\](.*?)\[\/hr(|:' . $uid . ')\]/is' => '<hr>',
                      '/\[url(|:' . $uid . ')\](.*?)\[\/url(|:' . $uid . ')\]/is' => '<a href="$2">$2</a>',
                      '/\[url=(.*?)(|:' . $uid . ')\](.*?)\[\/url(|:' . $uid . ')\]/is' => '<a href="$1">$3</a>',
                      '/<!-- m --><a class="postlink" href="(.*?)">(.*?)<\/a><!-- m -->/is' => '<a href="$1">$2</a>',
                      '/<!-- w --><a class="postlink" href="(.*?)">(.*?)<\/a><!-- w -->/is' => '<a href="$1">$2</a>',
                      '/\[quote=(.*?)(|:' . $uid . ')\](.*?)\[\/quote(|:' . $uid . ')\]/is' => '<blockquote><cite>$1 wrote:</cite>$3</blockquote>',
                      '/\[quote(|:' . $uid . ')="(.*?)"\](.*?)\[\/quote(|:' . $uid . ')\]/is' => '<blockquote><cite>$2 wrote:</cite>$3</blockquote>',
                      '/\[quote(|:' . $uid . ')\](.*?)\[\/quote(|:' . $uid . ')\]/is' => '<blockquote>$2</blockquote>',
                      '/\[img(|:' . $uid . ')\](.*?)\[\/img(|:' . $uid . ')\]/is' => '<img src="$2" onload="resizeIMG(this);">',
                      '/\[code(|:' . $uid . ')\](.*?)\[\/code(|:' . $uid . ')\]/is' => '<dl class="codebox"><dt>Code:</dt><dd><code>$2</code></dd></dl>',
                      /*					
                      '/\[\*(|:' . $uid . ')\](.*?)\[\/\*:m(|:' . $uid . ')\]/is' => '&bull;$2',
                      '/\[\*(|:' . $uid . ')\]/is' => '&bull;$2',
                      '/\[list(|:' . $uid . ')\](.*?)\[\/list:u(|:' . $uid . ')\]([\n]|)/is' => '$2',
					  */
                      '/\[list(|:' . $uid . ')\](.*?)\[\/list:u(|:' . $uid . ')\]([\n]|)/is' => '<ul><li>&bull;&#160;$2</li></ul>',
                      '/\[list=(1:' . $uid . ')\](.*?)\[\/list:o(|:' . $uid . ')\]([\n]|)/is' => '<ul style="list-style-type:decimal"><li>&bull;&#160;$2</li></ul>',
                      '/\[list=(a:' . $uid . ')\](.*?)\[\/list:o(|:' . $uid . ')\]([\n]|)/is' => '<ul style="list-style-type:lower-alpha"><li>&bull;&#160;$2</li></ul>',
                      '/\[\*(|:' . $uid . ')\](.*?)\[\/\*:m(|:' . $uid . ')\]/is' => '$2</li><li>&bull;&#160;',
                      '/\[\*(|:' . $uid . ')\]/is' => '$2</li><li>&bull;&#160;',
                      '/<li>&bull\;&#160\;<\/li><li>/is' => '<li>',
                      '/\[albumimg(|:' . $uid . ')\](.*?)\[\/albumimg(|:' . $uid . ')\]/is' => '<img src="http://' . $board_config['server_name'] . $board_config['script_path'] . '/album_thumbnail.php?pic_id=$2" alt="Image $2" title="Image $2" border="0" onload="resizeIMG(this);">',
                      '/\[youtube(|:' . $uid . ')\](.*?)v=(.*?)\[\/youtube(|:' . $uid . ')\]/is' => '<div align="center"><object width="300" height="243"><param name="movie" value="http://www.youtube.com/v/$3&f=gdata_videos&c=ytapi-my-clientID&d=nGF83uyVrg8eD4rfEkk22mDOl3qUImVMV6ramM"></param><param name="wmode" value="transparent"></param><embed src="http://www.youtube.com/v/$3&f=gdata_videos&c=ytapi-my-clientID&d=nGF83uyVrg8eD4rfEkk22mDOl3qUImVMV6ramM" type="application/x-shockwave-flash" wmode="transparent" width="300" height="243"></embed></object></div>',
                      '/\[stream(|:' . $uid . ')\](.*?)\[\/stream(|:' . $uid . ')\]/is' => '<a href="$2">$2</a>',
                      '/\[align=(.*?)(|:' . $uid . ')\](.*?)\[\/align(|:' . $uid . ')\]/is' => '<div align="$1">$3</div>',
                      '/\[siteimg(|:' . $uid . ')\](.*?)\[\/siteimg(|:' . $uid . ')\]/is' => '<img src="http://' . $board_config['server_name'] . $board_config['script_path'] . '/album_picm.php?pic_id=$2" alt="Image $2" title="Image $2" border="0" onload="resizeIMG(this);">',
                      '/\[s(|:' . $uid . ')\](.*?)\[\/s(|:' . $uid . ')\]/is' => '<span><s>$2</s></span>',
                      '/\[font=(.*?)(|:' . $uid . ')\](.*?)\[\/font(|:' . $uid . ')\]/is' => '<font face=$1>$3</font>',
      );

      // make the bbcode changes
      while (list($regex, $html) = each($bbcode)) {
        while (preg_match($regex, $str)) {
          $str = preg_replace($regex, $html, $str);
        }
      }
    }
  } else {
    $str = replace_all("\n", ' ', $str);
    $str = replace_all('<br>', ' ', $str);
    $str = html_entity_decode($str, ENT_COMPAT, 'ISO-8859-1');
  }

  $str = str_replace($delims, '', $str);
  $str = preg_replace('/\s\s+/', ' ', $str);
  $str = trim($str);

  return $str;
}

function update_pm_counts() {
  global $userdata, $db;

  // see how many unread PMs we have
  $result = $db->sql_query("SELECT COUNT(privmsgs_id) AS inbox_items FROM " . PRIVMSGS_TABLE . " WHERE (privmsgs_type=" . PRIVMSGS_NEW_MAIL . " OR privmsgs_type=" . PRIVMSGS_UNREAD_MAIL . ") AND privmsgs_to_userid=" . quote($userdata['user_id']));
  $row = $db->sql_fetchrow($result);

  // update unread status
  $sql = "UPDATE " . USERS_TABLE . " SET user_unread_privmsg=" . quote($row['inbox_items']) . " WHERE user_id=" . quote($userdata['user_id']);
  if (!$db->sql_query($sql)) {
    touchbb_error('INTERNAL_ERROR');
  }

  // update the status now before we exit
  $userdata['user_unread_privmsg'] = $row['inbox_items'];
}

function touchbb_sort($a, $b) {
  if (strlen($a['code']) == strlen($b['code'])) {
    return 0;
  }

  return (strlen($a['code']) > strlen($b['code']))?-1:1;
}

function replace_all($old, $new, $str) {
  while (substr_count($str, $old)) {
    $str = str_replace($old, $new, $str);
  }

  return $str;
}

function displayStatus() {
  global $appName, $scriptVersion, $board_config, $phpEx, $phpbb_root_path, $debugMode, $appURL;

  // script is working variable
  $works = true;

  // check for existance of these include files
  if (!@include($phpbb_root_path . 'includes/functions_admin.' . $phpEx)) { $works = false; }
  if (!@include($phpbb_root_path . 'includes/functions_post.' . $phpEx)) { $works = false; };
  if (!@include($phpbb_root_path . 'includes/bbcode.' . $phpEx)) { $works = false; };

  // only continue if we didn't receive an error above
  if ($works) {
    // list of functions to check for
    $functions[] = 'submit_post';
    $functions[] = 'preg_replace';
    $functions[] = 'replace_all';
    $functions[] = 'array_keys';
    $functions[] = 'array_values';
    $functions[] = 'html_entity_decode';
    $functions[] = 'obtain_word_list';
    $functions[] = 'make_clickable';
    $functions[] = 'dirname';

    // loop through functions to check
    for ($i=0;$i<count($functions);$i++) {
      if (!function_exists($functions[$i])) {
        $works = false;
      }
    }
  }

  // display status
  $fmfURL = "http://support.messageforums.net";
  echo "<html><head><title>TouchBB Status</title></head><body>$appName v$scriptVersion is ";
  echo ($works)?"<span style=\"color:green;font-weight:bold;\">installed</span>":"<span style=\"color:red;font-weight:bold;\">not working</span>";
  echo ' for ' . (($board_config['sitename'])?"<b>${board_config['sitename']}</b>":'your forum') . ' running phpBB v' . $board_config['version'] . '.<br>';
  echo ($works)?"You may download <a href=\"$appURL\">$appName</a> from the App Store.":"Please visit <a href=\"$fmfURL\">$fmfURL</a> for support.";

  // this is only shown when debug mode is on to help the TouchBB team
  if ($debugMode) {
    echo '<br><br><font color="white">';
    echo 'Script Path: ' . $board_config['script_path'] . '<br>';
    echo 'Cookie Domain: ' . $board_config['cookie_domain'] . '<br>';
    echo 'Cookie Name: ' . $board_config['cookie_name'] . '<br>';
    echo 'Cookie Path: ' . $board_config['cookie_path'] . '<br>';
    echo 'Script Writable: ' . ((is_writable(__FILE__))?'Yes':'No') . '<br>';
    echo '</font>';
  }

  echo "</body></html>";
  exit;
}

function quote($str, $quotes = true) {
  if (get_magic_quotes_gpc()) {
    $str = stripslashes($str);
  }

  $str = mysql_real_escape_string($str);

  if ($quotes) {
    $str = "'$str'";
  }

  return $str;
}

function touchbb_error($error = '') {
  global $debugMode;

  if ($debugMode) {
    echo 'ERROR:' . $error;
  }

  exit;
}

function randomString($len = 5) {
  $output = '';

  for ($i=0;$i<$len;$i++) {
    $d = rand(1, 30) % 2;
    $output .= ($d || !$i) ? chr(rand(65, 90)) : chr(rand(48, 57));
  }

  return $output;
}

function touchbb_clean_username($username) {
  $username = substr(htmlspecialchars(str_replace("\'", "'", trim($username)), ENT_COMPAT, 'ISO-8859-1'), 0, 25);
  $username = rtrim($username, "\\");
  $username = str_replace("'", "\'", $username);

  return $username;
}

function convert_smart_quotes($string) 
{ 
	$search = array(chr(133),
					chr(145), 
					chr(146), 
					chr(147), 
					chr(148),
					chr(150), 
					chr(151));  
 
	$replace = array('&hellip;',
					 '&lsquo;', 
					 '&rsquo;', 
					 '&ldquo;', 
					 '&rdquo;',
					 '&ndash;',
					 '&mdash;');

	return str_replace($search, $replace, $string); 
}

function html_to_straight_quotes($string) 
{ 
	$search = array('/&hellip\;/',
					'/&lsquo\;/', 
					'/&rsquo\;/', 
					'/&ldquo\;/', 
					'/&rdquo\;/',
					'/&ndash\;/',
					'/&mdash\;/');  
	$replace = array('...',
				     '\'', 
				     '\'', 
				     '"', 
				     '"',
				     '-',
				     '-');
 
	return strip_tags(preg_replace($search, $replace, clean($string)));
}

$li_decimal = 0;
$li_alpha = 0;
function increment_li($type = "decimal", $reset = true)
{
	global $li_decimal, $li_alpha;
	if($type == "decimal")
	{
		if($reset) $li_decimal = 0;
		else  $li_decimal++;
		$return = $li_decimal;
	}
	elseif($type == "alpha")
	{
		if($reset) $li_alpha = 0;
		else  $li_alpha++;
		$return = $li_alpha;
	}
	return $return;
}
 
?>