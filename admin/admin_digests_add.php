<?php
/***************************************************************************
 *                         admin_digests_add.php
 *                         ---------------------
 *   begin                : Thurs Apr 19, 2007
 *   copyright            : (c) Mark D. Hamill
 *   email                : mhamill@computer.org
 *
 *   $Id: admin_forums.php,v 1.40.2.13 2006/03/09 21:55:09 grahamje Exp $
 *
 ***************************************************************************/

/***************************************************************************
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

// Written by Mark D. Hamill, mhamill@computer.org
// This software is designed to work with phpBB Version 2.0.22

define('IN_PHPBB', 1);

if( !empty($setmodules) )
{
	$file = basename(__FILE__);
	$module['Digests']['Digests_Add_Subscribers'] = $file;
	return;
}

//
// Load default header
//
$phpbb_root_path = "./../";
require($phpbb_root_path . 'extension.inc');
require('./pagestart.' . $phpEx);
require($phpbb_root_path . 'includes/digest_functions.' . $phpEx); 
require($phpbb_root_path . 'language/lang_' . $board_config['default_lang'] . '/lang_digests.' . $phpEx);

$start = ( isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$start = ($start < 0) ? 0 : $start;

$settings = get_digest_settings();	// This will retrieve the current global settings
$defaults = get_digest_defaults();

// Convert the default send time into the local timezone
$board_timezone = date('Z')/3600;
$user_timezone = (float) $userdata['user_timezone'];
$offset = $board_timezone - $user_timezone;

if ($HTTP_SERVER_VARS['REQUEST_METHOD'] == 'GET')
{

	$template->set_filenames(array('body' => 'admin/digests_add_body.tpl'));

	switch($dbms)
	{
	
		case 'mysql':
		case 'mysql4':
		
			$sql = 'SELECT count(*) AS digest_count FROM ' . USERS_TABLE . ' LEFT JOIN ' . DIGEST_SUBSCRIPTIONS_TABLE . ' ON ' . USERS_TABLE . '.user_id = ' . DIGEST_SUBSCRIPTIONS_TABLE . '.user_id WHERE ' . DIGEST_SUBSCRIPTIONS_TABLE . '.user_id IS NULL AND ' .  USERS_TABLE . '.user_id <> ' . ANONYMOUS . ' AND ' . USERS_TABLE . '.user_active = 1';
			break;
			
		default:
		
			// Not entirely sure this SQL will work for all non-MySQL DBMSes. If this does not please try to correct SQL and send corrected SQL along with DBMS used to mhamill@computer.org
			$sql = 'SELECT count(*) AS digest_count FROM ' . USERS_TABLE . ' WHERE user_id NOT IN (SELECT user_id FROM ' . DIGEST_SUBSCRIPTIONS_TABLE . ') AND user_id <> ' . ANONYMOUS . ' AND user_active = 1';
			break;
			
	}
		
	if ( !($result = $db->sql_query($sql)))
	{
		message_die(GENERAL_ERROR, 'Could not select from ' . USERS_TABLE . ' table. SQL was '. $sql, '', __LINE__, __FILE__, $sql);
	}
	
	$row = $db->sql_fetchrow($result);
	$digest_count = $row['digest_count'];
	$digest_admin_users_per_page = ($settings['users_per_page'] == 0) ? $digest_count : $settings['users_per_page'];

	$pagination = generate_pagination("admin_digests_add.$phpEx?start=$start", $digest_count, $digest_admin_users_per_page, $start). '&nbsp;';
	if ($settings['users_per_page'] <> 0 && ($digest_count > $settings['users_per_page']))
	{
		$template->assign_block_vars('switch_pagination', array());
	}
		
	$template->assign_vars(array(
		'DIGEST_VERSION' => $settings['version'],
		'L_ADMIN_EXPLAIN' => $lang['digest_admin_add_explain'],
		'L_DIGEST_TYPE_SHORT' => $lang['digest_admin_wanted_short'],
		'L_FORMAT_SHORT' => $lang['digest_admin_format_short'],
		'L_FORUM_SUBSCRIPTIONS' => $lang['digest_admin_forum_subscriptions'],
		'L_GLOBAL_SUBMIT' => $lang['digest_admin_global_submit'],
		'L_GLOBAL_SUBSCRIBE' => $lang['digest_admin_global_subscribe'],
		'L_PAGE_NUMBER' => sprintf($lang['Page_of'], ( floor( $start / $digest_admin_users_per_page ) + 1 ), ceil( $digest_count / $digest_admin_users_per_page )),
		'L_PAGE_TITLE' => $lang['digest_admin_add_interface'],
		'L_PAGINATION' => $pagination,
		'L_PRIVATE_MGS_IN_FEED_SHORT' => $lang['digest_admin_private_messages_in_digest_short'],
		'L_RESET' => $lang['digest_reset_text'],
		'L_SELECT' => $lang['digest_admin_select'],
		'L_SELECT_SUBSCRIBE' => $lang['digest_admin_select_subscribe'],
		'L_SELECT_UNSELECT_ALL' => $lang['digest_admin_select_unselect_all'],
		'L_SEND_HOUR_SHORT' => $lang['digest_admin_hour_to_send_short'],
		'L_SEND_ON_NO_MESSAGES_SHORT' => $lang['digest_admin_send_if_no_msgs_short'],
		'L_SHOW_MINE_SHORT' => $lang['digest_admin_show_messages_short'],
		'L_SHOW_NEW_ONLY_SHORT' => $lang['digest_admin_show_new_only_short'],
		'L_SHOW_TEXT_SHORT' => $lang['digest_admin_excerpt_short'],
		'L_SUBCRIBE_ALL' => $lang['digest_admin_subscribe_all'],
		'L_SUBCRIBE_ALL_EXPLAIN' => $lang['digest_admin_subscribe_all_explain'],
		'L_TEXT_LENGTH_SHORT' => $lang['digest_admin_size_short'],
		'L_UPDATE_DELETE_ALL' => $lang['digest_admin_update_delete_all'],
		'L_USERNAME' => $lang['digest_admin_username'],
		'S_ADD_URL' => append_sid("./admin_digests_add.$phpEx"),
		'U_DIGEST_PAGE_URL' => DIGEST_PAGE_URL));
	
	$sql_limit = ($settings['users_per_page'] == 0) ? '' : "LIMIT $start, " . $settings['users_per_page'];
	
	switch($dbms)
	{
	
		case 'mysql':
		case 'mysql4':
		
			$sql = 'SELECT ' . USERS_TABLE . '.user_id, username, user_level FROM ' . USERS_TABLE . ' LEFT JOIN ' . DIGEST_SUBSCRIPTIONS_TABLE . ' ON ' . USERS_TABLE . '.user_id = ' . DIGEST_SUBSCRIPTIONS_TABLE . '.user_id WHERE ' . DIGEST_SUBSCRIPTIONS_TABLE . '.user_id IS NULL AND ' .  USERS_TABLE . '.user_id <> ' . ANONYMOUS . ' AND ' . USERS_TABLE . '.user_active = 1 ORDER by username ' .  $sql_limit;
			break;
		
		default:
		
			// Not entirely sure this SQL will work for all non-MySQL DBMSes. If this does not please try to correct SQL and send corrected SQL along with DBMS used to mhamill@computer.org
			$sql = 'SELECT user_id, username, user_level FROM ' . USERS_TABLE . ' WHERE user_id NOT IN (SELECT user_id FROM ' . DIGEST_SUBSCRIPTIONS_TABLE . ') AND user_id <> ' . ANONYMOUS . ' AND user_active = 1 ORDER by username ' .  $sql_limit;
			break;

	}	
			
	if ( !($result = $db->sql_query($sql)))
	{
		message_die(GENERAL_ERROR, 'Could not select from ' . DIGEST_SUBSCRIPTIONS_TABLE . ' table. SQL was '. $sql, '', __LINE__, __FILE__, $sql);
	}
	
	$send_hour = (float) $defaults['send_hour'];
	if ($send_hour <> -1) // -1 means a random hour is to be used
	{
		$send_hour = $send_hour - $offset;
		$send_hour = ($send_hour < 0) ? $send_hour + 24 : $send_hour;
		$send_hour = ($send_hour >= 24) ? $send_hour - 24 : $send_hour; 	
	}
		
	while( $row = $db->sql_fetchrow($result) )
	{
	
		// Apply a color to the name, to identify admins and moderators, as in the Main Index
		if ( $row['user_level'] == ADMIN )
		{
			$row['username'] = '<b>' . $row['username'] . '</b>';
			$style_color = ' style="color:#' . $theme['fontcolor3'] . '"';
		}
		else if ( $row['user_level'] == MOD )
		{
			$row['username'] = '<b>' . $row['username'] . '</b>';
			$style_color = ' style="color:#' . $theme['fontcolor2'] . '"';
		}
		else
		{
			$style_color = '';
		}

		$template->assign_block_vars('digestrow', array(
			'10AM_SELECTED' => ($send_hour==10) ? 'selected="selected"' : '',
			'10PM_SELECTED' => ($send_hour==22) ? 'selected="selected"' : '',
			'11AM_SELECTED' => ($send_hour==11) ? 'selected="selected"' : '',
			'11PM_SELECTED' => ($send_hour==23) ? 'selected="selected"' : '', 
			'12PM_SELECTED' => ($send_hour==12) ? 'selected="selected"' : '',
			'1AM_SELECTED' => ($send_hour==1) ? 'selected="selected"' : '',
			'1PM_SELECTED' => ($send_hour==13) ? 'selected="selected"' : '',
			'2AM_SELECTED' => ($send_hour==2) ? 'selected="selected"' : '',
			'2PM_SELECTED' => ($send_hour==14) ? 'selected="selected"' : '',
			'3AM_SELECTED' => ($send_hour==3) ? 'selected="selected"' : '',
			'3PM_SELECTED' => ($send_hour==15) ? 'selected="selected"' : '',
			'4AM_SELECTED' => ($send_hour==4) ? 'selected="selected"' : '',
			'4PM_SELECTED' => ($send_hour==16) ? 'selected="selected"' : '',
			'5AM_SELECTED' => ($send_hour==5) ? 'selected="selected"' : '',
			'5PM_SELECTED' => ($send_hour==17) ? 'selected="selected"' : '',
			'6AM_SELECTED' => ($send_hour==6) ? 'selected="selected"' : '',
			'6PM_SELECTED' => ($send_hour==18) ? 'selected="selected"' : '',
			'7AM_SELECTED' => ($send_hour==7) ? 'selected="selected"' : '',
			'7PM_SELECTED' => ($send_hour==19) ? 'selected="selected"' : '',
			'8AM_SELECTED' => ($send_hour==8) ? 'selected="selected"' : '',
			'8PM_SELECTED' => ($send_hour==20) ? 'selected="selected"' : '',
			'9AM_SELECTED' => ($send_hour==9) ? 'selected="selected"' : '',
			'9PM_SELECTED' => ($send_hour==21) ? 'selected="selected"' : '',
			'50_SELECTED' => (trim($defaults['text_length'])=='50') ? 'selected="selected"' : '',
			'100_SELECTED' => (trim($defaults['text_length'])=='100') ? 'selected="selected"' : '',
			'150_SELECTED' => (trim($defaults['text_length'])=='150') ? 'selected="selected"' : '',
			'300_SELECTED' => (trim($defaults['text_length'])=='300') ? 'selected="selected"' : '',
			'600_SELECTED' => (trim($defaults['text_length'])=='600') ? 'selected="selected"' : '',
			'DAILY_SELECTED' => (trim($defaults['digest_type']) == 'DAY') ? ' selected="selected"' : '',
			'EXCERPT_NO_SELECTED' => (trim($defaults['show_text']) == 'NO') ? ' selected="selected"' : '',
			'EXCERPT_YES_SELECTED' => (trim($defaults['show_text']) == 'YES') ? ' selected="selected"' : '',
			'HTML_ENH_SELECTED' => ($defaults['format'] == 'HTML') ? ' selected="selected"' : '',
			'HTML_PLAIN_SELECTED' => ($defaults['format'] == 'PHTM') ? ' selected="selected"' : '',
			'ID' => $row['user_id'],
			'L_10AM' => $lang['digest_10am'],
			'L_10PM' => $lang['digest_10pm'],
			'L_11AM' => $lang['digest_11am'],
			'L_11PM' => $lang['digest_11pm'],
			'L_12PM' => $lang['digest_12pm'],
			'L_1AM' => $lang['digest_1am'],
			'L_1PM' => $lang['digest_1pm'],
			'L_2AM' => $lang['digest_2am'],
			'L_2PM' => $lang['digest_2pm'],
			'L_3AM' => $lang['digest_3am'],
			'L_3PM' => $lang['digest_3pm'],
			'L_4AM' => $lang['digest_4am'],
			'L_4PM' => $lang['digest_4pm'],
			'L_5AM' => $lang['digest_5am'],
			'L_5PM' => $lang['digest_5pm'],
			'L_6AM' => $lang['digest_6am'],
			'L_6PM' => $lang['digest_6pm'],
			'L_7AM' => $lang['digest_7am'],
			'L_7PM' => $lang['digest_7pm'],
			'L_8AM' => $lang['digest_8am'],
			'L_8PM' => $lang['digest_8pm'],
			'L_9AM' => $lang['digest_9am'],
			'L_9PM' => $lang['digest_9pm'],
			'L_50' => $lang['digest_size_50'],
			'L_100' => $lang['digest_size_100'],
			'L_150' => $lang['digest_size_150'],
			'L_300' => $lang['digest_size_300'],
			'L_600' => $lang['digest_size_600'],
			'L_DAILY' => $lang['digest_daily'],
			'L_HTML_ENH' => $lang['digest_html_enhanced'],
			'L_HTML_PLAIN' => $lang['digest_html_plain'],
			'L_MAX' => $lang['digest_size_max'],
			'L_MIDNIGHT' => $lang['digest_midnight'],
			'L_NO' => $lang['digest_no'],
			'L_RANDOM' => $lang['digest_admin_random_hour'],
			'L_SUBSCRIPTIONS' => $lang['digest_admin_forum_subscriptions_all'],
			'L_TEXT' => $lang['digest_text'],
			'L_WEEKLY' => $lang['digest_weekly'],
			'L_YES' => $lang['digest_yes'],
			'MAX_SELECTED' => (trim($defaults['text_length'])=='32000') ? 'selected="selected"' : '',
			'MIDNIGHT_SELECTED' => ($send_hour==0) ? 'selected="selected"' : '',
			'MINE_NO_SELECTED' => (trim($defaults['show_mine']) == 'NO') ? ' selected="selected"' : '',
			'MINE_YES_SELECTED' => (trim($defaults['show_mine']) == 'YES') ? ' selected="selected"' : '',
			'NEW_NO_SELECTED' => (trim($defaults['new_only']) == 'FALSE') ? ' selected="selected"' : '',
			'NEW_YES_SELECTED' => (trim($defaults['new_only']) == 'TRUE') ? ' selected="selected"' : '',
			'ONLY_IF_MSGS_NO_SELECTED' => (trim($defaults['send_on_no_messages']) == 'NO') ? ' selected="selected"' : '',
			'ONLY_IF_MSGS_YES_SELECTED' => (trim($defaults['send_on_no_messages']) == 'YES') ? ' selected="selected"' : '',
			'PMS_NO_SELECTED' => (trim($defaults['show_pms']) == 'NO') ? ' selected="selected"' : '',
			'PMS_YES_SELECTED' => (trim($defaults['show_pms']) == 'YES') ? ' selected="selected"' : '',
			'RANDOM_SELECTED' => ($send_hour==-1) ? 'selected="selected"' : '',
			'TEXT_SELECTED' => ($defaults['format'] == 'TEXT') ? ' selected="selected"' : '', 
			'USERNAME' => '<a href="' . append_sid($phpbb_root_path . 'profile.php?mode=viewprofile&amp;u=' . $row['user_id']) . "\"$style_color>" . $row['username'] . '</a>',
			'WEEKLY_SELECTED' => (trim($defaults['digest_type']) == 'WEEK') ? ' selected="selected"' : ''));
				
	} // $row = $db->sql_fetchrow($result)

	$template->pparse('body');
	
	include('./page_footer_admin.'.$phpEx);
					
} 

else 
{

	// The user has submitted the form. This logic takes the necessary action to add selected unsubscribed users into the database
	// and gives an appropriate confirmation message.

	if ($HTTP_POST_VARS['add-select'] == "1")	// Selected subscriptions form submitted
	
	{
	
		//$current_user_id = trim(substr($key,$underscore_loc+1));
		$current_user_id = '';
		$users_found = 0;
	
		do
		{
			// Update the current user being processed
			$underscore_loc = strrpos($key,'_');
			if ($underscore_loc == true)
			{
				if (trim(substr($key,$underscore_loc+1)) <> $current_user_id)
				{
					$users_found++;
					$current_user_id = trim(substr($key,$underscore_loc+1));
					// Add the digest subscription
					$action = 'action_' . $current_user_id;
					$type = 'type_' . $current_user_id;
					$format = 'format_' . $current_user_id;
					$excerpt = 'excerpt_' . $current_user_id;
					$mine = 'mine_' . $current_user_id;
					$new = 'new_' . $current_user_id;
					$only_if_msgs = 'only_if_msgs_' . $current_user_id;
					$pms = 'pms_' . $current_user_id;
					$send_hour = 'send_hour_' . $current_user_id;
					if (htmlspecialchars($HTTP_POST_VARS[$send_hour]) == 'R')
					{
						$send_hour = rand(0,23);
					}
					else
					{
						$send_hour = intval($HTTP_POST_VARS[$send_hour])+$offset;
						$send_hour = ($send_hour < 0) ? $send_hour + 24 : $send_hour;
						$send_hour = ($send_hour >= 24) ? $send_hour - 24 : $send_hour; 	
					}
					$text_length = 'text_length_' . $current_user_id;
					$this_username = save_digest_settings($current_user_id, htmlspecialchars($HTTP_POST_VARS[$action]), htmlspecialchars($HTTP_POST_VARS[$type]), htmlspecialchars($HTTP_POST_VARS[$format]), htmlspecialchars($HTTP_POST_VARS[$excerpt]), htmlspecialchars($HTTP_POST_VARS[$mine]), htmlspecialchars($HTTP_POST_VARS[$new]), htmlspecialchars($HTTP_POST_VARS[$only_if_msgs]), htmlspecialchars($HTTP_POST_VARS[$pms]), $send_hour, intval($HTTP_POST_VARS[$text_length]));
				}
			}
		}
		while (list($key, $value) = each($HTTP_POST_VARS)); 
		
		$display_message = ($users_found > 0) ? $lang['digest_admin_post_add_msg'] : $lang['digest_admin_post_msg_none'];

	}
	
	else if ($HTTP_POST_VARS['add-all'] == "1")	// Global Subscriptions form submitted
	{
	
		// This SQL will create a mass digest subscription, creating digests using the digest defaults for EVERY user on the board that is not already a subscriber
		// excepting only administrators.

		switch($dbms)
		{
		
			case 'mysql':
			case 'mysql4':
		
				$sql = 'SELECT ' . USERS_TABLE . '.user_id 
					FROM ' . USERS_TABLE . ' LEFT JOIN ' . DIGEST_SUBSCRIPTIONS_TABLE . ' ON ' . USERS_TABLE . '.user_id = ' . DIGEST_SUBSCRIPTIONS_TABLE . '.user_id WHERE ' . DIGEST_SUBSCRIPTIONS_TABLE . '.user_id IS NULL AND ' .  USERS_TABLE . '.user_id <> ' . ANONYMOUS . ' AND user_level <> ' . ADMIN . ' AND user_active = 1';
				break;
			
			default:
			
				// Not entirely sure this SQL will work for all non-MySQL DBMSes. If this does not please try to correct SQL and send corrected SQL along with DBMS used to mhamill@computer.org
				$sql = 'SELECT user_id
					FROM ' . USERS_TABLE . ' WHERE user_id NOT IN 
					(SELECT user_id FROM ' . DIGEST_SUBSCRIPTIONS_TABLE . ') AND user_id <> ' . ANONYMOUS . ' AND user_level <> ' . ADMIN . ' AND user_active = 1';
				break;
				
		}

		if ( !($result = $db->sql_query($sql)))
		{
			message_die(GENERAL_ERROR, 'Could not select from ' . USERS_TABLE . ' table. SQL was '. $sql, '', __LINE__, __FILE__, $sql);
		}
		
		$insert_count = 0;
		while( $row = $db->sql_fetchrow($result) )
		{
		
			$send_hour = ($defaults['send_hour'] == -1) ? rand(0,23) : $defaults['send_hour'];

			$sql2 = 'INSERT INTO ' . DIGEST_SUBSCRIPTIONS_TABLE . ' (user_id, digest_type, format, show_text, show_mine, new_only, send_on_no_messages, 
				show_pms, send_hour, text_length) 
				VALUES (' . $row['user_id'] . ", '" . $defaults['digest_type'] . "', '" . $defaults['format'] . "', '" . $defaults['show_text'] . "', '" . 
					$defaults['show_mine'] . "', '" . $defaults['new_only'] . "', '" . $defaults['send_on_no_messages'] . "', '" . $defaults['show_pms'] . "', " .
					$send_hour . ", " . $defaults['text_length'] . ')';
					
			if ( !($result2 = $db->sql_query($sql2)))
			{
				message_die(GENERAL_ERROR, 'Could not insert into ' . DIGEST_SUBSCRIPTIONS_TABLE . ' table', '', __LINE__, __FILE__, $sql);
			}
			$insert_count++;
			
		}
		
		$display_message = ($insert_count > 0) ? $lang['digest_admin_add_all_msg'] : $lang['digest_admin_add_all_none_msg'];

	}

	$message = $display_message . '<br /><br />' . 
		sprintf($lang['digest_admin_return_admin_digests_add'], '<a href="' . append_sid("admin_digests_add.$phpEx") . '">', '</a> ') . '<br /><br />' .
		sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");
	message_die(GENERAL_MESSAGE, $message);
		
}

?>