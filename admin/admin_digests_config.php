<?php
/***************************************************************************
 *                         admin_digests_config.php
 *                         ------------------------
 *   begin                : Thurs Apr 19, 2007
 *   copyright            : (c) Mark D. Hamill
 *   email                : mhamill@computer.org
 *
 *   $Id: admin_forums.php,v 1.40.2.13 2006/03/09 21:55:09 grahamje Exp $
 *
 ***************************************************************************/

/***************************************************************************
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

// Written by Mark D. Hamill, mhamill@computer.org
// This software is designed to work with phpBB Version 2.0.22

define('IN_PHPBB', 1);

if( !empty($setmodules) )
{
	$file = basename(__FILE__);
	$module['Digests']['Digests_Configuration'] = $file;
	return;
}

//
// Load default header
//
$phpbb_root_path = "./../";
require($phpbb_root_path . 'extension.inc');
require('./pagestart.' . $phpEx);
require($phpbb_root_path . 'includes/digest_functions.' . $phpEx); 
require($phpbb_root_path . 'language/lang_' . $board_config['default_lang'] . '/lang_digests.' . $phpEx);

// Convert the default send time into the local timezone
$board_timezone = date('Z')/3600;
$user_timezone = (float) $userdata['user_timezone'];
$offset = $board_timezone - $user_timezone;

if ($HTTP_SERVER_VARS['REQUEST_METHOD'] == 'GET')
{

	$defaults = get_digest_defaults(); // This will retrieve the current digest defaults, and create them if necessary
	$settings = get_digest_settings();	// This will retrieve the current global settings
	
	// Check for new version
	$current_version = explode('.', $settings['version']);
	$major_revision = (int) $current_version[0];
	$minor_revision = (int) $current_version[1];
	$micro_revision = (int) $current_version[2];

	$errno = 0;
	$errstr = $version_info = '';

	if ($fsock = @fsockopen('phpbb.potomactavern.org', 80, $errno, $errstr, 10))
	{
		@fputs($fsock, "GET /digests/updatecheck/version.txt HTTP/1.1\r\n");
		@fputs($fsock, "HOST: phpbb.potomactavern.org\r\n");
		@fputs($fsock, "Connection: close\r\n\r\n");

		$get_info = false;
		while (!@feof($fsock))
		{
			if ($get_info)
			{
				$version_info .= @fread($fsock, 1024);
			}
			else
			{
				if (@fgets($fsock, 1024) == "\r\n")
				{
					$get_info = true;
				}
			}
		}
		@fclose($fsock);

		$version_info = explode("\n", $version_info);
		$latest_head_revision = (int) $version_info[0];
		$latest_minor_revision = (int) $version_info[1];
		$latest_micro_revision = (int) $version_info[2];
		$latest_version = (int) $version_info[0] . '.' . (int) $version_info[1] . '.' . (int) $version_info[2];

		if (($latest_head_revision == $major_revision) && ($latest_minor_revision == $minor_revision) && ($latest_micro_revision == $micro_revision))
		{
			$version_info = '<span style="color:green">' . $lang['digest_admin_version_up_to_date'] . '</span>';
		}
		else
		{
			$version_info = '<span style="color:red">' . $lang['digest_admin_version_not_up_to_date'];
			$version_info .= ' ' . sprintf($lang['digest_admin_latest_version_info'], $latest_version) . ' ' . sprintf($lang['digest_admin_current_version_info'], $settings['version']) . '</span>';
		}
	}
	else
	{
		if ($errstr)
		{
			$version_info = '<p style="color:red">' . sprintf($lang['digest_admin_connect_socket_error'], $errstr) . '</p>';
		}
		else
		{
			$version_info = '<p>' . $lang['Socket_functions_disabled'] . '</p>';
		}
	}
	
	// Convert the default send time into the local timezone
	$send_hour = (float) $defaults['send_hour'];
	if ($send_hour <> -1) // -1 means a random hour is to be used
	{
		$send_hour = $send_hour - $offset;
		$send_hour = ($send_hour < 0) ? $send_hour + 24 : $send_hour;
		$send_hour = ($send_hour >= 24) ? $send_hour - 24 : $send_hour; 	
	}
	
	$template->set_filenames(array('body' => 'admin/digests_config_body.tpl'));

	$template->assign_vars(array(
		'L_ADMIN_EXPLAIN' => $lang['digest_admin_config_explain'],
		'L_NO' => $lang['digest_no'],
		'L_PAGE_TITLE' => $lang['digest_admin_configuration'],
		'L_YES' => $lang['digest_yes'],
		'S_POST_ACTION' => append_sid("admin_digests_config.$phpEx"),
		'U_DIGEST_PAGE_URL' => DIGEST_PAGE_URL,
		
		// This part is for the Digest Default Settings
		'10AM_SELECTED' => ($send_hour==10) ? 'selected="selected"' : '',
		'10PM_SELECTED' => ($send_hour==22) ? 'selected="selected"' : '',
		'11AM_SELECTED' => ($send_hour==11) ? 'selected="selected"' : '',
		'11PM_SELECTED' => ($send_hour==23) ? 'selected="selected"' : '',
		'12PM_SELECTED' => ($send_hour==12) ? 'selected="selected"' : '',
		'1AM_SELECTED' => ($send_hour==1) ? 'selected="selected"' : '',
		'1PM_SELECTED' => ($send_hour==13) ? 'selected="selected"' : '',
		'2AM_SELECTED' => ($send_hour==2) ? 'selected="selected"' : '',
		'2PM_SELECTED' => ($send_hour==14) ? 'selected="selected"' : '',
		'3AM_SELECTED' => ($send_hour==3) ? 'selected="selected"' : '',
		'3PM_SELECTED' => ($send_hour==15) ? 'selected="selected"' : '',
		'4AM_SELECTED' => ($send_hour==4) ? 'selected="selected"' : '',
		'4PM_SELECTED' => ($send_hour==16) ? 'selected="selected"' : '',
		'5AM_SELECTED' => ($send_hour==5) ? 'selected="selected"' : '',
		'5PM_SELECTED' => ($send_hour==17) ? 'selected="selected"' : '',
		'6AM_SELECTED' => ($send_hour==6) ? 'selected="selected"' : '',
		'6PM_SELECTED' => ($send_hour==18) ? 'selected="selected"' : '',
		'7AM_SELECTED' => ($send_hour==7) ? 'selected="selected"' : '',
		'7PM_SELECTED' => ($send_hour==19) ? 'selected="selected"' : '',
		'8AM_SELECTED' => ($send_hour==8) ? 'selected="selected"' : '',
		'8PM_SELECTED' => ($send_hour==20) ? 'selected="selected"' : '',
		'9AM_SELECTED' => ($send_hour==9) ? 'selected="selected"' : '',
		'9PM_SELECTED' => ($send_hour==21) ? 'selected="selected"' : '',
		'50_SELECTED' => (trim($defaults['text_length'])=='50') ? 'selected="selected"' : '',
		'100_SELECTED' => (trim($defaults['text_length'])=='100') ? 'selected="selected"' : '',
		'150_SELECTED' => (trim($defaults['text_length'])=='150') ? 'selected="selected"' : '',
		'300_SELECTED' => (trim($defaults['text_length'])=='300') ? 'selected="selected"' : '',
		'600_SELECTED' => (trim($defaults['text_length'])=='600') ? 'selected="selected"' : '',
		'DAY_CHECKED' => (trim($defaults['digest_type']) == 'DAY') ? ' checked="checked"' : '',
		'DIGEST_VERSION' => $settings['version'],
		'HTML_CHECKED' => ($defaults['format'] == 'HTML') ? ' checked="checked"' : '',
		'L_100' => $lang['digest_size_100'],
		'L_10AM' => $lang['digest_10am'],
		'L_10PM' => $lang['digest_10pm'],
		'L_11AM' => $lang['digest_11am'],
		'L_11PM' => $lang['digest_11pm'],
		'L_12PM' => $lang['digest_12pm'],
		'L_150' => $lang['digest_size_150'],
		'L_1AM' => $lang['digest_1am'],
		'L_1PM' => $lang['digest_1pm'],
		'L_2AM' => $lang['digest_2am'],
		'L_2PM' => $lang['digest_2pm'],
		'L_300' => $lang['digest_size_300'],
		'L_3AM' => $lang['digest_3am'],
		'L_3PM' => $lang['digest_3pm'],
		'L_4AM' => $lang['digest_4am'],
		'L_4PM' => $lang['digest_4pm'],
		'L_50' => $lang['digest_size_50'],
		'L_5AM' => $lang['digest_5am'],
		'L_5PM' => $lang['digest_5pm'],
		'L_600' => $lang['digest_size_600'],
		'L_6AM' => $lang['digest_6am'],
		'L_6PM' => $lang['digest_6pm'],
		'L_7AM' => $lang['digest_7am'],
		'L_7PM' => $lang['digest_7pm'],
		'L_8AM' => $lang['digest_8am'],
		'L_8PM' => $lang['digest_8pm'],
		'L_9AM' => $lang['digest_9am'],
		'L_9PM' => $lang['digest_9pm'],
		'L_ADMIN_FORUM_SELECTION_EXPLAIN' => $lang['digest_admin_forum_selection_explain'],
		'L_DAILY' => $lang['digest_daily'],
		'L_DIGEST_TYPE' => $lang['digest_admin_wanted'],
		'L_DIGEST_USER_DEFAULTS' => $lang['digest_admin_user_defaults'],
		'L_FORMAT' => $lang['digest_admin_format'],
		'L_HTML_ENH' => $lang['digest_html_enhanced'],
		'L_HTML_PLAIN' => $lang['digest_html_plain'],
		'L_MAX' => $lang['digest_size_max'],
		'L_MIDNIGHT' => $lang['digest_midnight'],
		'L_NEW_ONLY' => $lang['digest_admin_show_new_only'],
		'L_PRIVATE_MGS_IN_FEED' => $lang['digest_admin_private_messages_in_digest'],
		'L_RANDOM' => $lang['digest_admin_random_hour'],
		'L_RESET' => $lang['digest_reset_text'],
		'L_SEND_HOUR' => $lang['digest_admin_hour_to_send'],
		'L_SEND_ON_NO_MESSAGES' => $lang['digest_admin_send_if_no_msgs'],
		'L_SHOW_MINE' => $lang['digest_admin_show_my_messages'],
		'L_SHOW_TEXT' => $lang['digest_admin_excerpt'],
		'L_SUBMIT' => $lang['digest_admin_submit_text'],
		'L_TEXT' => $lang['digest_text'],
		'L_TEXT_LENGTH' => $lang['digest_admin_size'],
		'L_WEEKLY' => $lang['digest_weekly'],
		'MAX_SELECTED' => (trim($defaults['text_length'])=='32000') ? 'selected="selected"' : '',
		'MIDNIGHT_SELECTED' => ($send_hour==0) ? 'selected="selected"' : '',
		'NEW_ONLY_NO_CHECKED' => (trim($defaults['new_only']) == 'FALSE') ? ' checked="checked"' : '',
		'NEW_ONLY_YES_CHECKED' => (trim($defaults['new_only']) == 'TRUE') ? ' checked="checked"' : '',
		'PLAIN_HTML_CHECKED' => ($defaults['format'] == 'PHTM') ? ' checked="checked"' : '',
		'RANDOM_SELECTED' => ($send_hour==-1) ? 'selected="selected"' : '',
		'SEND_ON_NO_MESSAGES_NO_CHECKED' => (trim($defaults['send_on_no_messages']) == 'NO') ? ' checked="checked"' : '',
		'SEND_ON_NO_MESSAGES_YES_CHECKED' => (trim($defaults['send_on_no_messages']) == 'YES') ? ' checked="checked"' : '',
		'SHOW_MINE_NO_CHECKED' => (trim($defaults['show_mine']) == 'NO') ? ' checked="checked"' : '',
		'SHOW_MINE_YES_CHECKED' => (trim($defaults['show_mine']) == 'YES') ? ' checked="checked"' : '',
		'SHOW_PMS_NO_CHECKED' => (trim($defaults['show_pms']) == 'NO') ? ' checked="checked"' : '',
		'SHOW_PMS_YES_CHECKED' => (trim($defaults['show_pms']) == 'YES') ? ' checked="checked"' : '',
		'SHOW_TEXT_NO_CHECKED' => (trim($defaults['show_text']) == 'NO') ? ' checked="checked"' : '',
		'SHOW_TEXT_YES_CHECKED' => (trim($defaults['show_text']) == 'YES') ? ' checked="checked"' : '',
		'TEXT_CHECKED' => ($defaults['format'] == 'TEXT') ? ' checked="checked"' : '',
		'WEEK_CHECKED' => (trim($defaults['digest_type']) == 'WEEK') ? ' checked="checked"' : '',
				
		// This part for the Global Settings
		'AUTOSUBSCRIBED_CHECKED' => (trim($settings['autosubscribe_users']) == 'YES') ? ' checked="checked"' : '',
		'AUTOSUBSCRIBE_DAILY_CHECKED' => (trim($settings['autosubscribe_daily']) == 'DAY') ? ' checked="checked"' : '',
		'AUTOSUBSCRIBE_WEEKLY_CHECKED' => (trim($settings['autosubscribe_daily']) == 'WEEK') ? ' checked="checked"' : '',
		'CUSTOM_STYLESHEET_PATH' => $settings['custom_stylesheet_path'],
		'DIGEST_DATE_FORMAT' => $settings['date_format'],
		'DIGEST_VERSION' => $settings['version'],
		'ENABLE_DISABLE' => (trim($settings['autosubscribe_users']) == 'NO') ? 'disabled="disabled"' : '',
		'ENABLE_DISABLE_KEY' => (trim($settings['require_send_key']) == 'NO') ? 'disabled="disabled"' : '',
		'ENABLE_DISABLE_SUMMARY_DATE' => (trim($settings['show_summary']) == 'NO') ? 'disabled="disabled"' : '',
		'FRIDAY_SELECTED' => (trim($settings['weekly_digest_day'])=='5') ? 'selected="selected"' : '',
		'HTML_ENCODING' => $settings['html_encoding'],
		'L_AUTOSUBSCRIBE_DAILY_USERS' => $lang['digest_admin_autosubscribe_daily'],
		'L_AUTOSUBSCRIBE_USERS' => $lang['digest_admin_autosubscribe'],
		'L_CUSTOM_STYLESHEET_PATH' => $lang['digest_admin_custom_stylesheet_path'],
		'L_DIGEST_DATE_FORMAT' => $lang['digest_admin_date_format'],
		'L_DIGEST_SETTING_DEFAULTS' => $lang['digest_admin_user_settings'],
		'L_FRIDAY' => $lang['digest_weekday'][5],
		'L_HTML_ENCODING' => $lang['digest_admin_html_encoding'],
		'L_INVALID_SITE_URL' => $lang['digest_admin_invalid_site_url'],
		'L_INVALID_TEMPLATE_NAME' => $lang['digest_admin_invalid_template_name'],
		'L_MAY_NOT_BE_BLANK' => $lang['digest_admin_may_not_be_blank'],
		'L_MONDAY' => $lang['digest_weekday'][1],
		'L_NOT_A_NUMBER' => $lang['digest_admin_not_a_number'],
		'L_NOT_NEGATIVE' => $lang['digest_admin_not_negative'],
		'L_REQUIRE_SEND_KEY' => $lang['digest_admin_require_send_key'],
		'L_SATURDAY' => $lang['digest_weekday'][6],
		'L_SEND_KEY' => $lang['digest_admin_send_key'],
		'L_SHOW_SUMMARY' => $lang['digest_admin_show_summary'],
		'L_SITE_URL' => $lang['digest_admin_site_url'],
		'L_SUBMIT2' => $lang['digest_admin_user_settings_submit'],
		'L_SUMMARY_DATE_DISPLAY_FORMAT' => $lang['digest_admin_summary_date_format'],
		'L_SUMMARY_FORMAT' => $lang['digest_admin_summary_format'],
		'L_SUMMARY_HTML' => $lang['digest_admin_html'],
		'L_SUMMARY_TEXT' => $lang['digest_text'],
		'L_SUNDAY' => $lang['digest_weekday'][0],
		'L_TEXT_ENCODING' => $lang['digest_admin_text_encoding'],
		'L_THURSDAY' => $lang['digest_weekday'][4],
		'L_TUESDAY' => $lang['digest_weekday'][2],
		'L_USE_CUSTOM_STYLESHEET' => $lang['digest_admin_use_custom_stylesheet'],
		'L_USE_ENCODINGS' => $lang['digest_admin_use_encodings'],
		'L_USERS_PER_PAGE' => $lang['digest_admin_users_per_page'],
		'L_VERSION_INFORMATION'	=> $lang['Version_information'],
		'L_WEDNESDAY' => $lang['digest_weekday'][3],
		'L_WEEKLY_DAY_OF_WEEK' => $lang['digest_admin_weekly_digest_day_of_week'],
		'MONDAY_SELECTED' => (trim($settings['weekly_digest_day'])=='1') ? 'selected="selected"' : '',
		'REQUIRE_SEND_KEY' => (trim($settings['require_send_key']) == 'YES') ? ' checked="checked"' : '',
		'SATURDAY_SELECTED' => (trim($settings['weekly_digest_day'])=='6') ? 'selected="selected"' : '',
		'SEND_KEY' => $settings['send_key'],
		'SHOW_SUMMARY' => (trim($settings['show_summary']) == 'YES') ? ' checked="checked"' : '',
		'SITE_URL' => $settings['site_url'],
		'SUMMARY_DATE_DISPLAY_FORMAT' => $settings['server_display_date'],
		'SUMMARY_HTML_ENABLED' => (trim($settings['summary_type']) == 'HTML') ? ' checked="checked"' : '',
		'SUMMARY_HTML_HIDDEN' => (trim($settings['show_summary']) == 'NO') ? 'disabled="disabled"' : '',
		'SUMMARY_TEXT_ENABLED' => (trim($settings['summary_type']) == 'TEXT') ? ' checked="checked"' : '',
		'SUMMARY_TEXT_HIDDEN' => (trim($settings['show_summary']) == 'NO') ? 'disabled="disabled"' : '',
		'SUNDAY_SELECTED' => (trim($settings['weekly_digest_day'])=='0') ? 'selected="selected"' : '',
		'TEXT_ENCODING' => $settings['text_encoding'],
		'TEXTBOX_ENABLE_DISABLE' => (trim($settings['use_custom_stylesheet']) == 'NO') ? 'disabled="disabled"' : '',
		'THURSDAY_SELECTED' => (trim($settings['weekly_digest_day'])=='4') ? 'selected="selected"' : '',
		'TUESDAY_SELECTED' => (trim($settings['weekly_digest_day'])=='2') ? 'selected="selected"' : '',
		'USE_CUSTOM_STYLESHEET_CHECKED' => (trim($settings['use_custom_stylesheet']) == 'YES') ? ' checked="checked"' : '',
		'USERS_PER_PAGE' => $settings['users_per_page'],
		'VERSION_INFO'	=> $version_info,
		'WEDNESDAY_SELECTED' => (trim($settings['weekly_digest_day'])=='3') ? 'selected="selected"' : ''));

	$template->pparse('body');
	
	include('./page_footer_admin.'.$phpEx);
					
} 

else 
{

	if (htmlspecialchars($HTTP_POST_VARS['digest_defaults']) == '1')  // Save the user default settings
	{

		// Convert send hour, which is expressed in user's time, back into server time
		$send_hour = htmlspecialchars($HTTP_POST_VARS['send_hour']);
		if ($send_hour == 'R') // -1 means a random hour is to be used
		{
			$send_hour = -1;
		}
		else
		{
			$send_hour = intval($send_hour) + $offset;
			$send_hour = ($send_hour < 0) ? $send_hour + 24 : $send_hour;
			$send_hour = ($send_hour >= 24) ? $send_hour - 24 : $send_hour; 	
		}
			
		save_digest_settings(ANONYMOUS, 'UPD', htmlspecialchars($HTTP_POST_VARS['digest_type']), htmlspecialchars($HTTP_POST_VARS['format']), htmlspecialchars($HTTP_POST_VARS['show_text']), htmlspecialchars($HTTP_POST_VARS['show_mine']), htmlspecialchars($HTTP_POST_VARS['new_only']), htmlspecialchars($HTTP_POST_VARS['send_on_no_messages']), htmlspecialchars($HTTP_POST_VARS['show_pms']), $send_hour, intval($HTTP_POST_VARS['text_length']));
	
		$message = $lang['digest_admin_default_setting_saved'] . '<br /><br />' . 	
			sprintf($lang['digest_admin_return_admin_digests_configure'], '<a href="' . append_sid("admin_digests_config.$phpEx") . '">', '</a> ') . '<br /><br />' .
			sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");
			
	}
	
	else if ($HTTP_POST_VARS['digest_settings'] == '1')  // Save the global settings
	{
	
		$site_url = htmlspecialchars($HTTP_POST_VARS['site_url']);
		$autosubscribe_users = (htmlspecialchars($HTTP_POST_VARS['autosubscribe']) == 'on') ? 'YES' : 'NO';
		$autosubscribe_daily = ($autosubscribe_users == 'NO') ? '' : "autosubscribe_daily = '" . htmlspecialchars($HTTP_POST_VARS['autosubscribedaily']) . "',";
		$weekly_digest_day = intval($HTTP_POST_VARS['day_of_week']);
		$use_custom_stylesheet = (htmlspecialchars($HTTP_POST_VARS['use_custom_ss']) == 'on') ? 'YES' : 'NO';
		$custom_stylesheet_path = ($use_custom_stylesheet == 'NO') ? '' : "custom_stylesheet_path = '" . htmlspecialchars($HTTP_POST_VARS['custom_ss_path']) . "',";
		$date_format = htmlspecialchars($HTTP_POST_VARS['date_format']);
		$server_display_date = htmlspecialchars($HTTP_POST_VARS['summary_date']);
		$mail_digests_phpbb_path = htmlspecialchars($HTTP_POST_VARS['mail_digests_path']);
		if (htmlspecialchars($HTTP_POST_VARS['use_encodings']) == 'on')
		{
			$html_encoding_sql = ", html_encoding = '" . htmlspecialchars($HTTP_POST_VARS['html_encoding']) . "'";
			$text_encoding_sql = ", text_encoding = '" .htmlspecialchars($HTTP_POST_VARS['text_encoding']) . "', ";
		}
		else
		{
			$html_encoding_sql = ', ';
			$text_encoding_sql = '';
		}
		$users_per_page =  intval($HTTP_POST_VARS['users_per_page']);
		$require_send_key = (htmlspecialchars($HTTP_POST_VARS['req_send_key']) == 'on') ? 'YES' : 'NO';
		$send_key = ($require_send_key == 'NO') ? '' : "send_key = '" . htmlspecialchars($HTTP_POST_VARS['send_key']) . "',";
		$show_summary = (htmlspecialchars($HTTP_POST_VARS['show_summary']) == 'on') ? 'YES' : 'NO';
		$summary_type = ($show_summary == 'NO') ? '' : ", summary_type = '" . htmlspecialchars($HTTP_POST_VARS['sformat']) . "',";
		$server_display_date = ($show_summary == 'NO') ? '' : "server_display_date = '" . htmlspecialchars($HTTP_POST_VARS['summary_date']) . "'";
		
		$sql = 'UPDATE ' . DIGEST_SETTINGS_TABLE . " SET site_url = '$site_url', autosubscribe_users = '$autosubscribe_users', $autosubscribe_daily weekly_digest_day = $weekly_digest_day, use_custom_stylesheet = '$use_custom_stylesheet', $custom_stylesheet_path date_format = '$date_format'" . $html_encoding_sql . $text_encoding_sql . "users_per_page = $users_per_page, require_send_key = '$require_send_key', $send_key show_summary = '$show_summary'$summary_type $server_display_date, mail_digests_phpbb_path = '$mail_digests_phpbb_path' where table_key = 1";
		
		if ( !($result = $db->sql_query($sql)))
		{
			message_die(GENERAL_ERROR, 'Could not update row for ' . DIGEST_SETTINGS_TABLE . ' table', '', __LINE__, __FILE__, $sql);
		}
		$message = $lang['digest_admin_config_setting_saved'] . '<br /><br />' . 	
			sprintf($lang['digest_admin_return_admin_digests_configure'], '<a href="' . append_sid("admin_digests_config.$phpEx") . '">', '</a> ') . '<br /><br />' .
			sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");
	}
	message_die(GENERAL_MESSAGE, $message);
		
}

?>
