<?php
/***************************************************************************
 *                         admin_digests_modify.php
 *                         ------------------------
 *   begin                : Thurs Apr 19, 2007
 *   copyright            : (c) Mark D. Hamill
 *   email                : mhamill@computer.org
 *
 *   $Id: admin_forums.php,v 1.40.2.13 2006/03/09 21:55:09 grahamje Exp $
 *
 ***************************************************************************/

/***************************************************************************
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

// Written by Mark D. Hamill, mhamill@computer.org
// This software is designed to work with phpBB Version 2.0.22

define('IN_PHPBB', 1);

if( !empty($setmodules) )
{
	$file = basename(__FILE__);
	$module['Digests']['Digests_Modify_Subscribers'] = $file;
	return;
}

//
// Load default header
//
$phpbb_root_path = "./../";
require($phpbb_root_path . 'extension.inc');
require('./pagestart.' . $phpEx);
require($phpbb_root_path . 'includes/digest_functions.' . $phpEx); 
require($phpbb_root_path . 'language/lang_' . $board_config['default_lang'] . '/lang_digests.' . $phpEx);

$start = ( isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$start = ($start < 0) ? 0 : $start;

$settings = get_digest_settings();	// This will retrieve the current global settings

// Convert the default send time into the local timezone
$board_timezone = date('Z')/3600;
$user_timezone = (float) $userdata['user_timezone'];
$offset = $board_timezone - $user_timezone;

if ($HTTP_SERVER_VARS['REQUEST_METHOD'] == 'GET')
{

	$template->set_filenames(array('body' => 'admin/digests_modify_body.tpl'));

	$sql = 'SELECT count(*) AS digest_count FROM ' . DIGEST_SUBSCRIPTIONS_TABLE . ' WHERE user_id <> ' . ANONYMOUS;

	if ( !($result = $db->sql_query($sql)))
	{
		message_die(GENERAL_ERROR, 'Could not select from ' . USERS_TABLE . ' table', '', __LINE__, __FILE__, $sql);
	}
	
	$row = $db->sql_fetchrow($result);
	$digest_count = $row['digest_count'];
	$digest_admin_users_per_page = ($settings['users_per_page'] == 0) ? $digest_count : $settings['users_per_page'];
	$pagination = generate_pagination("admin_digests_modify.$phpEx?start=$start", $digest_count, $digest_admin_users_per_page, $start). '&nbsp;';
	if ($settings['users_per_page'] <> 0 && ($digest_count > $settings['users_per_page']))
	{
		$template->assign_block_vars('switch_pagination', array());
	}

	$template->assign_vars(array(
		'DIGEST_VERSION' => $settings['version'],
		'L_ACTION' => $lang['digest_admin_action'],
		'L_ADMIN_EXPLAIN' => $lang['digest_admin_modify_explain'],
		'L_DIGEST_TYPE_SHORT' => $lang['digest_admin_wanted_short'],
		'L_FORMAT_SHORT' => $lang['digest_admin_format_short'],
		'L_FORUM_SUBSCRIPTIONS' => $lang['digest_admin_forum_subscriptions'],
		'L_GLOBAL_SUBMIT' => $lang['digest_admin_global_submit'],
		'L_PAGE_NUMBER' => sprintf($lang['Page_of'], ( floor( $start / $digest_admin_users_per_page ) + 1 ), ceil( $digest_count / $digest_admin_users_per_page )),
		'L_PAGE_TITLE' => $lang['digest_admin_modify_interface'],
		'L_PAGINATION' => $pagination,
		'L_PRIVATE_MGS_IN_FEED_SHORT' => $lang['digest_admin_private_messages_in_digest_short'],
		'L_RESET' => $lang['digest_reset_text'],
		'L_SELECT' => $lang['digest_admin_select'],
		'L_SELECT_SUBSCRIBE' => $lang['digest_admin_select_subscribe'],
		'L_SELECT_UNSELECT_ALL' => $lang['digest_admin_select_unselect_all'],
		'L_SEND_HOUR_SHORT' => $lang['digest_admin_hour_to_send_short'],
		'L_SEND_ON_NO_MESSAGES_SHORT' => $lang['digest_admin_send_if_no_msgs_short'],
		'L_SHOW_MINE_SHORT' => $lang['digest_admin_show_messages_short'],
		'L_SHOW_NEW_ONLY_SHORT' => $lang['digest_admin_show_new_only_short'],
		'L_SHOW_TEXT_SHORT' => $lang['digest_admin_excerpt_short'],
		'L_TEXT_LENGTH_SHORT' => $lang['digest_admin_size_short'],
		'L_UPDATE_DELETE_ALL' => $lang['digest_admin_update_delete_all'],
		'L_USERNAME' => $lang['digest_admin_username'],
		'S_MODIFY_URL' => append_sid("./admin_digests_modify.$phpEx"),
		'U_DIGEST_PAGE_URL' => DIGEST_PAGE_URL));	
		
	$sql_limit = ($settings['users_per_page'] == 0) ? '' : "LIMIT $start, " . $settings['users_per_page'];
	$sql = 'SELECT d.*, u.username, u.user_level FROM ' . DIGEST_SUBSCRIPTIONS_TABLE . ' d, ' . USERS_TABLE . ' u WHERE d.user_id = u.user_id AND u.user_id <> ' . ANONYMOUS . ' ORDER BY u.username ' . $sql_limit;

	if ( !($result = $db->sql_query($sql)))
	{
		message_die(GENERAL_ERROR, 'Could not select from ' . DIGEST_SUBSCRIPTIONS_TABLE . ' table', '', __LINE__, __FILE__, $sql);
	}
		
	while( $row = $db->sql_fetchrow($result) )
	{
	
		// Apply a color to the name, to identify admins and moderators, as in the Main Index
		if ( $row['user_level'] == ADMIN )
		{
			$row['username'] = '<b>' . $row['username'] . '</b>';
			$style_color = ' style="color:#' . $theme['fontcolor3'] . '"';
		}
		else if ( $row['user_level'] == MOD )
		{
			$row['username'] = '<b>' . $row['username'] . '</b>';
			$style_color = ' style="color:#' . $theme['fontcolor2'] . '"';
		}
		else
		{
			$style_color = '';
		}

		// $row['send_hour'] is stored in server timezone, not the default phpBB board timezone. We want to display a uniform time that
		// the digest is delivered to the Administrator. So hour of digest delivery is converted into the phpBB board timezone for display.
		
		switch (trim($row['digest_type']))
		{
			case 'DAY':
				$daily_selected = ' selected="selected"';
				$weekly_selected = '';
				break;
			case 'WEEK':
				$daily_selected = '';
				$weekly_selected = ' selected="selected"';
				break;
		}
				
		switch (trim($row['format']))
		{
			case 'HTML':
				$html_selected = ' selected="selected"';
				$phtm_selected = '';
				$text_selected = '';
				break;
			case 'PHTM':
				$html_selected = '';
				$phtm_selected = ' selected="selected"';
				$text_selected = '';
				break;
			case 'TEXT':
				$html_selected = '';
				$phtm_selected = '';
				$text_selected = ' selected="selected"';
				break;
		}
	
		$sql2 = 'SELECT count(*) AS subscriptions FROM ' . DIGEST_SUBSCRIBED_FORUMS_TABLE . ' where user_id = ' . $row['user_id'];

		if ( !($result2 = $db->sql_query($sql2)))
		{
			message_die(GENERAL_ERROR, 'Could not select from ' . DIGEST_SUBSCRIBED_FORUMS_TABLE . ' table', '', __LINE__, __FILE__, $sql2);
		}
		$row2 = $db->sql_fetchrow($result2);
		$subscriptions = ($row2['subscriptions'] == 0) ? $lang['digest_admin_forum_subscriptions_all'] : $row2['subscriptions'];

		$this_user_send_hour = (float) $row['send_hour'] - $offset;
		$this_user_send_hour = ($this_user_send_hour < 0) ? $this_user_send_hour + 24 : $this_user_send_hour;
		$this_user_send_hour = ($this_user_send_hour >= 24) ? $this_user_send_hour - 24 : $this_user_send_hour;

		$template->assign_block_vars('digestrow', array(
			'1AM_SELECTED' => ($this_user_send_hour==1) ? 'selected="selected"' : '',
			'1PM_SELECTED' => ($this_user_send_hour==13) ? 'selected="selected"' : '',
			'2AM_SELECTED' => ($this_user_send_hour==2) ? 'selected="selected"' : '',
			'2PM_SELECTED' => ($this_user_send_hour==14) ? 'selected="selected"' : '',
			'3AM_SELECTED' => ($this_user_send_hour==3) ? 'selected="selected"' : '',
			'3PM_SELECTED' => ($this_user_send_hour==15) ? 'selected="selected"' : '',
			'4AM_SELECTED' => ($this_user_send_hour==4) ? 'selected="selected"' : '',
			'4PM_SELECTED' => ($this_user_send_hour==16) ? 'selected="selected"' : '',
			'5AM_SELECTED' => ($this_user_send_hour==5) ? 'selected="selected"' : '',
			'5PM_SELECTED' => ($this_user_send_hour==17) ? 'selected="selected"' : '',
			'6AM_SELECTED' => ($this_user_send_hour==6) ? 'selected="selected"' : '',
			'6PM_SELECTED' => ($this_user_send_hour==18) ? 'selected="selected"' : '',
			'7AM_SELECTED' => ($this_user_send_hour==7) ? 'selected="selected"' : '',
			'7PM_SELECTED' => ($this_user_send_hour==19) ? 'selected="selected"' : '',
			'8AM_SELECTED' => ($this_user_send_hour==8) ? 'selected="selected"' : '',
			'8PM_SELECTED' => ($this_user_send_hour==20) ? 'selected="selected"' : '',
			'9AM_SELECTED' => ($this_user_send_hour==9) ? 'selected="selected"' : '',
			'9PM_SELECTED' => ($this_user_send_hour==21) ? 'selected="selected"' : '',
			'10AM_SELECTED' => ($this_user_send_hour==10) ? 'selected="selected"' : '',
			'10PM_SELECTED' => ($this_user_send_hour==22) ? 'selected="selected"' : '',
			'11AM_SELECTED' => ($this_user_send_hour==11) ? 'selected="selected"' : '',
			'11PM_SELECTED' => ($this_user_send_hour==23) ? 'selected="selected"' : '',
			'12PM_SELECTED' => ($this_user_send_hour==12) ? 'selected="selected"' : '',
			'50_SELECTED' => ($row['text_length']=='50') ? 'selected="selected"' : '',
			'100_SELECTED' => ($row['text_length']=='100') ? 'selected="selected"' : '',
			'150_SELECTED' => ($row['text_length']=='150') ? 'selected="selected"' : '',
			'300_SELECTED' => ($row['text_length']=='300') ? 'selected="selected"' : '',
			'600_SELECTED' => ($row['text_length']=='600') ? 'selected="selected"' : '',
			'DAILY_SELECTED' => $daily_selected,
			'EXCERPT_NO_SELECTED' => (trim($row['show_text']) == 'NO') ? ' selected="selected"' : '',
			'EXCERPT_YES_SELECTED' => (trim($row['show_text']) == 'YES') ? ' selected="selected"' : '',
			'HTML_ENH_SELECTED' => $html_selected,
			'HTML_PLAIN_SELECTED' => $phtm_selected,
			'ID' => $row['user_id'],
			'L_100' => $lang['digest_size_100'],
			'L_10AM' => $lang['digest_10am'],
			'L_10PM' => $lang['digest_10pm'],
			'L_11AM' => $lang['digest_11am'],
			'L_11PM' => $lang['digest_11pm'],
			'L_12PM' => $lang['digest_12pm'],
			'L_150' => $lang['digest_size_150'],
			'L_1AM' => $lang['digest_1am'],
			'L_1PM' => $lang['digest_1pm'],
			'L_2AM' => $lang['digest_2am'],
			'L_2PM' => $lang['digest_2pm'],
			'L_300' => $lang['digest_size_300'],
			'L_3AM' => $lang['digest_3am'],
			'L_3PM' => $lang['digest_3pm'],
			'L_4AM' => $lang['digest_4am'],
			'L_4PM' => $lang['digest_4pm'],
			'L_50' => $lang['digest_size_50'],
			'L_5AM' => $lang['digest_5am'],
			'L_5PM' => $lang['digest_5pm'],
			'L_600' => $lang['digest_size_600'],
			'L_6AM' => $lang['digest_6am'],
			'L_6PM' => $lang['digest_6pm'],
			'L_7AM' => $lang['digest_7am'],
			'L_7PM' => $lang['digest_7pm'],
			'L_8AM' => $lang['digest_8am'],
			'L_8PM' => $lang['digest_8pm'],
			'L_9AM' => $lang['digest_9am'],
			'L_9PM' => $lang['digest_9pm'],
			'L_DAILY' => $lang['digest_daily'],
			'L_DELETE' => $lang['digest_admin_delete'],
			'L_HTML_ENH' => $lang['digest_html_enhanced'],
			'L_HTML_PLAIN' => $lang['digest_html_plain'],
			'L_MAX' => $lang['digest_size_max'],
			'L_MIDNIGHT' => $lang['digest_midnight'],
			'L_NO' => $lang['digest_no'],
			'L_TEXT' => $lang['digest_text'],
			'L_UPDATE' => $lang['digest_admin_update'],
			'L_WEEKLY' => $lang['digest_weekly'],
			'L_YES' => $lang['digest_yes'],
			'MAX_SELECTED' => ($row['text_length']=='32000') ? 'selected="selected"' : '',
			'MIDNIGHT_SELECTED' => ($this_user_send_hour==0) ? 'selected="selected"' : '',
			'MINE_NO_SELECTED' => (trim($row['show_mine']) == 'NO') ? ' selected="selected"' : '',
			'MINE_YES_SELECTED' => (trim($row['show_mine']) == 'YES') ? ' selected="selected"' : '',
			'NEW_NO_SELECTED' => (trim($row['new_only']) == 'FALSE') ? ' selected="selected"' : '',
			'NEW_YES_SELECTED' => (trim($row['new_only']) == 'TRUE') ? ' selected="selected"' : '',
			'ONLY_IF_MSGS_NO_SELECTED' => (trim($row['send_on_no_messages']) == 'NO') ? ' selected="selected"' : '',
			'ONLY_IF_MSGS_YES_SELECTED' => (trim($row['send_on_no_messages']) == 'YES') ? ' selected="selected"' : '',
			'PMS_NO_SELECTED' => (trim($row['show_pms']) == 'NO') ? ' selected="selected"' : '',
			'PMS_YES_SELECTED' => (trim($row['show_pms']) == 'YES') ? ' selected="selected"' : '',
			'PRIVATE_MGS_IN_FEED_SHORT' => $row['show_pms'],
			'SEND_HOUR_SHORT' => $this_user_send_hour,
			'SUBSCRIPTIONS' => $subscriptions,
			'TEXT_LENGTH_SHORT' => $row['text_length'],
			'TEXT_SELECTED' => $text_selected,
			'USERNAME' => '<a href="' . append_sid($phpbb_root_path . 'profile.php?mode=viewprofile&amp;u=' . $row['user_id']) . "\"$style_color>" . $row['username'] . '</a>',
			'WEEKLY_SELECTED' => $weekly_selected));		
	} // $row = $db->sql_fetchrow($result)

	$template->pparse('body');
	
	include('./page_footer_admin.'.$phpEx);
					
} 

else 
{

	// The user has submitted the form. This logic takes the necessary action to update the database
	// and gives an appropriate confirmation message.

	$current_user_id = trim(substr($key,$underscore_loc+1));
	$users_found = 0;

	do
	{
		// Update the current user being processed
		$underscore_loc = strrpos($key,'_');
		if ($underscore_loc == true)
		{
			if (trim(substr($key,$underscore_loc+1)) <> $current_user_id)
			{
				$users_found++;
				$current_user_id = trim(substr($key,$underscore_loc+1));
				// Update or delete the digest information
				$action = 'action_' . $current_user_id;
				$type = 'type_' . $current_user_id;
				$format = 'format_' . $current_user_id;
				$excerpt = 'excerpt_' . $current_user_id;
				$mine = 'mine_' . $current_user_id;
				$new = 'new_' . $current_user_id;
				$only_if_msgs = 'only_if_msgs_' . $current_user_id;
				$pms = 'pms_' . $current_user_id;
				$send_hour = 'send_hour_' . $current_user_id;
				$text_length = 'text_length_' . $current_user_id;
				$this_username = save_digest_settings($current_user_id, htmlspecialchars($HTTP_POST_VARS[$action]), htmlspecialchars($HTTP_POST_VARS[$type]), htmlspecialchars($HTTP_POST_VARS[$format]), htmlspecialchars($HTTP_POST_VARS[$excerpt]), htmlspecialchars($HTTP_POST_VARS[$mine]), htmlspecialchars($HTTP_POST_VARS[$new]), htmlspecialchars($HTTP_POST_VARS[$only_if_msgs]), htmlspecialchars($HTTP_POST_VARS[$pms]), intval($HTTP_POST_VARS[$send_hour])+$offset, intval($HTTP_POST_VARS[$text_length]));
			}
		}
	}
	while (list($key, $value) = each($HTTP_POST_VARS)); 

	$display_message = ($users_found > 0) ? $lang['digest_admin_post_msg'] : $lang['digest_admin_post_msg_none'];
	$message = $display_message . '<br /><br />' . 
		sprintf($lang['digest_admin_return_admin_digests_modify'], '<a href="' . append_sid("admin_digests_modify.$phpEx") . '">', '</a> ') . '<br /><br />' .
		sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");
	message_die(GENERAL_MESSAGE, $message);
		
}

?>
