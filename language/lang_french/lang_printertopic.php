<?php
/***************************************************************************
 *                            lang_printertopic.php [French]
 *                              -------------------
 *   begin                : Tuesday, Dec 30 2003
 *   copyright            : (C) 2003 by Svyatozar
 *   email                : svyatozar@pochtamt.ru
 *
 *   $Id: lang_printertopic.php,v 1.2 2004/01/03 06:51:40 webmaster Exp $
 *
 ****************************************************************************/

/***************************************************************************
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/


//
// printer-friendly topic mod
//

// mod specific
$lang['printertopic_button'] = 'Version imprimable'; // Printer button alternate text
$lang['printertopic_Select_messages_from'] = 'S�lectionner messages � partir du N�';
$lang['printertopic_through'] = 'jusqu\'au N�';
$lang['printertopic_box1_desc'] = 'num�ro du premier message � afficher';
$lang['printertopic_box2_desc'] = 'num�ro du dernier message � afficher&nbsp;; si n�gatif, d�finit un intervalle';
$lang['printertopic_Show'] = 'Afficher'; // "Show" button label in the printable topic view
$lang['printertopic_Print'] = 'Imprimer'; // "Print" button label in the printable topic view
$lang['printertopic_Print_desc'] = 'Imprimer cette page'; // Description for the "Print" button on the printable topic view
$lang['printertopic_cancel_pagination_desc'] = 'Annuler la pagination'; // Description for the "Cancel Pagination" button in the printable topic view
$lang['printertopic_restore_pagination_desc'] = 'Restaurer la pagination'; // Description for the "Restore Pagination" button in the printable topic view


?>