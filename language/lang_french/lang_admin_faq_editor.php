<?php
/***************************************************************************
 *                       lang_admin_faq_editor.php [English]
 *                              -------------------
 *     begin                : Sat Dec 16 2000
 *     copyright            : (C) 2001 The phpBB Group
 *     email                : support@phpbb.com
 *
 *     $Id: lang_admin_faq_editor.php,v 1.0.0.0 2003/07/13 23:24:12 Selven Exp $
 *
 ****************************************************************************/

/***************************************************************************
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/


$lang['faq_editor'] = '�diteur de FAQ';
$lang['faq_edit_language'] = 'S�lectionnez la langue';
$lang['faq_editor_explain'] = 'Ce module vous permet d\'�diter et de r�organiser la FAQ g�n�rale et la FAQ du BBCode. Vous <u>ne devez pas</u> supprimer ou modifier la section intitul�e <b>phpBB 2</b>.';

$lang['faq_select_language'] = 'S�lectionnez la langue de la FAQ que vous souhaitez �diter';
$lang['faq_retrieve'] = 'Envoyer';

$lang['faq_block_delete'] = '�tes-vous s�r de vouloir supprimer ce bloc ?';
$lang['faq_quest_delete'] = '�tes-vous s�r de vouloir supprimer cette question (et sa r�ponse) ?';

$lang['faq_quest_edit'] = '�diter la question et la r�ponse';
$lang['faq_quest_create'] = 'Cr�er une nouvelle question';

$lang['faq_quest_edit_explain'] = '�diter la question et la r�ponse. Changez ce bloc si vous le souhaitez.';
$lang['faq_quest_create_explain'] = 'Tapez la nouvelle question et sa r�ponse, puis cliquez sur <em>Envoyer</em>.';

$lang['faq_block'] = 'Bloc';
$lang['faq_quest'] = 'Question';
$lang['faq_answer'] = 'R�ponse';

$lang['faq_block_name'] = 'Nom du bloc';
$lang['faq_block_rename'] = 'Renomer un bloc';
$lang['faq_block_rename_explain'] = 'Change le nom du bloc dans le fichier';

$lang['faq_block_add'] = 'Ajouter un bloc';
$lang['faq_quest_add'] = 'Ajouter une question';

$lang['faq_no_quests'] = 'Il n\'y a aucune question dans ce bloc. Ceci emp�chera les blocs qui suivent celui-ci d\'�tre affich�s. Veuillez ajoutez au moins une question ou supprimez ce bloc.';
$lang['faq_no_blocks'] = 'Aucun bloc d�fini. Ajoutez un nouveau bloc en sp�cifiant un nom ci-dessous.';

$lang['faq_write_file'] = 'Erreur. Impossible d\'�crire dans le fichier de langue !';
$lang['faq_write_file_explain'] = 'You must make the language file in language/lang_english/ or equivilent <i>writeable</i> to use this control panel. On UNIX, this means running <code>chmod 666 filename</code>. Most FTP clients can do through the properties sheet for a file, otherwise you can use telnet or SSH.';

?>