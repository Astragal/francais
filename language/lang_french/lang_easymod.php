<?php
/***************************************************************************
 *                        lang_easymod_french.php [Fran�ais]
 *                              -------------------
 *   begin                : Saturday, Mar 22 2003
 *   copyright            : (C) 2002-2004 by Nuttzy - Craig Nuttall
 *   email                : nuttzy@blizzhackers.com
 *   translated by        : -=ET=- < n/a > - Rev. 26/06/2005
 *
 *   $Id: lang_easymod_french.php,v 1.7 2005/07/14 00:00:00 -=ET=- Exp $
 *
 ****************************************************************************/

/***************************************************************************
 *
 *   Ce programme est gratuit ; Vous pouvez le redistribuer et/ou le modifier
 *   suivant les termes de la GNU General Public License comme publi�e par
 *   la Free Software Foundation ; soit en version 2, soit (� votre guise)
 *   dans n'importe quelle version plus r�cente.
 *
 ***************************************************************************/

//
// EasyMOD
//

// Name of the language, to write in this language
$lang['EM_lang_name'] = 'Fran�ais'; //'English' ;

// EM module entries in the ACP
$lang['Modifications'] = 'Gestion des MODs'; //'MOD Center';
$lang['MOD_ainstall'] = 'Installation'; //'Install MODs';
$lang['MOD_settings'] = 'Param�trage d\'EM'; //'EasyMOD Settings';
$lang['MOD_history'] = 'Historique'; //'EasyMOD History';
$lang['MOD_control_tag'] = '$easymod_install_version';

// EasyMOD alpha specific" ;
//$lang['EM_SQL_Alpha2'] = '<b>INFORMATION :</b> l\'ex�cution de requ�tes SQL a �t� d�sactiv�e sur les versions Alpha d\'EasyMOD et aucune modification de votre base de donn�es ne sera effectu�e. Cette fonction sera impl�ment�e sur la Beta 1. Cliquez sur "Terminer l\'installation" pour continuer.'; //'<b>NOTE:</b> SQL processing has been disabled in Alpha 3 and no alterations to your database will actually be preformed.  It will be implemented in Beta 1.  Press "Complete Installation" to continue.' ;

// header
$lang['EM_Title'] = 'EasyMOD - Installeur de MODs'; //'EasyMOD - Automatic MOD Installer' ;

// login
$lang['EM_access_warning'] = 'Un mot de passe est n�cessaire pour acc�der � l\'installeur de MOD EasyMOD.'; //'A password is required to access the EasyMOD automatic MOD installer.  Anyone with access could potentially access the database and FTP login information without the board owner knowing.' ;
$lang['EM_password_title'] = 'Merci de saisir le mot de passe d\'acc�s � EasyMOD'; //'Please enter the EasyMOD access password.' ;
$lang['EM_password'] = 'Mot de passe'; //'Password' ;
$lang['EM_access_EM'] = 'Acc�der � EasyMOD'; //'Access EasyMOD';

// history (installed MODs)
$lang['EM_Installed'] = 'MODs install�s'; //'Installed MODs' ;
$lang['EM_installed_desc'] = 'Tous ces MODs ont �t� install�s sur votre forum. Dans de prochaines versions d\'EasyMOD vous pourrez � partir de cette page obtenir plus de d�tails et/ou les d�sinstaller.'; //'All of these MODs have been installed at one time or another on your board.  In later versions you will be able to get more details or uninstall the MODs from here.' ;
$lang['EM_install_date'] = 'Install� le'; //'Installed' ;
$lang['EM_details'] = 'D�tails'; //'Details';
$lang['EM_No_mod_selected'] = 'Aucun MOD s�lectionn�. Merci de revenir � la page pr�c�dente et d\'en s�lectionner un.'; //'No MOD selected.  Please go back and select one.';
$lang['EM_tables_added'] = 'Tables ajout�es'; //'Tables Added';
$lang['EM_tables_altered'] = 'Tables modifi�es'; //'Tables Altered';
$lang['EM_rows_added'] = 'Lignes ajout�es'; //'Rows Added';
$lang['EM_db_alt'] = 'Modifications de la base de donn�es'; //'Database Alterations';
$lang['EM_del_files'] = 'Supprimer les fichiers du MOD'; //'Delete MOD Files';
$lang['EN_del_record'] = 'Supprimer l\'enregistrement du MOD'; //'Delete MOD Record';
$lang['EM_install_new_lang'] = 'Installer le MOD sur de nouvelles langues'; //'Install MOD on new languages';
$lang['EM_install_new_themes'] = 'Installer le MOD sur de nouveaux th�mes'; //'Install MOD on new themes';
$lang['EM_restore_backups'] = 'Restaurer les sauvegardes'; //'Restore Backups';
$lang['EM_uninstall'] = 'D�sinstaller le MOD'; //'Uninstall MOD';
$lang['Coming_soon'] = 'Bient�t disponible !'; //'Coming Soon!';

// settings
$lang['EM_settings_pw'] = 'Le mot de passe d\'EasyMOD va vous permettre de d�terminer quels administrateur pourront utiliser EasyMOD. N\'oubliez pas que par cet acc�s un admin peut discr�tement obtenir vos comptes et mots de passe de votre base de donn�es et de votre FTP. A l\'inverse, si vous ne voulez pas de mot de passe laissez vide les champs "Mot de passe" et "Confirmation du mot de passe". Enfin, si vous voulez conserver votre mot de passe sans le modifier, laissez uniquement le champ de confirmation vide.'; //'The EasyMOD password will allow you to restrict which admins can use EasyMOD. By having access to EasyMOD an admin could covertly obtain your database user/pass and FTP info.  Leave both the password and the confirm password empty to have no password set.  Leave the confirm empty to not change the password.' ;
$lang['EM_read_server'] = 'serveur'; //'server' ;
$lang['EM_write_server'] = 'serveur'; //'server' ;
$lang['EM_write_ftp'] = 'm�moire puis FTP'; //'buffer & ftp' ;
$lang['EM_write_download'] = 'DL en local'; //'download' ;
$lang['EM_write_screen'] = 'affichage �cran'; //'on screen' ;
$lang['EM_move_copy'] = 'par copy'; //'copy' ;
$lang['EM_move_ftp'] = 'par FTP'; //'automated FTP' ;
$lang['EM_move_exec'] = 'par un script'; //'execute script' ;
$lang['EM_move_manual'] = 'en manuel'; //'manually load' ;
$lang['EM_settings'] = 'Param�trage'; //'Settings';
$lang['EM_settings_desc'] = 'Param�trez EasyMOD ici.<br /><b>Cette page a encore besoin d\'�tre retravaill�e.</b> Je n\'ai pas valid� ce qui y est saisi alors faites attention de ne pas tout chambouler !'; //'Set the settings here.  <b>This page still needs work.</b>  I do not validate what you enter yet so be careful not to mess it up!' ;
$lang['EM_settings_update'] = 'Mettre � jour les param�tres'; //'Update Settings' ;
$lang['EM_settings_success'] = 'Les param�tres d\'EasyMOD ont �t� mis � jour avec succ�s '; //'Your EasyMOD settings have been updated successfully.' ;
$lang['EM_pass_disabled'] = '(mot de passe d�sactiv�)'; //'(EM password disabled)' ;
$lang['EM_pass_updated'] = '(mot de passe mis � jour)'; //'(EM password updated)' ;
$lang['EM_pass_not_updated'] = '(mot de passe inchang�)'; //'(EM password not updated)' ;
$lang['EM_supply_on_change'] = 'Seulement si vous voulez le changer'; //'Only supply if you want to change it';
$lang['EM_emv_description'] = 'Si vous voulez r�installer EasyMOD, alors vous pouvez avoir besoin de changer le num�ro de version. Vous pouvez le faire ici.'; //'If you need to reinstall EM, then you might need to change the version.  You can do so here.';
$lang['EM_easymod_version'] = 'Version d\'EasyMOD'; //'EasyMOD Version';

// EasyMOD install
$lang['EM_Intro'] = 'EasyMOD r�alise en quelques secondes l\'�dition auparavant manuelle et fastidieuse des fichiers phpBB n�cessaires � l\'installation des MODs. Il essayera naturellement d\'installer n\'importe quel MOD. Cependant, les MODs ayant le plus de chance d\'�tre install�s avec succ�s sont ceux qui auront �t� certifi�s EasyMOD Compliant.'; //'EasyMOD does in seconds what formally was a tedious process of manually editing files to install phpBB MODs.  EasyMOD will attempt to install any phpBB MOD.  However, approved EasyMOD Compliant MODs have the best chance to install successfully.' ;
$lang['EM_none_installed'] = 'Aucun MOD n\'a �t� install�.'; //'No MODs have been installed.' ;
$lang['EM_All_Processed'] = 'Tous les MODs ont �t� install�s'; //'All MODs have been processed.' ;
$lang['EM_unprocessed_mods'] = 'Ces MODs sont pr�sent dans votre r�pertoire des MODs et n\'ont pas �t� install�s sur votre version actuelle de phpBB. En cliquant sur "Installer" vous d�buterez le processus d\'installation en plusieurs �tapes. Vos fichiers phpBB ne seront PAS �cras�s avant la derni�re �tape. Les MODs EasyMOD Compliant (EMC) ont plus de chance de s\'installer que les autres. Pour plus d\'information sur comment installer des MODs via EM cliquez <a href="http://www.phpbb.com/phpBB/viewtopic.php?p=689082#689082">ici</a>.'; //'These MODs appear in your MODs directory and have not been processed for your current version of phpBB. Clicking "Process" initiates a multi-step installation.  Your current phpBB files will not be overwritten until the final step.  MODs that are EasyMOD Compliant (EMC) are more likely to install than other MODs.  More info on how to have EM install MODs is <a href="http://www.phpbb.com/phpBB/viewtopic.php?p=689082#689082">here</a>.' ;
$lang['EM_Unprocessed'] = 'MODs non install�s'; //'Unprocessed MODs' ;
$lang['EM_process'] = 'Installer'; //'Process' ;
$lang['EM_support_thread'] = 'Support'; //'Support' ;
$lang['EM_EMC'] = 'EMC'; //'EMC' ;
$lang['EM_undefined_write'] = 'M�thode d\'�criture non d�finie'; //'Undefined write method.';
$lang['EM_check_permissions'] = 'Ecrire quelque chose � propos de la v�rification des droits, et comment.'; //'Write stuff about checking permissions and how to.';
$lang['EM_undefined_move_method'] = 'M�thode de d�placement non d�finie'; //'Undefined move method.';
$lang['EM_checking_permissions'] = 'Ecrire quelque chose � propos de la v�rification des droits, et comment.'; //'Write stuff about checking permissions and how to.';
$lang['EM_MOD_center'] = 'Gestion des MODs'; //'MOD Center';
$lang['EM_install_MODs'] = 'Installation'; //'Install MODs';
$lang['EM_settings'] = 'Param�trage d\'EM'; //'EasyMOD Settings';
$lang['EM_history'] = 'Historique'; //'EasyMOD History';

// Preview
$lang['EM_preview'] = 'Aper�u'; //'Preview' ;
$lang['EM_preview_mode'] = 'Mode aper�u'; //'Preview Mode' ;
$lang['EM_preview_desc'] = 'Le tableau ci-dessous est la liste des fichiers que le MOD veut faire modifier. Cliquez sur "Voir" pour voir les changements demand�s. Ils sont mis en �vidence en caract�res rouge gras. Malheureusement, � cause du formatage HTML quelques retour � la ligne peuvent �tre occasionnellement ajout�s, mais ne seront pas pr�sent dans le fichier modifi�.'; //'The following is a list of files that the MOD specifies to be modified.  Click "View" to view what changes will take place.  The changes that EasyMOD will make to the files are bolded in red.  Unfortunately, becasue of HTML formatting, some extra carriage returns are occasionally added, but they will not appear when the file is actually written.' ;
$lang['EM_preview_filename'] = 'Fichiers'; //'Filename' ;
$lang['EM_preview_view'] = 'Voir'; //'View' ;
$lang['EM_preview_nofile'] = 'Ce MOD ne doit pas modifier de fichier. Il n\'y a rien � pr� visualiser.'; //'This MOD will not modify any files.  Nothing to preview.' ;

// History + Install
$lang['EM_Mod'] = 'MOD'; //'MOD' ;
$lang['EM_File'] = 'Fichier'; //'File' ;
$lang['EM_Version'] = 'Version'; //'Version' ;
$lang['EM_Author'] = 'Auteur'; //'Author' ;
$lang['EM_Description'] = 'Description'; //'Description' ;
$lang['EM_phpBB_Version'] = 'Version phpBB'; //'phpBB ver' ;
$lang['EM_Themes'] = 'Th�me(s)'; //'Themes' ;
$lang['EM_Languages'] = 'Langue(s)'; //'Languages' ;

// process
$lang['EM_proc_step1'] = 'Etape 1 sur 3'; //'Step 1 of 3' ;
$lang['EM_proc_complete'] = 'Pr�paration termin�e avec succ�s !'; //'Processing completed successfully!' ;
$lang['EM_proc_desc'] = 'EasyMOD a termin� la pr�paration des fichiers pour ce MOD. Vos fichiers originaux phpBB n\'ont pas encore �t� modifi�s. L\'�tape suivante va mettre � jour votre base de donn�es et remplacer vos fichiers phpBB par ceux qui viennent d\'�tre pr�par�s. Vos fichiers originaux seront automatiquement sauvegard�s. Cependant, <b>ce logiciel est en version beta et il vous est donc express�ment demand� d\'effectuer vos propres sauvegardes !</b> Cliquez sur le bouton "Etape suivante" pour continuer.'; //'EasyMOD has completed processing of this MOD. Your original phpBB files remain unaltered. The next step will update your DB and replace your phpBB files with the newly altered ones.  Your original phpBB files will automatically be backed up. However, <b>this is beta quality software and you are urged to make your own backups!!</b>  Press the "Next Step" button to continue.' ;
$lang['EM_unprocessed_commands'] = 'Action(s) non ex�cut�e(s)'; //'Unprocessed Commands' ;
$lang['EM_unprocessed_desc'] = 'Les actions suivantes n\'ont pas �t� reconnues par EasyMOD et ont donc �t� ignor�es. Le num�ro de ligne dans le script du MOD est affich�.'; //'The following commands were not recognized by EasyMOD and were ignored.  The MOD script line number is displayed.' ;
$lang['EM_processed_commands'] = 'Action(s) ex�cut�e(s)'; //'Commands Processed' ;
$lang['EM_processed_desc'] = 'Les commandes suivantes ont �t� trait�es avec succ�s par EasyMOD :'; //'EasyMOD successfully processed the following commands:';
$lang['EM_proc_failed'] = 'L\'installation a �chou�e'; //'Installation Failed' ;
$lang['EM_proc_failed_desc'] = 'EasyMOD a rencontr� les erreurs suivantes...'; //'EasyMOD encountered the following error(s).  A general error could be ABC.  A critical error means D and you should do XYZ.' ;
$lang['EM_text_depend_move'] = 'CE TEXTE DEPENDRA DE LA METHODE DE DEPLACEMENT'; //'THIS TEXT WILL DEPEND ON MOVE METHOD';

// process + post process
$lang['EM_Mod_Data'] = 'Donn�es du MOD'; //'MOD Data' ;
$lang['EM_Mod_Title'] = 'Titre du MOD'; //'MOD Title' ;
$lang['EM_Proc_Themes'] = 'Th�me(s) trait�(s)'; //'Processed Themes' ;
$lang['EM_Proc_Languages'] = 'Langue(s) trait�e(s)'; //'Processed Languages' ;
$lang['EM_Files'] = 'Fichier(s) modifi�(s)'; //'Files Edited' ;

// EasyMOD sql
$lang['EM_sql_step2'] = 'Etape 2 sur 3'; //'Step 2 of 3' ;
$lang['EM_SQL_Intro'] = '<b>Modifications de la base de donn�es</b> - DESACTIVEES'; //'<b>Database Alterations</b> - DISABLED' ;
$lang['EM_Alterations'] = 'Modifications de la base de donn�es'; //'Proposed Database Alterations' ;
$lang['EM_Pseudo'] = 'EasyMOD SQL'; //'Pseudo SQL' ;
$lang['EM_Allow'] = 'Autoriser'; //'Allow' ;
$lang['EM_Perform'] = 'R�aliser les modifications de la BD'; //'Perform DB alterations' ;
$lang['EM_complete_install'] = 'Terminer l\'installation'; //'Complete Installation' ;
$lang['EM_proposed_alterations'] = 'Modification de votre base de donn�es %s propos�es'; //'Proposed Database Alterations for your %s Database';
$lang['EM_sql_intro_explain'] = 'EasyMOD va maintenant effectuer les modifications. Toute instruction SQL coch� va �tre ex�cut�e par EM. Si vous r�installez un MOD, vous ne devez probablement <b>pas</b> ex�cuter une seconde fois ces instructions, <b>donc attention !</b><br /><br /><b>Avertissement important :</b> il s\'agit d\'un processus m�canique. Les instructions que vous coch�es vont �tre ex�cut�es mais EM ne v�rifiera PAS si ces modifications ont eu un effet n�gatif sur votre base de donn�es. Nous vous conseillons FORTEMENT de <b>sauvegarder votre base de donn�es</b> avant d\'effectuer ces modifications. D\'autre part, avant d\'ex�cuter quoi que ce soit nous vous conseillons d\'examiner minutieusement chaque requ�te, ou de demander de l\'aide aupr�s de personne ayant l\'exp�rience de l\'installation de MODs. Encore une fois, si vous �tes en train de r�installer un MOD, la r�p�tition de l\'ex�cution des m�mes requ�tes SQL peut endommager votre base de donn�es.'; //'EasyMOD will now make changes to your database if you command it to.  Any SQL with a check next to it will be performed by EM.  If you are reinstalling a MOD, you probably <b>do not</b> want to run the SQL a second time, <b>so be careful!</b><br /><br /><b>Official Warning:</b> This is a DUMB process.  Commands you check off will be executed but EM does NOT check to seeif these changes will negatively impact your database.  You are STRONGLY advised to <b>backup your database</b> before makingany changes.  Prior to executing, you are advised to examine each command thoroughly or ask for feedback from experienced MOD installers.  Again, if you are reinstalling a MOD, multiple executions of the same SQL lines could adversely effect your database.';
$lang['EM_sql_error'] = 'ERREUR SQL'; //'SQL ERROR';
$lang['EM_not_attempted'] = 'Non tent�'; //'Not Attempted';
$lang['EM_success'] = 'R�ussi'; //'Success';
$lang['EM_skipped'] = 'Saut�'; //'Skipped';
$lang['EM_processing_results'] = 'R�sultat des modifications SQL'; //'SQL Processing Results';
$lang['EM_sql_attempted'] = 'Les requ�tes SQL suivantes ont �t� tent�es:'; //'The following SQL was attempted:';
$lang['EM_all_lines_successfull'] = 'TOUTES LES LIGNES ONT ETE EXECUTEES AVEC SUCCES'; //'ALL LINES EXECUTED SUCCESSFULLY';
$lang['EM_errors_detected'] = 'ERREURS DETECTEES'; //'ERRORS DETECTED';
$lang['EM_failed'] = 'ECHEC'; //'FAILED';
$lang['EM_line_results'] = 'Voil� le r�sultat de chaque requ�te SQL ex�cut�e.'; //'The following is the result for each line of SQL executed.';
$lang['EM_sql_error_explain'] = 'Une erreur est intervenue � l\'ex�cution d\'une requ�te SQL. La suivre de l\'ex�cution des requ�tes SQL a �t� stopp�. Vous pouvez choisir de terminer l\'installation du MOD et vous ex�cuterez les requ�tes SQL � la main vous m�me. Cependant, � ce stade EasyMOD ne peut pas garantir que le MOD fonctionnera correctement donc vous devriez demander l\'aide de l\'auteur du MOD avant d\'aller plus loin.'; //'An error was encountered while processing the SQL commands.  Further SQL processing has been halted.  You may choose to complete the MOD installation anyway and perform the SQL commands manually yourself.  However, at this point EM cannot guarantee the MOD will work correctly so you are best off seeking support from the Author before continuing further.';
$lang['EM_sql_halted'] = 'MODIFICATIONS SQL ARRETE'; //'SQL PROCESSING HALTED';
$lang['EM_sql_process_error'] = 'ERREUR DE MODIFICATION SQL'; //'SQL PROCESSING ERROR';
$lang['EM_failed_line'] = 'Les lignes qui ont �chou� sont'; //'The failed line was';
$lang['EM_no_sql_preformed'] = 'Aucune modification SQL ne sera ex�cut�e.</b> Cependant, vous pouvez sauter ces modifications, continuer l\'installation du MOD, et les r�aliser ensuite manuellement'; //'No SQL alterations will be performed.</b> However, you may skip SQL processing, continue installing the MOD, and deal with the SQL manually';
$lang['EM_following_error'] = 'L\'erreur suivant est apparu'; //'The following error occured';
$lang['EM_no_sql'] = 'Aucune requ�te SQL � ex�cuter'; //'No SQL to process.  Click \'Complete Installation\' to proceed.';
$lang['EM_notice'] = 'Note'; //'Notice';
$lang['EM_urgent_warning'] = 'AVERTISSEMENT URGENT'; //'URGENT WARNING';
$lang['EM_sql_drop_warning'] = 'Une ou plusieurs instructions pour supprimer des colonnes ou des tables enti�res ont �t� d�tect�es. Bien que ce type d\'instruction puisse �tre l�gitime, vous devriez v�rifier que ce soit bien ce que vous voulez. Supprimer une table ou une colonne est irr�versible. Nous vous recommandons donc avec insistance d\'effectuer une sauvegarde de vous base de donn�es avant d\'aller plus loin, m�me si ces instructions sont l�gitimes !'; //'SQL commands for dropping either a column or an entire table have been detected.  Although these commands may be legitamate, you should double check that you want this.  Dropping a table or column is irreversable. You are strongly urged to backup your database before proceeding even if the commands are legitamate!';
$lang['EM_sql_msaccess_warning'] = 'Vous avez une base de donn�es MS Access. EM devrait ex�cuter correctement la plupart des instructions SQL pour Access. Cependant, quand on cr�e des tables ou des colonnes il n\'y a aucune solution pour d�finir des valeurs par d�faut. Vous devrez le faire manuellement dans Access. Sans ces valeurs par d�faut, il se peut que le MOD ne fonctionne pas correctement et les cons�quences pourraient �tre graves. Si vous savez comment automatiser cette action, merci de nous contacter !'; //'You have a MS Access database.  EM should perform most SQL properly for Access.  However, when creating tables or adding columns there is no way to assign default values.  You will have to do this manually in Access. Without default values, the MOD may not function as intended and the consequences could be severe.  If you know how to automate this, be sure to contact us!';
$lang['EM_experimental_explain'] = 'Vous avez une base de donn�es %s. Il est probable qu\'EM ne sache pas g�n�rer les requ�tes SQL correctement. La raison est simplement que nous ne savons pas � quoi les requ�tes SQL doivent ressembler. Si vous savez � quoi ces requ�tes SQL doivent ressembler, merci de nous le dire dans <a href="%s" target="_sql">ce sujet</a>. Autrement, attendez vous � ce que �a ne marche pas tr�s bien ;-)'; //'You have a %s database. Most likely EM isn\'t generating the SQL properly.  The reason is simply because we don\'t know what it is supposed to look like.  If you know what the SQL should look like, please let us know in <a href="%s" target="_sql">this topic</a>.  Otherwise just expect that maybe this won\'t work so well ;-)';
$lang['EM_database_alterations'] = 'Modifications de la base de donn�es'; //'Database Alterations';
$lang['EM_tables_added'] = 'Tables ajout�es'; //'Tables Added';
$lang['EM_tables_altered'] = 'Tables modifi�es'; //'Tables Altered';
$lang['EM_rows_added'] = 'Lignes ajout�es'; //'Rows Added';

// EasyMOD sql errors
$lang['EM_Unable_to_parse'] = '<b>ERREUR FATALE</b>: impossible d\'analyser la requ�te SQL'; //'<b>FATAL ERROR</b>: Unable to parse SQL statement; ';
$lang['EM_malformed_type'] = 'type de longueur mal �crit dans le champ � c�t�'; //'malformed type length in field near';
$lang['EM_unmatched_NOT'] = 'NOT non trouv� dans le champ � c�t�'; //'unmatched NOT in field near';
$lang['EM_missing_DEFAULT'] = 'manque DEFAULT dans le champ � c�t�'; //'missing DEFAULT value in field near';
$lang['EM_not_enough'] = 'pas assez de param�tres pour analyser la colonne.'; //'not enough parameters to parse column.';
$lang['EM_improper_key'] = 'KEY mal format�'; //'improperly formated key';
$lang['EM_type_invalid'] = 'type %s non valide'; //'type %s invalid';
$lang['EM_length_invalid'] = 'longueur %s non valide'; //'length %s invalid';
$lang['EM_malformed_DROP'] = 'action DROP mal �crite'; //'malformed DROP action';
$lang['EM_malformed_DROP2'] = 'DROP mal �crit, trop de param�tres.'; //'malformed DROP statement, too many attributes.';
$lang['EM_postgresql_ABORTED'] = 'ARRETE: [%d]<br /> La suppression de champ dans postgresql n\'est pas g�r�. Contacter Nuttzy si vous savez comment le faire de mani�re s�re sans supprimer toute la table.'; //"ABORTED: [%d]<br /> Dropping a field in postgresql was not implemented. Contact Nuttzy if you know how to safely do this without having to drop the whole table.";
$lang['EM_malformed_sql'] = 'Requ�te SQL mal �crite, aucune cible d�finie'; //'malformed SQL, no target defined';
$lang['EM_type_unknown'] = 'type \'%s\' inconnu.'; //'type \'%s\' unknown.';
$lang['EM_subaction_unknown'] = 'sous action \'%s\' inconnue.'; //'subaction \'%s\' unknown.';
$lang['EM_unknown_action'] = 'action \'%s\' inconnue.'; //'action \'%s\' unknown.';
$lang['EM_SQL_line'] = 'Ligne SQL'; //'SQL Line:';

// post process
$lang['EM_pp_step3'] = 'Etape 3 sur 3'; //'Step 3 of 3' ;
$lang['EM_pp_install_comp'] = 'Installation termin�e !'; //'Installation Complete!' ;
$lang['EM_pp_comp_desc'] = 'L\'installation de ce MOD est termin�e ! Vous devez maintenant v�rifier qu\'il fonctionne correctement pour tous les th�mes et toutes les langues.'; //'Installation of this MOD is now complete!  You should verify that the MOD is now functioning properly for all installed themes and languages.' ;
$lang['EM_pp_complete'] = 'Fait'; //'completed' ;
$lang['EM_pp_ready'] = 'Pr�t'; //'ready' ;
$lang['EM_pp_manual'] = 'A faire en manuel !'; //'MANUAL' ;
$lang['EM_pp_from'] = 'Copier de [%s]'; //'Copy From [%s]' ;
$lang['EM_pp_backups'] = 'Sauvegarder dans [%s]'; //'Making Backups in [%s]' ;
$lang['EM_pp_backup'] = 'Sauvegarder'; //'Backup' ;
$lang['EM_pp_download'] = 'T�l�charger'; //'Download' ;
$lang['EM_pp_to'] = 'Vers'; //'To' ;
$lang['EM_pp_status'] = 'Statut'; //'Status' ;

// diy
$lang['DIY_final'] = 'Etape finale'; //'Final Step';
$lang['DIY_Instructions'] = 'Instructions � faire vous m�me'; //'\'Do it yourself\' Instructions';
$lang['DIY_intro'] = 'Les instructions � faire vous m�me doivent ex�cut�es par <strong>vous manuellement</strong>, EasyMOD ne peut <strong>pas</strong> ex�cuter ce type d\'actions'; //'\'Do it yourself\' instructions need to be executed by <strong>you manually</strong>, EasyMOD can <strong>not</strong> perform these actions';
$lang['Final_install_step'] = 'Voir les �tapes d\'installation finales'; //'View the final install steps';
$lang['Install_complete'] = 'Installation termin�e'; //'Installation Complete';

// general use
$lang['EM_next_step'] = 'Etape suivante'; //'Next Step' ;


//
// installer
//
$lang['Safe_mode'] = 'PHP : Mode Safe'; //'Safe Mode';
$lang['Go'] = 'Valider'; //'Go';
$lang['EM_installing_beta1'] = 'Installation d\'EasyMOD beta 1 (%s)'; //'Installing EasyMOD beta 1 (%s)';
$lang['EM_more_info'] = '+ d\'info'; //'More information';
$lang['EM_see_file_access'] = 'Allons voir ce que vous avez en terme de droits sur le serveur. Tout ne doit pas n�cessairement �tre "OK".'; //'Let\'s see what you have for file access.  You do not need everything to read \'ok\'.';
$lang['EN_reinstall_version'] = 'Si vous voulez quand m�me r�installer cette version, changez d\'abord le num�ro de version d\'EasyMOD dans le Panneau d\'administration, menu "Param�trage d\'EM". Ou vous pouvez aussi utiliser "Version changer" de GPHemsley <a href="http://gphemsley.music-hq.net/phpBB/EasyMOD/">ici</a>.'; //'If you are trying to reinstall this version, change the EM version number from the Admin Control Panel under EasyMOD Settings.  Or you could also use the EM Version Changer (by GPHemsley) <a href="http://gphemsley.music-hq.net/phpBB/EasyMOD/">here</a>.';
$lang['EM_simple_mode'] = 'Mode simple'; //'Simple Mode';
$lang['EM_advanced_mode'] = 'Mode avanc�'; //'Advanced Mode';

// translate this and I'll hardcode the message into easymod_install.php
$lang['EM_no_lang'] = '<b>ERREUR CRITIQUE :</b> le fichier lang_easymod.$phpEx n\'a pas pu �tre trouv� dans le r�pertoire easymod. EasyMOD ne peut pas s\'installer sans que ce fichier soit pr�sent.'; //'<b>CRITICAL ERROR:</b> the lang_easymod.$phpEx file could not be found in the easymod directory.  EasyMOD cannot be installed without this file present.' ;


// step 1

$lang['EM_step1'] = '<b>Etape 1 sur 5 :</b> EasyMOD vient de scanner votre serveur pour voir quel type d\'acc�s aux fichiers �tait disponible pour les phases cl�s de lecture, �criture et d�placement de fichiers notamment. EasyMOD vous a recommand� les param�tres qui semblent convenir au mieux � votre configuration.'; //'<b>Step 1 (of 5):</b> Welcome to the EasyMOD installer.  In this step EasyMOD has scanned the server to see what file access is available for the key steps of reading, writing, and moving files.  EasyMOD has recommended what settings seem to best fit your configuration.' ;

$lang['EM_Install_Info'] = 'Informations d\'installation'; //'Install Info' ;
$lang['EM_Select_Language'] = 'Langue'; //'Select Language' ;
$lang['EM_Database_type'] = 'Type de BD'; //'Database type' ;
$lang['EM_phpBB_version'] = 'Version phpBB'; //'phpBB version' ;
$lang['EM_EasyMOD_version'] = 'Version EasyMOD'; //'EasyMOD version' ;
$lang['EM_EM_status'] = 'Type d\'instal.'; //'EM status' ;
$lang['EM_new_install'] = 'Nouvelle instal.'; //'New Install' ;
$lang['EM_update_from'] = 'MAJ d\'EM depuis'; //'Update EM from' ;

$lang['EM_File_Access'] = 'Droits & param�trages serveur'; //'File Access Info' ;
$lang['EM_failed'] = 'Erreur'; //'failed' ;
$lang['EM_unattempted'] = 'Non tent�'; //'unattempted' ;
$lang['EM_no_module'] = 'Module non charg�'; //'module not loaded' ;
$lang['EM_no_problem'] = 'Remarque : �a ne pose pas de probl�me que certains tests �chouent ou ne soient pas tent�s. C\'est normal.'; //'NOTE: there is no problem if some items failed or were not attempted.  This is normal.' ;
$lang['EM_support'] = 'Pour obtenir de l\'aide, rendez-vous au <a href="http://area51.phpbb.com/phpBB/viewforum.php?f=15" target="_blank">QG d\'EasyMOD</a> sur l\'Area51.'; //'For support, visit <a href="http://area51.phpbb.com/phpBB/viewforum.php?f=15" target="_blank">EasyMOD Central</a> over at Area51.' ;

$lang['EM_read_access'] = 'Lecture'; //'read access' ;
$lang['EM_write_access'] = 'Ecriture'; //'write access' ;
$lang['EM_root_write'] = 'Ecrit. ds racine'; //'root path write' ;
$lang['EM_chmod_access'] = 'CHMOD'; //'chmod access' ;
$lang['EM_unlink_access'] = 'UNLINK'; //'unlink access' ;
$lang['EM_mkdir_access'] = 'Cr�ation de r�p.'; //'mkdir access' ;
$lang['EM_tmp_write'] = 'Ecrit. ds tmp'; //'tmp path write' ;
$lang['EM_ftp_ext'] = 'PHP : Extension FTP'; //'FTP extension' ;
$lang['EM_copy_access'] = 'Copie'; //'copy access' ;

$lang['EM_Settings'] = 'Param�trages'; //'Settings' ;
$lang['EM_password_title'] = 'Protection d\'EasyMOD par mot de passe'; //'EasyMOD Password Protection' ;
$lang['EM_password_desc'] = 'Le mot de passe d\'EasyMOD permet de restreindre l\'utilisation d\'EasyMOD � certains administrateurs quand il y en a plusieurs car les possibilit�s d\'EasyMOD peuvent permettre � un administrateur ind�licat d\'obtenir vos comptes et mots de passe de votre base de donn�es et de votre FTP.'; //'The EasyMOD password will allow you to restrict which admins can use EasyMOD.  By having access to EasyMOD an admin could covertly obtain your database user/pass and FTP info.' ;
$lang['EM_password_set'] = 'Mot de passe'; //'Set EM password' ;
$lang['EM_password_confirm'] = 'Confirmation'; //'Confirm EM password' ;
$lang['EM_file_title'] = 'M�thodes d\'acc�s aux fichiers'; //'File Access' ;
$lang['EM_file_desc'] = 'L\'acc�s FTP est la m�thode privil�gi�e pour acc�der aux fichiers. Si vous ne disposez pas d\'un acc�s FTP, EasyMOD vous propose des param�tres alternatifs.'; //'FTP access is the perferred method for file access.  If you do not have FTP access, EasyMOD has recommended alternate settings.' ;
$lang['EM_file_reading'] = 'Lecture'; //'Reading' ;
$lang['EM_file_writing'] = 'Ecriture'; //'Writing' ;
$lang['EM_file_moving'] = 'D�placement'; //'Moving' ;
$lang['EM_file_alt'] = 'Alternative'; //'alternate' ;
$lang['EM_ftp_title'] = 'Informations FTP'; //'FTP Information' ;
$lang['EM_ftp_desc'] = 'Si vous avez un acc�s FTP au serveur Web, entrez le ci-dessous. Les informations seront stock�es de mani�re relativement s�re dans la base de donn�es phpBB. Elles ne seront plus accessibles qu\'� travers EasyMOD.'; //'If you have FTP access to the web server, enter it below.  The info will be stored fairly securely in the phpBB database.  It will only be accessible through EasyMOD.' ;
$lang['EM_ftp_dir'] = 'Chemin FTP de phpBB'; //'FTP path to phpBB2' ;
$lang['EM_ftp_user'] = 'Compte FTP'; //'FTP Username' ;
$lang['EM_ftp_pass'] = 'Mot de passe FTP'; //'FTP Password' ;
$lang['EM_ftp_host'] = 'Serveur FTP'; //'FTP Server' ;
$lang['EM_ftp_host_info'] = '("localhost" doit �tre OK)'; //'(localhost should be fine)' ;
$lang['EM_ftp_port'] = 'Port FTP'; //'FTP Port' ;
$lang['EM_ftp_port_info'] = '("21" doit �tre OK)'; //'(21 should be fine)' ;

$lang['EM_ftp_advance_settings'] = 'Param�tres FTP avanc�s (optionnel)'; //'Advanced FTP Settings (optional!)';
$lang['EM_ftp_debug'] = 'Mode d�bug FTP'; //'FTP Debug Mode' ;
$lang['EM_ftp_debug_not'] = '(n\'utiliser qu\'en cas de pb)'; //'(only use if there is a problem)' ;
$lang['EM_ftp_use_ext'] = 'Extension FTP PHP'; //'PHP FTP Extension' ;
$lang['EM_ftp_ext_not'] = '(ne changer que si demand�)'; //'(only change if instructed to)' ;
$lang['EM_ftp_ext_noext'] = 'Ce n\'est pas une option. Module FTP PHP non charg�'; //'Not an option.  PHP FTP module not loaded.' ;
$lang['EM_ftp_ext_notmp'] = 'Ce n\'est pas une option. Pas d\'acc�s en �criture � /tmp'; //'Not an option.  No /tmp write access.' ;
$lang['EM_ftp_cache'] = 'Utiliser le cache FTP'; //'Use FTP cache' ;
$lang['EM_yes'] = 'Oui'; //'Yes' ;
$lang['EM_no'] = 'Non'; //'No' ;

// simple step 1
$lang['EM_step1_simple_header'] = '<strong>Etape 1 sur 5 :</strong> Bienvenu dans l\'installeur d\'EasyMOD ! EasyMOD va essayer de vous guider � toutes les �tapes de son installation. Et pour commencer, il nous faut savoir quel type de serveur vous avez...'; //'<strong>Step 1 (gathering settings):</strong> Welcome to the EasyMOD installer.  EasyMOD will try to guide you every step of the way.  First, we need to know a little about your server.';
$lang['EM_step1_ftp_header'] = '<strong>Etape 1 sur 5 (suite) :</strong> Vous avez indiqu� utiliser un acc�s FTP donc merci de saisir vos param�tres d\'acc�s FTP ci-dessous.'; //'<strong>Step 1 (gathering settings):</strong> You have specified that you have FTP access.  Enter your FTP information below.';
$lang['EM_step1_password_header'] = '<strong>Etape 1 sur 5 (suite) :</strong> EasyMOD prend la s�curit� tr�s au s�rieux. Un mot de passe permet de restreindre d\'avantage l\'acc�s � EM. Si vous utilisez un acc�s FTP, alors un mot de passe est obligatoire pour que vos param�tres FTP puissent �tre crypt�s de mani�re s�re dans la base de donn�es !'; //'<strong>Step 1 (gathering settings):</strong> EasyMOD takes security very seriously.  A password will further restict who has access.  If you are using FTP, then a password is required so that your FTP information can safely be crypted into the database.';
$lang['EM_server_style'] = 'Type de serveur'; //'Server Style';
$lang['EM_about_server'] = 'A propos de votre serveur'; //'About your server';
$lang['EM_describes_server'] = 'Lequel des choix suivant d�crit le mieux votre situation ?'; //'Which of the following choices best describes your phpBB server:';
$lang['EM_have_ftp'] = 'Mes fichiers phpBB sont h�berg�s et j\'y acc�de par FTP'; //'I have FTP access to my phpBB files on the server.';
$lang['EM_have_windows'] = 'Mes fichiers phpBB sont sur mon ordinateur sous Windows'; //'This is a Windows server and I don\'t have to worry about file permissions.';
$lang['EM_no_ftp_suggest'] = 'Aucun ne convient, ou les 2 ne marchent pas. Qu\'EasyMOD me sugg�re quoi faire'; //'I don\'t have FTP access.  Have EasyMOD suggest what to do please!';
$lang['EM_auto_detect'] = 'Auto d�tection'; //'Auto Detection';
$lang['EM_diagnosis'] = 'Diagnostic'; //'Diagnosis';
$lang['EM_auto_tech_detected'] = 'M�thode automatique d�tect�e'; //'Automation Technique detected!';
$lang['EM_ftp_desc'] = 'Renseignez les informations dont vous avez besoin normalement quand vous voulez acc�der � vos fichiers phpBB en FTP.'; //'Enter the information you would normally need to access your phpBB files via FTP.';

// no write no copy
$lang['Select_one'] = 'S�lectionnez en un:'; //'Select One:';
$lang['EM_nowrite_nocopy__desc'] = 'C\'est le pire cas de figure. EasyMOD n\'a ni les droits de cr�er de nouveaux fichiers, ni ceux de remplacer les vieux fichiers par des nouveaux. Il y a plusieurs choses que vous puissiez faire :<br />
<ol>
<li>Si vous avez un acc�s FTP, alors utilisez l\'option FTP.</li>
{{SAFE_MODE}}<li>Le "safe mode" est activ� sur votre serveur, ce qui veut dire qu\'EasyMOD ne peut pas automatiquement remplacer vos fichiers phpBB. Vous pouvez envisager d\'utiliser <a href="#" target="_blank">chmod</a> ou <a href="#" target="_blank">chown</a> pour lui donner les droits d\'acc�s.</li>
<li>Vous pouvez envisager d\'utiliser <a href="#" target="_blank">chmod</a> pour lui donner les droits d\'acc�s. Cependant, �a n\'est pas conseill� sur un serveur partag�.</li>
<li>Autrement, vous allez devoir t�l�charger les fichiers et les mettre en place manuellement.</li>
</ol>';
$lang['EM_try_ftp'] = 'Je vais essayer d\'utiliser l\'option FTP. (n�cessite un acc�s FTP)'; //'I\'ll try using the FTP option. (requires FTP access)';
$lang['EM_perms_mod_rescan'] = 'J\'ai modifi� mes droits d\'acc�s aux fichiers, essayer de re-v�rifier si EasyMOD y a acc�s.'; //'I have now modified my file permissions, try rescanning to see if EasyMOD has access.';
$lang['EM_download_manual'] = 'Je vais avoir � t�l�charger les fichiers et les mettre en place manuellement'; //'I will have to download the files and manually move them into place';
$lang['EM_select_else'] = 'Je voudrais s�lectionner quelques chose d\'autre. (Mode avanc�)'; //'I\'d like to select something else. (Advanced Mode)';

// write no copy
$lang['EM_write_nocopy_desc'] = 'Probl�me. EasyMOD a le droit de cr�er de nouveaux fichiers, mais n\'a pas le droit de remplacer les anciens fichiers par les nouveaux. Il y a plusieurs choses que vous puissiez faire :<br />
<ol>
<li>Si vous avez un acc�s FTP, alors utilisez l\'option FTP.</li>
{{SAFE_MODE}}<li>Le "safe mode" est activ� sur votre serveur, ce qui veut dire qu\'EasyMOD ne peut pas automatiquement remplacer vos fichiers phpBB. Vous pouvez envisager d\'utiliser <a href="#" target="_blank">chmod</a> ou <a href="#" target="_blank">chown</a> pour lui donner les droits d\'acc�s.</li>
<li>Vous pouvez envisager d\'utiliser <a href="#" target="_blank">chmod</a> pour lui donner les droits d\'acc�s. Cependant, �a n\'est pas conseill� sur un serveur partag�.</li>
<li>Autrement, vous allez devoir utiliser le <a href="#" target="_blank">script post_process</a> ou mettre en place les fichiers manuellement.</li>
</ol>';
$lang['EM_use_post_process'] = 'Je vais utiliser le script post_process pour mettre en place automatiquement les fichiers. (n�cessite la connaissance de comment ex�cuter un script)'; //'I will use the post_process script to automatically move files into place. (requires knowledge of how to execute a script)';

// write and copy
$lang['EM_write_copy_desc'] = 'Bonne nouvelle ! EasyMOD a d�tect� qu\'il a les droits d\'acc�s n�cessaires pour installer automatiquement des MODs. S�lectionnez Oui ci-dessous.'; //'Good news! EasyMOD has detected that it has the necessary access to automatically install MODs.  You should select yes below.';
$lang['EM_yes_use_auto'] = 'Oui, utiliser la m�thode automatique.'; //'Yes, use this automated method.';
$lang['EM_no_use_else'] = 'Non, je voudrais s�lectionner quelque chose d\'autre. (Mode avanc�)'; //'No, I\'d like to select something else. (Advanced Mode)';


// step 2
$lang['EM_step2'] = '<b>Etape 2 sur 5 :</b> Confirmation des param�trages d\'acc�s aux fichiers.'; //'<b>Step 2 (of 5):</b> EasyMOD is now confirming your file access settings.' ;
$lang['EM_test_write'] = 'Test de la m�thode d\'�criture s�lectionn�e'; //'Testing selected write method' ;
$lang['EM_confirm_write'] = 'M�thode d\'�criture valid�e !'; //'Write access method confirmed!';
$lang['EM_confirm_write_server'] = 'Les fichiers modifi�s seront �crits directement sur le serveur.'; //'The modified files will be written on the server.' ;
$lang['EM_confirm_write_ftp'] = 'Les fichiers modifi�s seront �crits en m�moire, puis t�l�charg� en FTP.' ; //"The modified files will be written to a buffer and then FTP'd into place." ;
$lang['EM_confirm_write_local'] = 'Les fichiers modifi�s seront t�l�charg�s en local via votre navigateur Web.'; //'The modified files will be downloaded locally through your web browser.' ;
$lang['EM_confirm_write_screen'] = 'Les fichiers modifi�s seront affich�s sur l\'�cran.'; //'The modified file contents will be displayed on screen.' ;
$lang['EM_test_move'] = 'Test de la m�thode de d�placement s�lectionn�e'; //'Testing selected move method' ;
$lang['EM_test_ftp1'] = 'Connect� avec succ�s'; //'1) Logged in successfully' ;
$lang['EM_test_ftp2'] = 'Changement de r�pertoire r�ussi'; //'2) CD to EasyMOD path successfully' ;
$lang['EM_test_ftp3'] = 'Ecriture r�ussie dans le racine de phpBB'; //'3) wrote to phpBB root successfully' ;
$lang['EM_ftp_sync1'] = 'Vous avez s�lectionn� le FTP pour l\'�criture des fichiers mais pas pour les d�placer. Pour pouvoir utiliser le FTP vous devez s�lectionner cette m�thode en �criture et en d�placement, sinon c\'est impossible.'; //'You have selected FTP for writing files but not for moving them.  You must set both write and move to use FTP or else you cannot use FTP.' ;
$lang['EM_ftp_sync2'] = 'Vous avez s�lectionn� le FTP pour de d�placer les fichiers mais pas pour les �crire. Pour pouvoir utiliser le FTP vous devez s�lectionner cette m�thode en �criture et en d�placement, sinon c\'est impossible.'; //'You have selected FTP for moving files but not for writing them.  You must set both write and move to use FTP or else you cannot use FTP.' ;
$lang['EM_confirm_move'] = 'M�thode de d�placement valid�e !'; //'Move access method confirmed!' ;
$lang['EM_confirm_move_ftp'] = 'Les fichiers phpBB seront remplac�s par les fichiers modifi�s par transfert FTP.'; //'The core phpBB files will automatically be replaced by modified files via FTP.' ;
$lang['EM_confirm_move_copy'] = 'Les fichiers phpBB seront remplac�s par les fichiers modifi�s en utilisant la fonction copy.'; //'The core phpBB files will automatically be replaced by modified files using the copy function.' ;
$lang['EM_confirm_move_exec'] = 'Un script sera g�n�r� que vous pourrez ex�cuter pour remplacer automatiquement les fichiers originaux phpBB par les fichiers modifi�s.'; //'A script will be generated that you can execute to automatically replace the core phpBB files with the modified files.' ;
$lang['EM_confirm_move_ftpm'] = 'Vous avez s�lectionn� de remplacer manuellement les fichiers originaux phpBB par les fichiers modifi�s.'; //'You have selected to manually replace the core phpBB files with the modified files.' ;
$lang['EM_install_EM'] = 'Installer EasyMOD'; //'Install EasyMOD' ;
$lang['EM_confirm_download'] = '<b>IMPORTANT :</b> Pour tester compl�tement la m�thode de t�l�chargement, assurez vous que vous pouvez t�l�charger ce fichier. Si cela �choue, vous ne pouvez pas utiliser la m�thode d\'�criture "DL en local" et devez cliquer sur le bouton "Re scanner" pour en s�lectionner une autre.'; //'<b>IMPORTANT:</b> To fully test the download method, make sure you can download this file.  If it fails, you cannot use the "download" write method and should press "Rescan" to select another option.' ;

// step 2 ftp test
$lang['EM_ftp_testing'] = 'Test d\'acc�s FTP...'; //'Testing FTP access...' ;
$lang['EM_ftp_fail_conn'] = 'ERREUR FTP : �chec de la connexion sur %s:%s.'; //'FTP ERROR: connection to %s:%s failed.' ;
$lang['EM_ftp_fail_conn_lh'] = 'Cette erreur arrive fr�quemment, particuli�rement avec des h�bergeurs comme Lycos. Retournez � l\'�tape 1 et essayez de changer le serveur FTP de \'localhost\' � celui d\'un h�bergeur que vous utilisez couramment en FTP.'; //"This error occurs frequently, particularly on hosts like Lycos.  Back on step 1 you should try changing the FTP Server from 'localhost' to whatever hostname you typically use when you FTP." ;
$lang['EM_ftp_fail_conn_21'] = 'Cette erreur arrive fr�quemment quand le port FTP n\'est pas correct. Retournez � l\'�tape 1 et changez le port FTP 21 pour celui que vous utilisez habituellement pour vous connecter en FTP.'; //'This error occurs frequently when the port number is incorrect.  Back on step 1 you should try changing the FTP Port from 21 to whatever port you typically use when you FTP.' ;
$lang['EM_ftp_fail_conn_invalid'] = 'La connexion a �chou�e parce que vous avez renseign� un mauvais nom de serveur FTP. Son nom ne peut contenir ni slashs (/ ou \\), ni deux points (:). Essayez de re-saisir de champ Serveur FTP.'; //"The connection failed because it appears you have provided an invalid FTP Server hostname.  Hostnames cannot have slashes (/ or \\) or colons (:) in the name.  Try reentering the FTP Server field." ;
$lang['EM_ftp_fail_conn_invalid2'] = 'La connexion a �chou�e parce que vous avez renseign� un mauvais port FTP. Les ports ne doivent contenir que des nombres de 0 � 9. Essayez de resaisir le port FTP.'; //"The connection failed because it appears you have provided an invalid FTP Server port.  Ports must only contain the numbers 0 through 9.  Try reentering the FTP Port field." ;
$lang['EM_fail_conn_info'] = 'Il est impossible de se connecter au serveur FTP que vous avez sp�cifi�. Celui-ci est recommand� :'; //'The FTP Server you have specified could not be connected to.  The following is recommended:';
$lang['EM_fail_conn_op1'] = 'Avez vous essay� le param�trage par d�faut <b>localhost</b> pour le serveur, et 21 pour le port? Celui-ci doit �tre essay� en premier.'; //'Have you tried the default settings of <b>localhost</b> for the hostname and <b>21</b> for the port?  These should be tried first.' ;
$lang['EM_fail_conn_op2'] = 'Avez vous correctement saisi le nom du serveur FTP ? Essayez de le re-saisir.'; //'Did you correctly enter the hostname?  Try reentering.' ;
$lang['EM_fail_conn_op3'] = 'Etes vous s�rs que vous avez acc�s en FTP aux fichiers phpBB ? C\'est bien �videment une obligation.'; //'Are you sure you have FTP access to the phpBB2 files?  Obviously this is a requirement.' ;
$lang['EM_fail_conn_op4'] = 'Certains serveur ont des difficult�s avec la m�thode fsockopen qu\'EasyMOD essai d\'utiliser par d�faut. Si vous avez l\'extension FTP PHP charg�e, alors activez cette option � l\'�tape 1.'; //"Some servers have issues with the fsockopen method that EasyMOD attempts to use by default.  If you have the PHP FTP extension loaded, then enable that option in step 1." ;
$lang['EM_fail_login'] = 'ERREUR FTP : �chec sur le login (compte utilisateur)'; //'FTP ERROR: login failed' ;
$lang['EM_fail_login_info'] = 'La connexion au serveur FTP s\'est r�alis�e, mais le login (compte utilisateur) et le mot de passe ont �t� refus�s. EasyMOD vous recommande :'; //'The FTP Server was connected to, but the username and password were rejected.  The following is recommended:' ;
$lang['EM_fail_login_op1'] = 'Avez vous correctement saisi vos login (compte utilisateur) et mot de passe ? Faites attention que la touche CAPS LOCK (majuscules) ne soit pas enclench�e et re-essayez.'; //'Did you correctly type the username and password?  Make sure your CAPS LOCK key is off and try again.' ;
$lang['EM_fail_login_op2a'] = 'Si vous �tes s�rs � 100% que votre login (compte utilisateur), votre mot de passe sont corrects, alors peut �tre que vous ne vous connectez pas au bon serveur FTP ou sur le bon port. Essayez de changer la valeur du champ Serveur FTP de \'localhost\' au nom du serveur FTP utilis� et le port FTP de 21 au port utilis�.'; //'If you are 100% certain your user/pass is correct, then perhaps you are not connecting to the correct host or on the correct port.  Try changing your FTP Server entry from localhost to the actual ftp host name and your FTP Port entry from 21 to the actual ftp port.' ;
$lang['EM_fail_login_op2b'] = 'Peut �tre que vous ne vous connectez pas au bon serveur FTP. Essayez de remettre \'localhost\' dans le champ Serveur FTP ou v�rifiez que vous avez correctement saisi le nom de votre serveur FTP.'; //'Perhaps you are not connecting to the correct host.  Try changing your FTP Server entry back to localhost or verify you have correctly entered the ftp host name.' ;
$lang['EM_fail_pwd'] = 'ERREUR FTP : �chec de la commande pwd (print working directory)'; //'FTP ERROR: pwd failed' ;
$lang['EM_fail_pwd_info'] = 'Vous vous �tes connect� avec succ�s au serveur, mais la commande pwd (print working directory) a �chou�e.'; //'You successfully logged into the server, but the pwd command (print working directory) failed.' ;
$lang['EM_fail_cd'] = 'ERREUR FTP : impossible de changer de r�pertoire vers %s'; //'FTP ERROR: could not cd to %s' ;
$lang['EM_fail_cd_info'] = 'Vous vous �tes connect� avec succ�s au serveur, mais impossible de changer de r�pertoire (cd) vers celui d\'EasyMOD'; //'You successfully logged into the server, but could not change direcory (CD) to the easymod directory.  The following is recommended:' ;
$lang['EM_fail_cd_op1'] = '<b>Important :</b> il semble que vous ayez inclus le nom de domaine dans le chemin du serveur FTP. Pour le plupart des serveurs c\'est incorrect. Essayez de re-saisir le chemin du serveur FTP sans inclure le nom de domaine.'; //'<b>Important:</b> It appears you are including a domain name in the FTP Path setting.  For most servers this is incorrect.  Try reentering the FTP Path setting without the domain name included.' ;
$lang['EM_fail_cd_op2'] = '<b>Important :</b> vous avez un slash (/) � la fin du chemin du serveur FTP. Enlevez le et r� essayez.'; //'<b>Important:</b> You have a slash (/) at the end of your FTP Path.  Try removing this and retrying.' ;
$lang['EM_fail_cd_op3'] = 'Etes vous s�r que vous avez saisi le bon chemin ? Ci-dessous vous avez la liste des fichiers et r�pertoires dans la racine du FTP. Le r�pertoire racine du FTP est juste le point de d�part quand vous vous connectez. Le chemin vers l\'installation de phpBB doit d�marrer par l\'un de ces r�pertoires list�s ci-dessous.'; //'Are you sure you entered the correct path?  Below is a directory listing of the files in the FTP root directory.  The FTP root directory is simply the starting point when you connect.  The path to the phpBB2 installation should begin with one of the directory names listed below.' ;
$lang['EM_fail_cd_op4'] = 'Le nom des r�pertoires est sensible aux minuscules/majuscules. Assurez vous que le r�pertoire easymod est enti�rement en minuscules.'; //'Directory names are case sensitive.  Be sure the easymod directory is all lowercase.' ;
$lang['EM_fail_cd_op5'] = 'Dans quelques *tr�s rares* cas il est possible que vous ne vous connectiez pas au bon serveur FTP. Essayez de saisir le nom du serveur FTP dans le champ serveur FTP et le port dans le champ port FTP.'; //"In some *very rare* cases it's possible that you are not connecting to the proper FTP Server.  Try specifying the hostname in the FTP Server field and the port in the FTP Port field." ;
$lang['EM_fail_cd_op6'] = 'Certains serveurs ont des difficult�s avec le mode passif qu\'EasyMOD essai d\'utiliser par d�faut. Si vous avez l\'extension FTP PHP charg�e, alors activez cette option � l\'�tape 1.'; //"Some servers have issues with the passive mode that EasyMOD attempts to use by default.  If you have the PHP FTP extension loaded, then enable that option in step 1." ;
$lang['EM_fail_cd_pwd'] = 'Erreur FTP : les informations sur le r�pertoire n\'ont pas pu �tre obtenue. Cela indique souvent la solution 4 list�e ci-dessous.'; //'FTP Error: Directory info could not be obtained.  This usually indicates solution 4 listed above.' ;
$lang['EM_fail_cd_nlist'] = 'Erreur FTP : la liste des fichiers n\'a pas pu �tre obtenue. Cela indique souvent la solution 4 list�e ci-dessous.'; //'FTP Error: A file listing could not be obtained.  This usually indicates solution 4 listed above.' ;
$lang['EM_fail_cd_nlist_no'] = 'Aucun fichier � lister.'; //'No files to list.' ;
$lang['EM_ftp_root'] = 'R�pertoire racine FTP :'; //'FTP root directory:' ;
$lang['EM_dir_list'] = '<b>Contenu du r�pertoire :</b> votre chemin FTP doit d�buter de l\'un de ces r�pertoires list�s ci-dessous.'; //'Directory listing:</b> your FTP Path should start with one of the directories listed below' ;
$lang['EM_fail_put'] = 'ERREUR FTP : impossible d\'�crire dans la racine de phpBB'; //'FTP ERROR: could not write to phpBB root' ;
$lang['EM_fail_put_info'] = 'EasyMOD n�cessite que votre compte %s ait des droits en �criture sur tous les sous r�pertoires et fichiers dans le r�pertoire phpBB. Merci de v�rifier qu\'au minimum tous les r�pertoires et fichiers aient les droits d\'acc�s 744.'; //'EasyMOD requires that your <b>%s</b> account have write access on all directories and files in the phpBB directory.  Please confirm all files and directories are set to at least 744 access.' ;
$lang['EM_ftp_phpbb_root'] = 'R�pertoire racine de phpBB :'; //'phpBB root directory:' ;
$lang['EM_fail_reput'] = 'ERREUR FTP : impossible d\'�craser le fichier test de la racine phpBB'; //'FTP ERROR: could not overwrite phpBB root test file' ;
$lang['EM_fail_delete'] = '<b>AVERTISSEMENT FTP :</b> impossible de supprimer le fichier test (non critique)'; //'<b>FTP WARNING:</b> could not remove test file (not critical)' ;

// step 3
$lang['EM_step3'] = '<b>Etape 3 sur 5 :</b> EasyMOD va maintenant s\'installer comme il le ferait pour n\'importe quel MOD. Cela se fait en 2 �tapes consistant d\'abord � pr�parer les fichiers modifi�s et les actions � r�aliser, puis � mettre en place ces fichiers. Les fichiers modifi�s ne remplaceront donc les fichiers phpBB qu\'� la prochaine �tape.'; //'<b>Step 3 (of 5):</b> EasyMOD is now installing itself as it would any MOD.  There is a two step process of first creating the modified files and then moving them into place.  The modified file(s) do no effect the core phpBB files in any way until the next step.  Click the "Complete Processing" button to move the files into place.' ;
$lang['EM_processing_files'] = 'Fichiers trait�s'; //'Processing Files' ;
$lang['EM_parsing'] = 'Analyse de'; //'Parsing' ;
$lang['EM_finding'] = 'Trouver'; //'Finding' ;
$lang['EM_insert'] = 'Ins�rer'; //'Insert' ;
$lang['EM_ifinding'] = 'Dans la ligne, trouver'; //'In-line Finding' ;
$lang['EM_iafter'] = 'Dans la ligne apr�s, ajouter'; //'In-line after, add' ;
$lang['EM_before'] = 'avant'; //'before' ;
$lang['EM_after'] = 'apr�s'; //'after' ;
$lang['EM_build_post'] = 'Cr�ation des actions � r�aliser apr�s pr�paration du nouveau code'; //'Building Post Process Actions' ;
$lang['EM_build_post_desc'] = 'Les actions suivantes seront ex�cut�es dans la derni�re �tape '; //'The following actions will be executed in the final step' ;
$lang['EM_complete_processing'] = 'Poursuivre l\'installation'; //'Complete Processing' ;

// step 4
$lang['EM_step4'] = '<b>Etape 4 sur 5 :</b> En fonction des choix que vous avez fait, vos fichiers modifi�s viennent d\'�tre soit mis en place, soit pr�par�s pour �tre mis en place manuellement. S\'il n\'y a pas eu d\'erreur, cliquez sur le bouton "Terminer l\'installation" pour mettre � jour votre base de donn�es et ainsi terminer l\'installation d\'EasyMOD.'; //'<b>Step 4 (of 5):</b> Depending on your selection, the modified files have been automatically moved into place or prepared for you to move them manually.  If there are no errors, click the "Confirm" button to update your database and complete the installation process.' ;
$lang['EM_add_db'] = 'Ajout d\'une table EasyMOD � votre base de donn�es'; //'Adding EasyMOD table to your database' ;
$lang['EM_exec_sql'] = 'Requ�te SQL'; //'Executing SQL' ;
$lang['EM_progress'] = 'Etat d\'avancement'; //'Progress' ;
$lang['EM_done'] = 'Termin�'; //'Done' ;
$lang['EM_result'] = 'R�sultat'; //'Result' ;
$lang['EM_already_exist'] = 'La table a d�j� �t� cr��e'; //'The table was previously created' ;
$lang['EM_failed_sql'] = 'Certaines requ�tes ont �chou�es. Les instructions et erreurs sont list�es ci-dessous'; //'Some queries failed, the statements and errors are listing below' ;
$lang['EM_no_worry'] = 'Cela n\'a � priori rien d\'inqui�tant, l\'installation va continuer. N�anmoins, si les requ�tes ne se terminent pas correctement, demandez de l\'aide aupr�s de l\'�quipe de d�veloppement.'; //'This is probably nothing to worry about, install will continue. Should this fail to complete you may need to seek help at our development board.' ;
$lang['EM_no_errors'] = 'Pas d\'erreur'; //'No errors' ;
$lang['EM_update_db'] = 'Mise � jour des donn�es de la table EasyMOD'; //'Updating EasyMOD table data' ;
$lang['EM_store_entries'] = 'Ecriture des donn�es de configuration dans la table'; //'Storing config table entries' ;
$lang['EM_do_worry'] = 'Impossible de mettre � jour la table. Quelque chose ne va pas et l\'installation ne peut pas se terminer.'; //'Could not successfully update table.  Something is wrong and install cannot complete.' ;
$lang['EM_complete_post'] = 'R�sum� des actions effectu�es'; //'Completing Post-Process' ;
$lang['EM_completed'] = 'Termin�'; //'Completed' ;
$lang['EM_admin_panel'] = 'Vous pouvez maintenant aller dans le panneau d\'administration en s�lectionnant "Installation" dans la rubrique "Gestion des MODs". Retourner � l\'<a href="../../../index.php">index du forum</a>.'; //'You can now proceed to the Admin Control Panel and select "Install MODs" under "MOD Center".  You may install the included MODs if you desire.  Return to <a href="../../../index.php">Forum Index</a>.' ;
$lang['EM_confirm'] = 'Terminer l\'installation'; //'Confirm' ;
$lang['EM_move_files'] = '<b>IMPORTANT :</b> avant de cliquer sur le bouton "Terminer l\'installation", veuillez mettre en place manuellement les fichiers modifi�s (pr�par�s).'; //'<b>IMPORTANT:</b> Before pressing confirm, move files into place.' ;

// step 5
$lang['EM_step5'] = '<b>Etape 5 sur 5 :</b> Voil�, c\'est la derni�re �tape. EasyMOD vous confirme maintenant que tous les fichiers ont bien �t� mis en place. Et si c\'est le cas, que votre base de donn�es a bien �t� mise � jour et que l\'installation est termin�e.'; //'<b>Final Step:</b> EasyMOD is now confirming all files have been correctly moved into place.  If confirmed, then your database will be updated and installation will be complete!' ;
$lang['EM_confirming_mod'] = 'Confirmation des modifications'; //'Confirming Modifications' ;
$lang['EM_confirmed'] = 'Confirm�'; //'Confirmed!' ;
$lang['EM_confirm_lang'] = 'Recherche de lang_admin.php '; //'lang_admin.php, looking for' ;
$lang['EM_confirm_admin'] = 'Recherche d\'admin_easymod.php '; //'admin_easymod.php, looking for' ;
$lang['EM_confirm_exist'] = 'v�rification de son existence'; //'verifying existence' ;
$lang['EM_confirm_failed'] = 'L\'installation a �chou�e'; //'Install Failed' ;
$lang['EM_confirm_fix'] = 'EasyMOD n\'est pas correctement install� et vous allez devoir r�gler les erreurs ci-dessous.'; //'EM is not properly installed and you will need to fix the above error(s).' ;
$lang['EM_install_completed'] = 'Installation confirm�e. EasyMOD est install� !'; //'Installation Confirmed.  EasyMOD is installed!' ;

// debug info
$lang['EM_debug_header'] = '<b>Informations de d�bugage :</b> Les informations ci-dessous ont �t� pr� format�es pour �tre affich�es au mieux dans un message sur un forum phpBB.'; //'<b>Debug Info:</b> The following information about your system config has been formatted for display in a forum post.' ;
$lang['EM_debug_display'] = 'Afficher les infos de d�bugage'; //'Display Debug Info' ;
$lang['EM_debug_info'] = 'Informations d�taill�es de d�bugage'; //'Expanded Debug Info' ;
$lang['EM_debug_format'] = 'format� pour �tre post� sur un forum phpBB'; //'formatted for forum posting' ;
$lang['EM_debug_installer'] = 'Installeur d\'EasyMOD'; //'EM installer' ;
$lang['EM_debug_work_dir'] = 'R�pertoire de travail'; //'Working Dir' ;
$lang['EM_debug_step'] = 'Etape d\'installation'; //'Install Step' ;
$lang['EM_debug_mode'] = 'Mode'; //'Mode' ;
$lang['EM_debug_the_error'] = 'L\'erreur'; //'The Error' ;
$lang['EM_debug_no_error'] = 'Pas d\'erreur'; //'No error.' ;
$lang['EM_debug_permissions'] = 'Droits et param�trages du serveur'; //'Permissions' ;
$lang['EM_debug_sys_errors'] = 'y compris les erreurs syst�me'; //'including system errors' ;
$lang['EM_debug_recommend'] = 'Recommandations d\'EasyMOD'; //'Recommendations' ;
$lang['EM_debug_write'] = 'Ecriture'; //'write' ;
$lang['EM_debug_move'] = 'D�placement'; //'move' ;
$lang['EM_debug_ftp_dir'] = 'R�p. FTP'; //'ftp dir' ;
$lang['EM_debug_ftp_host'] = 'Serveur FTP'; //'ftp host';
$lang['EM_debug_ftp_post'] = 'Port FTP'; //'ftp port';
$lang['EM_debug_ftp_debug'] = 'D�but FTP'; //'ftp debug' ;
$lang['EM_debug_ftp_ext'] = 'Ext. FTP'; //'ftp ext' ;
$lang['EM_debug_ftp_cache'] = 'Cache ftp'; //'ftp cache';
$lang['EM_debug_ftp_notest'] = 'FTP non test� car non utilis�.'; //'Not testing FTP since it is not being used.' ;
$lang['EM_debug_selected'] = 'Param�trages utilisateur'; //'Selected settings' ;
$lang['EM_debug_listing'] = 'Dir du r�pertoire de travail'; //'CWD Listing' ;		// cwd = current working directory
$lang['EM_debug_ftp_test'] = 'Test d\'acc�s FTP'; //'FTP access test' ;
$lang['EM_debug_success'] = 'r�ussi'; //'successful' ;

// forms
$lang['Submit'] = 'Valider'; //'Submit';
$lang['Rescan'] = 'Re scanner'; //'Rescan';


//
// errors
//
$lang['EM_err_warning'] = 'Avertissement'; //'Warning' ;
$lang['EM_err_error'] = 'Erreur'; //'Error' ;
$lang['EM_err_critical_error'] = 'Erreur critique'; //'Critical Error' ;
$lang['EM_err_secondary'] = 'Erreur secondaire - critique'; //'Secondary Error - Critical' ;
$lang['EM_err_cwd'] = 'R�pertoire de travail actuel'; //'Current working directory' ;
$lang['EM_err_install_dir'] = '<b>Erreur critique :</b> EasyMOD n\'est pas dans le bon r�pertoire pour �tre install�. Il doit �tre plac� dans le r�pertoire admin/mods/easymod de la racine de phpBB avant toute installation.<br />'; //'<b>Critical Error:</b> EasyMOD is not in the correct directory to be installed.  It must be placed in a admin/mods/easymod off the phpBB root prior to installation.<br />' ;
$lang['EM_err_no_subsilver'] = '<b>Erreur critique :</b> EasyMOD ne peut pas �tre install�. Le th�me subSilver n\'est pas pr�sent dans le r�pertoire templates. Ce th�me est n�cessaire � l\'installation des MODs. Le th�me subSilver est disponible en standard en t�l�chargeant phpBB � l\'adresse <a href="http://www.phpbb.com">www.phpbb.com</a>.'; //'<b>Critical Error:</b> EasyMOD cannot be installed.  The subSilver template is not present in the templates directory.  This template is required as the baseline template for MOD installations.  The subSilver template is provided in the standard phpBB download at <a href="http://www.phpbb.com">www.phpbb.com</a>.' ;
$lang['EM_err_no_english'] = '<b>Erreur critique :</b> EasyMOD ne peut pas �tre install�. La langue anglaise n\'est pas pr�sent dans le r�pertoire language. Cette langue est n�cessaire � l\'installation des MODs. La langue anglaise est disponible en standard en t�l�chargeant phpBB � l\'adresse <a href="http://www.phpbb.com">www.phpbb.com</a>.'; //'<b>Critical Error:</b> EasyMOD cannot be installed.  The English language package is not present in the language directory.  This language is required as the baseline language package for MOD installations.  The English language package is provided in the standard phpBB download at <a href="http://www.phpbb.com">www.phpbb.com</a>.' ;
$lang['EM_err_dupe_install'] = 'Cette version d\'EasyMOD est d�j� install�e. La proc�dure d\'installation doit donc �tre arr�t�e pour emp�cher toute r�installation de la m�me version.'; //'This version of EM has already been installed.  Terminating to prevent reinstallation.' ;
$lang['EM_err_pw_match'] = '<b>ERREUR :</b> les mots de passe ne sont pas les m�mes. Merci de r�essayez en cliquez sur le bouton "Re scanner".'; //'<b>Error:</b> The EasyMOD passwords do not match.  Please retry by pressing the "Rescan" button.' ;
$lang['EM_err_acc_write'] = '<b>ERREUR D\'ACCES :</b> phpBB n\'a pas les droits pour �crire dans le r�pertoire d\'EasyMOD.'; //'<b>ACCESS ERROR:</b> phpBB does not have permission to write to the EasyMOD directory.' ;
$lang['EM_err_acc_mkdir'] = '<b>ERREUR D\'ACCES :</b> phpBB n\'a pas les droits pour cr�er un nouveau r�pertoire.'; //'<b>ACCESS ERROR:</b> phpBB does not have permission to create new directories.' ;
$lang['EM_err_copy'] = '<b>ERREUR DE COPIE :</b> vous n\'avez pas acc�s � la fonction copy. La m�thode de d�placement ne peut pas �tre utilis�e.'; //'<b>COPY ERROR:</b> You do not have copy access.  Move method cannot be used.' ;
$lang['EM_err_no_write'] = '<b>ERREUR DE DEPLACEMENT :</b> la m�thode d\'�criture que vous avez s�lectionn� ne permet pas de cr�er des fichiers sur le serveur. En cons�quence, l\'utilisation des m�thodes "par copy" ou "par FTP" n\'est pas possible pour le d�placement des fichiers.'; //'<b>MOVE ERROR:</b> The write method you have selected does not create the files on there server.  Therefore, using either automated FTP or the copy method is not permitted for the move method.' ;
$lang['EM_err_config_table'] = 'Impossible d\'obtenir la liste des tables de configuration'; //'Could not obtain Config Table list' ;
$lang['EM_err_open_pp'] = '<b>Erreur critique :</b> impossible d\'ouvrir pour �criture le fichier des actions � r�aliser apr�s la phase de pr�paration du nouveau code en �tape 1.'; //'<b>Critical Error:</b> Cannot open post process file for writing.' ;
$lang['EM_err_attempt_remainder'] = 'Echec de la tentative de traitement de la fin des actions � r�aliser apr�s la phase de pr�paration du nouveau code en �tape 1'; //'ATTEMPING REMAINDER OF POST PROCESS' ;
$lang['EM_err_write_pp'] = '<b>Erreur critique :</b> impossible de terminer l\'�criture du fichier des actions � r�aliser apr�s la phase de pr�paration du nouveau code en �tape 1.'; //'<b>Critical Error:</b> Unable to complete writing of post process file.' ;
$lang['EM_err_no_step'] = '<b>Erreur critique :</b> �tape d\'installation ind�finie'; //'<b>Critical Error:</b>  Undefinied install step.' ;
$lang['EM_err_insert'] = 'Impossible d\'ins�rer l\'information de configuration %s'; //'Could not insert %s config information.' ;
$lang['EM_err_update'] = 'Impossible de mettre � jour l\'information de configuration %s'; //'Could not update %s config information.' ;
$lang['EM_err_find'] = 'N\'a pas pu �tre trouv�'; //'Could not find' ;
$lang['EM_err_pw_fail'] = 'MOT DE PASSE NON VALIDE !'; //'INVALID PASSWORD SUPPLIED' ;
$lang['EM_err_find_fail'] = 'ECHEC DE LA RECHERCHE : [%s] n\'a pas pu �tre trouv� dans le fichier'; //'FIND FAILED: In file [%s] could not find' ;
$lang['EM_err_ifind_fail'] = 'ECHEC DE LA RECHERCHE DANS LA LIGNE : [%s] n\'a pas pu �tre trouv� dans la ligne'; //'IN-LINE FIND FAILED: In file [%s] could not find' ;

// admin_easymod errors
$lang['EM_trace'] = 'Fonction trace'; //'Function Trace' ;
$lang['EM_FAQ'] = 'FAQ'; //'FAQ' ;
$lang['EM_report'] = 'Rapport d\'erreur'; //'Report' ;
$lang['EM_error_detail'] = 'D�tail de l\'erreur'; //'Error Detail' ;
$lang['EM_line_num'] = 'Ligne du script n�'; //'MOD script line #' ;
$lang['EM_err_config_info'] = 'Impossible d\'obtenir les informations de configuration'; //'Could not obtain Config information' ;
$lang['EM_err_no_process_file'] = '<b>Erreur critique :</b> il n\'y a aucun fichier � traiter'; //'Critical Error: There is no filed specified to process.' ;
$lang['EM_err_set_pw'] = 'Les mots de passe EasyMOD ne sont pas identiques. Param�trage non mis � jour.'; //'The EasyMOD passwords do not match.  Settings not updated.' ;
$lang['EM_err_em_info'] = 'Impossible d\'obtenir les informations sur EasyMOD'; //'Could not obtain EasyMod information' ;
$lang['EM_err_phpbb_ver'] = 'Impossible d\obtenir la version de phpBB'; //'Could not obtain phpBB version info' ;
$lang['EM_err_backup_open'] = 'Impossible d\'ouvrir et lire [%s].'; //'Could not open [%s] for reading.' ;
$lang['EM_err_no_find'] = 'ECHEC : erreur de r�daction du script. Une action FIND n\'a pas �t� ex�cut�e pr�c�demment.'; //'FAILED: malformed script.  A FIND was not previously performed.' ;
$lang['EM_err_comm_open'] = 'OUVERTURE IMPOSSIBLE : aucun nom de fichier n\'est pr�cis� dans le script du MOD'; //'OPEN FAILED: No file name supplied in MOD script' ;
$lang['EM_err_comm_find'] = 'RECHERCHE IMPOSSIBLE : aucune cha�ne � rechercher n\'est pr�cis�e dans le script du MOD'; //'FIND FAILED: No target supplied for FIND command in MOD script' ;
$lang['EM_err_inline_body'] = 'ECHEC : commande du script du MOD incorrecte'; //'FAILED: Invalid command body supplied in MOD script' ;
$lang['EM_err_increment_body'] = 'ECHEC: param�trage de l\'action dans le MOD non valide'; //'FAILED: Invalid command body supplied in MOD script' ;
$lang['EM_err_no_ifind'] = 'ECHEC : erreur de r�daction du script. Une action IN-LINE FIND n\a pas �t� ex�cut�e pr�c�demment.'; //'FAILED: Malformed script.  An IN-LINE FIND was not previously performed.' ;
$lang['EM_err_comm_copy'] = 'COPIE IMPOSSIBLE : le fichier � copier [%s%s] n\'a pas pu �tre trouv�.'; //'COPY FAILED: The target file to be copied [%s%s] could not be found.' ;
$lang['EM_err_modify'] = '<b>ERREUR CRITIQUE :</b> impossible de trouver [%s]'; //'CRITICAL ERROR: Could not modify [%s]' ;
$lang['EM_err_theme_info'] = 'Impossible d\'interroger la base de donn�es pour des informations sur le th�me'; //'Could not query database for theme info' ;
$lang['EM_err_copy_format'] = 'Impossible d\'ex�cuter une action COPY mal r�dig�e.'; //'Could not perform improperly formed COPY command.' ;

// mod_io errors
$lang['EM_modio_mkdir_chdir'] = 'ERREUR FTP : impossible de changer de r�pertoire pour [%s]<br />R�pertoire actuel : [%s]'; //'FTP ERROR: could not change directory to [%s]<br />Current dir: [%s]' ;
$lang['EM_modio_mkdir_mkdir'] = 'ERREUR FTP : impossible de cr�er le r�pertoire [%s]<br />R�pertoire actuel : [%s]'; //'FTP ERROR: could not make directory [%s]<br />Current dir: [%s]' ;
$lang['EM_modio_open_read'] = 'Impossible d\'ouvrir et lire [%s].'; //'Could not open [%s] for reading.' ;
$lang['EM_modio_open_write'] = 'Impossible d\'ouvrir [%s] pour �crire.'; //'Could not open [%s] for writing.' ;
$lang['EM_modio_open_none'] = 'M�thode d\'�criture non reconnue.'; //'Write method not recognized.' ;
$lang['EM_modio_close_chdir'] = 'ERREUR FTP : impossible de changer de r�pertoire pour [%s]'; //'FTP ERROR: could not change directory to [%s]' ;
$lang['EM_modio_close_ftp'] = 'ERREUR FTP : impossible d\'�crire le fichier [%s]'; //'FTP ERROR: could not write file [%s]' ;
$lang['EM_modio_prep_conn'] = 'ERREUR FTP : impossible de se connecter au serveur'; //'FTP ERROR: could not connect to localhost' ;
$lang['EM_modio_prep_login'] = 'ERREUR FTP : �chec de connexion'; //'FTP ERROR: login failed' ;
$lang['EM_modio_prep_chdir'] = 'ERREUR FTP : impossible de changer de r�pertoire pour la racine de phpBB'; //'FTP ERROR: could not chdir to phpBB root directory' ;
$lang['EM_modio_move_copy'] = 'ERREUR DE COPIE : impossible de d�placer le fichier [%s] vers [%s]'; //'COPY ERROR: could not move file [%s] to [%s]' ;
$lang['EM_modio_move_ftpa'] = 'ERREUR FTP : impossible de d�placer le fichier [%s] vers [%s]'; //'FTP ERROR: could not move file [%s] to [%s]' ;

// EasyMOD Installer Help
$lang['EM_installer_help'] = 'Aide de l\'installeur d\'EasyMOD'; //'EasyMOD Installer Help' ;
$lang['help']['file_writing'][] = 'Ecriture de fichiers'; //'<b>File Writing</b>' ;
$lang['help']['file_writing'][] = 'A r�diger...'; //'File writing help here...' ;

$lang['help']['file_moving'][] = 'D�placement de fichier'; //'<b>File Moving</b>' ;
$lang['help']['file_moving'][] = 'A r�diger...'; //'Blah, Blah, Moving info here' ;
$lang['help']['ftp_dir'][] = 'R�pertoire FTP'; //'<b>FTP Directory</b>' ;
$lang['help']['ftp_dir'][] = 'A r�diger...'; //'Stuff here' ;
$lang['help']['ftp_host'][] = 'Serveur FTP'; //'<b>FTP Host</b>' ;
$lang['help']['ftp_host'][] = 'A r�diger...'; //'"Old McDonald had a farm, EIEIO..."' ;
$lang['help']['ftp_port'][] = 'Port FTP'; //'<b>FTP Port</b>' ;
$lang['help']['ftp_port'][] = 'A r�diger...'; //'I think we get the drift...' ;
$lang['help']['ftp_php_ext'][] = 'Extension FTP PHP'; //'<b>FTP PHP Extension</b>' ;
$lang['help']['ftp_php_ext'][] = 'A r�diger...'; //'php extension information' ;
$lang['help']['ftp_cache'][] = 'Cache FTP'; //'<b>FTP Cache</b>' ;
$lang['help']['ftp_cache'][] = 'A r�diger...'; //'cache stuff' ;
?>