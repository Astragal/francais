Subject: Compte activ�
Charset: iso-8859-1

Bonjour {USERNAME},

Votre inscription sur le forum � {SITENAME} � vient d'�tre valid�e par un administrateur.

Vous pouvez vous connecter en utilisant les nom d'utilisateur et mot de passe que vous avez choisis.

Il nous serait agr�able de vous compter longtemps parmi nous, aussi nous vous invitons � participer aux conversations que vous engagez ou, du moins, � donner � la fin votre avis sur les r�ponses � vos questions, ainsi qu'il est de mise sur ce forum.

Dans votre profil, n'oubliez pas de cocher � Oui � dans l'option � Toujours m'avertir des r�ponses �.

Bienvenue parmi les membres du forum.

{EMAIL_SIG}