Subject: Notification de r�ponse au sujet - {TOPIC_TITLE}
Charset: iso-8859-1

Sujet  : {TOPIC_TITLE}
Auteur : {POSTERNAME}
Forum  : {FORUM_NAME}

{U_TOPIC}

----------

Vous recevez ce courriel parce que vous surveillez le forum � {FORUM_NAME} � sur ��{SITENAME}��. Un des sujets de ce forum a re�u une r�ponse depuis votre derni�re visite. Vous pouvez utiliser le lien ci-dessus pour voir les r�ponses qui ont �t� faites ; aucune autre notification ne vous sera envoy�e jusqu'� ce que vous visitiez le sujet.
Si vous ne voulez plus surveiller le forum ��{FORUM_NAME}��, vous pouvez soit cliquer sur le lien � Arr�ter de surveiller ce forum � sur le site, soit cliquer sur le lien suivant :

{U_STOP_WATCHING_FORUM}

{EMAIL_SIG}