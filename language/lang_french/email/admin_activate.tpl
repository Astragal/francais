Subject: Nouveau compte d\'utilisateur
Charset: iso-8859-1

Bonjour,

Le compte appartenant � � {USERNAME} � vient d'�tre d�sactiv� ou nouvellement cr��. Vous devriez, si n�cessaire, v�rifier les informations de ce t�l�papoteur.

Pour activer son compte, cliquez sur ce lien :
----------------------------------------------
{U_ACTIVATE}


Une fois son compte activ�, vous pourrez consulter son profil en cliquant sur ce lien :
{U_USER_DETAILS}


Nombre de fois o� apparaissent le courriel et l'adresse IP du nouvel utilisateur dans la base de donn�es de stopforumspam.com :
Courriel : {U_SPAM_FREQUENCY_EMAIL}
IP       : {U_SPAM_FREQUENCY_IP}


{EMAIL_SIG}