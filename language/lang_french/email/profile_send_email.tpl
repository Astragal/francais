Charset: iso-8859-1

Bonjour {TO_USERNAME},

Ce qui suit est un courriel qui vous a �t� envoy� par {FROM_USERNAME} depuis votre compte sur {SITENAME}. Si ce message est du spam, contient des commentaires abusifs ou autre chose que vous trouvez offensif, veuillez contacter le webmestre de ce forum � l'adresse suivante :

{BOARD_EMAIL}

Incluez ce courriel en entier (plus particuli�rement les en-t�tes). Veuillez noter que l'adresse de retour de ce courriel a �t� d�finie sur {FROM_USERNAME}.

Message qui vous a �t� envoy� :
~~~~~~~~~~~~~~~~~~~~~~~~~~~

{MESSAGE}