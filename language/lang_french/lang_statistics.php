<?php
/***************************************************************************
 *                            lang_statistics.php [English]
 *                              -------------------
 *     begin                : Fri Jan 24 2003
 *     copyright            : (C) 2003 Meik Sievertsen
 *     email                : ---
 *
 *     $Id: lang_statistics.php,v 1.3 2003/01/24 14:04:57 acydburn Exp $
 *
 ****************************************************************************/

/***************************************************************************
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

// Statistics Informations
$lang['Statistics_Mod_Version'] = 'Statistics Mod Version';
$lang['Install_info'] = 'Installed on ';
$lang['Viewed_info1'] = 'Statistics Page Loaded ';
$lang['Viewed_info2'] = ' times';
$lang['Explain'] = 'Explain';
$lang['Statistics_Mod'] = 'Statistics Mod';
$lang['time'] = 'time';
$lang['queries'] = 'queries';

// Statistics Mod Language File
$lang['Board_statistics'] = 'Board Statistics'; // Page Title
$lang['Last_Update_Time'] = 'Last Update Time:';
$lang['Next_expected_Update:'] = 'Next expected Update:';
$lang['top'] = 'Top';

// Module specific Language Variables
$lang['Uses'] = 'Uses';
$lang['Rank'] = 'Rank';
$lang['Percent'] = 'Percent';
$lang['Graph'] = 'Graph';

// function make_hour
$lang['Years'] = 'Years';
$lang['Year'] = 'Year';
$lang['Weeks'] = 'Weeks';
$lang['Week'] = 'Week';
$lang['Days'] = 'Days';
$lang['Day'] = 'Day';
$lang['None'] = 'No Time';

?>