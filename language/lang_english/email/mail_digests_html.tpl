<html> 
<head>
  <title>{L_SITENAME}</title>
  {LINK}
</head>
<body>
<p><span class="postbody">{L_SALUTATION} {SALUTATION},</span></p>

<p><span class="postbody">{L_INTRODUCTION}</span></p>

<p><span class="postbody">{L_TOTAL_POSTS} {TOTAL_POSTS}</span><br /> 
<span class="postbody">{L_TOTAL_PMS} {TOTAL_PMS}</span></p> 

<hr />
{DIGEST_CONTENT}
<br />
<hr />
<p><span class="postbody">{L_DIGEST_OPTIONS}</span></p>
<p><span class="postbody">
{L_FORMAT} {FORMAT}<br />
{L_MESSAGE_TEXT} {MESSAGE_TEXT}<br />
{L_MY_MESSAGES} {MY_MESSAGES}<br />
{L_FREQUENCY} {FREQUENCY}<br />
{L_NEW_MESSAGES} {NEW_MESSAGES}<br />
{L_SEND_DIGEST} {SEND_DIGEST}<br />
{L_SEND_TIME} {SEND_TIME}<br />
{L_TEXT_LENGTH} {TEXT_LENGTH}<br />
</span></p>
<hr />
<p><span class="postbody">{DISCLAIMER}</span></p>
<p><span class="copyright">{L_POWERED_BY} {POWERED_BY} {L_VERSION} {VERSION}</span></p>
</body>
</html>
