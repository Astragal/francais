<?php 
/*************************************************************************** 
* lang_admin.php [Italian] 
* ------------------- 
* begin : Sat Dec 16 2000 
* copyright : (C) 2001 The phpBB Group 
* email : support@phpbb.com 
* 
* $Id: lang_admin.php,v 1.35.2.17 2006/02/05 15:59:48 grahamje Exp $ 
* 
****************************************************************************/ 
/*************************************************************************** 
* 
* This program is free software; you can redistribute it and/or modify 
* it under the terms of the GNU General Public License as published by 
* the Free Software Foundation; either version 2 of the License, or 
* (at your option) any later version. 
* 
***************************************************************************/ 
/* CONTRIBUTORS 
2005-10-23 phpbb.it - http://www.phpbb.it - info@phpbb.it 
Fixed many minor grammatical mistakes.
2006-12-15 Societ� dei Cruscanti - http://www.achyra.org/cruscate/
Thoroughly revised.
*/ 
// 
// Format is same as lang_main 
// 
// 
// Modules, this replaces the keys used 
// in the modules[][] arrays in each module file 
// 
$lang['General'] = 'Generale'; 
$lang['Users'] = 'Utenti'; 
$lang['Groups'] = 'Gruppi'; 
$lang['Forums'] = 'Sezioni'; 
$lang['Styles'] = 'Stili';
$lang['Configuration'] = 'Configurazione'; 
$lang['Permissions'] = 'Permessi'; 
$lang['Manage'] = 'Gestione'; 
$lang['Disallow'] = 'Controllo nomi non consentiti'; 
$lang['Prune'] = 'Eliminazione'; 
$lang['Mass_Email'] = 'Messaggi di posta generali'; 
$lang['Ranks'] = 'Gradi'; 
$lang['Smilies'] = 'Faccine'; 
$lang['Ban_Management'] = 'Controllo disabilitazione'; 
$lang['Word_Censor'] = 'Censura parole'; 
$lang['Export'] = 'Esporta'; 
$lang['Create_new'] = 'Crea'; 
$lang['Add_new'] = 'Aggiungi'; 
$lang['Backup_DB'] = 'Copia banca dati'; 
$lang['Restore_DB'] = 'Ripristino banca dati'; 
// 
// Index 
// 
$lang['Admin'] = 'Amministrazione'; 
$lang['Not_admin'] = 'Non sei autorizzato ad amministrare questo forum'; 
$lang['Welcome_phpBB'] = 'Benvenuto in phpBB'; 
$lang['Admin_intro'] = 'Grazie di aver scelto phpBB come forum. Questa schermata mostra alcune statistiche del tuo forum. Puoi tornare a questa pagina cliccando sul collegamento <u>Indice amministrazione</u> nel pannello di sinistra. Per tornare all\'indice del forum, clicca il logo phpBB nel pannello di sinistra. Gli altri collegamenti nella parte sinistra dello schermo ti permettono di controllare ogni aspetto del forum: in ogni schermata troverai le informazioni su come usare al meglio le varie funzioni. Per maggiori informazioni e supporto visita <a href="http://www.phpbb.com" target="_blank"><strong>phpbb.com</strong></a> o <a href="http://www.phpbb.it" target="_blank"><strong>phpbb.it</strong></a>.'; 
$lang['Main_index'] = 'Indice del forum'; 
$lang['Forum_stats'] = 'Statistiche forum'; 
$lang['Admin_Index'] = 'Indice amministrazione'; 
$lang['Preview_forum'] = 'Anteprima forum'; 
$lang['Click_return_admin_index'] = 'Clicca %squi%s per tornare alla schermata principale del pannello d\'amministrazione'; 
$lang['Statistic'] = 'Statistiche'; 
$lang['Value'] = 'Valore'; 
$lang['Number_posts'] = 'Numero di interventi'; 
$lang['Posts_per_day'] = 'Interventi al giorno'; 
$lang['Number_topics'] = 'Numero di filoni'; 
$lang['Topics_per_day'] = 'Filoni al giorno'; 
$lang['Number_users'] = 'Numero di utenti'; 
$lang['Users_per_day'] = 'Utenti al giorno'; 
$lang['Board_started'] = 'Forum attivato in data'; 
$lang['Avatar_dir_size'] = 'Dimensione cartella avatara'; 
$lang['Database_size'] = 'Dimensione banca dati'; 
$lang['Gzip_compression'] ='Compressione Gzip'; 
$lang['Not_available'] = 'Non disponibile'; 
$lang['ON'] = 'Attivato'; // This is for GZip compression 
$lang['OFF'] = 'Disattivato'; // 
// DB Utils 
// 
$lang['Database_Utilities'] = 'Utilit� banca dati'; 
$lang['Restore'] = 'Ripristino'; 
$lang['Backup'] = 'Copia'; 
$lang['Restore_explain'] = 'Questa funzione ripristiner� tutte le tabelle del forum phpBB da una copia della banca dati precedentemente salvata. Se il tuo serviente lo supporta, puoi caricare un archivio di testo con compressione Gzip e verr� automaticamente decompresso. <b>ATTENZIONE</b>: questa operazione sovrascriver� tutti i dati esistenti. L\'operazione di ripristino potrebbe impiegare molto tempo per essere completata. Non muoverti da questa pagina finch� l\'operazione non sar� completata.'; 
$lang['Backup_explain'] = 'Da qui puoi creare una copia di riserva di tutti i dati del forum. Se hai delle tabelle personalizzate addizionali che desideri salvare, inseriscine i nomi separati da virgole nel campo �Tabelle addizionali�. Se il tuo serviente lo supporta, puoi comprimere gli archivi utilizzando Gzip per ridurne la dimensione prima dello scaricamento.'; 
$lang['Backup_options'] = 'Opzioni copia'; 
$lang['Start_backup'] = 'Inizia copia'; 
$lang['Full_backup'] = 'Copia completa'; 
$lang['Structure_backup'] = 'Copia solo struttura'; 
$lang['Data_backup'] = 'Copia solo dati'; 
$lang['Additional_tables'] = 'Tabelle addizionali'; 
$lang['Gzip_compress'] = 'Comprimi l\'archivio con Gzip'; 
$lang['Select_file'] = 'Seleziona un archivio'; 
$lang['Start_Restore'] = 'Inizia ripristino'; 
$lang['Restore_success'] = 'La banca dati � stata ripristinata con successo.<br /><br />Il tuo forum dovrebbe tornare allo stato in cui si trovava al momento in cui ne � stata fatta la copia.'; 
$lang['Backup_download'] = 'Lo scaricamento comincer� presto. Attendi...'; 
$lang['Backups_not_supported'] = 'Spiacenti: la copia della banca dati non pu� essere portata a termine a causa della mancanza dei necessari permessi. Contatta il tuo fornito di spazio Internet.'; 
$lang['Restore_Error_uploading'] = 'Errore nel caricamento della copia della banca dati'; 
$lang['Restore_Error_filename'] = 'Problema col nome dell\'archivio: prova a rinominarlo.'; 
$lang['Restore_Error_decompress'] = 'Non � possibile decomprimere l\'archivio Gzip: carica l\'archivio di testo.'; 
$lang['Restore_Error_no_file'] = 'Nessun archivio selezionato.'; 
// 
// Auth pages 
// 
$lang['Select_a_User'] = 'Seleziona un utente'; 
$lang['Select_a_Group'] = 'Seleziona un gruppo'; 
$lang['Select_a_Forum'] = 'Seleziona una sezione'; 
$lang['Auth_Control_User'] = 'Controllo permessi utente';
$lang['Auth_Control_Group'] = 'Controllo permessi gruppo';
$lang['Auth_Control_Forum'] = 'Controllo permessi sezione';
$lang['Look_up_User'] = 'Mostra utente';
$lang['Look_up_Group'] = 'Mostra gruppo';
$lang['Look_up_Forum'] = 'Mostra sezione';
$lang['Group_auth_explain'] = 'Da qui puoi modificare i permessi e lo stato dei moderatori assegnati a ogni gruppo. Non dimenticare che quando cambi i permessi di un gruppo, l\'utente potrebbe accedere comunque alle sezioni grazie ai suoi permessi individuali. In questo caso sarai avvisato.'; 
$lang['User_auth_explain'] = 'Da qui puoi modificare i permessi e lo stato di moderatore assegnati a ogni singolo utente. Non dimenticare che quando cambi i permessi di un utente i permessi del suo gruppo gli potrebbero permettere di accedere comunque alle sezioni. In questo caso sarai avvisato.'; 
$lang['Forum_auth_explain'] = 'Da qui puoi modificare il livello di autorizzazione richiesto per ogni sezione. Puoi far ci� attraverso una modalit� semplice e una modalit� avanzata. La modalit� avanzata offre un maggior controllo sulle varie operazioni eseguibili all\'interno di ciascuna sezione. Ricorda che cambiare i permessi delle sezioni avr� un effetto sugli utenti che possono eseguire le varie operazioni all\'interno di esse.'; 
$lang['Simple_mode'] = 'Modalit� semplice'; 
$lang['Advanced_mode'] = 'Modalit� avanzata'; 
$lang['Moderator_status'] = 'Stato dei moderatori'; 
$lang['Allowed_Access'] = 'Accesso consentito'; 
$lang['Disallowed_Access'] = 'Accesso non consentito'; 
$lang['Is_Moderator'] = '� moderatore'; 
$lang['Not_Moderator'] = 'Non � moderatore'; 
$lang['Conflict_warning'] = 'Attenzione: conflitto di autorizzazione'; 
$lang['Conflict_access_userauth'] = 'Questo utente ha ancora diritti di accesso a questa sezione per il suo gruppo di appartenenza. Potresti voler cambiare i permessi del gruppo o rimuovere questo utente dal gruppo per togliergli completamente i suoi diritti di accesso. I diritti del gruppo (e le sezioni coinvolte) sono elencati qui sotto.'; 
$lang['Conflict_mod_userauth'] = 'Questo utente ha ancora i diritti di moderatore in queste sezioni per il suo gruppo di appartenenza. Potresti voler cambiare i permessi del gruppo o rimuovere questo utente dal gruppo per togliergli completamente i suoi diritti di moderatore. I diritti del gruppo (e le sezioni coinvolte) sono elencati qui sotto.'; 
$lang['Conflict_access_groupauth'] = 'I seguenti utenti hanno ancora diritti di accesso a questa sezione per le impostazioni dei permessi utenti. Potresti voler cambiare i permessi del gruppo o rimuovere questo utente dal gruppo per togliergli completamente i suoi diritti di accesso. I diritti del gruppo (e le sezioni coinvolte) sono elencati qui sotto.'; 
$lang['Conflict_mod_groupauth'] = 'I seguenti utenti hanno ancora i diritti di moderatore in questa sezione per le impostazioni dei permessi utenti. Potresti voler cambiare i permessi del gruppo o rimuovere questo utente dal gruppo per togliergli completamente i suoi diritti di moderatore. I diritti del gruppo (e le sezioni coinvolte) sono elencati qui sotto.'; 
$lang['Public'] = 'Pubblico'; 
$lang['Private'] = 'Privato'; 
$lang['Registered'] = 'Registrato'; 
$lang['Administrators'] = 'Amministratori'; 
$lang['Hidden'] = 'Nascosto'; 
// These are displayed in the drop down boxes for advanced 
// mode forum auth, try and keep them short! 
$lang['Forum_ALL'] = 'TUTTI'; 
$lang['Forum_REG'] = 'REG'; 
$lang['Forum_PRIVATE'] = 'PRIVATO'; 
$lang['Forum_MOD'] = 'MOD'; 
$lang['Forum_ADMIN'] = 'AMMIN'; 
$lang['View'] = 'Guarda'; 
$lang['Read'] = 'Leggi'; 
$lang['Post'] = 'Invia'; 
$lang['Reply'] = 'Rispondi'; 
$lang['Edit'] = 'Modifica'; 
$lang['Delete'] = 'Cancella'; 
$lang['Sticky'] = 'Importante'; 
$lang['Announce'] = 'Annuncio';
$lang['Vote'] = 'Vota'; 
$lang['Pollcreate'] = 'Crea Sondaggio'; 
$lang['Permissions'] = 'Permessi'; 
$lang['Simple_Permission'] = 'Permesso semplice'; 
$lang['User_Level'] = 'Livello utente';
$lang['Auth_User'] = 'Utente'; 
$lang['Auth_Admin'] = 'Amministratore'; 
$lang['Group_memberships'] = 'Membri gruppo utenti'; 
$lang['Usergroup_members'] = 'Questo gruppo utenti ha i seguenti membri'; 
$lang['Forum_auth_updated'] = 'Permessi sezione aggiornati'; 
$lang['User_auth_updated'] = 'Permessi utente aggiornati'; 
$lang['Group_auth_updated'] = 'Permessi gruppo aggiornati'; 
$lang['Auth_updated'] = 'I permessi sono stati aggiornati'; 
$lang['Click_return_userauth'] = 'Clicca %squi%s per tornare a permessi utenti'; 
$lang['Click_return_groupauth'] = 'Clicca %squi%s per tornare a permessi gruppo'; 
$lang['Click_return_forumauth'] = 'Clicca %squi%s per tornare a permessi sezione'; 
// 
// Banning 
// 
$lang['Ban_control'] = 'Controllo disabilitazione'; 
$lang['Ban_explain'] = 'Da qui puoi controllare la disabilitazione (o �[messa al] bando�) degli utenti. Puoi disabilitare singoli utenti e/o uno o pi� indirizzi IP o nomi serviente.  Questi metodi impediscono all\'utente di raggiungere anche solo l\'indice del tuo forum. Per impedire a un utente di registrarsi con un diverso nome utente, puoi anche disabilitare specifici indirizzi di posta elettronica. Nota che disabilitare il solo indirizzo di posta elettronica non impedir� al relativo utente di entrare o d\'intervenire nel tuo forum. Per evitare che ci� si verifichi, devi usare uno dei primi due metodi.'; 
$lang['Ban_explain_warn'] = 'Nota che, inserendo un intervallo d\'indirizzi IP, verranno disabilitati tutti gl\'indirizzi in esso compresi. Verranno fatti dei tentativi per minimizzare il numero degl\'indirizzi aggiunti alla banca dati introducendo abbreviazioni generate automaticamente in modo appropriato. Se davvero devi inserire un intervallo di indirizzi, cerca di ridurlo al minimo, se non proprio a un singolo indirizzo.'; 
$lang['Select_ip'] = 'Seleziona un indirizzo IP'; 
$lang['Select_email'] = 'Seleziona un indirizzo di posta elettronica'; 
$lang['Ban_username'] = 'Disabilita uno o pi� nomi utente specifici'; 
$lang['Ban_username_explain'] = 'Puoi disabilitare pi� nomi utente con una sola operazione utilizzando l\'appropriata combinazione di puntatore e tastiera per il tuo calcolatore e navigatore.'; 
$lang['Ban_IP'] = 'Disabilita uno o pi� indirizzi IP o nomi serviente ospite'; 
$lang['IP_hostname'] = 'Indirizzo IP o nome serviente'; 
$lang['Ban_IP_explain'] = 'Per specificare diversi indirizzi IP o nomi serviente separali con virgole (,). Per specificare un intervallo d\'indirizzi IP separa il primo dall\'ultimo con un trattino (-). Come carattere jolly usa l\'asterisco (*).'; 
$lang['Ban_email'] = 'Disabilita uno o pi� indirizzi di posta elettronica'; 
$lang['Ban_email_explain'] = 'Per specificare diversi indirizzi di posta separali con virgole (,). Come carattere jolly per i nomi utenti usa l\'asterisco (*): e.g., *@hotmail.com.'; 
$lang['Unban_username'] = 'Riabilita uno o pi� utenti specifici'; 
$lang['Unban_username_explain'] = 'Puoi riabilitare pi� utenti con un\'unica operazione usando l\'appropriata combinazione di puntatore e tastiera per il tuo calcolatore e il tuo navigatore.'; 
$lang['Unban_IP'] = 'Riabilita uno o pi� indirizzi IP'; 
$lang['Unban_IP_explain'] = 'Puoi riabilitare pi� indirizzi IP con un\'unica operazione usando l\'appropriata combinazione di puntatore e tastiera per il tuo calcolatore e il tuo navigatore.'; 
$lang['Unban_email'] = 'Riabilita uno o pi� indirizzi di posta elettronica'; 
$lang['Unban_email_explain'] = 'Puoi riabilitare pi� indirizzi di posta con un\'unica operazione usando l\'appropriata combinazione di puntatore e tastiera per il tuo calcolatore e il tuo navigatore.'; 
$lang['No_banned_users'] = 'Non ci sono utenti disabilitati'; 
$lang['No_banned_ip'] = 'Non ci sono indirizzi IP disabilitati'; 
$lang['No_banned_email'] = 'Non ci sono indirizzi di posta disabilitati'; 
$lang['Ban_update_sucessful'] = 'La lista degli utenti disabilitati � stata aggiornata con successo.'; 
$lang['Click_return_banadmin'] = 'Clicca %squi%s per tornare a Controllo disabilitazione.'; 
// 
// Configuration 
// 
$lang['General_Config'] = 'Configurazione generale'; 
$lang['Config_explain'] = 'Il modulo sottostante ti permette di personalizzare tutte le opzioni generali del forum. Per la configurazione delle sezioni e degli utenti usa i collegamenti appropriati nel pannello di sinistra.'; 
$lang['Click_return_config'] = 'Clicca %squi%s per tornare a Configurazione generale'; 
$lang['General_settings'] = 'Impostazioni generali forum'; 
$lang['Server_name'] = 'Nome dominio'; 
$lang['Server_name_explain'] = 'Il nome del dominio cui appartiene il tuo forum'; 
$lang['Script_path'] = 'Percorso cartella phpBB'; 
$lang['Script_path_explain'] = 'Il percorso dove � situato phpBB rispetto al nome del dominio'; 
$lang['Server_port'] = 'Porta serviente'; 
$lang['Server_port_explain'] = 'La porta usata dal tuo serviente, di solito la 80: specificala solo se � diversa'; 
$lang['Site_name'] = 'Nome del sito'; 
$lang['Site_desc'] = 'Descrizione del sito'; 
$lang['Board_disable'] = 'Disabilita il forum'; 
$lang['Board_disable_explain'] = 'Questo render� il forum non disponibile per gli utenti. Gli amministratori potranno comunque accedere al Pannello d\'amministrazione quando il forum � disabilitato.'; 
$lang['Acct_activation'] = 'Attivazione del conto'; 
$lang['Acc_None'] = 'Nessuna'; // These three entries are the type of activation 
$lang['Acc_User'] = 'Utente'; 
$lang['Acc_Admin'] = 'Amministratore'; 
$lang['Abilities_settings'] = 'Impostazioni base per utenti e forum'; 
$lang['Max_poll_options'] = 'Numero massimo di opzioni per sondaggio'; 
$lang['Flood_Interval'] = 'Tempo morto d\'invio';
$lang['Flood_Interval_explain'] = 'Numero di secondi di attesa tra due invii successivi di messaggi'; 
$lang['Board_email_form'] = 'Messaggistica di posta attraverso il forum'; 
$lang['Board_email_form_explain'] = 'Gli utenti possono inviarsi messaggi di posta elettronica usando il forum'; 
$lang['Topics_per_page'] = 'Filoni per pagina'; 
$lang['Posts_per_page'] = 'Interventi per pagina'; 
$lang['Hot_threshold'] = 'Numero di interventi perch� un filone sia popolare'; 
$lang['Default_style'] = 'Stile predefinito'; 
$lang['Override_style'] = 'Annulla lo stile dell\'utente'; 
$lang['Override_style_explain'] = 'Sostituisce lo stile dell\'utente con quello predefinito'; 
$lang['Default_language'] = 'Lingua predefinita'; 
$lang['Date_format'] = 'Formato data'; 
$lang['System_timezone'] = 'Fuso orario del sistema'; 
$lang['Enable_gzip'] = 'Abilita la compressione Gzip'; 
$lang['Enable_prune'] = 'Abilita l\'eliminazione delle sezioni'; 
$lang['Allow_HTML'] = 'Permetti l\'HTML'; 
$lang['Allow_BBCode'] = 'Permetti il BBCode'; 
$lang['Allowed_tags'] = 'Codici HTML permessi'; 
$lang['Allowed_tags_explain'] = 'Separa i codici con virgole'; 
$lang['Allow_smilies'] = 'Permetti faccine'; 
$lang['Smilies_path'] = 'Percorso salvataggio faccine'; 
$lang['Smilies_path_explain'] = 'Percorso principale cartella phpBB: e.g., images/smilies'; 
$lang['Allow_sig'] = 'Permetti firma'; 
$lang['Max_sig_length'] = 'Lunghezza massima firma'; 
$lang['Max_sig_length_explain'] = 'Numero massimo di caratteri per la firma degli utenti'; 
$lang['Allow_name_change'] = 'Permetti cambio di nome utente'; 
$lang['Avatar_settings'] = 'Impostazioni avatara'; 
$lang['Allow_local'] = 'Abilita galleria avatara'; 
$lang['Allow_remote'] = 'Abilita avatara remoti'; 
$lang['Allow_remote_explain'] = 'Avatara collegati a un altro sito'; 
$lang['Allow_upload'] = 'Abilita il caricamento degli avatara'; 
$lang['Max_filesize'] = 'Dimensione massima archivi avatara'; 
$lang['Max_filesize_explain'] = 'per gli archivi avatara caricati'; 
$lang['Max_avatar_size'] = 'Dimensioni massime avatara'; 
$lang['Max_avatar_size_explain'] = '(altezza x larghezza in punti di risoluzione [px])'; 
$lang['Avatar_storage_path'] = 'Percorso salvataggio avatara'; 
$lang['Avatar_storage_path_explain'] = 'Percorso principale cartella phpBB; e.g., images/avatars'; 
$lang['Avatar_gallery_path'] = 'Percorso galleria avatara'; 
$lang['Avatar_gallery_path_explain'] = 'Percorso principale cartella phpBB per le immagini precaricate: e.g., images/avatars/gallery'; 
$lang['COPPA_settings'] = 'Impostazioni registrazione minore'; 
$lang['COPPA_fax'] = 'Numero di FAX per registrazione'; 
$lang['COPPA_mail'] = 'Indirizzo per registrazione'; 
$lang['COPPA_mail_explain'] = 'Questo � l\'indirizzo al quale i genitori manderanno il modulo di registrazione di un minore'; 
$lang['Email_settings'] = 'Impostazioni posta elettronica'; 
$lang['Admin_email'] = 'Indirizzo posta amministratore'; 
$lang['Email_sig'] = 'Firma'; 
$lang['Email_sig_explain'] = 'Questo testo verr� apposto in calce a ogni messaggio di posta elettronica spedito dal forum'; 
$lang['Use_SMTP'] = 'Usa serviente SMTP per la posta elettronica'; 
$lang['Use_SMTP_explain'] = 'Rispondi s� se vuoi o devi inviare posta attraverso uno specifico indirizzo invece della funzione posta locale'; 
$lang['SMTP_server'] = 'Indirizzo serviente SMTP'; 
$lang['SMTP_username'] = 'Nome utente SMTP'; 
$lang['SMTP_username_explain'] = 'Compila questo campo solo se il serviente lo richiede'; 
$lang['SMTP_password'] = 'Parola d\'accesso SMTP'; 
$lang['SMTP_password_explain'] = 'Compila questo campo solo se il serviente lo richiede'; 
$lang['Disable_privmsg'] = 'Messaggi privati'; 
$lang['Inbox_limits'] = 'Numero massimo di messaggi nella cartella Messaggi privati ricevuti'; 
$lang['Sentbox_limits'] = 'Numero massimo di messaggi nella cartella Messaggi privati inviati'; 
$lang['Savebox_limits'] = 'Numero massimo di messaggi nella cartella Messaggi privati salvati'; 
$lang['Cookie_settings'] = 'Impostazioni biscottini'; 
$lang['Cookie_settings_explain'] = 'Questo modulo controlla come vengono definiti i �biscottini� (in inglese, <i>cookies</i>) inviati ai navigatori. In molti casi l\'impostazione predefinita � sufficiente. Se devi cambiare queste impostazioni, fallo con attenzione: impostazioni non corrette possono impedire agli utenti di entrare (amministratore incluso).'; 
$lang['Cookie_domain'] = 'Dominio biscottino'; 
$lang['Cookie_name'] = 'Nome biscottino'; 
$lang['Cookie_path'] = 'Percorso biscottino'; 
$lang['Cookie_secure'] = 'Biscottini sicuri'; 
$lang['Cookie_secure_explain'] = 'Se il serviente funziona via SSL, abilita questo; altrimenti, lascialo disabilitato'; 
$lang['Session_length'] = 'Lunghezza sessione [secondi]'; 
// Visual Confirmation 
$lang['Visual_confirm'] = 'Abilita conferma visuale'; 
$lang['Visual_confirm_explain'] = 'Richiede agli utenti d\'inserire un codice definito da un\'immagine al momento della registrazione'; 
// Autologin Keys - added 2.0.18 
$lang['Allow_autologin'] = 'Permetti accessi automatici'; 
$lang['Allow_autologin_explain'] = 'Determina se gli utenti possono accedere automaticamente al forum ad ogni visita'; 
$lang['Autologin_time'] = 'Durata della chiave d\'accesso automatico'; 
$lang['Autologin_time_explain'] = 'Determina la durata (in giorni) della chiave d\'accesso automatico a partire dall\'ultima volta che l\'utente ha visitato il forum. Inserisci 0 se vuoi che la chiave non scada mai.';
// Search Flood Control - added 2.0.20
$lang['Search_Flood_Interval'] = 'Tempo morto fra ricerche successive'; 
$lang['Search_Flood_Interval_explain'] = 'Numero di secondi che un utente deve aspettare dopo una ricerca prima di poterne effettuare un\'altra';

// 
// Forum Management 
// 
$lang['Forum_admin'] = 'Amministrazione sezioni'; 
$lang['Forum_admin_explain'] = 'Da questo pannello puoi aggiungere, modificare, cancellare, riordinare e risincronizzare le categorie e le sezioni.'; 
$lang['Edit_forum'] = 'Modifica sezione'; 
$lang['Create_forum'] = 'Crea una nuova sezione'; 
$lang['Create_category'] = 'Crea una nuova categoria'; 
$lang['Remove'] = 'Rimuovi'; 
$lang['Action'] = 'Azione'; 
$lang['Update_order'] = 'Aggiorna ordine'; 
$lang['Config_updated'] = 'La configurazione del forum � stata aggiornata con successo.'; 
$lang['Edit'] = 'Modifica'; 
$lang['Delete'] = 'Cancella'; 
$lang['Move_up'] = 'Sposta su'; 
$lang['Move_down'] = 'Sposta gi�'; 
$lang['Resync'] = 'Risincronizza'; 
$lang['No_mode'] = 'Nessuna modalit� impostata'; 
$lang['Forum_edit_delete_explain'] = 'Il modulo sottostante ti permette di personalizzare tutte le opzioni generali del forum. Per la configurazione degli utenti e delle sezioni usa i collegamenti appropriati nel pannello di sinistra'; 
$lang['Move_contents'] = 'Sposta tutti i contenuti'; 
$lang['Forum_delete'] = 'Cancella sezione'; 
$lang['Forum_delete_explain'] = 'Il modulo sottostante ti permette di cancellare una sezione (o una categoria) e decidere dove mettere tutti i filoni (o sezioni) in essa contenuti'; 
$lang['Status_locked'] = 'Chiusa'; 
$lang['Status_unlocked'] = 'Aperta'; 
$lang['Forum_settings'] = 'Impostazioni generali sezione'; 
$lang['Forum_name'] = 'Nome sezione'; 
$lang['Forum_desc'] = 'Descrizione'; 
$lang['Forum_status'] = 'Stato della sezione'; 
$lang['Forum_pruning'] = 'Eliminazione automatica'; 
$lang['prune_freq'] = 'Verifica l\'et� dei filoni ogni'; 
$lang['prune_days'] = 'Rimuovi i filoni che non hanno avuto risposte per'; 
$lang['Set_prune_data'] = 'Hai attivato l\'eliminazione automatica per questa sezione ma non hai impostato la frequenza o il numero di giorni per l\'autoeliminazione. Torna indietro e fallo.'; 
$lang['Move_and_Delete'] = 'Sposta e cancella'; 
$lang['Delete_all_posts'] = 'Cancella tutti gli interventi'; 
$lang['Nowhere_to_move'] = 'Nessun posto dove spostare'; 
$lang['Edit_Category'] = 'Modifica categoria'; 
$lang['Edit_Category_explain'] = 'Usa questa sezione per modificare un nome di categorie'; 
$lang['Forums_updated'] = 'Le informazioni delle sezioni e delle categorie sono state aggiornate con successo'; 
$lang['Must_delete_forums'] = 'Devi cancellare tutte le sezioni per cancellare questa categoria'; 
$lang['Click_return_forumadmin'] = 'Clicca %squi%s per tornare ad Amministrazione sezioni'; 
// 
// Smiley Management 
// 
$lang['smiley_title'] = 'Utilit� modifica faccine'; 
$lang['smile_desc'] = 'Da questa pagina puoi aggiungere, togliere e modificare le faccine (o �emotic�ne�) che i tuoi utenti possono usare nei loro interventi.'; 
$lang['smiley_config'] = 'Configurazione faccine'; 
$lang['smiley_code'] = 'Codice faccine'; 
$lang['smiley_url'] = 'Archivio immagini faccine'; 
$lang['smiley_emot'] = 'Espressione faccine'; 
$lang['smile_add'] = 'Aggiungi una nuova faccina'; 
$lang['Smile'] = 'Faccina'; 
$lang['Emotion'] = 'Espressione'; 
$lang['Select_pak'] = 'Seleziona Pacchetto (.pak)'; 
$lang['replace_existing'] = 'Sovrascrivi le faccine esistenti'; 
$lang['keep_existing'] = 'Mantieni le faccine esistenti'; 
$lang['smiley_import_inst'] = 'Devi decomprimere il pacchetto di faccine e caricare gli archivi nella cartella appropriata per l\'installazione. Poi seleziona le informazioni corrette da questo modulo per importare il pacchetto di faccine.'; 
$lang['smiley_import'] = 'Importazione pacchetto faccine'; 
$lang['choose_smile_pak'] = 'Seleziona un pacchetto di faccine, estensione .pak'; 
$lang['import'] = 'Importa le faccine'; 
$lang['smile_conflicts'] = 'Cosa devi fare in caso di conflitti'; 
$lang['del_existing_smileys'] = 'Cancella le faccine esistenti prima di importare'; 
$lang['import_smile_pack'] = 'Importa pacchetto faccine'; 
$lang['export_smile_pack'] = 'Crea pacchetto faccine'; 
$lang['export_smiles'] = 'Per creare un pacchetto di faccine dalle faccine installate, clicca %squi%s per scaricare l\'archivio di estensione .pak delle faccine. Nomina questo archivio in modo appropriato mantenendo l\'estensione .pak. Crea un archivio .zip che contenga tutti gli archivi immagine delle faccine e questo archivio .pak di configurazione.'; 
$lang['smiley_add_success'] = 'Le faccine sono state aggiunte con successo.'; 
$lang['smiley_edit_success'] = 'Le faccine sono state aggiornate con successo.'; 
$lang['smiley_import_success'] = 'Il pacchetto di faccine � stato importato con successo!'; 
$lang['smiley_del_success'] = 'Le faccine sono state rimosse con successo.'; 
$lang['Click_return_smileadmin'] = 'Clicca %squi%s per tornare ad Amministrazione faccine';
$lang['Confirm_delete_smiley'] = 'Sei sicuro di voler cancellare questa faccina?';

// 
// User Management 
// 
$lang['User_admin'] = 'Amministrazione utenti'; 
$lang['User_admin_explain'] = 'Da qui puoi cambiare le informazioni degli utenti e alcune opzioni specifiche. Per modificare i permessi degli utenti, usa il modulo di amministrazione dei permessi per utenti e gruppi.'; 

// Link to Edit User/Auth in ACP
$lang['Go_edit_profile'] = 'Edit this users Profile';
$lang['Go_edit_auth'] = 'Edit this users Permissions';

$lang['Look_up_user'] = 'Mostra utente'; 
$lang['Admin_user_fail'] = 'Non � stato possibile aggiornare il profilo utente.'; 
$lang['Admin_user_updated'] = 'Il profilo utente � stato aggiornato con successo.'; 
$lang['Click_return_useradmin'] = 'Clicca %squi%s per tornare ad Amministrazione utenti'; 
$lang['User_delete'] = 'Cancella questo utente'; 
$lang['User_delete_explain'] = 'Seleziona questa casella per cancellare questo utente. Questa operazione non pu� essere annullata.'; 
$lang['User_deleted'] = 'L\'utente � stato cancellato con successo.'; 
$lang['User_status'] = 'L\'utente � attivo'; 
$lang['User_allowpm'] = 'Pu� inviare messaggi privati'; 
$lang['User_allowavatar'] = 'Pu� mostrare gli avatara'; 
$lang['Admin_avatar_explain'] = 'Da qui puoi visualizzare e cancellare l\'avatara attuale dell\'utente.'; 
$lang['User_special'] = 'Campi speciali solo per l\'amministratore'; 
$lang['User_special_explain'] = 'Questi campi non possono essere modificati dagli utenti. Da qui puoi impostare il loro stato e altre opzioni che non vengono date agli utenti.';

// 
// Group Management 
// 
$lang['Group_administration'] = 'Amministrazione gruppi'; 
$lang['Group_admin_explain'] = 'Da questo pannello puoi amministrare tutti i gruppi utenti. Puoi cancellare, creare e modificare i gruppi esistenti. Puoi scegliere i moderatori, modificare lo stato del gruppo (aperto/chiuso) e impostare il nome del gruppo e la sua descrizione.'; 
$lang['Error_updating_groups'] = 'C\'� stato un errore durante l\'aggiornamento dei gruppi.'; 
$lang['Updated_group'] = 'Il gruppo � stato aggiornato con successo.'; 
$lang['Added_new_group'] = 'Il nuovo gruppo � stato creato con successo.'; 
$lang['Deleted_group'] = 'Il gruppo � stato cancellato con successo.'; 
$lang['New_group'] = 'Crea nuovo gruppo'; 
$lang['Edit_group'] = 'Modifica gruppo'; 
$lang['group_name'] = 'Nome gruppo'; 
$lang['group_description'] = 'Descrizione gruppo'; 
$lang['group_moderator'] = 'Moderatore gruppo'; 
$lang['group_status'] = 'Stato gruppo'; 
$lang['group_open'] = 'Gruppo aperto'; 
$lang['group_closed'] = 'Gruppo chiuso'; 
$lang['group_hidden'] = 'Gruppo nascosto'; 
$lang['group_delete'] = 'Cancella gruppo'; 
$lang['group_delete_check'] = 'Cancella questo gruppo'; 
$lang['submit_group_changes'] = 'Invia modifiche'; 
$lang['reset_group_changes'] = 'Annulla modifiche'; 
$lang['No_group_name'] = 'Devi specificare un nome per questo gruppo.'; 
$lang['No_group_moderator'] = 'Devi specificare un moderatore per questo gruppo.'; 
$lang['No_group_mode'] = 'Devi specificare uno stato per questo gruppo: aperto o chiuso.'; 
$lang['No_group_action'] = 'Nessuna azione specificata'; 
$lang['delete_group_moderator'] = 'Vuoi cancellare il vecchio moderatore del gruppo?'; 
$lang['delete_moderator_explain'] = 'Se cambi il moderatore del gruppo, seleziona questa casella per rimuovere il vecchio moderatore. In caso contrario, non selezionarla e l\'utente diverr� un normale membro del gruppo.'; 
$lang['Click_return_groupsadmin'] = 'Clicca %squi%s per tornare a Amministrazione gruppi'; 
$lang['Select_group'] = 'Seleziona un gruppo'; 
$lang['Look_up_group'] = 'Mostra gruppo'; 
// 
// Prune Administration 
// 
$lang['Forum_Prune'] = 'Eliminazione sezioni'; 
$lang['Forum_Prune_explain'] = 'Questa operazione eliminer� tutti i filoni in cui non viene inviata una risposta dal numero di giorni che hai selezionato. Se non inserisci un numero, TUTTI i filoni saranno eliminati. Non verranno eliminati i filoni con sondaggi ancora attivi e neppure gli annunci. Devi eliminare questi filoni manualmente.'; 
$lang['Do_Prune'] = 'Elimina'; 
$lang['All_Forums'] = 'Tutte le sezioni'; 
$lang['Prune_topics_not_posted'] = 'Elimina i filoni senza risposte da'; 
$lang['Topics_pruned'] = 'Filoni eliminati'; 
$lang['Posts_pruned'] = 'Interventi eliminati'; 
$lang['Prune_success'] = 'L\'eliminazione delle sezioni � avvenuta con successo.'; 

// 
// Word censor 
// 
$lang['Words_title'] = 'Censura parole'; 
$lang['Words_explain'] = 'Da questo pannello puoi aggiungere, modificare e rimuovere parole che saranno automaticamente censurate in tutto il forum. Inoltre non sar� possibile registrarsi con nomi utente che contengano tali parole. L\'asterisco (*) � accettato nel campo �parola� come carattere jolly: e.g., *tra* comprender� attraverso, tra* comprender� trave, *tra comprender� finestra.'; 
$lang['Word'] = 'Parola'; 
$lang['Edit_word_censor'] = 'Modifica lista'; 
$lang['Replacement'] = 'Sostituto'; 
$lang['Add_new_word'] = 'Aggiungi una nuova parola'; 
$lang['Update_word'] = 'Aggiorna lista'; 
$lang['Must_enter_word'] = 'Devi inserire una parola e il suo sostituto'; 
$lang['No_word_selected'] = 'Nessuna parola selezionata per la modifica'; 
$lang['Word_updated'] = 'La parola selezionata � stata aggiornata con successo.'; 
$lang['Word_added'] = 'La parola � stata aggiunta con successo.'; 
$lang['Word_removed'] = 'La parola selezionata � stata rimossa con successo.'; 
$lang['Click_return_wordadmin'] = 'Clicca %squi%s per tornare a Censura parole';
$lang['Confirm_delete_word'] = 'Sei sicuro di voler cancellare questa parola censurata?';

// 
// Mass Email 
// 
$lang['Mass_email_explain'] = 'Da qui puoi inviare un messaggio a tutti i tuoi utenti o agli utenti di un gruppo specifico. Per fare questo, verr� inviato un messaggio all\'indirizzo di posta dell\'amministratore che hai fornito, e in copia nascosta (BCC) agli effettivi destinatari. Se stai inviando un messaggio a un grosso gruppo di utenti, per favore sii paziente dopo aver inviato e non interrompere il caricamento della pagina. Un tempo lungo � normale per un messaggio generale. Quando il processo sar� finito, sarai avvisato.'; 
$lang['Compose'] = 'Scrivi';
$lang['Recipients'] = 'A';
$lang['All_users'] = 'Tutti gli utenti'; 
$lang['Email_successfull'] = 'Il tuo messaggio � stato inviato'; 
$lang['Click_return_massemail'] = 'Clicca %squi%s per tornare a Messaggi di posta generali';

// 
// Ranks admin 
// 
$lang['Ranks_title'] = 'Amministrazione gradi'; 
$lang['Ranks_explain'] = 'Con questo modulo puoi aggiungere, cancellare, modificare e visualizzare i gradi degli utenti. Puoi anche creare dei gradi personalizzati che possono essere assegnati a un utente attraverso la Gestione utenti.';
$lang['Add_new_rank'] = 'Aggiungi un nuovo grado'; 
$lang['Rank_title'] = 'Nome grado'; 
$lang['Rank_special'] = 'Imposta come grado speciale'; 
$lang['Rank_minimum'] = 'Numero minimo d\'interventi'; 
$lang['Rank_maximum'] = 'Numero massimo d\'interventi'; 
$lang['Rank_image'] = 'Percorso immagine grado (relativo alla cartella principale del forum)'; 
$lang['Rank_image_explain'] = 'Usa questo campo per definire il percorso di una piccola immagine associata al grado'; 
$lang['Must_select_rank'] = 'Devi selezionare un grado.'; 
$lang['No_assigned_rank'] = 'Nessun grado speciale assegnato'; 
$lang['Rank_updated'] = 'Il grado � stato aggiornato con successo.'; 
$lang['Rank_added'] = 'Il grado � stato aggiunto con successo.'; 
$lang['Rank_removed'] = 'Il grado � stato cancellato con successo.'; 
$lang['No_update_ranks'] = 'Il grado � stato cancellato con successo. Tuttavia, gli utenti che usavano questo grado non sono stati aggiornati. Dovrai reimpostare manualmente il grado di questi utenti.'; 
$lang['Click_return_rankadmin'] = 'Clicca %squi%s per tornare ad Amministrazione gradi';
$lang['Confirm_delete_rank'] = 'Sei sicuro di voler cancellare questo grado?';

// 
// Disallow Username Admin 
// 
$lang['Disallow_control'] = 'Controllo nomi non consentiti'; 
$lang['Disallow_explain'] = 'Da qui puoi gestire la lista dei nomi utenti non consentiti. I nomi utenti non consentiti possono contenere un asterisco (*) come carattere jolly. Nota che non puoi specificare nessun nome che appartenga a un utente gi� registrato: devi prima rimuovere l\'utente e poi aggiungere il nome alla lista dei nomi non consentiti.'; 
$lang['Delete_disallow'] = 'Rimuovi'; 
$lang['Delete_disallow_title'] = 'Rimuovi nome utente dalla lista dei non consentiti'; 
$lang['Delete_disallow_explain'] = 'Puoi rimuovere un nome utente dalla lista di quelli non consentiti selezionandolo da questa lista e cliccando su �Rimuovi�'; 
$lang['Add_disallow'] = 'Aggiungi'; 
$lang['Add_disallow_title'] = 'Aggiungi nome utente alla lista dei non consentiti'; 
$lang['Add_disallow_explain'] = 'Puoi aggiungere un nome utente alla lista di quelli non consentiti ricorrendo all\'asterisco (*) per denotare un carattere qualsiasi'; 
$lang['No_disallowed'] = 'Nessun nome utente non consentito'; 
$lang['Disallowed_deleted'] = 'Il nome utente � stato rimosso con successo dalla lista dei nomi utenti non consentiti.'; 
$lang['Disallow_successful'] = 'Il nome utente � stato aggiunto con successo alla lista dei nomi utenti non consentiti.'; 
$lang['Disallowed_already'] = 'Il nome utente che hai inserito non pu� essere aggiunto alla lista dei nomi utenti non consentiti. Questo pu� dipendere dal fatto che esso rientra gi� nella lista delle parole censurate o dal fatto che esiste un utente con questo nome.'; 
$lang['Click_return_disallowadmin'] = 'Clicca %squi%s per tornare a Controllo nomi non consentiti'; 
// 
// Styles Admin 
// 
$lang['Styles_admin'] = 'Amministrazione stili'; 
$lang['Styles_explain'] = 'Con questo modulo puoi aggiungere, rimuovere e gestire gli stili (modelli e temi) del tuo forum.'; 
$lang['Styles_addnew_explain'] = 'La lista seguente contiene tutti i temi che sono disponibili per i modelli che hai al momento. I temi nella lista non sono ancora stati caricati nella banca dati del forum. Per installare un tema, clicca semplicemente sul collegamento �Installa� a fianco del tema desiderato.'; 
$lang['Select_template'] = 'Seleziona un modello'; 
$lang['Style'] = 'Tema'; 
$lang['Template'] = 'Modello'; 
$lang['Install'] = 'Installa'; 
$lang['Download'] = 'Scarica'; 
$lang['Edit_theme'] = 'Modifica tema'; 
$lang['Edit_theme_explain'] = 'Col modulo sottostante puoi modificare le impostazioni per il tema selezionato.'; 
$lang['Create_theme'] = 'Crea tema'; 
$lang['Create_theme_explain'] = 'Usa il modulo sottostante per creare un nuovo tema per il modello selezionato. Quando inserisci i colori (devi usare la notazione esadecimale) non devi includere all\'inizio #: CCCCCC � valido, #CCCCCC non � valido.'; 
$lang['Export_themes'] = 'Esporta tema'; 
$lang['Export_explain'] = 'Da questo pannello puoi esportare i dati del tema per il modello selezionato. Seleziona un modello dalla lista qui sotto e il programma creer� l\'archivio di configurazione del tema e tenter� di salvarlo nella cartella dei modelli (<i>templates</i>). Se non pu� salvare l\'archivio, il programma ti dar� la possibilit� di scaricarlo. Per permettere al programma di salvare l\'archivio, devi dare il permesso di scrittura alla cartella dei modelli sul serviente. Per ulteriori informazioni consulta la guida utenti di phpBB.'; 
$lang['Theme_installed'] = 'Il tema selezionato � stato installato con successo.'; 
$lang['Style_removed'] = 'Lo stile selezionato � stato rimosso dalla banca dati. Per rimuovere completamente questo stile dal tuo sistema, devi cancellare lo stile dalla cartella dei modelli (<i>templates</i>).'; 
$lang['Theme_info_saved'] = 'Le informazioni del tema per il modello selezionato sono state salvate. Adesso devi reimpostare i permessi dell\'archivio theme_info.cfg (e se possibile anche nella cartella dei modelli) su sola lettura.'; 
$lang['Theme_updated'] = 'Il tema selezionato � stato aggiornato. Adesso devi esportare le impostazioni del nuovo tema.'; 
$lang['Theme_created'] = 'Tema creato. Adesso devi esportare il tema nell\'archivio di configurazione del tema per usarlo da un\'altra parte.'; 
$lang['Confirm_delete_style'] = 'Sei sicuro di voler cancellare questo stile?'; 
$lang['Download_theme_cfg'] = 'Il processo di esportazione non riesce a scrivere l\'archivio di configurazione del tema. Premi il pulsante qui sotto per scaricare questo archivio con il tuo navigatore. Dopo averlo scaricato puoi trasferirlo nella cartella che contiene gli archivi dei modelli. Dopo puoi compattare gli archivi per distribuirli o per riusarli.'; 
$lang['No_themes'] = 'Il modello che hai selezionato non ha temi allegati. Per creare un nuovo tema, clicca sul collegamento Crea tema nel pannello di sinistra.'; 
$lang['No_template_dir'] = 'Non � possibile aprire la cartella dei modelli. Potrebbe essere non leggibile dal serviente o potrebbe non esistere.'; 
$lang['Cannot_remove_style'] = 'Non puoi rimuovere lo stile selezionato poich� � attualmente quello predefinito nel forum. Cambia lo stile predefinito e prova di nuovo.'; 
$lang['Style_exists'] = 'Non puoi rimuovere lo stile selezionato perch� � quello predefinito nel forum. Cambia lo stile predefinito e poi riprova.'; 
$lang['Click_return_styleadmin'] = 'Clicca %squi%s per tornare ad Amministrazione stili'; 
$lang['Theme_settings'] = 'Impostazioni del tema'; 
$lang['Theme_element'] = 'Elemento del tema'; 
$lang['Simple_name'] = 'Nome semplice'; 
$lang['Value'] = 'Valore'; 
$lang['Save_Settings'] = 'Salva impostazioni'; 
$lang['Stylesheet'] = 'Foglio di stile CSS'; 
$lang['Stylesheet_explain'] = 'Nome dello stile CSS da usare per questo tema.'; 
$lang['Background_image'] = 'Immagine di sfondo'; 
$lang['Background_color'] = 'Colore di sfondo'; 
$lang['Theme_name'] = 'Nome tema'; 
$lang['Link_color'] = 'Colore collegamento'; 
$lang['Text_color'] = 'Colore testo'; 
$lang['VLink_color'] = 'Colore collegamento visitato'; 
$lang['ALink_color'] = 'Colore collegamento attivo'; 
$lang['HLink_color'] = 'Colore collegamento sorvolato'; 
$lang['Tr_color1'] = 'Tabella colonna colore 1'; 
$lang['Tr_color2'] = 'Tabella colonna colore 2'; 
$lang['Tr_color3'] = 'Tabella colonna colore 3'; 
$lang['Tr_class1'] = 'Tabella colonna classe 1'; 
$lang['Tr_class2'] = 'Tabella colonna classe 2'; 
$lang['Tr_class3'] = 'Tabella colonna classe 3'; 
$lang['Th_color1'] = 'Tabella intestazione colore 1'; 
$lang['Th_color2'] = 'Tabella intestazione colore 2'; 
$lang['Th_color3'] = 'Tabella intestazione colore 3'; 
$lang['Th_class1'] = 'Tabella intestazione classe 1'; 
$lang['Th_class2'] = 'Tabella intestazione classe 2'; 
$lang['Th_class3'] = 'Tabella intestazione classe 3'; 
$lang['Td_color1'] = 'Tabella cella colore 1'; 
$lang['Td_color2'] = 'Tabella cella colore 2'; 
$lang['Td_color3'] = 'Tabella cella colore 3'; 
$lang['Td_class1'] = 'Tabella cella classe 1'; 
$lang['Td_class2'] = 'Tabella cella classe 2'; 
$lang['Td_class3'] = 'Tabella cella classe 3'; 
$lang['fontface1'] = 'Nome carattere 1'; 
$lang['fontface2'] = 'Nome carattere 2'; 
$lang['fontface3'] = 'Nome carattere 3'; 
$lang['fontsize1'] = 'Dimensione carattere 1'; 
$lang['fontsize2'] = 'Dimensione carattere 2'; 
$lang['fontsize3'] = 'Dimensione carattere 3'; 
$lang['fontcolor1'] = 'Colore carattere 1'; 
$lang['fontcolor2'] = 'Colore carattere 2'; 
$lang['fontcolor3'] = 'Colore carattere 3'; 
$lang['span_class1'] = 'Classe <span> 1'; 
$lang['span_class2'] = 'Classe <span> 2'; 
$lang['span_class3'] = 'Classe <span> 3'; 
$lang['img_poll_size'] = 'Dimensione immagine votazione [px]'; 
$lang['img_pm_size'] = 'Dimensione stato messaggi privati [px]'; 
// 
// Install Process 
// 
$lang['Welcome_install'] = 'Benvenuto nell\'installazione di phpBB 2'; 
$lang['Initial_config'] = 'Configurazione base'; 
$lang['DB_config'] = 'Configurazione banca dati'; 
$lang['Admin_config'] = 'Configurazione amministrazione'; 
$lang['continue_upgrade'] = 'Dopo avere scaricato l\'archivio di configurazione sul tuo calcolatore puoi premere il pulsante \"Continua l\'Aggiornamento\" qui sotto per procedere col processo di aggiornamento. Per caricare l\'archivio di configurazione aspetta la fine del processo di aggiornamento.'; 
$lang['upgrade_submit'] = 'Continua aggiornamento'; 
$lang['Installer_Error'] = 'Si � verificato un errore durante l\'installazione'; 
$lang['Previous_Install'] = '� stata rilevata una precedente installazione'; 
$lang['Install_db_error'] = 'Si � verificato un errore durante l\'aggiornamento della banca dati'; 
$lang['Re_install'] = 'La tua installazione precedente � ancora attiva. <br /><br />Se vuoi installare di nuovo phpBB 2 clicca il bottone di conferma qui sotto. Sappi che questa operazione distrugger� tutti i dati esistenti, non verr� fatta alcuna copia! Il nome utente e la parola d\'accesso dell\'amministratore che hai usato per entrare nel forum verranno ricreati dopo la nuova installazione; nessun\'altra impostazione verr� mantenuta.<br /><br />Pensaci bene prima di CONFERMARE!'; 
$lang['Inst_Step_0'] = 'Grazie per avere scelto phpBB 2. Per completare correttamente l\'installazione devi riempire tutti i campi sottostanti. Prima di procedere assicurati di avere una banca dati con tutti i dati d\'accesso. Se stai installando il forum su una banca dati che usa ODBC - ad es. MS Access - devi prima creargli un DSN prima di procedere all\'installazione.'; 
$lang['Start_Install'] = 'Inizia installazione'; 
$lang['Finish_Install'] = 'Termina installazione'; 
$lang['Default_lang'] = 'Lingua predefinita del forum'; 
$lang['DB_Host'] = 'Nome serviente banca dati (DSN)'; 
$lang['DB_Name'] = 'Nome banca dati'; 
$lang['DB_Username'] = 'Nome utente banca dati'; 
$lang['DB_Password'] = 'Parola d\'accesso banca dati'; 
$lang['Database'] = 'La tua banca dati'; 
$lang['Install_lang'] = 'Scegli una lingua per l\'installazione'; 
$lang['dbms'] = 'Tipo di banca dati'; 
$lang['Table_Prefix'] = 'Prefisso tabelle banca dati'; 
$lang['Admin_Username'] = 'Nome utente amministratore'; 
$lang['Admin_Password'] = 'Parola d\'accesso amministratore'; 
$lang['Admin_Password_confirm'] = 'Parola d\'accesso amministratore [ Conferma ]'; 
$lang['Inst_Step_2'] = 'Il tuo nome utente di amministratore � stato creato. A questo punto la tua installazione di base � terminata. Adesso ti verr� mostrata una schermata dove potrai amministrare il tuo nuovo phpBB. Per favore verifica i dettagli della Configurazione generale e cambiali in base alle tue esigenze. Grazie di aver scelto phpBB 2.'; 
$lang['Unwriteable_config'] = 'Non posso scrivere l\'archivio config.php automaticamente. Scarica una copia del tuo archivio di configurazione cliccando sul pulsante sottostante. Devi caricare questo archivio nella stessa cartella di phpBB 2. Successivamente potrai accedere con nome e parola d\'accesso di amministrazione che hai fornito nel modulo precedente e andare nel pannello di controllo (un collegamento apparir� in fondo a ogni pagina dopo che sei entrato) per verificare le impostazioni generali di configurazione. Grazie di aver scelto phpBB 2.'; 
$lang['Download_config'] = 'Scarica l\'archivio di Configurazione'; 
$lang['ftp_choose'] = 'Scegli il metodo di scaricamento'; 
$lang['ftp_option'] = '<br />Poich� le estensioni FTP non sono disponibili in questa versione di PHP carica automaticamente via FTP l\'archivio di configurazione.'; 
$lang['ftp_instructs'] = 'Hai scelto di caricare automaticamente via FTP l\'archivio sul conto che contiene phpBB 2. Inserisci le informazioni per facilitare il processo. Il percorso FTP deve essere il percorso esatto dell\'installazione di phpBB2 come se stessi caricando via FTP con un normale programma cliente.'; 
$lang['ftp_info'] = 'Inserisci le tue informazioni FTP'; 
$lang['Attempt_ftp'] = 'Tentativo di caricare via FTP l\'archivio di configurazione'; 
$lang['Send_file'] = 'Inviatemi l\'archivio e lo caricher� via FTP manualmente'; 
$lang['ftp_path'] = 'Percorso FTP per phpBB 2'; 
$lang['ftp_username'] = 'Il tuo nome utente FTP'; 
$lang['ftp_password'] = 'La tua parola d\'accesso FTP'; 
$lang['Transfer_config'] = 'Inizio trasferimento'; 
$lang['NoFTP_config'] = 'Il tentativo di trasferire l\'archivio via FTP � fallito. Scarica l\'archivio config e trasferiscilo sul serviente manualmente.'; 
$lang['Install'] = 'Installa'; 
$lang['Upgrade'] = 'Aggiorna'; 
$lang['Install_Method'] = 'Scegli un metodo di installazione'; 
$lang['Install_No_Ext'] = 'La configurazione PHP del tuo serviente non supporta il tipo di banca dati che hai scelto'; 
$lang['Install_No_PCRE'] = 'phpBB2 richiede il Perl-Compatible Regular Expressions Module. La tua configurazione PHP non lo supporta!';

// 
// Admin Userlist Start 
// 
$lang['Userlist'] = 'Lista utenti'; 
$lang['Userlist_description'] = 'Da qui puoi visualizzare la lista completa degli utenti registrati ed effettuare varie operazioni su di essi.';

$lang['Add_group'] = 'Aggiungi a un gruppo'; 
$lang['Add_group_explain'] = 'Seleziona il gruppo a cui desideri aggiungere l\'utente selezionato'; 

$lang['Open_close'] = 'Apri/Chiudi'; 
$lang['Active'] = 'Attivo'; 
$lang['Group'] = 'Gruppo'; 
$lang['Rank'] = 'Ruolo'; 
$lang['Last_activity'] = 'Ultima attivit�'; 
$lang['Never'] = 'Mai'; 
$lang['User_manage'] = 'Gestisci'; 
$lang['Find_all_posts'] = 'Trova tutti i suoi interventi'; 

$lang['Select_one'] = 'Seleziona'; 
$lang['Ban'] = 'Disabilita'; 
$lang['Activate_deactivate'] = 'Attiva/Disattiva'; 

$lang['User_id'] = 'Identificativo utente'; 
$lang['User_level'] = 'Grado utente'; 
$lang['Ascending'] = 'Crescente'; 
$lang['Descending'] = 'Decrescente'; 
$lang['Show'] = 'Mostra';
$lang['All'] = 'Tutti'; 

$lang['Member'] = 'Membro'; 
$lang['Pending'] = 'In attesa'; 

$lang['Confirm_user_ban'] = 'Sei sicuro di voler disabilitare gli utenti selezionati?'; 
$lang['Confirm_user_deleted'] = 'Sei sicuro di voler eliminare gli utenti selezionati?'; 

$lang['User_status_updated'] = 'Lo stato degli utenti selezionati � stato aggiornato con successo.'; 
$lang['User_banned_successfully'] = 'Utenti disabilitati con successo.'; 
$lang['User_deleted_successfully'] = 'Utenti eliminati con successo.'; 
$lang['User_add_group_successfully'] = 'Utenti aggiunti al gruppo con successo.'; 

$lang['Click_return_userlist'] = 'Clicca %squi%s per tornare alla Lista utenti';
// 
// Admin Userlist End 
//

// 
// Version Check 
// 
$lang['Version_up_to_date'] = 'La tua versione � aggiornata: nessun aggiornamento disponibile per la versione di phpBB in uso.';
$lang['Version_not_up_to_date'] = 'La tua versione <b>non </b>sembra essere aggiornata: sono disponibili aggiornamenti per la versione di phpBB in uso. Visita <a href="http://www.phpbb.com/downloads.php" target="_new">http://www.phpbb.com/downloads.php</a> per ottenere l\'ultima versione.'; 
$lang['Latest_version_info'] = 'La versione pi� recente attualmente disponibile � <b>phpbb %s</b>.'; 
$lang['Current_version_info'] = 'La tua versione � <b>phpbb %s</b>.'; 
$lang['Connect_socket_error'] = 'Impossibile il collegamento a phpBB. Rapporto errore:<br />%s'; 
$lang['Socket_functions_disabled'] = 'Impossibile usare le funzioni di attacco (<i>socket</i>).'; 
$lang['Mailing_list_subscribe_reminder'] = 'Per le ultime informazioni sugli aggiornamenti di phpBB <a href="http://www.phpbb.com/support/" target="_new">iscriviti all\'indirizzario</a> (in inglese).'; 
$lang['Version_information'] = 'Informazioni versione'; 
// 
// Login attempts configuration 
// 
$lang['Max_login_attempts'] = 'Tentativi di accesso permessi'; 
$lang['Max_login_attempts_explain'] = 'Il numero di tentativi di accesso al forum permessi'; 
$lang['Login_reset_time'] = 'Durata blocco accesso'; 
$lang['Login_reset_time_explain'] = 'Intervallo di tempo in minuti che l\'utente deve attendere prima di poter accedere al forum dopo aver superato il limite di tentativi di accesso'; 
//
// Bookmark Mod
//
$lang['Max_bookmarks_links'] = 'Maximum bookmarks send in link-tag';
$lang['Max_bookmarks_links_explain'] = 'Number of bookmarks maximal send in link-tag at the beginning of the document. This information is e.g. used by Mozilla. Enter 0 to disable this function.';

// Admin Announcements Mod (cherokee red)
$lang['Admin_announcement'] = 'Admin Announcement';
$lang['Admin_announcement_explain'] = 'This message will show in the header of your forum, to all users';


// 
// That's all Folks! 
// ------------------------------------------------- 
$lang['Forum_notify'] = 'Allow forum notification';
$lang['Forum_notify_enabled'] = 'Allow';
$lang['Forum_notify_disabled'] = 'Do not allow';
$lang['ad_managment']  = 'Ad Management';
$lang['inline_ad_config']  = 'Inline Ad Config';
$lang['inline_ads']  = 'Inline Ads';
$lang['ad_code_about']  = 'This page lists current ads.  You may edit, delete or add new ads here.';
$lang['Click_return_firstpost'] = 'Click %sHere%s to return to Inline Ad Configuration';
$lang['Click_return_inline_code'] = 'Click %sHere%s to return to Inline Ad Code Configuration';
$lang['ad_after_post'] = 'Display Ad After x Post';
$lang['ad_after_last_post'] = 'Afficher la publicit� apr�s le dernier message si le nombre de messages est sup�rieur ou �gal �';
$lang['ad_every_post'] = 'Display Ad Every x Post';
$lang['ad_display'] = 'Display Ads To';
$lang['ad_all'] = 'All';
$lang['ad_reg'] = 'Registered Users';
$lang['ad_guest'] = 'Guests';
$lang['ad_exclude'] = 'Exclude These Groups (List by comma-seperated group ID)';
$lang['ad_forums'] = 'Exclude These Forums (List by comma-seperated forum ID)';
$lang['ad_code'] = 'Ad Code';
$lang['ad_style'] = 'Display Style';
$lang['ad_new_style'] = 'Ad looks like a special user post';
$lang['ad_old_style'] = 'Ad falls inline with the topic';
$lang['ad_post_threshold'] = 'Do not display if user has more than x posts (Leave blank to disable)';
$lang['ad_add']  = 'Add New Ad';
$lang['ad_name']  = 'Short name to identify ad';
?>