<?php
/***************************************************************************
 *                            lang_printertopic.php [Italian]
 *                              -------------------
 *   begin                : Tuesday, Dec 30 2003
 *   copyright            : (C) 2003 by Svyatozar
 *   email                : svyatozar@pochtamt.ru
 *
 *   $Id: lang_printertopic.php,v 1.2 2004/01/03 06:51:40 webmaster Exp $
 *
 ****************************************************************************/

/***************************************************************************
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/


//
// printer-friendly topic mod
//

// mod specific
$lang['printertopic_button'] = 'Versione stampabile'; // Printer button alternate text
$lang['printertopic_Select_messages_from'] = 'Seleziona interventi a partire dal n.';
$lang['printertopic_through'] = 'fino al n.';
$lang['printertopic_box1_desc'] = 'numero del primo messaggio da visualizzare';
$lang['printertopic_box2_desc'] = 'numero dell\'ultimo messaggio da visualizzare; se negativo, definisce un intervallo';
$lang['printertopic_Show'] = 'Mostra'; // "Show" button label in the printable topic view
$lang['printertopic_Print'] = 'Stampa'; // "Print" button label in the printable topic view
$lang['printertopic_Print_desc'] = 'Stampa questa pagina'; // Description for the "Print" button on the printable topic view
$lang['printertopic_cancel_pagination_desc'] = 'Annulla paginatura'; // Description for the "Cancel Pagination" button in the printable topic view
$lang['printertopic_restore_pagination_desc'] = 'Ripristina paginatura'; // Description for the "Restore Pagination" button in the printable topic view



?>