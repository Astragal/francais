<?php
/*************************************************************************** 
* lang_bbcode.php [Italian] 
* ------------------- 
* begin : Wednesday Oct 3, 2001 
* copyright : (C) 2001 The phpBB Group 
* email : support@phpbb.com 
* 
* $Id: lang_bbcode.php,v 1.3.2.2 2002/12/18 15:40:20 psotfx Exp $ 
* 
* 
***************************************************************************/ 
/*************************************************************************** 
* 
* This program is free software; you can redistribute it and/or modify 
* it under the terms of the GNU General Public License as published by 
* the Free Software Foundation; either version 2 of the License, or 
* (at your option) any later version. 
* 
***************************************************************************/ 
/* CONTRIBUTORS 
2005-10-23 phpbb.it - http://www.phpbb.it - info@phpbb.it 
Fixed many minor grammatical problems.
2006-03-07 Societ� dei Cruscanti - http://www.achyra.org/cruscate/
Thoroughly revised.
*/ 
// // To add an entry to your BBCode guide simply add a line to this file in this format: 
// $faq[] = array("question", "answer"); 
// If you want to separate a section enter $faq[] = array("--","Block heading goes here if wanted"); 
// Links will be created automatically 
// 
// DO NOT forget the ; at the end of the line. 
// Do NOT put double quotes (") in your BBCode guide entries, if you absolutely must then escape them ie. \"something\"; 
// 
// The BBCode guide items will appear on the BBCode guide page in the same order they are listed in this file 
// 
// If just translating this file please do not alter the actual HTML unless absolutely necessary, thanks :) 
// 
// In addition please do not translate the colours referenced in relation to BBCode any section, if you do 
// users browsing in your language may be confused to find they're BBCode doesn't work :D You can change 
// references which are 'in-line' within the text though. 
// 
$faq[] = array("--","Introduzione"); 
$faq[] = array("Cos'� il BBCode?", "Il BBCode � un ampliamento speciale dell'HTML. L'uso del BBCode nei tuoi messaggi � determinato dall'amministratore. Inoltre puoi disabilitare il BBCode in ogni messaggio attraverso il modulo di invio. Il BBCode ha uno stile simile all'HTML, i codici sono racchiusi in parentesi quadre [ e ] piuttosto che in &lt; e &gt; e offre grande controllo su cosa e come vogliamo mostrare il messaggio. Alcuni modelli rendono molto pi� semplice l'inserimento del BBCode mediante una serie di pulsanti posti nel pannello di composizione. Cionnonostante puoi trovare utile dare una scorsa a questa guida."); 
$faq[] = array("--","Formattazione del Testo"); 
$faq[] = array("Come creare il testo in grassetto, sottolineato o corsivo", "Il BBCode include dei codici per permetterti di cambiare velocemente lo stile di base del tuo testo. Questo avviene nel seguente modo: <ul><li>Per il testo in grassetto usa <b>[b][/b]</b>, es. <br /><br /><b>[b]</b>Ciao<b>[/b]</b><br /><br >diventer� <b>Ciao</b></li><li>Per il testo sottolineato usa<b>[u][/u]</b>, es.:<br /><br /><b>[u]</b>Buongiorno<b>[/u]</b><br /><br />diventa <u>Buongiorno</u></li><li>Per il testo in corsivo usa <b>[i][/i]</b>, es.<br /><br >Questo � <b>[i]</b>Grande!<b>[/i]</b><br /><br />diventa Questo � <i>Grande!</i></li></ul>"); 
$faq[] = array("Come modificare il colore o la dimensione del testo", "Per modificare il colore o la dimensione del testo possono essere usati i seguenti codici. Ricorda che il modo in cui il testo apparir� dipender� dal navigatore e dal sistema operativo del lettore: <ul><li>Il cambiamento del colore del testo si ottiene racchiudendolo in <b>[color=][/color]</b>. Puoi specificare il nome di un colore riconosciuto (ad esempio red, blue, yellow, ecc.) oppure la stringa esadecimale alternativa, come #FFFFFF, #000000. Per esempio, per ottenere un testo rosso dovresti usare:<br /><br /><b>[color=red]</b>Ciao!<b>[/color]</b><br /><br />o<br /><br /><b>[color=#FF0000]</b>Ciao!<b>[/color]</b><br /><br />daranno ambedue <span style=\"color:red\">Ciao!</span></li><li>Il cambiamento della dimensione del testo si ottiene in modo simile usando <b>[size=][/size]</b>. Questo codice dipende dallo stile che stai usando, ma il formato raccomandato � un valore numerico indicante la dimensione del carattere in punti, partendo da 1 (cos� piccolo che non riuscirai a vederlo) fino a 29 (molto grande). Per esempio:<br /><br /><b>[size=9]</b>PICCOLO<b>[/size]</b><br /><br />dar� generalmente <span style=\"font-size:0.5625rem\">PICCOLO</span><br /><br />mentre:<br /><br /><b>[size=24]</b>ENORME!<b>[/size]</b><br /><br />dar� <span style=\"font-size:1.5rem\">ENORME!</span></li></ul>"); 
$faq[] = array("Si possono combinare i codici di formattazione?", "Certo che si pu�; ad esempio per richiamare l'attenzione di qualcuno potresti scrivere:<br /><br /><b>[size=18][color=red][b]</b>GUARDAMI!<b>[/b][/color][/size]</b><br /><br />che darebbe <span style=\"color:red;font-size:1.125rem\"><b>GUARDAMI!</b></span><br /><br />Non ti consigliamo di scrivere molti testi come questo, per�! Ricorda che spetta a te, che scrivi, di controllare che i codici siano chiusi correttamente. Per esempio, il seguente � sbagliato:<br /><br /><b>[b][u]</b>Questo � errato<b>[/b][/u]</b>");
$faq[] = array("--","Citazioni e testo a larghezza fissa"); 
$faq[] = array("Citazioni di testo nelle risposte", "Ci sono due modi per fare una citazione, con un referente o senza.<ul><li>Quando usi la funzione Citazione per rispondere a un messaggio sul forum devi notare che il testo del messaggio viene incluso nella finestra del messaggio tra <b>[quote=\"\"][/quote]</b>. Questo metodo ti permette di fare una citazione riferendoti a una persona o qualsiasi altra cosa che hai deciso di inserire! Per esempio, per citare un pezzo di testo di Mario Rossi devi inserire:<br /><br /><b>[quote=\"Mario Rossi\"]</b>Il testo di Mario Rossi andr� qui<b>[/quote]</b><br /><br />Nel messaggio verr� automaticamente aggiunto Mario Rossi ha scritto: prima del testo citato. Ricorda che tu <b>devi</b> includere le virgolette \"\" attorno al nome che stai citando, non sono opzionali.</li><li>Il secondo metodo ti permette di citare qualcosa alla cieca. Per adottare questo metodo, racchiudi il testo fra i codici <b>[quote][/quote]</b>. Quando vedrai il messaggio comparir� semplicemente Citazione: prima del testo stesso.</li></ul>"); 
$faq[] = array("Mostrare il codice", "Se vuoi mostrare un pezzo di codice o qualcosa che ha bisogno di una larghezza fissa, es. Courier devi racchiudere il testo tra i codici <b>[code][/code]</b>, es.<br /><br /><b>[code]</b>echo \"Questo � un codice\";<b>[/code]</b><br /><br />Tutta la formattazione utilizzata tra i codici <b>[code][/code]</b> viene mantenuta quando viene visualizzata in seguito."); 
$faq[] = array("--","Creazione di liste"); 
$faq[] = array("Creare una lista non ordinata", "Il BBCode supporta due tipi di liste, ordinate e no. Sono essenzialmente la stessa cosa del loro equivalente in HTML. Una lista non ordinata mostra ogni oggetto nella tua lista in modo sequenziale, uno dopo l'altro inserendo un punto per ogni riga. Per creare una lista non ordinata usa <b>[list][/list]</b> e definisci ogni oggetto nella lista usando <b>[*]</b>. Per esempio per fare una lista dei tuoi colori preferiti puoi usare:<br /><br /><b>[list]</b><br /><b>[*]</b>Rosso<br /><b>[*]</b>Blu<br /><b>[*]</b>Giallo<br /><b>[/list]</b><br /><br />Questo mostrer� la lista:<ul><li>Rosso</li><li>Blu</li><li>Giallo</li></ul>"); 
$faq[] = array("Creare una lista ordinata", "Una lista ordinata ti permette di controllare il modo in cui ogni oggetto della lista viene mostrato. Per creare una lista ordinata usa <b>[list=1][/list]</b> per creare una lista numerata o alternativamente <b>[list=a][/list]</b> per una lista alfabetica. Come per la lista non ordinata gli oggetti vengono specificati usando <b>[*]</b>. Per esempio:<br /><br /><b>[list=1]</b><br /><b>[*]</b>Vai al negozio<br /><b>[*]</b>Compra un nuovo calcolatore<br /><b>[*]</b>Impreca sul calcolatore quando si blocca<br /><b>[/list]</b><br /><br />verr� mostrato cos�:<ol type=\"1\"><li>Vai al negozio</li><li>Compra un nuovo calcolatore</li><li>Impreca sul calcolatore quando si blocca</li></ol>mentre per una lista alfabetica devi usare:<br /><br /><b>[list=a]</b><br /><b>[*]</b>La prima risposta possibile<br /><b>[*]</b>La seconda risposta possibile<br /><b>[*]</b>La terza risposta possibile<br /><b>[/list]</b><br /><br />sar�<ol type=\"a\"><li>La prima risposta possibile</li><li>La seconda risposta possibile</li><li>La terza risposta possibile</li></ol>"); 
$faq[] = array("--", "Creare collegamenti"); 
$faq[] = array("Creare un collegamento a un altro sito", "Il BBCode di phpBB supporta diversi modi per creare URI, Uniform Resource Indicators meglio conosciuti come URL.<ul><li>Il primo di questi adopera il codice <b>[url=][/url]</b>, qualunque cosa digiti dopo il segno = generer� il contenuto del codice che si comporter� come URL. Per esempio per collegarsi a phpBB.com devi usare:<br /><br /><b>[url=http://www.phpbb.com/]</b>Visita phpBB!<b>[/url]</b><br /><br />Questo genera il seguente collegamento, <a href=\"http://www.phpbb.com/\" target=\"_blank\">Visita phpBB!</a> Come puoi vedere il collegamento si apre in una nuova finestra cos� l'utente pu� continuare a navigare nel forum.</li><li>Se vuoi che l'URL stesso venga mostrato come collegamento puoi fare questo semplicemente usando:<br /><br /><b>[url]</b>http://www.phpbb.com/<b>[/url]</b><br /><br />Questo genera il seguente collegamento, <a href=\"http://www.phpbb.com/\" target=\"_blank\">http://www.phpbb.com/</a></li><li>Inoltre phpBB dispone di una cosa chiamata <i>Collegamenti Magici</i>, questo cambier� ogni URL sintatticamente corretto in un collegamento senza la necessit� di specificare nessun codice o http://. Per esempio digitando www.phpbb.com nel tuo messaggio automaticamente verr� cambiato in <a href=\"http://www.phpbb.com/\" target=\"_blank\">www.phpbb.com</a> e verr� mostrato nel messaggio finale.</li><li>La stessa cosa accade per gli indirizzi di posta, puoi specificare un indirizzo esplicitamente, per esempio:<br /><br /><b>[email]</b>no.one@domain.adr<b>[/email]</b><br /><br />che mostrer� <a href=\"emailto:no.one@domain.adr\">no.one@domain.adr</a> o puoi digitare no.one@domain.adr nel tuo messaggio e verr� automaticamente convertito.</li></ul>Come per tutti i codici del BBCode puoi includere gli URL in ogni altro codice come <b>[img][/img]</b> (guarda il successivo punto), <b>[b][/b]</b>, ecc. Come per i codici di formattazione dipende da te verificare che tutti i codici siano correttamente aperti e chiusi, per esempio:<br /><br /><b>[url=http://www.phpbb.com/][img]</b>http://www.phpbb.com/images/phplogo.gif<b>[/url][/img]</b><br /><br /> <u>non</u> � corretto e potrebbe cancellare il tuo messaggio. Quindi presta attenzione. "); 
$faq[] = array("--", "Mostrare immagini nei messaggi"); 
$faq[] = array("Aggiungere una immagine al messaggio", "Il BBCode di phpBB incorpora un codice per l'inclusione di immagini nei tuoi messaggi. Ci sono due cose importanti da ricordare riguardo all'uso di questo codice: a molti utenti non piacciono molte immagini nei messaggi e, in secondo luogo, l'immagine deve essere gi� disponibile su Internet (non pu�, cio�, esistere solo sul tuo calcolatore, a meno che non sia esso stesso un serviente di pagine Internet). Non c'� modo di salvare le immagini localmente con phpBB (forse nella prossima versione). Per mostrare delle immagini, devi inserire l'URL che rimanda all'immagine con il codice <b>[img][/img]</b>. Per esempio:<br /><br /><b>[img]</b>http://www.phpbb.com/images/phplogo.gif<b>[/img]</b><br /><br />Puoi inserire un'immagine nel codice <b>[url][/url]</b> se vuoi, es.<br /><br /><b>[url=http://www.phpbb.com/][img]</b>http://www.phpbb.com/images/phplogo.gif<b>[/img][/url]</b><br /><br />genera:<br /><br /><a href=\"http://www.phpbb.com/\" target=\"_blank\"><img src=\"templates/subSilver/images/logo_phpBB_med.gif\" border=\"0\" alt=\"\" /></a><br />"); 
$faq[] = array("--", "Altro"); 
$faq[] = array("Posso aggiungere i miei codici personali?", "No, non direttamente in phpBB 2.0. Stiamo cercando di rendere i codici del BBCode pi� versatili per la prossima versione"); 
// 
// This ends the BBCode guide entries 
//

?>
