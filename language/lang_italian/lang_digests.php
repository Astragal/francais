<?php 
/***************************************************************************
                             lang_digests.php
                             ----------------
    begin                : Friday, August 17, 2007
    copyright            : (c) Mark D. Hamill
    email                : mhamill@computer.org

    $Id: $

 ***************************************************************************/

/***************************************************************************
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

// Written by Mark D. Hamill, mhamill@computer.org
// This software is designed to work with phpBB Version 2.0.22

if ( !defined('IN_PHPBB') )
{
	die('Hacking attempt');
}

// This block goes as default text in the emailed digest (mail_digests.php)
$lang['digest_a_digest_containing'] = 'A digest containing';
$lang['digest_a_total_of']='A total of ';
$lang['digest_and'] = 'and';
$lang['digest_author'] = 'Author';
$lang['digest_disclaimer_html'] = "\nThis digest is being sent to registered members of <a href=\"" . $settings['site_url']. '">' . $board_config['sitename'] . '</a> forums. Your email address is never disclosed to outside parties. See our <a href="' . $settings['site_url'] . 'faq.' . $phpEx . '">FAQ</a> for more information on our privacy policies. You can change or delete your subscription by logging into ' . $board_config['sitename'] . ' from the <a href="' . $settings['site_url'] . 'digests.' . $phpEx . '">Digest Page</a>. If you have questions or feedback on the format of this digest please send it to the <a href="mailto:' . $board_config['board_email'] . '">' . $board_config['sitename'] . ' Webmaster</a>.';
$lang['digest_disclaimer_text'] = "\nThis digest is being sent to registered members of " . $board_config['sitename'] . ' forums. Your email address is never disclosed to outside parties. See our FAQ for more information on our privacy policies. You can change or delete your subscription by logging into ' . $board_config['sitename'] . ' from the Digest Page. (You must be logged in to change your digest settings.) If you have questions or feedback on the format of this digest please send it to ' . $board_config['board_email'] . '.';
$lang['digest_format_footer'] = 'Format:';
$lang['digest_forum'] = 'Forum: ';
$lang['digest_frequency'] = 'Digest Frequency:';
$lang['digest_from_email_address'] = $board_config['board_email'];
$lang['digest_from_text_name'] = $board_config['sitename'] . ' Digest Robot';
$lang['digest_introduction'] = 'Here is the latest digest of messages posted on ' . $board_config['sitename'] . ' forums. Please come and join the discussion!';
$lang['digest_invalid_key'] = 'The "key" parameter that was specified to run this program was not correct.';
$lang['digest_link'] = 'Link';
$lang['digest_message_excerpt'] = 'Message Excerpt';
$lang['digest_message_size']='Maximum characters per message in digest:';
$lang['digest_no_key'] = '"key" parameter required to run this program';
$lang['digest_no_new_messages']='There are no new messages for the forums you selected.';
$lang['digest_or'] = 'or';
$lang['digest_period_1_week'] = '1 week';
$lang['digest_period_24_hrs'] = '24 hours';
$lang['digest_pm_explanation_html'] = 'You have unread private messages. To read all of them, use <a href="' . $settings['site_url'] . "privmsg.$phpEx?folder=inbox\">this link</a>, or select the link next to the message below to reply directly to a private message.";
$lang['digest_pm_explanation_text'] = 'You have unread private messages. To reply to them either log onto the board or use this link: ' . $settings['site_url'] . "privmsg.$phpEx?folder=inbox";
$lang['digest_post_time'] = 'Post Time'; 
$lang['digest_posted_at'] = 'at';
$lang['digest_posted_by'] = 'Posted by';
$lang['digest_posts'] = 'posts';
$lang['digest_powered_by']='Powered by';
$lang['digest_private_messages'] = 'private messages';
$lang['digest_salutation'] = 'Dear';
$lang['digest_send_if_no_new_messages'] = 'Send digest if no new messages:';
$lang['digest_server_date'] = 'Server Date:';
$lang['digest_server_hour'] = 'Server Hour:';
$lang['digest_server_time_zone'] = 'Server Time Zone:';
$lang['digest_show_message_text'] = 'Show Message Text:';
$lang['digest_show_my_messages'] = 'Show My Messages:';
$lang['digest_show_only_new_messages'] = 'Show only new messages since last time I logged in:';
$lang['digest_simulate_mode_salutation_text'] = '(in simulation mode)';
$lang['digest_subject'] = 'Subject';
$lang['digest_subject_line'] = $board_config['sitename'] . ' Digests';
$lang['digest_summary']='Digest Summary';
$lang['digest_test_mode'] = 'In test mode. No emails were sent.';
$lang['digest_topic'] = 'Topic: ';
$lang['digest_total_posts'] = 'Total posts in this digest:';
$lang['digest_total_unread_private_messages'] = 'Total unread private messages:';
$lang['digest_unread_private_messages'] = 'You have unread private messages';
$lang['digest_version_text'] = 'Version';
$lang['digest_was_sent_to'] = 'was sent to';
$lang['digest_were_emailed'] = 'digests were emailed.';
$lang['digest_your_digest_options'] = 'Your digest options:';

// This block shows in admin_digests.php (used only by administrators)
$lang['digest_admin_action'] = '<b>Action</b>';
$lang['digest_admin_add_all_msg']='All registered users (except administrators) have been given digest subscriptions. Digest defaults settings were used.';
$lang['digest_admin_add_all_none_msg']='No digest actions were performed because all users (except administrators) are already subscribed.';
$lang['digest_admin_add_explain'] = "This interface is used to create digest subscriptions for users who are not already receiving digests. Only users not currently subscribed can be selected. Your board's default digest settings will apply to these users unless you explicitly change a user's settings.<br /><br />Changes will be made only to the users that you select. You cannot change individual forum subscriptions with this interface. Subscribers will receive digests for all forums for which they are allowed access.";
$lang['digest_admin_add_interface'] = 'Digest Add Subscribers';
$lang['digest_admin_autosubscribe']='<b>Enable automatic digest subscription?</b><br /><span class="gensmall">If you want new users to automatically get digests, check this box. They will received postings in all forums for which they are allowed access. The user default settings for digests will be automatically used. Enabling this option will not create subscriptions for currently unsubscribed users.</span>';
$lang['digest_admin_autosubscribe_daily']='<b>Set automatic digest subscription type</b><br /><span class="gensmall">This setting only applies if the <i>Enable automatic digest subscription</i> option is checked. If it is checked, new users will automatically receive digests of this type.</span>';
$lang['digest_admin_config_explain'] = "This interface is used to set digest configuration settings. You can set the defaults used to create new digests, adjust the behavior of the Digest Modification, as well as adjust many other settings.";
$lang['digest_admin_config_setting_saved']='Digest global settings were saved.';
$lang['digest_admin_configuration'] = 'Digest Configuration';
$lang['digest_admin_connect_socket_error'] = 'Unable to open connection to phpBB Digests site, reported error is:<br />%s';
$lang['digest_admin_current_version_info'] = 'You are running <b>%s</b>.';
$lang['digest_admin_custom_stylesheet_path']='<b>Custom stylesheet path</b><br /><span class="gensmall">This setting only applies if the <i>Enable custom stylesheet</i> box is checked. If it is checked, mailed digests with embedded HTML will apply this stylesheet to all digests. The path should be a relative path from your phpBB root directory. <i>Note:</i> you are responsible for creating this stylesheet and placing it in a file with the name entered here on the appropriate location on your server. For information on creating stylesheets, <a href="http://w3schools.com/css/default.asp" target="_new">click here</a>.</span>';
$lang['digest_admin_date_format']='<b>Digest date format</b><br /><span class="gensmall">This code is used to format how dates are displayed in digests. Use the codes and syntax found <a href="http://www.php.net/manual/en/function.date.php" target="_new">here</a>.</span>';
$lang['digest_admin_default_setting_saved']='Digest default settings were saved.';
$lang['digest_admin_delete'] = 'Delete';
$lang['digest_admin_excerpt'] = '<b>Default Display excerpt of message:</b>';
$lang['digest_admin_excerpt_short'] = '<b>Display Excerpt</b>';
$lang['digest_admin_format'] = '<b>Default Digest format:</b><br /><span class="gensmall">Enhanced HTML is highly recommended.</span>';
$lang['digest_admin_format_short'] = '<b>Format</b>';
$lang['digest_admin_forum_selection_explain']='<b>Note on Forum Selection Defaults.</b> Unfortunately, you cannot select individual forums to be chosen by default for a user. The permission schemes in phpBB are too complex. Moreover, a user should never be given access to a forum for which they do not have appropriate permissions. The user chooses the forums they want for themselves when they subscribe to a digest, and they can only select from those forums that they are allowed to access.<br /><br />When adding users in bulk using the Administrator Control Panel, <i>Add Subscribers</i>, users will received posts for all the forums for which they are allowed access.';
$lang['digest_admin_forum_subscriptions'] = '<b># Subscribed<br />Forums</b>';
$lang['digest_admin_forum_subscriptions_all'] = 'All Permitted';
$lang['digest_admin_global_submit'] = 'Make Global Digest Changes on Selected Items';
$lang['digest_admin_global_subscribe']='Global Subscriptions';
$lang['digest_admin_hour_to_send'] = "<b>Default Time of day to send digest:</b><br /><span class=\"gensmall\">Random is recommended. This will even the server's processor load, since each digest is created dynamically.</span>";
$lang['digest_admin_hour_to_send_short'] = '<b>Hour sent (your time)</b>';
$lang['digest_admin_html'] = 'HTML';
$lang['digest_admin_html_encoding']='<b>HTML encoding</b><br /><span class="gensmall">This indicates the character set used to encode HTML formatted digests using the text/html MIME type. It is assumed that your phpBB board is configured to send text in this character set. Note that iso-8859-1 represents most Latin based languages so it should be correct in 99% of situations. Change this value only if you are certain clients will receive HTML email in non-Latin based languages. For a listing of character codes <a href="http://en.wikipedia.org/wiki/Category:Character_sets" target="_new">click here</a>.</span>';
$lang['digest_admin_invalid_site_url']="The Forum Site URL is either blank, or does not begin with 'http://' or 'https://', or does not end in a '/'. This field should include the full URL to your phpBB forum.";
$lang['digest_admin_invalid_template_name']='The style name you chose is invalid. It must start with templates/ since this is the phpBB convention, followed by the style name, followed by the name of the .css file.';
$lang['digest_admin_latest_version_info'] = 'The latest available version is <b>%s</b>.';
$lang['digest_admin_may_not_be_blank']='may not be blank';
$lang['digest_admin_modify_explain'] = "This interface is used to globally change or delete digest options for your users. Changes will be made only to the users that you select. Please note that if you delete a user through the Administrator Control Panel, their digest subscriptions are removed. You cannot change individual forum subscriptions with this interface. If <i>All Permitted</i> shows then all forums for which this user has access are currently selected.";
$lang['digest_admin_modify_interface'] = 'Digest Modify Subscribers';
$lang['digest_admin_not_a_number']='is not a number';
$lang['digest_admin_not_negative']='may not be a negative number';
$lang['digest_admin_post_add_msg'] = 'Requested users were given digest subscriptions';
$lang['digest_admin_post_msg'] = 'Requested digest actions were applied';
$lang['digest_admin_post_msg_none'] = 'No digest actions were performed because no users were selected';
$lang['digest_admin_private_messages_in_digest']='<b>Default Show private messages in the digest:</b>';
$lang['digest_admin_private_messages_in_digest_short'] = '<b>Show PMs</b>';
$lang['digest_admin_random_hour']='Random';
$lang['digest_admin_require_send_key'] = "<b>Require key to run mail_digests.$phpEx</b><br /><span class=\"gensmall\">This is an extra security precaution you can enable. If enabled, mail_digests.php will only send digests if the key parameter is specified and the key matches the key value you define below. You will have to program your cron job or equivalent to use the key. Example of the resulting URL: http://www.myforum.com/phpBB2/mail_digests.$phpEx?key=qwertyuiop</span>";
$lang['digest_admin_return_admin_digests_add'] = 'Click %sHere%s to return to Digest Add Subscribers';
$lang['digest_admin_return_admin_digests_configure'] = 'Click %sHere%s to return to Digest Configuration';
$lang['digest_admin_return_admin_digests_modify'] = 'Click %sHere%s to return to Digest Modify Subscribers';
$lang['digest_admin_select'] = '<b>Select</b>';
$lang['digest_admin_select_subscribe']='Selected Subscriptions';
$lang['digest_admin_select_unselect_all'] = 'Select/Unselect All';
$lang['digest_admin_send_if_no_msgs'] = '<b>Default Send a digest if no new messages were posted:</b>';
$lang['digest_admin_send_if_no_msgs_short'] = '<b>Send if no<br />new msgs</b>';
$lang['digest_admin_send_key'] = '<b>Key value</b><br /><span class="gensmall">Bear in mind this is open text and not encrypted. Keep it simple. Avoid multiple words, spaces and special characters. Example: qwertyuiop</span>';
$lang['digest_admin_show_messages_short'] = "<b>Show<br />user's msgs</b>";
$lang['digest_admin_show_my_messages'] = "<b>Default Show user's messages in the digest:</b>";
$lang['digest_admin_show_new_only'] = '<b>Default Show new messages only:</b><br /><span class="gensmall">This will filter out any messages posted prior to the date and time the user last visited that would otherwise be included in the digest.</span>';
$lang['digest_admin_show_new_only_short'] = '<b>Show new<br />msgs only</b>';
$lang['digest_admin_show_summary']="<b>Show summary?</b><br /><span class=\"gensmall\">It is useful to show the summary when setting up the digest modification. It aids in debugging problems mailing the digest. When the mailer program (mail_digests.$phpEx) is run manually, as is recommended when setting up the mod, the summary will indicate who received digests and show certain server information. The summary does not appear in the digest itself, but can be seen or captured by administrators. It is recommended that once the digest mod is working in an automated fashion that the summary be disabled.</span>";
$lang['digest_admin_site_url']='<b>Your forum\'s site URL</b><br /><span class="gensmall">Enter the complete URL to your phpBB forum. Getting this right is <i>critical</i>. Otherwise all links in the digest will be incorrect. Make sure you leave on the / at the end. Example: http://www.example.com/phpBB2/</span>';
$lang['digest_admin_size']='<b>Default maximum characters to display per message:</b><br /><span class="gensmall">Showing all text in the message is recommended, unless you have a very heavily trafficked board or very many digest subscribers.</span>';
$lang['digest_admin_size_short'] = '<b>Max chars per msg</b>';
$lang['digest_admin_submit_text']='Change User Defaults';
$lang['digest_admin_subscribe_all']='Create Digest Subscriptions for All Usubscribed Users';
$lang['digest_admin_subscribe_all_explain']='If you choose, you can create digest subscriptions for all unsubscribed registered users of your forum. Administrators are not given subscriptions. Your board\'s default digest settings will be used. Use this feature with caution, since users may not want to receive a digest unless they choose to opt in. Since each digest is created dynamically, if you have a very large user base, you may strain the capacity of your server.';
$lang['digest_admin_summary_date_format']='<b>Summary date format</b><br /><span class="gensmall">This code is used to format how dates are displayed in summaries. Use the codes and syntax found <a href="http://www.php.net/manual/en/function.date.php" target="_new">here</a>.</span>';
$lang['digest_admin_summary_format']='<b>Summary format</b>';
$lang['digest_admin_text_encoding']='<b>Text encoding</b><br /><span class="gensmall">This indicates the character set used to encode plain text formatted digests for the text/ascii MIME type. Typically us-ascii will suffice. For a listing of character codes <a href="http://en.wikipedia.org/wiki/Category:Character_sets" target="_new">click here</a>.</span>';
$lang['digest_admin_update'] = 'Update';
$lang['digest_admin_update_delete_all'] = 'Delete/Update All';
$lang['digest_admin_use_custom_stylesheet']='<b>Enable custom stylesheet</b><br /><span class="gensmall">If not enabled, the default stylesheet selected in the user\'s profile is applied to HTML versions of their digest.</span>';
$lang['digest_admin_use_encodings']='<b>Modify encodings</b><br /><span class="gensmall">Generally it is best not to change the encodings used to send digests. If you do need to change these values, click this checkbox.</span>';
$lang['digest_admin_user_defaults']='User Default Settings';
$lang['digest_admin_user_settings']='Global Settings';
$lang['digest_admin_user_settings_submit']='Change Global Settings';
$lang['digest_admin_username'] = '<b>User</b>';
$lang['digest_admin_users_per_page'] = '<b>Users per page</b><br /><span class="gensmall">The number of users to show per page in the Administrator Control Panel under <i>Add</i> and <i>Modify Subscribers</i>. Enter 0 to show all users on one page.</span>';
$lang['digest_admin_version_not_up_to_date'] = 'Your installation does <b>not</b> seem have the latest version of phpBB Digests. Updates are available for your version of phpBB, please visit <a href="http://phpbb.potomactavern.org/digests/" target="_new">http://phpbb.potomactavern.org/digests/</a> to obtain the latest version.';
$lang['digest_admin_version_up_to_date'] = 'phpBB Digests is up to date, no updates are available for this modification.';
$lang['digest_admin_wanted'] = '<b>Default Type of digest:</b>';
$lang['digest_admin_wanted_short'] = '<b>Type</b>';
$lang['digest_admin_weekly_digest_day_of_week']='<b>Day of week to send weekly digests</b>';

// This block goes on the digest settings user interface page (digests.php). Some variables are shared with other programs.
$lang['digest_1am']='1 AM';
$lang['digest_1pm']='1 PM';
$lang['digest_2am']='2 AM';
$lang['digest_2pm']='2 PM';
$lang['digest_3am']='3 AM';
$lang['digest_3pm']='3 PM';
$lang['digest_4am']='4 AM';
$lang['digest_4pm']='4 PM';
$lang['digest_5am']='5 AM';
$lang['digest_5pm']='5 PM';
$lang['digest_6am']='6 AM';
$lang['digest_6pm']='6 PM';
$lang['digest_7am']='7 AM';
$lang['digest_7pm']='7 PM';
$lang['digest_8am']='8 AM';
$lang['digest_8pm']='8 PM';
$lang['digest_9am']='9 AM';
$lang['digest_9pm']='9 PM';
$lang['digest_10am']='10 AM';
$lang['digest_10pm']='10 PM';
$lang['digest_11am']='11 AM';
$lang['digest_11pm']='11 PM';
$lang['digest_12pm']='12 PM';
$lang['digest_all_forums']='All Subscribed Forums';
$lang['digest_auth_acl_text']='<i>(Special Access Only)</i>';
$lang['digest_auth_admin_text']='<i>(Administrators Only)</i>';
$lang['digest_auth_mod_text']='<i>(Moderators Only)</i>';
$lang['digest_auth_reg_text']='<i>(Registered Users Only)</i>';
$lang['digest_create']='Your digest settings were successfully created';
$lang['digest_daily'] = 'Daily';
$lang['digest_excerpt'] = '<b>Display Excerpt of Message:</b>';
$lang['digest_explanation'] = "Digests are email summaries of messages posted here that are sent to you periodically. Digests can be sent daily or weekly at an hour of the day you select. You can specify those particular forums for which you want message summaries, or you can elect to receive all messages for all forums for which you are allowed access.<br /><br />\nConsistent with our privacy policy digests contain no \"spam\", nor is your email address used in any way connected to an advertisement. You can, of course, cancel your digest subscription at any time by simply coming back to this page. Most users find digests to be very useful. We encourage you to give it a try!\n";
$lang['digest_format'] = '<b>Digest Format:</b><br /><span class="gensmall">HTML is highly recommended unless your email program cannot display HTML. Enhanced HTML includes clickable links, images, BBCode support and smilies in the digest.</span>';
$lang['digest_hour_to_send'] = '<b>Time of day to send digest:</b><br /><span class="gensmall">This is the time based on the time zone you set in your profile. If you change your profile time zone and you want digests to arrive at the same time of day then change this value too.</span>';
$lang['digest_hour_to_send_footer'] = 'Hour sent:';
$lang['digest_html_enhanced'] = 'Enhanced HTML';
$lang['digest_html_plain'] = 'Plain HTML';
$lang['digest_midnight']='Midnight';
$lang['digest_modify']='Your digest settings were successfully updated';
$lang['digest_no'] = 'No';
$lang['digest_no_forums_selected']='You have not selected any forums, so you will be unsubscribed';
$lang['digest_none'] = 'None (Unsubscribe)';
$lang['digest_not_subscribed']='You are currently NOT subscribed to receive a digest.';
$lang['digest_page_title'] = 'Digest Subscription';
$lang['digest_page_title_short'] = 'Digests';
$lang['digest_private_messages_in_digest']='<b>Show private messages in the digest?</b>';
$lang['digest_reset_text']='Reset';
$lang['digest_return_digests']='Click %sHere%s to return to Digest Subscription';
$lang['digest_select_forums']='<b>Send digests for these forums:</b>';
$lang['digest_send_if_no_msgs'] = '<b>Send a digest if no new messages were posted:</b>';
$lang['digest_show_my_messages'] = '<b>Show my messages in the digest:</b>';
$lang['digest_show_my_messages_plain'] = 'Show my messages in the digest:';
$lang['digest_show_new_only'] = '<b>Show new messages only:</b><br /><span class="gensmall">This will filter out any messages posted prior to the date and time you last visited that would otherwise be included in the digest.</span>';
$lang['digest_size']='<b>Maximum characters to display per message:</b><br /><span class="gensmall">Caution: setting this too high may make for very long digests. A link is provided for each message that will let you see the full content of the message.</span>';
$lang['digest_size_100']='100';
$lang['digest_size_150']='150';
$lang['digest_size_300']='300';
$lang['digest_size_50']='50';
$lang['digest_size_600']='600';
$lang['digest_size_max']='All';
$lang['digest_submit_text']='Make Digest Changes';
$lang['digest_subscribed']='You currently have a digest subscription. Your digest preferences are shown below.';
$lang['digest_text'] = 'Text';
$lang['digest_unsubscribe']='You have been unsubscribed and will no longer receive a digest';
$lang['digest_wanted'] = '<b>Type of Digest Wanted:</b><br /><span class="gensmall">Weekly digests are sent on %s</span>.';
$lang['digest_weekday'][0]='Sunday';
$lang['digest_weekday'][1]='Monday';
$lang['digest_weekday'][2]='Tuesday';
$lang['digest_weekday'][3]='Wednesday';
$lang['digest_weekday'][4]='Thursday';
$lang['digest_weekday'][5]='Friday';
$lang['digest_weekday'][6]='Saturday';
$lang['digest_weekly'] = 'Weekly';
$lang['digest_yes'] = 'Yes';
?>
