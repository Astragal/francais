Subject: [{SITENAME}] Riattiva il tuo conto 
Charset: iso-8859-1 

Salve {USERNAME}, 

Il tuo conto su �{SITENAME}� � stato disattivato, probabilmente perch� hai modificato dati significativi del tuo profilo (e.g., indirizzo di posta elettronica o parola d'accesso). 

Clicca sul collegamento sottostante per riattivarlo: 

{U_ACTIVATE} 

{EMAIL_SIG}