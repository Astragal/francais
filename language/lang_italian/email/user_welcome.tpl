Subject: Benvenuto su �{SITENAME}� 
Charset: iso-8859-1

{WELCOME_MSG}

Conserva i seguenti dati:

----------------------------
Nome utente: {USERNAME}
Parola d'accesso: {PASSWORD}
----------------------------

Non dimenticare la parola d'accesso: non sar� possibile recuperarla dalla nostra banca dati in quanto viene criptata.
Sar� comunque possibile - dalla pagina d'ingresso - richiederne una nuova, che sar� attivata come questo conto.

Grazie d'aver scelto il nostro forum.

{EMAIL_SIG}