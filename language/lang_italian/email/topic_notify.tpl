Subject: [{SITENAME}] Risposta nel filone �{TOPIC_TITLE}�
Charset: iso-8859-1 

Salve, 

Ricevi questo messaggio perch� stai controllando il filone �{TOPIC_TITLE}� su �{SITENAME}� e hai chiesto di essere avvisato di ogni nuovo intervento in esso pubblicato. 
(Non ci saranno altre segnalazioni finch� non visiterai la pagina.) 

{U_TOPIC} 

Se non desideri pi� che ti venga inviato un messaggio d'avviso per ogni nuovo intervento pubblicato in questo filone, puoi disattivare l'opzione nel forum cliccando su �Smetti di controllare questo filone� o, pi� semplicemente, cliccando sul seguente collegamento: 

{U_STOP_WATCHING_TOPIC} 

{EMAIL_SIG} 