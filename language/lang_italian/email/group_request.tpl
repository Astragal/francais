Subject: [{SITENAME}] Richiesta d'iscrizione a gruppo 
Charset: iso-8859-1 

Salve {GROUP_MODERATOR}, 

Un utente ha richiesto di essere iscritto a un gruppo da te moderato su �{SITENAME}�. 
Per approvare o respingere la richiesta, clicca sul seguente collegamento: 

{U_GROUPCP} 

{EMAIL_SIG}