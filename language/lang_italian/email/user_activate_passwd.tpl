Subject: [{SITENAME}] Attivazione nuova parola d'accesso 
Charset: iso-8859-1 

Salve {USERNAME} 

Ricevi questo messaggio perch� � stata richiesta da te (o da qualcuno a tuo nome) una nuova parola d'accesso per il tuo conto su �{SITENAME}�. Se non hai effettuato questa richiesta, ignorala; se per caso dovesse ripresentarsi, contatta il gestore del forum. 

Per usare la nuova parola d'accesso � necessario attivarla, semplicemente cliccando sul collegamento seguente. 

{U_ACTIVATE} 

Se l'operazione andr� a buon fine, potrai connetterti al nostro forum adoperando la seguente parola d'accesso: 

{PASSWORD} 

ATTENZIONE. Assic�rati di accedere al forum dopo aver cliccato sul collegamento: una volta entrato, potrai modificare la parola d'accesso nel profilo utente. Per qualsiasi dubbio, contatta l'amministratore del forum. 

{EMAIL_SIG}