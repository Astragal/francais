Charset: iso-8859-1

Salve {TO_USERNAME},

Questo messaggio ti � stato inviato da {FROM_USERNAME} attraverso il tuo conto su �{SITENAME}�.
Se ritieni che questo messaggio costituisca posta spazzatura, o reputi il suo contenuto offensivo od oltraggioso, puoi contattare il gestore del sito al seguente indirizzo:

{BOARD_EMAIL}

Includi questo messaggio (comprese le intestazioni).
Eventuali risposte a questo messaggio saranno indirizzate a {FROM_USERNAME}.

Segue il testo del messaggio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

{MESSAGE}