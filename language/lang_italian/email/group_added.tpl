Subject: [{SITENAME}] Sei stato aggiunto a un gruppo di utenti
Charset: iso-8859-1

Congratulazioni,

Il tuo nominativo � stato inserito nel gruppo �{GROUP_NAME}� su �{SITENAME}�.
Questa operazione � stata effettuata dal moderatore del gruppo o dall'amministratore del sito: contattali per maggiori informazioni.

Per le informazioni sul gruppo:

{U_GROUPCP}

{EMAIL_SIG}