Subject: Benvenuto su {SITENAME} 
Charset: iso-8859-1 

{WELCOME_MSG} 

Conserva questo messaggio. I dati di accesso al forum sono: 

---------------------------- 
Nome utente: {USERNAME} 
Parola d'accesso: {PASSWORD} 
---------------------------- 

La tua registrazione � in fase di completamento: l'amministrazione del forum dovr� attivare il tuo conto, affinch� tu possa accedere al forum. 
Ti verr� inviato un nuovo messaggio per avvisarti dell'attivazione. 

Non scordare la tua parola d'accesso: non sar� possibile recuperarla dalla nostra banca dati in quanto viene criptata. 
In caso di smarrimento della stessa, sar� comunque possibile richiederne una nuova dalla pagina d'accesso. 

Grazie per l'iscrizione. 

{EMAIL_SIG} 