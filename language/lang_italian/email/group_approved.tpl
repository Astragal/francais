Subject: [{SITENAME}] Approvazione richiesta d'iscrizione a gruppo 
Charset: iso-8859-1 

Congratulazioni, 

La richiesta d'iscrizione al gruppo �{GROUP_NAME}� su �{SITENAME}� � stata approvata. 
Per ulteriori informazioni sul gruppo (e per visualizzare i tuoi dati) clicca su: 

{U_GROUPCP} 

{EMAIL_SIG}