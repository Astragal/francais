Subject: Benvenuto su {SITENAME} 
Charset: iso-8859-1 

{WELCOME_MSG} 

In conformit� all'atto di Responsabilit� Minore il tuo profilo rimarr� inattivo. 

Stampa questo modulo; fallo firmare da un tuo genitore o tutore legale e datalo, quindi spediscilo via FAX a: 

{FAX_INFO} 

o via posta elettronica a: 

{MAIL_INFO} 

------------------------------ TAGLIA QUI ------------------------------ 
Consenso di partecipazione al forum {SITENAME} 
Nome utente: {USERNAME} 
Parola d'accesso: {PASSWORD} 
Indirizzo di posta elettronica: {EMAIL_ADDRESS} 

ICQ: {ICQ} 
AIM: {AIM} 
MSN: {MSN} 
Yahoo Messenger: {YIM} 
Sito personale: {WEB_SITE} 
Da: {FROM} 
Occupazione: {OCC} 
Interessi: {INTERESTS} 

HO CONTROLLATO I DATI INSERITI DA MIO/A FIGLIO/A E CON LA PRESENTE DO IL CONSENSO A {SITENAME} A USARE QUESTE INFORMAZIONI. 
SONO CONSAPEVOLE CHE LE INFORMAZIONI POSSONO ESSERE CAMBIATE IN OGNI MOMENTO INSERENDO LA PAROLA D'ACCESSO. 
SONO CONSAPEVOLE CHE POSSO RICHIEDERE CHE TALI INFORMAZIONI VENGANO RIMOSSE DA {SITENAME} IN QUALSIASI MOMENTO. 

Genitore o chi ne fa le veci 
(Nome e Cognome): _____________________ 

(Firma): __________________ 

Data: _______________ 

------------------------------ TAGLIA QUI ------------------------------ 

Appena l'amministratore avr� ricevuto il modulo soprastante via FAX o posta elettronica, il tuo conto sar� attivato. 

Attenzione: non dimenticare la parola d'accesso. Essa viene automaticamente criptata nella banca dati del forum, e non abbiamo modo di recuperarla. Tuttavia, dovessi mai dimenticarla, potrai sempre richiederne un'altra, che sar� attivata con questa stessa procedura. 

Grazie per la registrazione. 

{EMAIL_SIG} 