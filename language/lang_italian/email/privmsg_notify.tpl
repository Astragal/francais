Subject: [{SITENAME}] Nuovo messaggio privato
Charset: iso-8859-1

Salve {USERNAME},

Hai ricevuto un nuovo messaggio privato su �{SITENAME}�. Questo avviso ti viene inviato perch�, nelle impostazioni del forum, hai richiesto la notifica via posta dei nuovi messaggi privati.

Puoi guardare il messaggio a questo indirizzo:

{U_INBOX}

Se non desideri pi� essere avvisato dei nuovi messaggi privati, modifica l'opzione nelle preferenze del tuo profilo all'interno del forum.

{EMAIL_SIG}