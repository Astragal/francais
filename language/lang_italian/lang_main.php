<?php
/*************************************************************************** 
* lang_main.php [Italian] 
* ------------------- 
* begin : Sat Dec 16 2000 
* copyright : (C) 2001 The phpBB Group 
* email : support@phpbb.com 
* 
* $Id: lang_main.php,v 1.85.2.21 2006/02/05 15:59:48 grahamje Exp $ 
* 
****************************************************************************/ 
/*************************************************************************** 
* 
* This program is free software; you can redistribute it and/or modify 
* it under the terms of the GNU General Public License as published by 
* the Free Software Foundation; either version 2 of the License, or 
* (at your option) any later version. 
* 
***************************************************************************/ 
// 
// CONTRIBUTORS: 
// 2005-10-23 phpbb.it - http://www.phpbb.it - info@phpbb.it
// 2007-08-16 Societ� dei Cruscanti - http://www.achyra.org/cruscate/
// 
// 
// The format of this file is ---> $lang['message'] = 'text'; 
// 
// You should also try to set a locale and a character encoding (plus direction). The encoding and direction 
// will be sent to the template. The locale may or may not work, it's dependent on OS support and the syntax 
// varies ... give it your best guess! 
//
$lang['ENCODING'] = 'iso-8859-1'; 
$lang['DIRECTION'] = 'ltr'; 
$lang['LEFT'] = 'sinistra'; 
$lang['RIGHT'] = 'destra'; 
$lang['DATE_FORMAT'] = 'd/m/y H:i'; // This should be changed to the default date format for your language, php date() format 
// This is optional, if you would like a _SHORT_ message output 
// along with our copyright message indicating you are the translator 
// please add it here. 
// $lang['TRANSLATION'] = '';
$lang['TRANSLATION_INFO'] = 'Traduzione italiana a cura della <a href="http://www.achyra.org/cruscate/" class="copyright" target="_blank">Societ� dei Cruscanti</a>'; 
$lang['TRANSLATION'] = 'Traduzione italiana a cura della <a href="http://www.achyra.org/cruscate/" class="copyright" target="_blank">Societ� dei Cruscanti</a>'; 
// 
// Common, these terms are used 
// extensively on several pages 
// 
$lang['Forum'] = 'Sezione'; 
$lang['Category'] = 'Categoria'; 
$lang['Topic'] = 'Filone'; 
$lang['Topics'] = 'Filoni'; 
$lang['Replies'] = 'Risposte'; 
$lang['Views'] = 'Consultazioni'; 
$lang['Post'] = 'Intervento'; 
$lang['Posts'] = 'Interventi'; 
$lang['Posted'] = 'Inviato'; 
$lang['Username'] = 'Nome utente'; 
$lang['Password'] = 'Parola d\'accesso'; 
$lang['Email'] = 'Posta elettronica'; 
$lang['Poster'] = 'Scritto da'; 
$lang['Author'] = 'Autore'; 
$lang['Time'] = 'Data'; 
$lang['Hours'] = 'Ore'; 
$lang['Message'] = 'Intervento'; 
$lang['1_Day'] = '1 giorno';
$lang['7_Days'] = '7 giorni';
$lang['2_Weeks'] = '2 settimane';
$lang['1_Month'] = '1 mese'; 
$lang['3_Months'] = '3 mesi'; 
$lang['6_Months'] = '6 mesi'; 
$lang['1_Year'] = '1 anno'; 
$lang['Go'] = 'Vai'; 
$lang['Jump_to'] = 'Vai a'; 
$lang['Submit'] = 'Invia'; 
$lang['Reset'] = 'Azzera'; 
$lang['Cancel'] = 'Annulla';
$lang['Preview'] = 'Anteprima'; 
$lang['Confirm'] = 'Conferma'; 
$lang['Spellcheck'] = 'Controllo ortografico'; 
$lang['Yes'] = 'S�'; 
$lang['No'] = 'No'; 
$lang['Enabled'] = 'Abilitato'; 
$lang['Disabled'] = 'Disabilitato'; 
$lang['Error'] = 'Errore'; 
$lang['Next'] = 'successiva'; 
$lang['Previous'] = 'precedente'; 
$lang['Goto_page'] = 'Vai alla pagina'; 
$lang['Joined'] = 'Registrato'; 
$lang['IP_Address'] = 'Indirizzo IP'; 
$lang['Select_forum'] = 'Seleziona una sezione'; 
$lang['View_latest_post'] = 'Visualizza l\'ultimo intervento'; 
$lang['View_newest_post'] = 'Visualizza l\'intervento pi� recente';
$lang['Page_of'] = 'Pagina <b>%d</b> di <b>%d</b>'; // Replaces with: Page 1 of 2 for example 
$lang['ICQ'] = 'ICQ'; 
$lang['AIM'] = 'AIM'; 
$lang['MSNM'] = 'MSN'; 
$lang['YIM'] = 'Yahoo'; 
$lang['Forum_Index'] = 'Indice del forum'; // eg. sitename Forum Index, %s can be removed if you prefer 
$lang['Post_new_topic'] = 'Apri filone'; 
$lang['Reply_to_topic'] = 'Intervieni'; 
$lang['Reply_with_quote'] = 'Intervieni citando'; 
$lang['Click_return_topic'] = 'Clicca %squi%s per tornare al filone'; // %s's here are for uris, do not remove! 
$lang['Click_return_login'] = 'Clicca %squi%s per riprovare l\'accesso'; 
$lang['Click_return_forum'] = 'Clicca %squi%s per tornare alla sezione'; 
$lang['Click_view_message'] = 'Clicca %squi%s per vedere il tuo intervento'; 
$lang['Click_return_modcp'] = 'Clicca %squi%s per tornare al pannello di controllo moderatore'; 
$lang['Click_return_group'] = 'Clicca %squi%s per tornare a Informazioni gruppo'; 
$lang['Admin_panel'] = 'Pannello d\'amministrazione';
$lang['Board_disable'] = 'Spiacenti, ma il forum al momento non � disponibile: prova pi� tardi.'; 
// 
// Global Header strings 
// 
$lang['Registered_users'] = 'Utenti registrati:'; 
$lang['Browsing_forum'] = 'Utenti presenti in questa sezione:'; 
$lang['Online_users_zero_total'] = 'In totale ci sono <b>0</b> utenti in linea :: '; 
$lang['Online_users_total'] = 'In totale ci sono <b>%d</b> utenti in linea :: '; 
$lang['Online_user_total'] = 'In totale c\'� <b>%d</b> utente in linea :: '; 
$lang['Reg_users_zero_total'] = '0 Registrati, '; 
$lang['Reg_users_total'] = '%d Registrati, '; 
$lang['Reg_user_total'] = '%d Registrato, '; 
$lang['Hidden_users_zero_total'] = '0 Nascosti, ';
$lang['Hidden_user_total'] = '%d Nascosto, ';
$lang['Hidden_users_total'] = '%d Nascosti, ';
$lang['Guest_users_zero_total'] = '0 Ospiti e ';
$lang['Guest_users_total'] = '%d Ospiti e ';
$lang['Guest_user_total'] = '%d Ospite e ';
$lang['Bot_users_zero_total'] = '0 Robot';
$lang['Bot_users_total'] = '%d Robot';
$lang['Bot_user_total'] = '%d Robot';
$lang['Record_online_users'] = 'Numero massimo di utenti in linea contemporaneamente: <b>%s</b> (registrato in data %s)'; // first %s = number of users, second %s is the date. 
$lang['Admin_online_color'] = '%sAmministratore%s'; 
$lang['Mod_online_color'] = '%sModeratore%s'; 
$lang['You_last_visit'] = 'Ultimo accesso: %s'; // %s replaced by date/time 
$lang['Current_time'] = 'La data di oggi � %s'; // %s replaced by time 
$lang['Search_new'] = 'Visualizza gli interventi non letti'; 
$lang['Search_your_posts'] = 'Visualizza tutti i miei interventi'; 
$lang['Search_unanswered'] = 'Visualizza gli interventi senza risposta'; 
$lang['Register'] = 'Registrami'; 
$lang['Profile'] = 'Profilo'; 
$lang['Edit_profile'] = 'Modifica il tuo profilo'; 
$lang['Search'] = 'Cerca'; 
$lang['Memberlist'] = 'Lista utenti'; 
$lang['FAQ'] = 'Domande ricorrenti'; 
$lang['BBCode_guide'] = 'Guida BBCode'; 
$lang['Usergroups'] = 'Gruppi utenti'; 
$lang['Last_Post'] = 'Ultimo intervento'; 
$lang['Moderator'] = 'Moderatore'; 
$lang['Moderators'] = 'Moderatori'; 
// 
// Stats block text 
// 
$lang['Posted_articles_zero_total'] = 'Non ci sono interventi nel forum'; // Number of posts 
$lang['Posted_articles_total'] = 'Ci sono <b>%d</b> interventi nel forum'; // Number of posts 
$lang['Posted_article_total'] = 'C\'� <b>%d</b> intervento nel forum'; // Number of posts 
$lang['Registered_users_zero_total'] = 'Abbiamo <b>0</b> utenti registrati'; // # registered users 
$lang['Registered_users_total'] = 'Abbiamo <b>%d</b> utenti registrati'; // # registered users 
$lang['Registered_user_total'] = 'Abbiamo <b>%d</b> utente registrato'; // # registered users 
$lang['Newest_user'] = 'Ultimo utente registratosi: <b>%s%s%s</b>'; // a href, username, /a
$lang['No_new_posts_last_visit'] = 'Nessun nuovo intervento dal momento della tua ultima visita.'; 
$lang['No_new_posts'] = 'Non ci sono nuovi interventi'; 
$lang['New_posts'] = 'Nuovi interventi'; 
$lang['New_post'] = 'Nuovo intervento'; 
$lang['No_new_posts_hot'] = 'Non ci sono nuovi interventi [Filone popolare]'; 
$lang['New_posts_hot'] = 'Nuovi interventi [Filone popolare]'; 
$lang['No_new_posts_locked'] = 'Non ci sono nuovi interventi [Filone chiuso]'; 
$lang['New_posts_locked'] = 'Nuovi interventi [Filone chiuso]'; 
$lang['Forum_is_locked'] = 'La sezione � chiusa';
// 
// Login 
// 
$lang['Enter_password'] = 'Inserisci nome utente e parola d\'accesso per entrare.'; 
$lang['Login'] = 'Entra'; 
$lang['Logout'] = 'Esci';
$lang['Forgotten_password'] = 'Ho dimenticato la parola d\'accesso'; 
$lang['Log_me_in'] = 'Connessione automatica ad ogni visita'; 
$lang['Error_login'] = 'I dati inseriti non sono corretti.'; 
// 
// Index page 
// 
$lang['Index'] = 'Indice'; 
$lang['No_Posts'] = 'Nessun intervento'; 
$lang['No_forums'] = 'Non ci sono sezioni in questo forum.'; 
$lang['Private_Message'] = 'Messaggio privato'; 
$lang['Private_Messages'] = 'Messaggi privati'; 
$lang['Who_is_Online'] = 'Chi c\'� in linea';
$lang['Mark_all_forums'] = 'Segna tutte le sezioni come gi� lette'; 
$lang['Forums_marked_read'] = 'Tutte le sezioni sono state segnate come gi� lette'; 
// 
// Viewforum 
// 
$lang['View_forum'] = 'Guarda sezione'; 
$lang['Forum_not_exist'] = 'La sezione selezionata non esiste.'; 
$lang['Reached_on_error'] = 'Sei arrivato in questa pagina per errore.'; 
$lang['Display_topics'] = 'Mostra i filoni di'; 
$lang['All_Topics'] = 'Tutti i filoni'; 
$lang['Topic_Announcement'] = '<b>Annuncio:</b>'; 
$lang['Topic_Sticky'] = '<b>Importante:</b>'; 
$lang['Topic_Moved'] = '<b>Spostato:</b>'; 
$lang['Topic_Poll'] = '<b>[ Sondaggio ]</b>'; 
$lang['Mark_all_topics'] = 'Segna tutti i filoni come gi� letti'; 
$lang['Topics_marked_read'] = 'Tutti i filoni di questa sezione sono stati segnati come gi� letti'; 
$lang['Rules_post_can'] = '<b>Puoi</b> aprire nuovi filoni'; 
$lang['Rules_post_cannot'] = '<b>Non puoi</b> aprire nuovi filoni'; 
$lang['Rules_reply_can'] = '<b>Puoi</b> intervenire in tutti i filoni'; 
$lang['Rules_reply_cannot'] = '<b>Non puoi</b> intervenire in nessun filone'; 
$lang['Rules_edit_can'] = '<b>Puoi</b> modificare i tuoi interventi'; 
$lang['Rules_edit_cannot'] = '<b>Non puoi</b> modificare i tuoi interventi'; 
$lang['Rules_delete_can'] = '<b>Puoi</b> cancellare i tuoi interventi'; 
$lang['Rules_delete_cannot'] = '<b>Non puoi</b> cancellare i tuoi interventi'; 
$lang['Rules_vote_can'] = '<b>Puoi</b> votare nei sondaggi'; 
$lang['Rules_vote_cannot'] = '<b>Non puoi</b> votare nei sondaggi'; 
$lang['Rules_moderate'] = '<b>Puoi</b> %smoderare questa sezione%s'; // %s replaced by a href links, do not remove!
$lang['No_topics_post_one'] = 'Non ci sono filoni in questa sezione.<br />Clicca <b>Apri filone</b> per crearne uno.'; 
// 
// Viewtopic 
// 
$lang['View_topic'] = 'Filone'; 
$lang['Guest'] = 'Ospite'; 
$lang['Post_subject'] = 'Oggetto'; 
$lang['View_next_topic'] = 'Vai al filone successivo'; 
$lang['View_previous_topic'] = 'Vai al filone precedente'; 
$lang['Submit_vote'] = 'Invia voto'; 
$lang['View_results'] = 'Guarda i risultati'; 
$lang['No_newer_topics'] = 'Non ci sono nuovi filoni in questa sezione'; 
$lang['No_older_topics'] = 'Non ci sono vecchi filoni in questa sezione'; 
$lang['Topic_post_not_exist'] = 'Il filone o l\'intervento che hai richiesto non esiste'; 
$lang['No_posts_topic'] = 'Il filone non contiene interventi'; 
$lang['Display_posts'] = 'Mostra gli interventi di'; 
$lang['All_Posts'] = 'Tutti gli interventi'; 
$lang['Newest_First'] = 'prima i nuovi'; 
$lang['Oldest_First'] = 'prima i vecchi'; 
$lang['Back_to_top'] = 'Torna a inizio pagina'; 
$lang['Read_profile'] = 'Guarda il profilo';
$lang['Visit_website'] = 'Visita il sito personale';
$lang['ICQ_status'] = 'ICQ'; 
$lang['Edit_delete_post'] = 'Modifica/Cancella intervento'; 
$lang['View_IP'] = 'Mostra indirizzo IP'; 
$lang['Delete_post'] = 'Cancella intervento'; 
$lang['wrote'] = 'ha scritto'; // proceeds the username and is followed by the quoted text 
$lang['Quote'] = 'Citazione'; // comes before bbcode quote output. 
$lang['Code'] = 'Codice'; // comes before bbcode code output. 
$lang['Edited_time_total'] = 'Ultima modifica effettuata da %s in data %s; modificato %d volta in totale.'; // Last edited by me on 12 Oct 2001; edited 1 time in total 
$lang['Edited_times_total'] = 'Ultima modifica effettuata da %s in data %s; modificato %d volte in totale.'; // Last edited by me on 12 Oct 2001; edited 2 times in total 
$lang['Lock_topic'] = 'Chiudi filone'; 
$lang['Unlock_topic'] = 'Riapri filone'; 
$lang['Move_topic'] = 'Sposta filone'; 
$lang['Delete_topic'] = 'Cancella filone'; 
$lang['Split_topic'] = 'Dividi filone'; 
$lang['Stop_watching_topic'] = 'Smetti di controllare questo filone'; 
$lang['Start_watching_topic'] = 'Controlla questo filone'; 
$lang['No_longer_watching'] = 'Non stai pi� controllando questo filone'; 
$lang['You_are_watching'] = 'Adesso stai controllando questo filone'; 
$lang['Total_votes'] = 'Voti totali';
// 
// Posting/Replying (Not private messaging!) 
// 
$lang['Message_body'] = 'Testo'; 
$lang['Topic_review'] = 'Sinossi filone'; 
$lang['No_post_mode'] = 'Metodo d\'invio non specificato'; // If posting.php is called without a mode (newtopic/reply/delete/etc, shouldn't be shown normaly) 
$lang['Post_a_new_topic'] = 'Apri filone'; 
$lang['Post_a_reply'] = 'Intervieni'; 
$lang['Post_topic_as'] = 'Tipo di filone'; 
$lang['Edit_Post'] = 'Modifica intervento'; 
$lang['Options'] = 'Opzioni'; 
$lang['Post_Announcement'] = 'Annuncio'; 
$lang['Post_Sticky'] = 'Importante'; 
$lang['Post_Normal'] = 'Normale'; 
$lang['Confirm_delete'] = 'Sei sicuro di voler cancellare questo intervento?'; 
$lang['Confirm_delete_poll'] = 'Sei sicuro di voler cancellare questo sondaggio?'; 
$lang['Flood_Error'] = 'Non puoi inviare un nuovo intervento! Attendi qualche istante prima d\'inviare un nuovo intervento.';
$lang['Empty_subject'] = 'Devi specificare l\'oggetto quando apri un nuovo filone.'; 
$lang['Empty_message'] = 'Devi scrivere qualcosa nel campo �Testo� per procedere con l\'operazione.'; 
$lang['Forum_locked'] = 'Questa sezione � chiusa: non puoi aprire nuovi filoni, intervenire in o modificare i filoni  esistenti.';
$lang['Topic_locked'] = 'Questo filone � chiuso: non puoi inviare nuovi interventi o modificare gli interventi in esso contenuti.'; 
$lang['No_post_id'] = 'Non � stato specificato nessun intervento'; 
$lang['No_topic_id'] = 'Non � stato specificato nessun filone in cui intervenire'; 
$lang['No_valid_mode'] = 'Puoi solo inviare, modificare o citare interventi. Torna indietro e prova di nuovo.'; 
$lang['No_such_post'] = 'Questo intervento non esiste. Torna indietro e prova di nuovo.'; 
$lang['Edit_own_posts'] = 'Puoi modificare solo i tuoi interventi.'; 
$lang['Delete_own_posts'] = 'Puoi cancellare solo i tuoi interventi.'; 
$lang['Cannot_delete_replied'] = 'Non puoi cancellare gli interventi che hanno una risposta.'; 
$lang['Cannot_delete_poll'] = 'Non puoi cancellare un sondaggio attivo.'; 
$lang['Empty_poll_title'] = 'Devi inserire un titolo per il tuo sondaggio.'; 
$lang['To_few_poll_options'] = 'Devi inserire almeno due opzioni per il sondaggio.'; 
$lang['To_many_poll_options'] = 'Ci sono troppe opzioni per il sondaggio.'; 
$lang['Post_has_no_poll'] = 'Questo filone non ha sondaggi.'; 
$lang['Already_voted'] = 'Hai gi� votato in questo sondaggio.'; 
$lang['No_vote_option'] = 'Devi specificare un\'opzione quando voti.'; 
$lang['Add_poll'] = 'Aggiungi sondaggio'; 
$lang['Add_poll_explain'] = 'Se non vuoi aggiungere un sondaggio, lascia vuoti i campi'; 
$lang['Poll_question'] = 'Domanda del sondaggio'; 
$lang['Poll_option'] = 'Opzione del sondaggio'; 
$lang['Add_option'] = 'Aggiungi un\'opzione'; 
$lang['Update'] = 'Aggiorna'; 
$lang['Delete'] = 'Cancella'; 
$lang['Poll_for'] = 'Attiva il sondaggio per'; 
$lang['Days'] = 'giorni'; // This is used for the Run poll for ... Days + in admin_forums for pruning 
$lang['Poll_for_explain'] = '(Inserisci 0 o lascia vuoto il campo per un sondaggio senza fine)'; 
$lang['Delete_poll'] = 'Cancella sondaggio'; 
$lang['Disable_HTML_post'] = 'Disabilita HTML nell\'intervento'; 
$lang['Disable_BBCode_post'] = 'Disabilita BBCode nell\'intervento'; 
$lang['Disable_Smilies_post'] = 'Disabilita faccine nell\'intervento'; 
$lang['HTML_is_ON'] = 'HTML <u>ATTIVO</u>'; 
$lang['HTML_is_OFF'] = 'HTML <u>DISATTIVATO</u>'; 
$lang['BBCode_is_ON'] = '%sBBCode%s <u>ATTIVO</u>'; // %s are replaced with URI pointing to FAQ 
$lang['BBCode_is_OFF'] = '%sBBCode%s <u>DISATTIVATO</u>'; 
$lang['Smilies_are_ON'] = 'Faccine <u>ATTIVE</u>'; 
$lang['Smilies_are_OFF'] = 'Faccine <u>DISATTIVATE</u>'; 
$lang['Attach_signature'] = 'Aggiungi la firma (puoi cambiare la firma nel tuo profilo)'; 
$lang['Notify'] = 'Avvisami quando viene inviata una risposta'; 
$lang['Stored'] = 'Intervento inserito con successo.'; 
$lang['Deleted'] = 'L\'intervento � stato cancellato.'; 
$lang['Poll_delete'] = 'Il sondaggio � stato cancellato.'; 
$lang['Vote_cast'] = 'Il tuo voto � stato aggiunto.'; 
$lang['Topic_reply_notification'] = 'Notifica nuovo intervento nel filone'; 
$lang['bbcode_b_help'] = 'Grassetto: [b]testo[/b] (alt+b)'; 
$lang['bbcode_i_help'] = 'Corsivo: [i]testo[/i] (alt+i)'; 
$lang['bbcode_u_help'] = 'Sottolineato: [u]testo[/u] (alt+u)'; 
$lang['bbcode_q_help'] = 'Citazione: [quote]testo[/quote] (alt+q)'; 
$lang['bbcode_c_help'] = 'Codice: [code]codice[/code] (alt+c)'; 
$lang['bbcode_l_help'] = 'Lista: [list]testo[/list] (alt+l)'; 
$lang['bbcode_o_help'] = 'Lista ordinata: [list=]testo[/list] (alt+o)'; 
$lang['bbcode_p_help'] = 'Inserisci immagine: [img]http://image_url[/img] (alt+p)'; 
$lang['bbcode_w_help'] = 'Inserisci URL: [url]http://url[/url] o [url=http://url]testo URL[/url] (alt+w)'; 
$lang['bbcode_a_help'] = 'Chiudi tutti i codici BBCode aperti'; 
$lang['bbcode_s_help'] = 'Colore testo: [color=red]testo[/color] Suggerimento: puoi anche usare color=#FF0000'; 
$lang['bbcode_f_help'] = 'Dimensione testo: [size=x-small]testo piccolo[/size]'; 
$lang['Emoticons'] = 'Faccine'; 
$lang['More_emoticons'] = 'Altre faccine'; 
$lang['Font_color'] = 'Colore'; 
$lang['color_default'] = 'Predefinito'; 
$lang['color_dark_red'] = 'Rosso scuro'; 
$lang['color_red'] = 'Rosso'; 
$lang['color_orange'] = 'Arancione'; 
$lang['color_brown'] = 'Marrone'; 
$lang['color_yellow'] = 'Giallo'; 
$lang['color_green'] = 'Verde'; 
$lang['color_olive'] = 'Oliva'; 
$lang['color_cyan'] = 'Ciano'; 
$lang['color_blue'] = 'Blu'; 
$lang['color_dark_blue'] = 'Blu scuro'; 
$lang['color_indigo'] = 'Indaco'; 
$lang['color_violet'] = 'Viola'; 
$lang['color_white'] = 'Bianco'; 
$lang['color_black'] = 'Nero'; 
$lang['Font_size'] = 'Dimensione'; 
$lang['font_tiny'] = 'Minuscolo'; 
$lang['font_small'] = 'Piccolo'; 
$lang['font_normal'] = 'Normale'; 
$lang['font_large'] = 'Grande'; 
$lang['font_huge'] = 'Enorme'; 
$lang['Close_Tags'] = 'Chiudi i codici'; 
$lang['Styles_tip'] = 'Suggerimento: gli stili possono essere applicati velocemente al testo selezionato';
//$lang['Code_of_Conduct_Discalmer'] = 'Inviando questo intervento, dichiaro che esso � conforme al <a href="http://www.achyra.org/cruscate/viewtopic.php?t=3018">codice di buona condotta</a> del forum';
// 
// Private Messaging 
//
$lang['Private_Messaging'] = 'Messaggi privati'; 
$lang['Login_check_pm'] = 'Messaggi privati'; 
$lang['New_pms'] = '%d nuovi messaggi'; // You have 2 new messages 
$lang['New_pm'] = '%d nuovo messaggio'; // You have 1 new message 
$lang['No_new_pm'] = 'Non ci sono nuovi messaggi'; 
$lang['Unread_pms'] = '%d messaggi non letti'; 
$lang['Unread_pm'] = '%d messaggio non letto'; 
$lang['No_unread_pm'] = 'Hai letto tutti i messaggi'; 
$lang['You_new_pm'] = 'Hai un nuovo messaggio in Messaggi ricevuti'; 
$lang['You_new_pms'] = 'Ci sono nuovi messaggi in Messaggi ricevuti'; 
$lang['You_no_new_pm'] = 'Non ci sono nuovi messaggi'; 
$lang['Unread_message'] = 'Messaggio da leggere'; 
$lang['Read_message'] = 'Messaggio letto'; 
$lang['Read_pm'] = 'Leggi messaggio';
$lang['Post_new_pm'] = 'Nuovo messaggio'; 
$lang['Post_reply_pm'] = 'Rispondi'; 
$lang['Post_quote_pm'] = 'Cita messaggio'; 
$lang['Edit_pm'] = 'Modifica messaggio'; 
$lang['Inbox'] = 'Messaggi ricevuti'; 
$lang['Outbox'] = 'Messaggi inviati'; 
$lang['Savebox'] = 'Messaggi salvati'; 
$lang['Sentbox'] = 'Messaggi recapitati'; 
$lang['Flag'] = 'Stato'; 
$lang['Subject'] = 'Oggetto'; 
$lang['From'] = 'Da'; 
$lang['To'] = 'A'; 
$lang['Date'] = 'Data'; 
$lang['Mark'] = 'Seleziona'; 
$lang['Sent'] = 'Inviato'; 
$lang['Saved'] = 'Salvato'; 
$lang['Delete_marked'] = 'Cancella selezionati'; 
$lang['Delete_all'] = 'Cancella tutti'; 
$lang['Save_marked'] = 'Salva selezionati'; 
$lang['Save_message'] = 'Salva messaggio'; 
$lang['Delete_message'] = 'Cancella messaggio'; 
$lang['Display_messages'] = 'Mostra i messaggi di'; // Followed by number of days/weeks/months 
$lang['All_Messages'] = 'Tutti i messaggi'; 
$lang['No_messages_folder'] = 'Non ci sono messaggi in questa cartella'; 
$lang['PM_disabled'] = 'I messaggi privati sono stati disabilitati dall\'amministratore del forum.'; 
$lang['Cannot_send_privmsg'] = 'L\'amministratore del forum ti ha revocato i permessi per inviare messaggi privati.'; 
$lang['No_to_user'] = 'Devi specificare un nome utente per inviare il messaggio.'; 
$lang['No_such_user'] = 'L\'utente non esiste.'; 
$lang['Disable_HTML_pm'] = 'Disabilita HTML nel messaggio'; 
$lang['Disable_BBCode_pm'] = 'Disabilita BBCode nel messaggio'; 
$lang['Disable_Smilies_pm'] = 'Disabilita faccine nel messaggio'; 
$lang['Message_sent'] = 'Il tuo messaggio � stato inviato.'; 
$lang['Click_return_inbox'] = 'Torna alla cartella %sMessaggi ricevuti%s'; 
$lang['Click_return_index'] = 'Torna %sall\'Indice%s'; 
$lang['Send_a_new_message'] = 'Invia nuovo messaggio privato'; 
$lang['Send_a_reply'] = 'Rispondi a messaggio privato'; 
$lang['Edit_message'] = 'Modifica messaggio privato'; 
$lang['Notification_subject'] = 'Hai un nuovo messaggio privato!'; 
$lang['Find_username'] = 'Cerca utente';
$lang['Find'] = 'Cerca'; 
$lang['No_match'] = 'Nessun risultato'; 
$lang['No_post_id'] = 'Non � stato specificato nessun ID.'; 
$lang['No_such_folder'] = 'Questa cartella non esiste.'; 
$lang['No_folder'] = 'Nessuna cartella specificata'; 
$lang['Mark_all'] = 'Seleziona tutti'; 
$lang['Unmark_all'] = 'Deseleziona tutti'; 
$lang['Confirm_delete_pm'] = 'Sei sicuro di voler cancellare questo messaggio?'; 
$lang['Confirm_delete_pms'] = 'Sei sicuro di voler cancellare questi messaggi?'; 
$lang['Inbox_size'] = 'Uso cartella Messaggi ricevuti %d%%'; // eg. Your Inbox is 50% full 
$lang['Sentbox_size'] = 'Uso cartella Messaggi recapitati %d%%';
$lang['Savebox_size'] = 'Uso cartella Messaggi salvati %d%%';
$lang['Click_view_privmsg'] = 'Clicca %squi%s per andare alla cartella di Messaggi ricevuti'; 
// 
// Profiles/Registration 
// 
$lang['Viewing_user_profile'] = 'Guarda il profilo :: %s'; // %s is username
$lang['About_user'] = 'Tutto su %s'; // %s is username 
$lang['Preferences'] = 'Preferenze'; 
$lang['Items_required'] = 'Le voci contrassegnate con * sono obbligatorie.'; 
$lang['Registration_info'] = 'Dettagli registrazione'; 
$lang['Profile_info'] = 'Dettagli profilo'; 
$lang['Profile_info_warn'] = 'Queste informazioni saranno visibili a tutti gli utenti.'; 
$lang['Avatar_panel'] = 'Pannello di controllo avatara'; 
$lang['Avatar_gallery'] = 'Galleria avatara'; 
$lang['Website'] = 'Sito personale'; 
$lang['Location'] = 'Residenza'; 
$lang['Contact'] = 'Contatta'; 
$lang['Email_address'] = 'Indirizzo di posta elettronica'; 
$lang['Send_private_message'] = 'Invia messaggio privato'; 
$lang['Hidden_email'] = '[ Nascosto ]'; 
$lang['Interests'] = 'Interessi'; 
$lang['Occupation'] = 'Impiego'; 
$lang['Poster_rank'] = 'Livello utente'; 
$lang['Total_posts'] = 'Interventi totali'; 
$lang['User_post_pct_stats'] = '%.2f%% del totale'; // 1.25% of total 
$lang['User_post_day_stats'] = '%.2f interventi al giorno'; // 1.5 posts per day 
$lang['Search_user_posts'] = 'Guarda tutti gli interventi scritti da %s'; // Find all posts by username 
$lang['No_user_id_specified'] = 'L\'utente non esiste o non � attivo.'; 
$lang['Wrong_Profile'] = 'Non puoi modificare questo profilo.'; 
$lang['Only_one_avatar'] = 'Pu� essere specificato un solo tipo di avatara'; 
$lang['File_no_data'] = 'L\'archivio all\'indirizzo che hai fornito non contiene dati'; 
$lang['No_connection_URL'] = 'Non � possibile connettersi all\'indirizzo che hai fornito'; 
$lang['Incomplete_URL'] = 'L\'indirizzo che hai fornito � incompleto'; 
$lang['Wrong_remote_avatar_format'] = 'L\'indirizzo dell\'avatara remoto non � valido'; 
$lang['No_send_account_inactive'] = 'Spiacenti, ma la tua parola d\'accesso non pu� essere recuperata perch� il tuo profilo al momento � inattivo. Contatta l\'amministratore per ulteriori informazioni.'; 
$lang['Always_smile'] = 'Abilita sempre le faccine'; 
$lang['Always_html'] = 'Abilita sempre l\'HTML'; 
$lang['Always_bbcode'] = 'Abilita sempre il BBCode'; 
$lang['Always_add_sig'] = 'Aggiungi sempre la mia firma'; 
$lang['Always_notify'] = 'Avvisami sempre dei nuovi interventi'; 
$lang['Always_notify_explain'] = 'Verrai avvisato con un messaggio di posta elettronica ogni volta che verr� pubblicato un intervento in un filone in cui tu sia precedentemente intervenuto. Questa impostazione pu� essere individualmente disabilitata per ogni nuovo intervento.'; 
$lang['Board_style'] = 'Stile forum'; 
$lang['Board_lang'] = 'Lingua'; 
$lang['No_themes'] = 'Non sono presenti stili nella banca dati'; 
$lang['Timezone'] = 'Fuso orario'; 
$lang['Date_format'] = 'Formato data'; 
$lang['Date_format_explain'] = 'La sintassi usata � la stessa della funzione <a href="http://www.php.net/manual/it/html/function.date.html" target="_other">date()</a> del PHP.'; 
$lang['Signature'] = 'Firma'; 
$lang['Signature_explain'] = 'Testo che verr� visualizzato come firma in tutti i tuoi interventi. C\'� un limite di %d caratteri.'; 
$lang['Public_view_email'] = 'Mostra sempre il mio indirizzo di posta elettronica'; 
$lang['Current_password'] = 'Parola d\'accesso attuale'; 
$lang['New_password'] = 'Nuova parola d\'accesso'; 
$lang['Confirm_password'] = 'Conferma parola d\'accesso'; 
$lang['Confirm_password_explain'] = 'Devi confermare la tua parola d\'accesso attuale se vuoi cambiarla o modificare il tuo indirizzo di posta elettronica'; 
$lang['password_if_changed'] = 'Devi inserire la parola d\'accesso solo se vuoi cambiarla'; 
$lang['password_confirm_if_changed'] = 'Devi confermare la tua parola d\'accesso solo se ne hai inserita una nuova qui sopra'; 
$lang['Avatar'] = 'Avatara'; 
$lang['Avatar_explain'] = 'Mostra una piccola immagine sopra i tuoi estremi a fianco di ogni intervento. Pu� essere mostrata una sola immagine: la sua larghezza massima � di %d px, l\'altezza di %d px, e l\'archivio deve essere pi� piccolo di %dkB.'; 
$lang['Upload_Avatar_file'] = 'Carica avatara da PC'; 
$lang['Upload_Avatar_URL'] = 'Carica avatara da un URL'; 
$lang['Upload_Avatar_URL_explain'] = 'Inserisci l\'URL dell\'avatara, che verr� copiato su questo sito'; 
$lang['Pick_local_Avatar'] = 'Seleziona avatara dalla galleria'; 
$lang['Link_remote_Avatar'] = 'Collegamento esterno avatara'; 
$lang['Link_remote_Avatar_explain'] = 'Inserisci l\'URL dell\'avatara che vuoi inserire'; 
$lang['Avatar_URL'] = 'URL avatara'; 
$lang['Select_from_gallery'] = 'Seleziona avatara dalla galleria'; 
$lang['View_avatar_gallery'] = 'Mostra galleria'; 
$lang['Select_avatar'] = 'Seleziona avatara'; 
$lang['Return_profile'] = 'Cancella avatara'; 
$lang['Select_category'] = 'Seleziona categoria'; 
$lang['Delete_Image'] = 'Cancella immagine'; 
$lang['Current_Image'] = 'Immagine attuale'; 
$lang['Notify_on_privmsg'] = 'Avvisami dei nuovi messaggi privati'; 
$lang['Popup_on_privmsg'] = 'Finestra di avviso nuovo messaggio privato'; 
$lang['Popup_on_privmsg_explain'] = 'Apre una finestra a comparsa per informarti quando arriva un nuovo messaggio privato'; 
$lang['Hide_user'] = 'Nascondi il mio stato in linea'; 
$lang['Profile_updated'] = 'Il tuo profilo � stato aggiornato'; 
$lang['Profile_updated_inactive'] = 'Il tuo profilo � stato aggiornato. Hai modificato dettagli importanti anche se il tuo profilo non � attivo. Controlla la tua posta per riattivare il tuo profilo, o, se richiesta, attendi la riattivazione da parte dell\'amministratore.'; 
$lang['Password_mismatch'] = 'La parola d\'accesso inserita non corrisponde.'; 
$lang['Current_password_mismatch'] = 'La parola d\'accesso inserita non corrisponde a quella presente nella banca dati.'; 
$lang['Password_long'] = 'La parola d\'accesso non deve essere pi� lunga di 32 caratteri.'; 
$lang['Username_taken'] = 'Spiacenti: il nome utente scelto � gi� in uso.';
$lang['Username_invalid'] = 'Spiacenti: il nome utente scelto contiene un carattere non valido come, e.g., \'.';
$lang['Username_disallowed'] = 'Spiacenti: il nome utente scelto rientra fra quelli non consentiti.'; 
$lang['Email_taken'] = 'Spiacenti: l\'indirizzo di posta inserito risulta gi� associato a un utente.'; 
$lang['Email_banned'] = 'Spiacenti: l\'indirizzo di posta inserito rientra fra quelli non consentiti.'; 
$lang['Email_invalid'] = 'Spiacenti: l\'indirizzo di posta non � valido.'; 
$lang['Signature_too_long'] = 'La firma � troppo lunga.'; 
$lang['Fields_empty'] = 'Devi riempire tutti i campi richiesti.'; 
$lang['Avatar_filetype'] = 'L\'archivio avatara deve essere .jpg, .gif o .png'; 
$lang['Avatar_filesize'] = 'La grandezza dell\'archivio dell\'avatara deve essere inferiore a %d kB.'; // The avatar image file size must be less than 6 KB 
$lang['Avatar_imagesize'] = 'L\'avatara non pu� superare la dimensione di %d px di larghezza e di %d px d\'altezza.';
$lang['Welcome_subject'] = 'Benvenuto su %s!'; // Welcome to my.com forums 
$lang['New_account_subject'] = 'Conto nuovo utente'; 
$lang['Account_activated_subject'] = 'Conto attivato'; 
$lang['Account_added'] = 'Grazie di esserti registrato. Il tuo conto � stato creato. Usa nome utente e parola d\'accesso per entrare.'; 
$lang['Account_inactive'] = 'Il tuo conto � stato creato. Questo forum richiede l\'attivazione del conto. La chiave per l\'attivazione � stata inviata all\'indirizzo di posta elettronica che hai inserito. Controlla la tua casella postale per ulteriori informazioni.'; 
$lang['Account_inactive_admin'] = 'Il tuo conto � stato creato. Questo forum richiede l\'attivazione del conto da parte dell\'amministratore. Ti verr� inviato un messaggio di posta elettronica dall\'amministratore e sarai informato sullo stato di attivazione del tuo conto.'; 
$lang['Account_active'] = 'Il tuo conto � stato attivato. Grazie di esserti registrato.'; 
$lang['Account_active_admin'] = 'Il tuo conto � stato attivato. Grazie di esserti registrato.'; 
$lang['Reactivate'] = 'Riattiva il tuo conto!'; 
$lang['Already_activated'] = 'Questo conto � gi� stato attivato.'; 
$lang['COPPA'] = 'Il tuo conto � stato creato, ma deve essere approvato. Controlla la tua casella postale per ulteriori informazioni.'; 
$lang['Registration'] = 'Condizioni per la registrazione';
$lang['Reg_agreement'] = 'Anche se gli amministratori e i moderatori di questo forum cercheranno di rimuovere o modificare tutto il materiale inopportuno il pi� velocemente possibile, � comunque impossibile verificare ogni intervento pubblicato. L\'utente riconosce che tutti gl\'interventi pubblicati in questo forum esprimono il punto di vista e le opinioni dell\'autore e non quelle degli amministratori, dei moderatori o del gestore del sito (eccetto gl\'interventi degli stessi), i quali non sono per questi perseguibili.<br /><br />L\'utente acconsente a non inviare interventi abusivi, osceni, volgari, diffamat�ri, di odio, minat�ri, a sfondo sessuale o qualunque altro materiale che possa violare qualunque legge applicabile. L\'invio d\'interventi di questo tipo comporter� la disabilitazione immediata e permanente del conto dell\'autore (e ci� verr� reso noto al suo fornitore d\'accesso). L\'indirizzo IP dal quale ogni intervento � stato inviato viene registrato affinch� queste condizioni siano rispettate. L\'utente riconosce e accetta che l\'amministratore e i moderatori di questo forum nonch� il gestore del sito hanno il diritto di rimuovere, modificare o chiudere filoni qualora lo ritengano necessario.<br /><br /><center><strong>L\'utente dichiara inoltre di aver preso visione del <a href="viewtopic.php?p=29382" target="_blank">codice di buona condotta</a> del forum e d\'impegnarsi a rispettarne le regole.</strong></center><br /><br />Come utente, acconsenti a che i dati del tuo profilo siano conservati in una banca dati. Tali informazioni non verranno cedute a terzi senza il tuo consenso. Tuttavia, il gestore del sito, l\'amministratore e i moderatori non possono essere ritenuti responsabili di eventuali attacchi da parte di pirati informatici che compromettano la sicurezza dei tuoi dati.<br /><br />Questo forum usa i �biscottini� (in inglese, <i>cookies</i>) per registrare sul tuo calcolatore informazioni utili al suo funzionamento. Questi biscottini non contengono i dati personali di cui sopra; servono unicamente a migliorare la fruizione dei contenuti del forum da parte tua. L\'indirizzo di posta elettronica viene utilizzato unicamente per la notifica dei dati di registrazione e della parola d\'accesso (nonch� per l\'invio di una nuova parola d\'accesso in caso di smarrimento di quella attuale).';
$lang['Agree_under_13'] = 'Accetto queste condizioni e ho <b>meno</b> di 13 anni'; 
$lang['Agree_over_13'] = 'Accetto queste condizioni e ho <b>pi�</b> di 13 anni';
$lang['Agree_not'] = 'Non accetto queste condizioni'; 
$lang['Wrong_activation'] = 'La chiave di attivazione che hai fornito non corrisponde a nessuna presente nella banca dati.'; 
$lang['Send_password'] = 'Inviami una nuova parola d\'accesso';
$lang['Password_updated'] = 'Una nuova parola d\'accesso � stata creata, controlla la tua posta per maggiori dettagli su come attivarla.'; 
$lang['No_email_match'] = 'L\'indirizzo di posta inserito non corrisponde a quello attuale per questo utente.'; 
$lang['New_password_activation'] = 'Attivazione nuova parola d\'accesso'; 
$lang['Password_activated'] = 'Il tuo conto � stato riattivato. Per entrare usa la parola d\'accesso ricevuta via posta elettronica.'; 
$lang['Send_email_msg'] = 'Invia un messaggio di posta elettronica'; 
$lang['No_user_specified'] = 'Non � stato specificato nessun utente'; 
$lang['User_prevent_email'] = 'L\'utente non gradisce ricevere posta. Prova a inviare un messaggio privato.'; 
$lang['User_not_exist'] = 'Questo utente non esiste'; 
$lang['CC_email'] = 'Inviami una copia del messaggio'; 
$lang['Email_message_desc'] = 'Questo messaggio verr� inviato come testo, non includere nessun codice HTML o BBCode. L\'indirizzo per la risposta di questo messaggio � il tuo indirizzo di posta elettronica.'; 
$lang['Flood_email_limit'] = 'Non puoi inviare un altro messaggio al momento. Prova pi� tardi.'; 
$lang['Recipient'] = 'A';
$lang['Email_sent'] = 'Il messaggio � stato inviato.'; 
$lang['Send_email'] = 'Invia messaggio di posta elettronica'; 
$lang['Empty_subject_email'] = 'Devi specificare un oggetto per il messaggio.'; 
$lang['Empty_message_email'] = 'Devi inserire un messaggio da inviare.'; 
// 
// Visual confirmation system strings 
// 
$lang['Confirm_code_wrong'] = 'Il codice di conferma inserito non � corretto'; 
$lang['Too_many_registers'] = 'Hai superato il numero massimo di tentativi per questa sessione. Ritenta pi� tardi.'; 
$lang['Confirm_code_impaired'] = 'Se hai problemi alla vista o comunque non riesci a visualizzare il codice di registrazione, contatta l\'%samministratore%s.'; 
$lang['Confirm_code'] = 'Codice di conferma'; 
$lang['Confirm_code_explain'] = 'Inserisci il codice di conferma visuale come indicato nell\'immagine. Il sistema riconosce la differenza tra maiuscole e minuscole, lo zero ha una barra diagonale per distinguerlo dalla lettera O.'; 
// 
// Memberslist 
//
$lang['Select_sort_method'] = 'Seleziona un tipo di ordinamento'; 
$lang['Sort'] = 'Ordina'; 
$lang['Sort_Top_Ten'] = 'I 10 autori pi� prolifici'; 
$lang['Sort_Joined'] = 'Data di registrazione'; 
$lang['Sort_Username'] = 'Nome utente'; 
$lang['Sort_Location'] = 'Residenza'; 
$lang['Sort_Posts'] = 'Numero interventi'; 
$lang['Sort_Email'] = 'Posta elettronica'; 
$lang['Sort_Website'] = 'Sito personale'; 
$lang['Sort_Ascending'] = 'Crescente'; 
$lang['Sort_Descending'] = 'Decrescente'; 
$lang['Order'] = 'Ordine'; 
// 
// Group control panel 
// 
$lang['Group_Control_Panel'] = 'Pannello di controllo gruppo'; 
$lang['Group_member_details'] = 'Dettagli utenti gruppo'; 
$lang['Group_member_join'] = 'Iscrivi un gruppo'; 
$lang['Group_Information'] = 'Informazioni gruppo'; 
$lang['Group_name'] = 'Nome gruppo'; 
$lang['Group_description'] = 'Descrizione gruppo'; 
$lang['Group_membership'] = 'Appartenenza al gruppo'; 
$lang['Group_Members'] = 'Utenti del gruppo'; 
$lang['Group_Moderator'] = 'Moderatore gruppo'; 
$lang['Pending_members'] = 'Nuovi iscritti in attesa'; 
$lang['Group_type'] = 'Tipo di gruppo'; 
$lang['Group_open'] = 'Gruppo aperto'; 
$lang['Group_closed'] = 'Gruppo chiuso'; 
$lang['Group_hidden'] = 'Gruppo nascosto'; 
$lang['Current_memberships'] = 'Utenti attuali gruppo'; 
$lang['Non_member_groups'] = 'Non sei iscritto al gruppo'; 
$lang['Memberships_pending'] = 'Nuovi iscritti al gruppo in attesa'; 
$lang['No_groups_exist'] = 'Non esistono gruppi'; 
$lang['Group_not_exist'] = 'Gruppo non esistente'; 
$lang['Join_group'] = 'Iscriviti al gruppo'; 
$lang['No_group_members'] = 'Questo gruppo non ha utenti iscritti'; 
$lang['Group_hidden_members'] = 'Gruppo nascosto, non puoi vedere i suoi utenti'; 
$lang['No_pending_group_members'] = 'Questo gruppo non ha utenti in attesa'; 
$lang['Group_joined'] = 'Ti sei iscritto a questo gruppo con successo.<br />Sarai avvisato quando la tua iscrizione verr� approvata dal moderatore del gruppo.'; 
$lang['Group_request'] = 'C\'� una richiesta di iscrizione al tuo gruppo.'; 
$lang['Group_approved'] = 'La tua richiesta � stata approvata.'; 
$lang['Group_added'] = 'Sei stato aggiunto a questo gruppo.';
$lang['Already_member_group'] = 'Sei gi� iscritto a questo gruppo'; 
$lang['User_is_member_group'] = 'L\'utente � gi� iscritto a questo gruppo'; 
$lang['Group_type_updated'] = 'Tipo di gruppo aggiornato.'; 
$lang['Could_not_add_user'] = 'L\'utente selezionato non esiste.'; 
$lang['Could_not_anon_user'] = 'L\'utente anonimo non pu� essere iscritto a un gruppo.'; 
$lang['Confirm_unsub'] = 'Sei sicuro di volerti cancellare da questo gruppo?'; 
$lang['Confirm_unsub_pending'] = 'La tua iscrizione a questo gruppo non � ancora stata approvata, sei sicuro di volerti cancellare?'; 
$lang['Unsub_success'] = 'Sei stato cancellato da questo gruppo.'; 
$lang['Approve_selected'] = 'Approvazione selezionata'; 
$lang['Deny_selected'] = 'Rifiuto selezionato'; 
$lang['Not_logged_in'] = 'Per iscriverti a un gruppo devi essere registrato.'; 
$lang['Remove_selected'] = 'Rimuovi selezionati'; 
$lang['Add_member'] = 'Aggiungi utente'; 
$lang['Not_group_moderator'] = 'Non sei moderatore di questo gruppo, non puoi eseguire questa azione.'; 
$lang['Login_to_join'] = 'Entra per iscriverti o gestire un gruppo di utenti'; 
$lang['This_open_group'] = 'Gruppo aperto, clicca per richiedere l\'adesione'; 
$lang['This_closed_group'] = 'Gruppo chiuso, non si accettano altri membri'; 
$lang['This_hidden_group'] = 'Gruppo nascosto, non � permesso aggiungere nuovi utenti automaticamente'; 
$lang['Member_this_group'] = 'Sei iscritto a questo gruppo'; 
$lang['Pending_this_group'] = 'La tua iscrizione a questo gruppo � in attesa di approvazione'; 
$lang['Are_group_moderator'] = 'Sei moderatore di questo gruppo'; 
$lang['None'] = 'Nessuno'; 
$lang['Subscribe'] = 'Iscriviti'; 
$lang['Unsubscribe'] = 'Cancella'; 
$lang['View_Information'] = 'Guarda informazioni'; 
// 
// Search 
// 
$lang['Search_query'] = 'Cerca'; 
$lang['Search_options'] = 'Opzioni di ricerca'; 
$lang['Search_keywords'] = 'Cerca per parole chiave'; 
$lang['Search_keywords_explain'] = 'Puoi usare <u>AND</u> per separare parole che devono essere presenti nei risultati della ricerca, <u>OR</u> per separare parole che possono essere presenti nei risultati e <u>NOT</u> per escludere parole che non devono trovarsi nei risultati. Usa l\'asterisco (*) come carattere jolly per le corrispondenze parziali.';
$lang['Search_author'] = 'Cerca per autore'; 
$lang['Search_author_explain'] = 'Usa l\'asterisco (*) come carattere jolly per le corrispondenze parziali.'; 
$lang['Search_for_any'] = 'Cerca almeno una parola o usa l\'interrogazione immessa'; 
$lang['Search_for_all'] = 'Cerca tutte le parole';
$lang['Search_title_msg'] = 'Cerca nel titolo del filone e nel testo dell\'intervento'; 
$lang['Search_msg_only'] = 'Cerca solo nel testo'; 
$lang['Return_first'] = 'Mostra i primi'; // followed by xxx characters in a select box 
$lang['characters_posts'] = 'caratteri dell\'intervento'; 
$lang['Search_previous'] = 'Cerca gli interventi di'; // followed by days, weeks, months, year, all in a select box 
$lang['Sort_by'] = 'Ordina per'; 
$lang['Sort_Time'] = 'Data intervento'; 
$lang['Sort_Post_Subject'] = 'Oggetto intervento'; 
$lang['Sort_Topic_Title'] = 'Titolo filone'; 
$lang['Sort_Author'] = 'Autore'; 
$lang['Sort_Forum'] = 'Sezione'; 
$lang['Display_results'] = 'Mostra i risultati come'; 
$lang['All_available'] = 'Tutte';
$lang['All_characters'] = 'Tutti';
$lang['No_searchable_forums'] = 'Non hai i permessi per usare il motore di ricerca del forum.'; 
$lang['No_search_match'] = 'Nessun filone o intervento con questo criterio di ricerca.'; 
$lang['Found_search_match'] = 'La ricerca ha trovato %d risultato.'; // eg. Search found 1 match
$lang['Found_search_matches'] = 'La ricerca ha trovato %d risultati.'; // eg. Search found 24 matches
$lang['Search_Flood_Error'] = 'Non puoi eseguire una nuova ricerca cos� ravvicinata alla precedente. Riprova tra qualche istante.';
$lang['Close_window'] = 'Chiudi finestra';
$lang['Search_classic'] = 'Modalit� di ricerca phpBB2 classica';
$lang['Search_loading'] = 'Carico&hellip;';
$lang['Search_next_result_page'] = 'Avanti';
$lang['Search_previous_result_page'] = 'Indietro';
$lang['Search_no_results'] = 'Nessun risultato';
$lang['Search_powered_by'] = 'fornito da';

// 
// Auth related entries 
// 
// Note the %s will be replaced with one of the following 'user' arrays 
$lang['Sorry_auth_announce'] = 'Solo %s possono inviare annunci.'; 
$lang['Sorry_auth_sticky'] = 'Solo %s possono inviare comunicazioni importanti.';
$lang['Sorry_auth_read'] = 'Solo %s possono leggere i filoni.';
$lang['Sorry_auth_post'] = 'Solo %s possono aprire filoni.';
$lang['Sorry_auth_reply'] = 'Solo %s possono intervenire nel filone.'; 
$lang['Sorry_auth_edit'] = 'Solo %s possono modificare gli interventi.';
$lang['Sorry_auth_delete'] = 'Solo %s possono cancellare gli interventi.'; 
$lang['Sorry_auth_vote'] = 'Solo %s possono votare ai sondaggi.'; 
// These replace the %s in the above strings 
$lang['Auth_Anonymous_Users'] = '<b>gli utenti anonimi</b>'; 
$lang['Auth_Registered_Users'] = '<b>gli utenti registrati</b>'; 
$lang['Auth_Users_granted_access'] = '<b>gli utenti con accesso speciale</b>'; 
$lang['Auth_Moderators'] = '<b>i moderatori</b>'; 
$lang['Auth_Administrators'] = '<b>gli amministratori</b>'; 
$lang['Not_Moderator'] = 'Non sei moderatore di questa sezione.'; 
$lang['Not_Authorised'] = 'Non autorizzato'; 
$lang['You_been_banned'] = 'Sei stato bandito da questo forum. <br />Contatta l\'amministratore o il gestore del sito per ulteriori informazioni.'; 

// 
// Viewonline 
// 
$lang['Reg_users_zero_online'] = 'Ci sono 0 utenti registrati e '; // There are 5 Registered and 
$lang['Reg_users_online'] = 'Ci sono %d utenti registrati e '; // There are 5 Registered and 
$lang['Reg_user_online'] = 'C\'� %d utente registrato e '; // There is 1 Registered and 
$lang['Hidden_users_zero_online'] = '0 utenti nascosti in linea'; // 6 Hidden users online 
$lang['Hidden_users_online'] = '%d utenti nascosti in linea'; // 6 Hidden users online 
$lang['Hidden_user_online'] = '%d utente nascosto in linea'; // 6 Hidden users online 
$lang['Guest_users_online'] = 'Ci sono %d ospiti in linea'; // There are 10 Guest users online 
$lang['Guest_users_zero_online'] = 'Ci sono 0 ospiti in linea'; // There are 10 Guest users online 
$lang['Guest_user_online'] = 'C\'� %d ospite in linea'; // There is 1 Guest user online 
$lang['No_users_browsing'] = 'Al momento non ci sono utenti nel forum'; 
$lang['Online_explain'] = 'Questi dati si basano sugli utenti connessi negli ultimi cinque minuti'; 
$lang['Forum_Location'] = 'Localit� del forum'; 
$lang['Last_updated'] = 'Ultimo aggiornamento'; 
$lang['Forum_index'] = 'Indice del forum'; 
$lang['Logging_on'] = 'Sta entrando'; 
$lang['Posting_message'] = 'Sta scrivendo un intervento'; 
$lang['Searching_forums'] = 'Sta cercando nelle sezioni'; 
$lang['Viewing_profile'] = 'Sta guardando il profilo'; 
$lang['Viewing_online'] = 'Sta guardando chi c\'� in linea'; 
$lang['Viewing_member_list'] = 'Sta guardando la lista utenti'; 
$lang['Viewing_priv_msgs'] = 'Sta guardando i messaggi privati'; 
$lang['Viewing_FAQ'] = 'Sta guardando le domande ricorrenti'; 
// 
// Moderator Control Panel 
//
$lang['Mod_CP'] = 'Pannello di controllo moderatori'; 
$lang['Mod_CP_explain'] = 'Usando il modulo sottostante puoi eseguire operazioni collettive su questa sezione. Puoi chiudere, riaprire, spostare o cancellare filoni.'; 
$lang['Select'] = 'Seleziona'; 
$lang['Delete'] = 'Cancella'; 
$lang['Move'] = 'Sposta'; 
$lang['Lock'] = 'Chiudi'; 
$lang['Unlock'] = 'Riapri'; 
$lang['Topics_Removed'] = 'I filoni selezionati sono stati rimossi dalla banca dati.'; 
$lang['Topics_Locked'] = 'I filoni selezionati sono stati chiusi.'; 
$lang['Topics_Moved'] = 'I filoni selezionati sono stati spostati.'; 
$lang['Topics_Unlocked'] = 'I filoni selezionati sono stati riaperti.'; 
$lang['No_Topics_Moved'] = 'Non � stato spostato nessun filone.'; 
$lang['Confirm_delete_topic'] = 'Sei sicuro di voler cancellare i filoni selezionati?'; 
$lang['Confirm_lock_topic'] = 'Sei sicuro di voler chiudere i filoni selezionati?'; 
$lang['Confirm_unlock_topic'] = 'Sei sicuro di voler riaprire i filoni selezionati?'; 
$lang['Confirm_move_topic'] = 'Sei sicuro di volere spostare i filoni selezionati?'; 
$lang['Move_to_forum'] = 'Vai alla sezione'; 
$lang['Leave_shadow_topic'] = 'Lascia una traccia nella sezione di creazione.'; 
$lang['Split_Topic'] = 'Divisione filoni'; 
$lang['Split_Topic_explain'] = 'Usando il modulo sottostante puoi dividere il filone in due selezionando individualmente gli interventi da spostare oppure selezionando l\'intervento a partire dal quale effettuare la suddivisione.'; 
$lang['Split_title'] = 'Titolo nuovo filone'; 
$lang['Split_forum'] = 'Sezione per il nuovo filone'; 
$lang['Split_posts'] = 'Sposta in un nuovo filone gli interventi selezionati'; 
$lang['Split_after'] = 'Dividi in due il filone partendo dall\'intervento selezionato'; 
$lang['Topic_split'] = 'Il filone selezionato � stato diviso'; 
$lang['Too_many_error'] = 'Hai selezionato troppi interventi. Puoi selezionare solo l\'intervento da cui dividere il filone!'; 
$lang['None_selected'] = 'Nessun filone selezionato nel quale eseguire questa operazione. Torna indietro e selezionane almeno uno.'; 
$lang['New_forum'] = 'Nuova sezione'; 
$lang['This_posts_IP'] = 'Indirizzo IP per questo intervento'; 
$lang['Other_IP_this_user'] = 'Altri indirizzi IP usati da questo utente'; 
$lang['Users_this_IP'] = 'Utenti che usano questo indirizzo IP'; 
$lang['IP_info'] = 'Informazioni indirizzo IP'; 
$lang['Lookup_IP'] = 'Cerca indirizzo IP'; 
// 
// Timezones ... for display on each page 
// 
$lang['All_times'] = 'Tutte le ore sono relative al %s'; // eg. All times are GMT - 12 Hours (times from next block) 
$lang['-12'] = 'GMT - 12 ore'; 
$lang['-11'] = 'GMT - 11 ore'; 
$lang['-10'] = 'GMT - 10 ore'; 
$lang['-9'] = 'GMT - 9 ore'; 
$lang['-8'] = 'GMT - 8 ore'; 
$lang['-7'] = 'GMT - 7 ore'; 
$lang['-6'] = 'GMT - 6 ore'; 
$lang['-5'] = 'GMT - 5 ore'; 
$lang['-4'] = 'GMT - 4 ore'; 
$lang['-3.5'] = 'GMT - 3.5 ore'; 
$lang['-3'] = 'GMT - 3 ore'; 
$lang['-2'] = 'GMT - 2 ore'; 
$lang['-1'] = 'GMT - 1 ore'; 
$lang['0'] = 'GMT'; 
$lang['1'] = 'GMT + 1 ora'; 
$lang['2'] = 'GMT + 2 ore'; 
$lang['3'] = 'GMT + 3 ore'; 
$lang['3.5'] = 'GMT + 3.5 ore'; 
$lang['4'] = 'GMT + 4 ore'; 
$lang['4.5'] = 'GMT + 4.5 ore'; 
$lang['5'] = 'GMT + 5 ore'; 
$lang['5.5'] = 'GMT + 5.5 ore'; 
$lang['6'] = 'GMT + 6 ore'; 
$lang['6.5'] = 'GMT + 6.5 ore'; 
$lang['7'] = 'GMT + 7 ore'; 
$lang['8'] = 'GMT + 8 ore'; 
$lang['9'] = 'GMT + 9 ore'; 
$lang['9.5'] = 'GMT + 9.5 ore'; 
$lang['10'] = 'GMT + 10 ore'; 
$lang['11'] = 'GMT + 11 ore'; 
$lang['12'] = 'GMT + 12 ore'; 
$lang['13'] = 'GMT + 13 ore'; 
// These are displayed in the timezone select box 
$lang['tz']['-12'] = 'GMT -12:00 ore'; 
$lang['tz']['-11'] = 'GMT -11:00 ore'; 
$lang['tz']['-10'] = 'GMT -10:00 ore'; 
$lang['tz']['-9'] = 'GMT -9:00 ore'; 
$lang['tz']['-8'] = 'GMT -8:00 ore'; 
$lang['tz']['-7'] = 'GMT -7:00 ore'; 
$lang['tz']['-6'] = 'GMT -6:00 ore'; 
$lang['tz']['-5'] = 'GMT -5:00 ore'; 
$lang['tz']['-4'] = 'GMT -4:00 ore'; 
$lang['tz']['-3.5'] = 'GMT -3:30 ore'; 
$lang['tz']['-3'] = 'GMT -3:00 ore'; 
$lang['tz']['-2'] = 'GMT -2:00 ore'; 
$lang['tz']['-1'] = 'GMT -1:00 ora'; 
$lang['tz']['0'] = 'GMT'; 
$lang['tz']['1'] = 'GMT +1:00 ora'; 
$lang['tz']['2'] = 'GMT +2:00 ore'; 
$lang['tz']['3'] = 'GMT +3:00 ore'; 
$lang['tz']['3.5'] = 'GMT +3:30 ore'; 
$lang['tz']['4'] = 'GMT +4:00 ore'; 
$lang['tz']['4.5'] = 'GMT +4:30 ore'; 
$lang['tz']['5'] = 'GMT +5:00 ore'; 
$lang['tz']['5.5'] = 'GMT +5:30 ore'; 
$lang['tz']['6'] = 'GMT +6:00 ore'; 
$lang['tz']['6.5'] = 'GMT +6:30 ore'; 
$lang['tz']['7'] = 'GMT +7:00 ore'; 
$lang['tz']['8'] = 'GMT +8:00 ore'; 
$lang['tz']['9'] = 'GMT +9:00 ore'; 
$lang['tz']['9.5'] = 'GMT +9:30 ore'; 
$lang['tz']['10'] = 'GMT + 10 ore'; 
$lang['tz']['11'] = 'GMT + 11 ore'; 
$lang['tz']['12'] = 'GMT + 12 ore'; 
$lang['tz']['13'] = 'GMT + 13 ore'; 
$lang['datetime']['Sunday'] = 'domenica'; 
$lang['datetime']['Monday'] = 'luned�'; 
$lang['datetime']['Tuesday'] = 'marted�'; 
$lang['datetime']['Wednesday'] = 'mercoled�'; 
$lang['datetime']['Thursday'] = 'gioved�'; 
$lang['datetime']['Friday'] = 'venerd�'; 
$lang['datetime']['Saturday'] = 'sabato'; 
$lang['datetime']['Sun'] = 'dom'; 
$lang['datetime']['Mon'] = 'lun'; 
$lang['datetime']['Tue'] = 'mar'; 
$lang['datetime']['Wed'] = 'mer'; 
$lang['datetime']['Thu'] = 'gio'; 
$lang['datetime']['Fri'] = 'ven'; 
$lang['datetime']['Sat'] = 'sab'; 
$lang['datetime']['January'] = 'gennaio'; 
$lang['datetime']['February'] = 'febbraio'; 
$lang['datetime']['March'] = 'marzo'; 
$lang['datetime']['April'] = 'aprile'; 
$lang['datetime']['May'] = 'maggio'; 
$lang['datetime']['June'] = 'giugno'; 
$lang['datetime']['July'] = 'luglio'; 
$lang['datetime']['August'] = 'agosto'; 
$lang['datetime']['September'] = 'settembre'; 
$lang['datetime']['October'] = 'ottobre'; 
$lang['datetime']['November'] = 'novembre'; 
$lang['datetime']['December'] = 'dicembre'; 
$lang['datetime']['Jan'] = 'gen'; 
$lang['datetime']['Feb'] = 'feb'; 
$lang['datetime']['Mar'] = 'mar'; 
$lang['datetime']['Apr'] = 'apr'; 
$lang['datetime']['May'] = 'mag'; 
$lang['datetime']['Jun'] = 'giu'; 
$lang['datetime']['Jul'] = 'lug'; 
$lang['datetime']['Aug'] = 'ago'; 
$lang['datetime']['Sep'] = 'set'; 
$lang['datetime']['Oct'] = 'ott'; 
$lang['datetime']['Nov'] = 'nov'; 
$lang['datetime']['Dec'] = 'dic'; 
// 
// Errors (not related to a 
// specific failure on a page) 
// 
$lang['Information'] = 'Informazione'; 
$lang['Critical_Information'] = 'Informazione critica'; 
$lang['General_Error'] = 'Errore generale'; 
$lang['Critical_Error'] = 'Errore critico'; 
$lang['An_error_occured'] = 'Si � verificato un errore'; 
$lang['A_critical_error'] = 'Si � verificato un errore critico'; 
$lang['Admin_reauthenticate'] = 'Per amministrare il forum ti devi riautenticare'; 
// 
$lang['Login_attempts_exceeded'] = 'Il numero massimo di %s tentativi di accesso � stato superato. Non ti � permesso accedere al forum per i prossimi %s minuti.'; 
$lang['Please_remove_install_contrib'] = 'Assic�rati che le due cartelle install/ e contrib/ siano state rimosse';
$lang['Session_invalid'] = 'Sessione non valida. Per favore rinvia il modulo.';
//
// Bookmark Mod
//
$lang['Bookmarks'] = 'Bookmarks';
$lang['Set_Bookmark'] = 'Set a bookmark for this topic';
$lang['Remove_Bookmark'] = 'Remove the bookmark for this topic';
$lang['No_Bookmarks'] = 'You do not have set a bookmark';
$lang['Always_set_bm'] = 'Set bookmark automatically when posting';
$lang['Found_bookmark'] = 'You have set %d bookmark.'; // eg. Search found 1 match
$lang['Found_bookmarks'] = 'You have set %d bookmarks.'; // eg. Search found 24 matches
$lang['More_bookmarks'] = 'More bookmarks...'; // For mozilla navigation bar

// 
// That's all, Folks! 
// -------------------------------------------------
// Statitics Mod Header Link
$lang['statistics'] = 'Statistics'; 
$lang['Stop_watching_forum'] = 'Stop watching this forum';
$lang['Start_watching_forum'] = 'Watch this forum for posts';
$lang['No_longer_watching_forum'] = 'You are no longer watching this forum.';
$lang['You_are_watching_forum'] = 'You are now watching this forum.';

$lang['User_not_approved_warn'] = <<<TAG
<h6 class="gen warn" style="margin: 10px 0;">Mesures contre les messages publicitaires</h6>
<p class="gen">Si vous ins�rez un lien vers un site externe, ce lien sera modifi� pour �tre rendu inactif.<br>
Par exemple�: un lien vers ��<a href="http://example.com">http://example.com</a>�� deviendra ��http:// exemple com��.<br>
Si votre lien est l�gitime (pour citer votre source, par exemple), un mod�rateur pourra l'activer.<br>
Cette mesure a pour but de limiter les messages ind�sirables qui n'ont aucun rapport avec la langue fran�aise. Merci de votre compr�hension.</p>
TAG;

$lang['Sponsor'] = 'Sponsor';
?>