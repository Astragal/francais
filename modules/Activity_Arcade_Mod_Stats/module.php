<?php
/***************************************************************************
*                            arcade_stats.pak
*                            ------------------
*     begin                :
*     copyright            : (C)      Wicher
*     email                : ---
*
*     $Id: arcade_stats.pak, 2006/11/28 12:46:00 Wicher Exp $
*          for Statistics MOD version 3.0.1beta3s / 4.0.x
****************************************************************************/
/***************************************************************************
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
***************************************************************************/

if ( !defined('IN_PHPBB') )
{
	die("Hacking attempt");
}

//
// Age Statistics
//

$core->start_module(true);

$core->set_content('values');

//
// Arcade Activity Stats
//

// check if mod installed
$sql = "SELECT at_first_places FROM " . iNA_USER_DATA; 
if (!$result = $db->sql_query($sql))
{
	$there=0;
}
else
{
	$there=1;
}
if ($there == 0)
{
	message_die(GENERAL_MESSAGE, 'This module requires Arcade_MOD_v2.1.2 (<a href="http://www.phpbb-arcade.com/download.php?id=421" target="_blank">http://www.phpbb-arcade.com/download.php?id=421</a>) to be installed.');
}

/* Player with the most first places */
   $sql = "SELECT u.user_id, u.username, a.first_places FROM " . USERS_TABLE . " as u, " . iNA_USER_DATA . " as a
      WHERE u.user_id = a.user_id
      AND first_places > 0
      ORDER by first_places DESC LIMIT 0, 1";
   if(!$user = $db->sql_query($sql))
   {
      message_die(GENERAL_ERROR, $lang['no_user_data'], "", __LINE__, __FILE__, $sql);
   }
   $users_count = $db->sql_numrows($user);
   $users_rows = $db->sql_fetchrowset($user);

   for($i = 0; $i < $users_count; $i++)
   {
      $best_player .= '<a href="profile.php?mode=viewprofile&u=' . $users_rows[$i]['user_id'] . '">' . $users_rows[$i]['username'] . '</a><br />';
      $first_places = '<b>' . $users_rows[$i][first_places] . '</b>';
   }

/* Most Played Game */
	$sql = "SELECT * FROM ". iNA_GAMES ."
	       WHERE game_id > 0
	   	   ORDER BY played DESC LIMIT 0,1";
	$result      	= $db -> sql_query($sql);
	$row 	        = $db -> sql_fetchrow($result);
	$game_name		= $row['game_desc'];
	$played_amount 	= $row['played'];
    $game_link      = $row['game_desc'];

/* Total Games Played */
	$sql =  "SELECT SUM(played) AS total FROM  ". $table_prefix ."ina_games";
	$result      	= $db -> sql_query($sql);
	$row 	        = $db -> sql_fetchrow($result);
	$total_played	= $row['total'];

/* Total Comments */
	$sql =  "SELECT COUNT(*) AS total FROM ". $table_prefix ."ina_comment";
	$result     	= $db -> sql_query($sql);
	$row 	        = $db -> sql_fetchrow($result);
	$total_comments = $row['total'];

/* Total Ratings */
	$sql =  "SELECT COUNT(*) AS total FROM ". $table_prefix ."ina_rate";
	$result      	= $db -> sql_query($sql);
	$row 	        = $db -> sql_fetchrow($result);
	$total_ratings  = $row['total'];

/* Total Monthly Highscores */
	$sql = "SELECT COUNT(*) AS total FROM " . iNA_HIGHSCORES;
	if ( !($result = $db->sql_query($sql)) )
	{
		message_die(GENERAL_ERROR, $lang['no_game_total'], '', __LINE__, __FILE__, $sql);
	}
	$total = $db->sql_fetchrow($result);
	$total_highscores = $total['total'];

/* Total Games */
	$sql =  "SELECT COUNT(game_id) AS total FROM ". $table_prefix ."ina_games";
	$result     	= $db -> sql_query($sql);
	$row        	= $db -> sql_fetchrow($result);
	$total_games    = $row['total'];

if (!$best_player){$best_player = $lang['nobody'];}
if (!$first_places){$first_places = '0';}
if (!$game_name){$game_name = $lang['no_game'];}

$statistic_array = array($lang['best_player'].$board_config['sitename'],$lang['fav_game'].$board_config['sitename'],$lang['total_games'],$lang['total_games_played'],$lang['total_comments'],$lang['total_ratings'],$lang['total_highscores']);
$value_array = array($best_player.'&nbsp;&nbsp;&nbsp;'.str_replace("%t%", $first_places, $lang['first_places']),$game_link.'&nbsp;&nbsp;&nbsp;'.str_replace("%t%", number_format($played_amount), $lang['games']),number_format($total_games),number_format($total_played),number_format($total_comments),number_format($total_ratings),number_format($total_highscores));

$core->set_view('columns', 2);
$core->set_view('num_blocks', 1);
$core->set_view('value_order', 'left_right');
//$core->set_view('value_order', 'up_down');

$core->define_view('set_columns', array(
	'stats' => $lang['Statistic'],
	'value' => $lang['Value'])
);

$core->set_header($lang['module_name']);

$data = $core->assign_defined_view('value_array', array($statistic_array, $value_array));

$core->set_data($data);

$core->define_view('iterate_values', array());

$core->run_module();



?>