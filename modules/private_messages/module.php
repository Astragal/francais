<?php

/***************************************************************************
*                            private_messages.pak
*                            --------------------
*     begin                :
*     copyright            : (C)      Wicher
*     email                : ---
*
*     $Id: private_messages.pak, 2006/10/12 20:00:00 Wicher Exp $
*          for Statistics MOD version 3.0.1beta3s and higher
****************************************************************************/
/***************************************************************************
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
****************************************************************************/

if ( !defined('IN_PHPBB') ) 
{ 
   die("Hacking attempt"); 
} 


$core->start_module(true);

$core->set_content('statistical');

$core->set_view('columns', 11);

$core->set_header($lang['module_name']); 
$core->assign_defined_view('align_rows', array(
	'left',
	'left')
);

$core->assign_defined_view('width_rows', array(
	'',
	'')
);

   for ($mycount=0; $mycount<=5; $mycount++) 
   { 
      $where_my = "WHERE privmsgs_type = " . $mycount; 
      $sql = "SELECT count(privmsgs_id) AS total 
      FROM " . PRIVMSGS_TABLE . " 
      $where_my"; 
    
      if(!$count_result = $db->sql_query($sql)) 
      { 
         message_die(GENERAL_ERROR, "Error getting total private messages.", "", __LINE__, __FILE__, $sql); 
      } 
      else 
      { 
         $total = $db->sql_fetchrow($count_result); 
         $mytotal[$mycount] = $total['total']; 
      } 
    } 


//   Find largest private message id to show how many have been sent 
   $where_my2 = "WHERE privmsgs_id >= 0"; 
   $sql = "SELECT max(privmsgs_id) AS total 
   FROM " . PRIVMSGS_TABLE . " 
   $where_my2"; 
    
   if(!$count_result = $db->sql_query($sql)) 
      { 
         message_die(GENERAL_ERROR, "Error getting total private messages written.", "", __LINE__, __FILE__, $sql); 
      } 
      else 
      { 
         $total = $db->sql_fetchrow($count_result); 
         $total_messages = $total['total']; 
      } 

//Find this month's number of private messages 
$month = array(); 
$current_time = time(); 
$year = create_date('Y', $current_time, $board_config['board_timezone']); 
$month [0] = mktime (0,0,0,1,1, $year); 
$month [1] = $month [0] + 2678400; 
$month [2] = mktime (0,0,0,3,1, $year); 
$month [3] = $month [2] + 2678400; 
$month [4] = $month [3] + 2592000; 
$month [5] = $month [4] + 2678400; 
$month [6] = $month [5] + 2592000; 
$month [7] = $month [6] + 2678400; 
$month [8] = $month [7] + 2678400; 
$month [9] = $month [8] + 2592000; 
$month [10] = $month [9] + 2678400; 
$month [11] = $month [10] + 2592000; 
$month [12] = $month [11] + 2592000; 
$arr_num = (date('n')-1);
if ($arr_num==0){
	$arr_num_1=11;
	$diff=1;
}
else {
	$arr_num_1 = (date('n')-2);
	$diff=0;
}
$time_thismonth = $month[$arr_num]; 
$time_lastmonth = $month[$arr_num_1]; 

   $monthno = create_date('n', $current_time, $board_config['board_timezone']); 
   $lastmonthno = $monthno - 1; 
   $nextmonthno = $monthno + 1; 
    
   $thismonth=mktime (0,0,0, $monthno,1, $year); 
    
   $lyear=$year; 
   If ($lastmonthno == 0 ) 
   { 
      $lastmonthno = 12; 
      $lyear = $year - 1; 
   } 
   $lastmonth=mktime (0,0,0, $lastmonthno,1, $lyear); 
    
   $nyear=$year; 
   If ($nextmonthno == 13)    
   { 
      $nextmonthno = 1; 
      $nyear = $year + 1; 
   }    
   $nextmonth=mktime (0,0,0, $nextmonthno,1, $nyear); 
    
   $l_this_month = create_date('F', $time_thismonth, $board_config['board_timezone']); 
   // $thismonthname = create_date('F', $current_time, $board_config['board_timezone']); 
   // create_date gives wrong last month so use PHP's own function 
   // $lastmonthname = date('F', $lastmonth); 
   $l_last_month = create_date('F', $time_lastmonth, $board_config['board_timezone']); 
    
   $where_my2 = "WHERE privmsgs_date >= " . $lastmonth . "   AND privmsgs_date < " . $thismonth; 

   $sql = "SELECT max(privmsgs_id) AS maxp, min(privmsgs_id) AS minp 
   FROM " . PRIVMSGS_TABLE . " 
   $where_my2"; 

   if(!$count_result = $db->sql_query($sql)) 
         { 
            message_die(GENERAL_ERROR, "Error getting last month's private messages written.", "", __LINE__, __FILE__, $sql); 
         } 
         else 
         { 
            $total = $db->sql_fetchrow($count_result); 
             
            If ($total['maxp'] == 0) 
               { 
               $lastmonth_messages = 0; 
               } 
               else 
               { 
               $lastmonth_messages = $total['maxp'] - $total['minp'] + 1; 
               } 
         } 

   $where_my2 = "WHERE privmsgs_date >= " . $thismonth . "   AND privmsgs_date < " . $nextmonth; 

   $sql = "SELECT max(privmsgs_id) AS maxp, min(privmsgs_id) AS minp 
   FROM " . PRIVMSGS_TABLE . " 
   $where_my2"; 

   if(!$count_result = $db->sql_query($sql)) 
         { 
            message_die(GENERAL_ERROR, "Error getting this month's private messages written.", "", __LINE__, __FILE__, $sql); 
         } 
         else 
         { 
            $total = $db->sql_fetchrow($count_result); 
            If ($total['maxp'] == 0) 
               { 
               $thismonth_messages = 0; 
               } 
               else 
               { 
               $thismonth_messages = $total['maxp'] - $total['minp'] + 1; 
               } 
         } 
    
    
   $current_time = time(); 
   $minutes = date('is', $current_time); 
   $hour_now = $current_time - (60*($minutes[0].$minutes[1])) - ($minutes[2].$minutes[3]); 
   $date = date('H'); 
   $time_today = $hour_now - (3600 * $date); 
   $time_yesterday = $current_time - 86400; 
   $time_lastweek = $current_time - 7*86400; 
   $time_lastmonth = $current_time - ($thismonth - $lastmonth); 
       
   //find number of PMs in last 24 hours 
   $where_my2 = "WHERE privmsgs_date >= " . $time_yesterday; 
    
   $sql = "SELECT max(privmsgs_id) AS maxp, min(privmsgs_id) AS minp 
   FROM " . PRIVMSGS_TABLE . " 
   $where_my2"; 
    
   if(!$count_result = $db->sql_query($sql)) 
         { 
            message_die(GENERAL_ERROR, "Error getting today's private messages written.", "", __LINE__, __FILE__, $sql); 
         } 
         else 
         { 
            $total = $db->sql_fetchrow($count_result); 
            If ($total['maxp'] == 0) 
               { 
               $todays_messages = 0; 
               } 
               else 
               { 
               $todays_messages = $total['maxp'] - $total['minp'] + 1; 
               } 
         } 
    
   //find number of PMs in last 7 days 
   $where_my2 = "WHERE privmsgs_date >= " . $time_lastweek; 
    
   $sql = "SELECT max(privmsgs_id) AS maxp, min(privmsgs_id) AS minp 
   FROM " . PRIVMSGS_TABLE . " 
   $where_my2"; 
    
   if(!$count_result = $db->sql_query($sql)) 
         { 
            message_die(GENERAL_ERROR, "Error getting last 7 day's private messages written.", "", __LINE__, __FILE__, $sql); 
         } 
         else 
         { 
            $total = $db->sql_fetchrow($count_result); 
            If ($total['maxp'] == 0) 
               { 
               $lastweeks_messages = 0; 
               } 
               else 
               { 
               $lastweeks_messages = $total['maxp'] - $total['minp'] + 1; 
               } 
         } 

   //find number of PMs in last month 
   $where_my2 = "WHERE privmsgs_date >= " . $time_lastmonth; 
    
   $sql = "SELECT max(privmsgs_id) AS maxp, min(privmsgs_id) AS minp 
   FROM " . PRIVMSGS_TABLE . " 
   $where_my2"; 
    
   if(!$count_result = $db->sql_query($sql)) 
         { 
            message_die(GENERAL_ERROR, "Error getting last month's private messages written.", "", __LINE__, __FILE__, $sql); 
         } 
         else 
         { 
            $total = $db->sql_fetchrow($count_result); 
            If ($total['maxp'] == 0) 
               { 
               $lastmonths_messages = 0; 
               } 
               else 
               { 
               $lastmonths_messages = $total['maxp'] - $total['minp'] + 1; 
               } 
         } 

$core->define_view('set_columns', array( 
   'prevmonth' => $l_last_month . ' ' . (create_date('Y', $current_time, $board_config['board_timezone']) - $diff), 
   'thismonth' => $l_this_month . ' ' . create_date('Y', $current_time, $board_config['board_timezone']), 
   'lastmonth' => $lang['lastmonth'], 
   '24hours' => $lang['24hours'],
   '7days' => $lang['7days'],
   'send' => $lang['send'],
   'read' => $lang['read'],
   'new' => $lang['new'],
   'saved_in' => $lang['saved_in'],
   'saved_out' => $lang['saved_out'],
   'unread' => $lang['unread']) 
); 

$core->set_data('',1);

$core->define_view('set_rows', array( 
$lastmonth_messages,
$thismonth_messages,
$lastmonths_messages,
$todays_messages,
$lastweeks_messages,
$total_messages,
$mytotal[2],
$mytotal[0],
$mytotal[1],
$mytotal[3],
$mytotal[4],
$mytotal[5])
); 

$core->run_module(); 

?>