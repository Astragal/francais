<?php

/***************************************************************************
*                            advanced_karma.pak
*                            --------------------
*     begin                :
*     copyright            : (C)      Wicher
*     email                : ---
*
*     $Id: advanced_karma.pak, 2007/01/22 18:05:00 Wicher Exp $
*          for Statistics MOD version 3.0.1beta3s and higher
****************************************************************************/
/***************************************************************************
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
****************************************************************************/

if ( !defined('IN_PHPBB') ) 
{ 
   die("Hacking attempt"); 
} 


$core->start_module(true); 

$core->set_content('statistical'); 

$core->set_view('rows', $core->return_limit); 
$core->set_view('columns', 7); 

$core->define_view('set_columns', array( 
   $core->pre_defined('rank'), 
   'date' => $lang['High'], 
   'user' => $lang['name'],
   'empty' => '&nbsp;',
   $core->pre_defined('rank'), 
   'date2' => $lang['Low'], 
   'user2' => $lang['name']) 
); 

$core->set_header($lang['module_name']); 

$core->assign_defined_view('align_rows', array( 
   'left', 
   'center', 
   'left',
   'center', 
   'left', 
   'center', 
   'left') 
); 

$core->assign_defined_view('width_rows', array( 
   '10%', 
   '10%', 
   '10%',
   '40%', 
   '10%', 
   '10%', 
   '10%') 
); 


// check if mod installed
$sql = "SELECT karma_plus FROM " . USERS_TABLE . " ORDER BY karma_plus DESC	LIMIT 1"; 
if (!$result = $db->sql_query($sql))
{
	$there=0;
}
else
{
	$there=1;
}
if ($there == 0)
{
	message_die(GENERAL_MESSAGE, $lang['Error_message']);
}

// ------------------------- BEGIN HIGH KARMA-------------------------
$sql = 'SELECT user_id, username, karma_plus
FROM ' . USERS_TABLE .	' 
WHERE (user_id <> ' . ANONYMOUS . ' )
ORDER BY karma_plus DESC 
LIMIT ' . $core->return_limit;

$result = $core->sql_query($sql, 'Shit, that didn\'t work');

$user_count_high = $core->sql_numrows($result);
$user_data_high = $core->sql_fetchrowset($result);

// ------------------------- END HIGH KARMA-------------------------

// ------------------------- BEGIN LOW KARMA-------------------------
$sql = 'SELECT user_id, username, karma_minus
FROM ' . USERS_TABLE .	' 
WHERE (user_id <> ' . ANONYMOUS . ' )
ORDER BY karma_minus DESC 
LIMIT ' . $core->return_limit;

$result = $core->sql_query($sql, 'Shit, that didn\'t work');

$user_count_low = $core->sql_numrows($result);
$user_data_low = $core->sql_fetchrowset($result);

// ------------------------- END LOW KARMA-------------------------

if ($user_count_high >= $user_count_low)
{
	$usercount = $user_count_high;
}
else
{
	$usercount = $user_count_low;
}

$karmadata = array();
for ($i = 0; $i < $usercount; $i++)
{
	$karmadata[] = array('username_high' => $user_data_high[$i]['username'],
						 'user_id_high' => $user_data_high[$i]['user_id'],
						 'user_karma_plus' => '+'.$user_data_high[$i]['karma_plus'],
						 'divider' => '<img src="images/divider_left.gif"><img src="images/divider_mid.gif" width="90%" height="20"><img src="images/divider_right.gif">',
						 'username_low' => $user_data_low[$i]['username'],
						 'user_id_low' => $user_data_low[$i]['user_id'],
						 'user_karma_minus' => '-'.$user_data_low[$i]['karma_minus']
						 );
}

$core->set_data($karmadata); 


$core->define_view('set_rows', array( 
   '$core->pre_defined()', 
   '$core->data(\'user_karma_plus\')',
   '$core->generate_link(append_sid($phpbb_root_path . \'profile.php?mode=viewprofile&u=\' . $core->data(\'user_id_high\')), $core->data(\'username_high\'), \'target="_blank"\')',
   '$core->data(\'divider\')', 
   '$core->pre_defined()', 
   '$core->data(\'user_karma_minus\')',
   '$core->generate_link(append_sid($phpbb_root_path . \'profile.php?mode=viewprofile&u=\' . $core->data(\'user_id_low\')), $core->data(\'username_low\'), \'target="_blank"\')') 
); 



$core->run_module(); 

?>