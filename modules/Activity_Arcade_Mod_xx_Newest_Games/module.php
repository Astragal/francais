<?php

/***************************************************************************
*                            Arcade_All_Time_High_Score_Players.pak
*                            --------------------
*     begin                :
*     copyright            : (C)      Wicher
*     email                : ---
*
*     $Id: Arcade_All_Time_High_Score_Players.pak, 2006/11/28 23:25:00 Wicher Exp $
*          for Statistics MOD version 3.0.1beta3s and higher
****************************************************************************/
/***************************************************************************
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
****************************************************************************/

if ( !defined('IN_PHPBB') ) 
{ 
   die("Hacking attempt"); 
} 


// 
// Activity / Arcade Mod xx Newest Games 
// 
$core->start_module(true); 

$core->set_content('statistical'); 

$core->set_view('rows', $core->return_limit); 
$core->set_view('columns', 4); 

$core->define_view('set_columns', array( 
   $core->pre_defined('rank'), 
   'l_game' => $lang['game'], 
   'l_plays' => $lang['plays'],
   'l_date' => $lang['date']) 
); 

$core->set_header($lang['module_name']); 

$core->assign_defined_view('align_rows', array( 
   'left', 
   'center', 
   'left',
   'left') 
); 

$core->assign_defined_view('width_rows', array( 
   '10%', 
   '30%', 
   '30%',
   '30%') 
); 


// check if mod installed
$sql = "SELECT at_first_places FROM " . iNA_USER_DATA; 
if (!$result = $db->sql_query($sql))
{
	$there=0;
}
else
{
	$there=1;
}
if ($there == 0)
{
	message_die(GENERAL_MESSAGE, 'This module requires Arcade_MOD_v2.1.2 (<a href="http://www.phpbb-arcade.com/download.php?id=421" target="_blank">http://www.phpbb-arcade.com/download.php?id=421</a>) to be installed.');
}


	$sql = 'SELECT i.game_id, i.game_desc, i.played, i.date_added, u.user_dateformat, u.user_timezone
			FROM '. iNA_GAMES .' i, '. USERS_TABLE .' u
			WHERE u.user_id = '. $userdata['user_id'] .'
			ORDER BY date_added DESC
			LIMIT ' . $core->return_limit;


$result = $core->sql_query($sql, 'Couldn\'t retrieve games data'); 
$data = $core->sql_fetchrowset($result); 
$core->set_data($data); 


$core->define_view('set_rows', array( 
   '$core->pre_defined()', 
   '$core->generate_link(append_sid($phpbb_root_path . \'activity.php?mode=game&id=\' . $core->data(\'game_id\')), $core->data(\'game_desc\'), \'target="_blank"\')', 
   '$core->data(\'played\')', 
   'create_date($core->data(\'user_dateformat\'), $core->data(\'date_added\'), $core->data(\'user_timezone\'))') 
); 

$core->run_module(); 

?>