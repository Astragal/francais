<?php
/***************************************************************************
*                            Activity_plus_stats.pak
*                            ------------------
*     begin                :
*     copyright            : (C)      Wicher
*     email                : ---
*
*     $Id: Activity_plus_stats.pak, 2007/07/04 12:46:00 Wicher Exp $
*          for Statistics MOD version 3.0.1beta3s / 4.0.x
****************************************************************************/
/***************************************************************************
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
***************************************************************************/

if ( !defined('IN_PHPBB') )
{
	die("Hacking attempt");
}

$core->start_module(true);

$core->set_content('values');

//
// Activity Mod Plus
//

// check if mod installed
$sql = "SELECT user_trophies FROM " . USERS_TABLE; 
if (!$result = $db->sql_query($sql))
{
	$there=0;
}
else
{
	$there=1;
}
if ($there == 0)
{
	message_die(GENERAL_MESSAGE, 'This module requires Activity Mod Plus_v1.1.0 (<a href="http://phpbb-amod.com/down_db.php?page=cat&id=14" target="_blank">http://phpbb-amod.com/down_db.php?page=cat&id=14</a>) to be installed.');
}


/* Person with the most trophies */
	$q =  "SELECT *   
		   FROM ". USERS_TABLE ."   
	   	   ORDER BY user_trophies DESC  
	   	   LIMIT 1";    
	$r     	= $db -> sql_query($q);    
	$row 	= $db -> sql_fetchrow($r);
		if($row['user_id'] != ANONYMOUS) $link = "<a href='profile.". $phpEx ."?mode=viewprofile&u=". $row['user_id'] ."&sid=". $userdata['session_id'] ."' class='nav'>". $row['username'] ."</a>";
		if($row['user_id'] == ANONYMOUS) $link = $lang['nobody'];
		
	$best_user 		= $link;
	$trophy_count 	= "<a href='activity_top_scores_search.". $phpEx ."?user=". $row['username'] ."&sid=". $userdata['session_id'] ."' class='nav'>". number_format($row['user_trophies']) ."</a>";
						
/* Most played game */
	$q =  "SELECT *   
		   FROM ". iNA_GAMES ."   
	   	   ORDER BY played DESC  
	   	   LIMIT 1";    
	$r     	= $db -> sql_query($q);    
	$row 	= $db -> sql_fetchrow($r);

	$game_name		= $row['proper_name'];
	$played_amount 	= $row['played'];
if (!$game_name){$game_name = $lang['no_game'];}
				
/* Total comments */
	$q =  "SELECT COUNT(*) AS total
		   FROM ". $table_prefix ."ina_trophy_comments";    
	$r     	= $db -> sql_query($q);    
	$row 	= $db -> sql_fetchrow($r);
	
	$total_comments = $row['total'];
	
/* Total gambles */
	$q =  "SELECT COUNT(*) AS total
		   FROM ". $table_prefix ."ina_gamble";    
	$r     	= $db -> sql_query($q);    
	$row 	= $db -> sql_fetchrow($r);
	
	$total_bets = $row['total'];
	
/* Total challenges */
	$q =  "SELECT SUM(count) AS total
		   FROM ". $table_prefix ."ina_challenge_tracker";    
	$r     	= $db -> sql_query($q);    
	$row 	= $db -> sql_fetchrow($r);
	
	$total_challenges = $row['total'];
	
/* Total games */
	$q =  "SELECT COUNT(game_id) AS total
		   FROM ". $table_prefix ."ina_games";    
	$r     	= $db -> sql_query($q);    
	$row 	= $db -> sql_fetchrow($r);
	
	$total_games = $row['total'];			


$statistic_array = array(
	$lang['best_player'].$board_config['sitename'],
	$lang['fav_game'].$board_config['sitename'],
	$lang['total_games'],
	$lang['total_comments'],
	$lang['total_bets'],
	$lang['total_challenges']);
$value_array = array(
	$best_user.'&nbsp;&nbsp;&nbsp;'.str_replace("%t%", $trophy_count, $lang['trophies']),
	$game_name.'&nbsp;&nbsp;&nbsp;'.str_replace("%t%", number_format($played_amount), $lang['played']),
	number_format($total_games),
	number_format($total_comments),
	number_format($total_bets),
	number_format($total_challenges));

$core->set_view('columns', 2);
$core->set_view('num_blocks', 1);
$core->set_view('value_order', 'left_right');
//$core->set_view('value_order', 'up_down');

$core->define_view('set_columns', array(
	'stats' => $lang['Statistic'],
	'value' => $lang['Value'])
);

$core->set_header($lang['module_name']);

$data = $core->assign_defined_view('value_array', array($statistic_array, $value_array));

$core->set_data($data);

$core->define_view('iterate_values', array());

$core->run_module();



?>