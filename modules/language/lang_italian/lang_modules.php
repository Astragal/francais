<?php

// [stats_overview]
$stats_overview = array();

$stats_overview['module_name'] = 'Statistiche';

// Admin Specific
$stats_overview['num_columns_title'] = 'Imposta il numero di colonne';
$stats_overview['num_columns_explain'] = 'Qui puoi impostare il numero di colonne visualizzate';

// [/stats_overview]

// [admin_statistics]
$admin_statistics = array();

// Administrative Statistics
$admin_statistics['module_name'] = 'Statistiche amministrative';

$admin_statistics['Board_Up_Days'] = 'Giorni di attivit&agrave; del forum';
$admin_statistics['Latest_Reg_User'] = 'Ultimo utente registrato';
$admin_statistics['Latest_Reg_User_Date'] = 'Data registrazione ultimo utente';
$admin_statistics['Most_Ever_Online'] = 'Record utenti online';
$admin_statistics['Most_Ever_Online_Date'] = 'Data del record utenti online';
$admin_statistics['Disk_usage'] = 'Utilizzo disco';

// [/admin_statistics]

// [most_active_forums]
$most_active_forums = array();

$most_active_forums['module_name'] = 'La Maggior parte Delle Tribune Attive';
// [/most_active_forums]

// [most_active_topics]
$most_active_topics = array();

$most_active_topics['module_name'] = 'La Maggior parte Dei Soggetti Attivi';
// [/most_active_topics]

// [most_viewed_topics]
$most_viewed_topics = array();

$most_viewed_topics['module_name'] = 'La Maggior parte Dei Soggetti Osservati';
// [/most_viewed_topics]

// [most_active_topicstarter]
$most_active_topicstarter = array();

$most_active_topicstarter['module_name'] = 'La Maggior parte Del Topicstarter Attivo';
// [/most_active_topicstarter]

// [most_interesting_topics]
$most_interesting_topics = array();

$most_interesting_topics['module_name'] = 'La Maggior parte Dei Soggetti Interessanti';
$most_interesting_topics['Rate'] = 'Tasso (viste/messaggi)';
// [/most_interesting_topics]

// [least_interesting_topics]
$least_interesting_topics = array();

$least_interesting_topics['module_name'] = 'Meno Soggetti Interessanti';
$least_interesting_topics['Rate'] = 'Tasso (viste/messaggi)';
// [/least_interesting_topics]

// [most_least_interesting_topics]
$most_least_interesting_topics = array();

$most_least_interesting_topics['module_name'] = 'La Maggior parte Dei Soggetti Interessanti&nbsp;-&nbsp;Meno Soggetti Interessanti';
$most_least_interesting_topics['Rate'] = 'Tasso (viste/messaggi)';
// [/most_least_interesting_topics]

// [top_posters]
$top_posters = array();

$top_posters['module_name'] = 'Manifesti Superiori';
// [/top_posters]

// [fastest_users]
$fastest_users = array();

$fastest_users['module_name'] = 'Utenti Pi� veloci';
$fastest_users['days_on_forum'] = 'Giorni Su Tribuna';
$fastest_users['messperday'] = 'Messaggi Al Giorno';
// [/fastest_users]

// [last_online]
$last_online = array();

$last_online['module_name'] = 'Ultimo In linea';
$last_online['name'] = 'Username';
$last_online['last_online'] = 'Ultimo In linea';
$last_online['Error_message'] = 'Last Visit Mod (<a href="http://www.phpbbhacks.com/download/237">http://www.phpbbhacks.com/download/237</a>) non � stato trovato, prego lo installano prima di per mezzo di questo modulo';
// [/last_online]

// [top_posters_month]
$top_posters_month = array();

$top_posters_month['module_name'] = 'Utenti d\'invio superiori questo mese';
// [/top_posters_month]

// [users_by_month]
$users_by_month = array();

$users_by_month['module_name'] = 'Numero di nuovi utenti entro il mese';
$users_by_month['Year'] = 'Year';
$users_by_month['Month_jan'] = 'Jan';
$users_by_month['Month_feb'] = 'Feb';
$users_by_month['Month_mar'] = 'Mar';
$users_by_month['Month_apr'] = 'Apr';
$users_by_month['Month_may'] = 'May';
$users_by_month['Month_jun'] = 'Jun';
$users_by_month['Month_jul'] = 'Jul';
$users_by_month['Month_aug'] = 'Aug';
$users_by_month['Month_sep'] = 'Sep';
$users_by_month['Month_oct'] = 'Oct';
$users_by_month['Month_nov'] = 'Nov';
$users_by_month['Month_dec'] = 'Dec';
// [/users_by_month]

// [top_posters_week]
$top_posters_week = array();

$top_posters_week['module_name'] = 'Utenti d\'invio superiori questa settimana';
// [/top_posters_week]

// [most_active_polls]
$most_active_polls = array();

$most_active_polls['module_name'] = 'La Maggior parte Del Scrutinio Attivo';
// [/most_active_polls]

// [most_active_pollstarter]
$most_active_pollstarter = array();

$most_active_pollstarter['module_name'] = 'La Maggior parte Del Dispositivo d\'avviamento Attivo Di Scrutinio';
$most_active_pollstarter['polls'] = 'Scrutinio';
// [/most_active_pollstarter]

// [last_created_posts]
$last_created_posts = array();

$last_created_posts['module_name'] = 'Ultimi Alberini';
$last_created_posts['start_date'] = 'Inviato';
$last_created_posts['posted_in'] = 'Nell\'Oggetto';
$last_created_posts['post'] = 'Identificazione del messaggio/oggetto';
$last_created_posts['hekje'] = '#';
$last_created_posts['by'] = 'Da';
// [/last_created_posts]

// [last_created_topics]
$last_created_topics = array();

$last_created_topics['module_name'] = 'Ultimi Soggetti';
$last_created_topics['start_date'] = 'Iniziato';
// [/last_created_topics]

// [posts_by_month]
$posts_by_month = array();

$posts_by_month['module_name'] = 'Numero di nuovi alberini entro il mese';
$posts_by_month['Year'] = 'Year';
$posts_by_month['Month_jan'] = 'Jan';
$posts_by_month['Month_feb'] = 'Feb';
$posts_by_month['Month_mar'] = 'Mar';
$posts_by_month['Month_apr'] = 'Apr';
$posts_by_month['Month_may'] = 'May';
$posts_by_month['Month_jun'] = 'Jun';
$posts_by_month['Month_jul'] = 'Jul';
$posts_by_month['Month_aug'] = 'Aug';
$posts_by_month['Month_sep'] = 'Sep';
$posts_by_month['Month_oct'] = 'Oct';
$posts_by_month['Month_nov'] = 'Nov';
$posts_by_month['Month_dec'] = 'Dec';
// [/posts_by_month]

// [topics_by_month]
$topics_by_month = array();

$topics_by_month['module_name'] = 'Numero di nuovi soggetti entro il mese';
$topics_by_month['Year'] = 'Year';
$topics_by_month['Month_jan'] = 'Jan';
$topics_by_month['Month_feb'] = 'Feb';
$topics_by_month['Month_mar'] = 'Mar';
$topics_by_month['Month_apr'] = 'Apr';
$topics_by_month['Month_may'] = 'May';
$topics_by_month['Month_jun'] = 'Jun';
$topics_by_month['Month_jul'] = 'Jul';
$topics_by_month['Month_aug'] = 'Aug';
$topics_by_month['Month_sep'] = 'Sep';
$topics_by_month['Month_oct'] = 'Oct';
$topics_by_month['Month_nov'] = 'Nov';
$topics_by_month['Month_dec'] = 'Dec';
// [/topics_by_month]

// [top_attachments]
$top_attachments = array();

$top_attachments['module_name'] = 'Collegamenti Trasferiti Parte superiore';
$top_attachments['Filename'] = 'Nome di schedario';
$top_attachments['Filecomment'] = 'Commento Della Lima';
$top_attachments['Size'] = 'Filesize';
$top_attachments['Downloads'] = 'Trasferimenti dal sistema centrale verso i satelliti';
$top_attachments['Posttime'] = 'Tempo Dell\'Alberino';
$top_attachments['Posted_in_topic'] = 'Inviato nel soggetto';
// Admin Variables
$top_attachments['exclude_images_title'] = 'Escluda Le Immagini';
$top_attachments['exclude_images_explain'] = 'Se questa regolazione � permessa, le immagini non sono considerate all\'interno delle statistiche superiori dei collegamenti.';
// [/top_attachments]

// [top_avatars]
$top_avatars = array();

$top_avatars['module_name'] = 'Avatars Gallary Superiori';
$top_avatars['Avatar_image'] = 'Immagine Del Avatar';
$top_avatars['Users'] = 'Utenti';
// [/top_avatars]

// [top_smilies]
$top_smilies = array();

$top_smilies['module_name'] = 'Smilies Superiore';
$top_smilies['Smilie_image'] = 'Immagine Di Smiley';
$top_smilies['Smilie_code'] = 'Codice Di Smiley';
// [/top_smilies]

// [top_shoutbox_poster]
$top_shoutbox_poster = array();

$top_shoutbox_poster['module_name'] = 'Manifesti Superiori Di Shoutbox';
$top_shoutbox_poster['Shouts'] = 'Grida';
$top_shoutbox_poster['Error_message'] = 'Il MOD completamente integrato di Shoutbox deve essere installato per vedere i manifesti superiori di Shoutbox.<br>Installi il MOD (<a href="http://www.phpbbhacks.com/download/1255" target="_blank">http://www.phpbbhacks.com/download/1255</a>) o disattivi questo modulo.';
// [/top_shoutbox_poster]

// [top_words]
$top_words = array();

$top_words['module_name'] = 'La Maggior parte Delle Parole Usate';
$top_words['Word'] = 'Parola';
$top_words['Count'] = 'Conteggio';
// [/top_words]

// [most_ignored_users]
$most_ignored_users = array();

$most_ignored_users['module_name'] = 'La maggior parte Hanno ignorato Gli Utenti';
$most_ignored_users['ignored_by'] = ' utenti ignorati vicino ';
$most_ignored_users['other'] = ' altri utenti';
$most_ignored_users['Ignores'] = 'Ignorato da X Users';
$most_ignored_users['Error_message'] = 'Il MOD degli utenti di ignor deve essere installato per vedere gli utenti ignorati.<br>Installi il MOD (<a href="http://www.phpbbhacks.com/download/789" target="_blank">http://www.phpbbhacks.com/download/789</a>) o disattivi questo modulo.';
// [/most_ignored_users]

// [private_messages]
$private_messages = array();

$private_messages['module_name'] = 'Messaggi Riservati';
$private_messages['lastmonth'] = '30 giorni';
$private_messages['24hours'] = '24 ore';
$private_messages['7days'] = 'Settimana';
$private_messages['send'] = 'Trasmetta';
$private_messages['read'] = 'Colto';
$private_messages['new'] = 'Nuovo';
$private_messages['saved_in'] = 'Inbox Conservato';
$private_messages['saved_out'] = 'Outbox Conservato';
$private_messages['unread'] = 'Unread';
// [/private_messages]

// [top_styles]
$top_styles = array();

$top_styles['module_name'] = 'Stili Superiori';
// [/top_styles]

// [top_languages]
$top_languages = array();

$top_languages['module_name'] = 'Lingue Superiori';
$top_languages['langname'] = 'Lingua';
// [/top_languages]

// [most_logged_on_users]
$most_logged_on_users = array();

$most_logged_on_users['module_name'] = 'La maggior parte Annotati Sugli Utenti';
$most_logged_on_users['totalforumtime'] = 'Boardtime';
$most_logged_on_users['totallogins'] = 'x ceppi';
$most_logged_on_users['daysmember'] = 'Registrato';
$most_logged_on_users['visitedpages'] = 'Colpi Della Pagina';
$most_logged_on_users['Error_message'] = 'L\'ultimo MOD di chiamata deve essere installato per vedere pi� annotato sugli utenti.<br>Installi il MOD (<a href="http://www.phpbbhacks.com/download/237" target="_blank">http://www.phpbbhacks.com/download/237</a>) o disattivi questo modulo.';
// [/most_logged_on_users]

// [Cash]
$Cash = array();

$Cash['module_name'] = 'Contanti';
$Cash['username'] = 'Utente';
$Cash['Error_message'] = 'Il MOD dei contanti (cash mod) non � stato trovato, prego lo installa prima di per mezzo di questo modulo';
// [/Cash]

// [genders]
$genders = array();

// Administrative Statistics
$genders['module_name'] = 'Utenti maschii e femminili';
$genders['Gender'] = 'Genere';
$genders['undefined'] = 'Non definito';
$genders['male'] = '<img src="' . $images['icon_minigender_male'] . '"> Maschio';
$genders['female'] = '<img src="' . $images['icon_minigender_female'] . '"> Femmina';
$genders['total'] = 'Totale';
$genders['Error_message'] = 'Gender Mod (from Niels <a href="http://phpbbhacks.com/download/182" target="_blank">http://phpbbhacks.com/download/182</a>) non � stato trovato, prego lo installano prima di per mezzo di questo modulo';
// [/genders]

// [posts_by_gender_niels]
$posts_by_gender_niels = array();

// Administrative Statistics
$posts_by_gender_niels['module_name'] = 'Alberini da Gender';
$posts_by_gender_niels['Gender'] = 'Genere';
$posts_by_gender_niels['undefined'] = 'Non definito';
$posts_by_gender_niels['male'] = '<img src="' . $images['icon_minigender_male'] . '"> Maschio';
$posts_by_gender_niels['female'] = '<img src="' . $images['icon_minigender_female'] . '"> Femmina';
$posts_by_gender_niels['Error_message'] = 'Gender Mod (from Niels <a href="http://phpbbhacks.com/download/182" target="_blank">http://phpbbhacks.com/download/182</a>) non � stato trovato, prego lo installano prima di per mezzo di questo modulo';
// [/posts_by_gender_niels]

// [genders_evil]
$genders_evil = array();

// Genders
$genders_evil['module_name'] = 'Utenti maschii e femminili';
$genders_evil['Gender'] = 'Genere';
$genders_evil['undefined'] = 'Non definito';
$genders_evil['male'] = '<img src="' . $images['gender_m'] . '"> Maschio';
$genders_evil['female'] = '<img src="' . $images['gender_f'] . '"> Femmina';
$genders_evil['total'] = 'Totale';
$genders_evil['Error_message'] = 'Gender Mod (from Evil <a href="http://www.phpbb.com/phpBB/viewtopic.php?t=467168" target="_blank">http://www.phpbb.com/phpBB/viewtopic.php?t=467168</a>) non � stato trovato, prego lo installano prima di per mezzo di questo modulo';
// [/genders_evil]

// [posts_by_gender_evil]
$posts_by_gender_evil = array();

// Administrative Statistics
$posts_by_gender_evil['module_name'] = 'Alberini da Gender';
$posts_by_gender_evil['Gender'] = 'Genere';
$posts_by_gender_evil['undefined'] = 'Non definito';
$posts_by_gender_evil['male'] = '<img src="' . $images['gender_m'] . '"> Maschio';
$posts_by_gender_evil['female'] = '<img src="' . $images['gender_f'] . '"> Femmina';
$posts_by_gender_evil['Error_message'] = 'Gender Mod (from Evil <a href="http://www.phpbb.com/phpBB/viewtopic.php?t=467168" target="_blank">http://www.phpbb.com/phpBB/viewtopic.php?t=467168</a>) non � stato trovato, prego lo installano prima di per mezzo di questo modulo';
// [/posts_by_gender_evil]

// [age_statistics]
$age_statistics = array();

$age_statistics['module_name'] = 'Age Statistics';
$age_statistics['Users_Age'] = 'Numero di membri con l\'et�';
$age_statistics['Average_Age'] = 'Et� Media';
$age_statistics['Youngest_Member'] = 'Membro Pi� giovane';
$age_statistics['Youngest_Age'] = 'Et� Pi� giovane';
$age_statistics['Oldest_Member'] = 'Membro Pi� anziano';
$age_statistics['Oldest_Age'] = 'Pi� vecchia Et�';
$age_statistics['Error_message'] = '<a href="http://www.phpbbhacks.com/viewhack.php?id=187">Birthday Hack</a> non � stato trovato, non installano quel MOD prima di per mezzo di questo modulo';
// [/age_statistics]

// [age_statistics_terrafrost]
$age_statistics_terrafrost = array();

$age_statistics_terrafrost['module_name'] = 'Age Statistics';
$age_statistics_terrafrost['Users_Age'] = 'Numero di membri con l\'et�';
$age_statistics_terrafrost['Average_Age'] = 'Et� Media';
$age_statistics_terrafrost['Youngest_Member'] = 'Membro Pi� giovane';
$age_statistics_terrafrost['Youngest_Age'] = 'Et� Pi� giovane';
$age_statistics_terrafrost['Oldest_Member'] = 'Membro Pi� anziano';
$age_statistics_terrafrost['Oldest_Age'] = 'Pi� vecchia Et�';
$age_statistics_terrafrost['Error_message'] = '<a href="http://www.phpbb.com/phpBB/viewtopic.php?t=342028">Birthdays mod from terrafrost</a> non � stato trovato, non installano quel MOD prima di per mezzo di questo modulo';
// [/age_statistics_terrafrost]

// [Activity_Arcade_Mod_Stats]
$Activity_Arcade_Mod_Stats = array();

$Activity_Arcade_Mod_Stats['module_name']        = 'Arcade Activity Stats';
$Activity_Arcade_Mod_Stats['best_player']	    = 'Best Player at ';
$Activity_Arcade_Mod_Stats['fav_game']		    = 'Gioco del favorito a ';
$Activity_Arcade_Mod_Stats['total_games']	    = 'Giochi Totali Avaliable:';
$Activity_Arcade_Mod_Stats['total_games_played'] = 'I Giochi Totali Hanno giocato:';
$Activity_Arcade_Mod_Stats['total_comments']	    = 'Il Totale Commenta A sinistra:';
$Activity_Arcade_Mod_Stats['total_ratings']	    = 'Valutazioni Totali:';
$Activity_Arcade_Mod_Stats['total_highscores']   = 'Highscores Mensile Totale:';
$Activity_Arcade_Mod_Stats['games']			    = 'Volte Giocate %t%.';
$Activity_Arcade_Mod_Stats['first_places']	    = 'Con I Primi Posti %t%.';
$Activity_Arcade_Mod_Stats['nobody']             = 'Nessuno Ancora';
$Activity_Arcade_Mod_Stats['no_game']            = 'Nessun gioco ha giocato ancora';
// [/Activity_Arcade_Mod_Stats]

// [Arcade_All_Time_High_Score_Players]
$Arcade_All_Time_High_Score_Players = array();

$Arcade_All_Time_High_Score_Players['module_name'] 	= 'Activity Mod [Top '.$core->return_limit.' All Time High Score Players]';
$Arcade_All_Time_High_Score_Players['name'] = 'Username';
$Arcade_All_Time_High_Score_Players['all_time'] = 'Tutto il Tempo Highscores';
// [/Arcade_All_Time_High_Score_Players]

// [Activity_Arcade_Mod_Top_xx_Players]
$Activity_Arcade_Mod_Top_xx_Players = array();

$Activity_Arcade_Mod_Top_xx_Players['module_name'] 	= 'Activity Mod [Top '.$core->return_limit.' Players]';
$Activity_Arcade_Mod_Top_xx_Players['name'] = 'Username';
$Activity_Arcade_Mod_Top_xx_Players['currenthigh'] = 'Current Highscores';
$Activity_Arcade_Mod_Top_xx_Players['all_time'] = 'Tutto il Tempo Highscores';
// [/Activity_Arcade_Mod_Top_xx_Players]

// [Activity_Arcade_Mod_Top_xx_Games]
$Activity_Arcade_Mod_Top_xx_Games = array();

$Activity_Arcade_Mod_Top_xx_Games['module_name'] 	= 'MOD Di Attivit� [Giochi Del Principale '.$core->return_limit.']';
$Activity_Arcade_Mod_Top_xx_Games['plays'] = 'Giocato';
$Activity_Arcade_Mod_Top_xx_Games['game'] = 'Gioco';
// [/Activity_Arcade_Mod_Top_xx_Games]

// [Activity_Arcade_Mod_xx_Newest_Games]
$Activity_Arcade_Mod_xx_Newest_Games = array();

$Activity_Arcade_Mod_xx_Newest_Games['module_name'] = 'MOD Di Attivit� ['.$core->return_limit.' Giochi Aggiunti I pi� recenti]';
$Activity_Arcade_Mod_xx_Newest_Games['plays'] = 'Giocato';
$Activity_Arcade_Mod_xx_Newest_Games['game'] = 'Gioco';
$Activity_Arcade_Mod_xx_Newest_Games['date'] = 'Installed On';
// [/Activity_Arcade_Mod_xx_Newest_Games]

// [Activity_Arcade_Mod_Top_10_Users_Game_Plays]
$Activity_Arcade_Mod_Top_10_Users_Game_Plays = array();

$Activity_Arcade_Mod_Top_10_Users_Game_Plays['module_name'] 	= 'MOD Di Attivit� [ Utenti Del Principale '.$core->return_limit.' (Giochi Del Gioco)';
$Activity_Arcade_Mod_Top_10_Users_Game_Plays['played'] = 'Games Played';
// [/Activity_Arcade_Mod_Top_10_Users_Game_Plays]

// [last_created_album_posts]
$last_created_album_posts = array();

$last_created_album_posts['module_name'] = 'Ultimi Alberini Dell\'Album';
$last_created_album_posts['start_date'] = 'Inviato';
$last_created_album_posts['posted_in'] = 'Descrizione';
$last_created_album_posts['post'] = 'ID / Titolo';
$last_created_album_posts['br'] = '<br />';
// [/last_created_album_posts]

// [Top_album_posters_ever]
$Top_album_posters_ever = array();

$Top_album_posters_ever['module_name'] = 'Manifesti superiori dell\'album';
// [/Top_album_posters_ever]

// [Top_album_posters_this_month]
$Top_album_posters_this_month = array();

$Top_album_posters_this_month['module_name'] = 'Manifesti superiori dell\'album questo mese';
// [/Top_album_posters_this_month]

// [Top_album_posters_this_week]
$Top_album_posters_this_week = array();

$Top_album_posters_this_week['module_name'] = 'Manifesti superiori dell\'album questa settimana';
// [/Top_album_posters_this_week]

// [most_viewed_photos_ever]
$most_viewed_photos_ever = array();

$most_viewed_photos_ever['module_name'] = 'La Maggior parte Delle Foto Osservate Mai';
$most_viewed_photos_ever['photo'] = 'Foto';
$most_viewed_photos_ever['views'] = 'Viste';
$most_viewed_photos_ever['start_date'] = 'Inviato';
$most_viewed_photos_ever['desc'] = 'Descrizione';
$most_viewed_photos_ever['br'] = '<br />';
// [/most_viewed_photos_ever]

// [most_viewed_photos_this_month]
$most_viewed_photos_this_month = array();

$most_viewed_photos_this_month['module_name'] = 'La maggior parte delle foto osservate questo mese';
$most_viewed_photos_this_month['photo'] = 'Foto';
$most_viewed_photos_this_month['views'] = 'Viste';
$most_viewed_photos_this_month['start_date'] = 'Inviato';
$most_viewed_photos_this_month['desc'] = 'Descrizione';
$most_viewed_photos_this_month['br'] = '<br />';
// [/most_viewed_photos_this_month]

// [most_viewed_photos_this_week]
$most_viewed_photos_this_week = array();

$most_viewed_photos_this_week['module_name'] = 'La maggior parte delle foto osservate questa settimana';
$most_viewed_photos_this_week['photo'] = 'Foto';
$most_viewed_photos_this_week['views'] = 'Viste';
$most_viewed_photos_this_week['start_date'] = 'Inviato';
$most_viewed_photos_this_week['desc'] = 'Descrizione';
$most_viewed_photos_this_week['br'] = '<br />';
// [/most_viewed_photos_this_week]

// [top_countries_ipcountryflags]
$top_countries_ipcountryflags = array();

$top_countries_ipcountryflags['module_name'] = 'Paesi Superiori';
$top_countries_ipcountryflags['country'] = 'Paese';
$top_countries_ipcountryflags['flag'] = 'Bandierina';
$top_countries_ipcountryflags['users'] = 'Utenti';
$top_countries_ipcountryflags['Error_message'] = 'IpCountryFlags (<a href="http://www.you3d.net/ip2/index.php" target="_blank">http://www.you3d.net/ip2/index.php</a>) non � stato trovato, prego lo installano prima di per mezzo di questo modulo';
// [/top_countries_ipcountryflags]

// [Activity_plus_Mod_Top_xx_Games]
$Activity_plus_Mod_Top_xx_Games = array();

$Activity_plus_Mod_Top_xx_Games['module_name'] 	= 'MOD Di Attivit� Plus [Giochi Del Principale '.$core->return_limit.']';
$Activity_plus_Mod_Top_xx_Games['plays'] = 'Giocato';
$Activity_plus_Mod_Top_xx_Games['game'] = 'Gioco';
// [/Activity_plus_Mod_Top_xx_Games]

?>