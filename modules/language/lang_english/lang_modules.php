<?php

// [stats_overview]
$stats_overview = array();

$stats_overview['module_name'] = 'Statistics Overview';

// Admin Specific
$stats_overview['num_columns_title'] = 'Set the number of columns';
$stats_overview['num_columns_explain'] = 'Here you are able to set the number of columns displayed';

// [/stats_overview]

// [admin_statistics]
$admin_statistics = array();

// Administrative Statistics
$admin_statistics['module_name'] = 'Administrative Statistics';

$admin_statistics['Board_Up_Days'] = 'Board Up Days';
$admin_statistics['Latest_Reg_User'] = 'Latest User Registered';
$admin_statistics['Latest_Reg_User_Date'] = 'Latest User Registered Date';
$admin_statistics['Most_Ever_Online'] = 'Most Users Ever Online';
$admin_statistics['Most_Ever_Online_Date'] = 'Most Users Ever Online Date';
$admin_statistics['Disk_usage'] = 'Disk Usage';

// [/admin_statistics]

// [most_active_forums]
$most_active_forums = array();

$most_active_forums['module_name'] = 'Most Active Forums';
// [/most_active_forums]

// [most_active_topics]
$most_active_topics = array();

$most_active_topics['module_name'] = 'Most Active Topics';
// [/most_active_topics]

// [most_viewed_topics]
$most_viewed_topics = array();

$most_viewed_topics['module_name'] = 'Most Viewed Topics';
// [/most_viewed_topics]

// [most_active_topicstarter]
$most_active_topicstarter = array();

$most_active_topicstarter['module_name'] = 'Most Active Topicstarter';
// [/most_active_topicstarter]

// [most_interesting_topics]
$most_interesting_topics = array();

$most_interesting_topics['module_name'] = 'Most Interesting Topics';
$most_interesting_topics['Rate'] = 'Rate (views/messages)';
// [/most_interesting_topics]

// [least_interesting_topics]
$least_interesting_topics = array();

$least_interesting_topics['module_name'] = 'Least Interesting Topics';
$least_interesting_topics['Rate'] = 'Rate (views/messages)';
// [/least_interesting_topics]

// [most_least_interesting_topics]
$most_least_interesting_topics = array();

$most_least_interesting_topics['module_name'] = 'Most Interesting Topics&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Least Interesting Topics';
$most_least_interesting_topics['Rate'] = 'Rate (views/messages)';
// [/most_least_interesting_topics]

// [top_posters]
$top_posters = array();

$top_posters['module_name'] = 'Top Posters';
// [/top_posters]

// [fastest_users]
$fastest_users = array();

$fastest_users['module_name'] = 'Fastest Users';
$fastest_users['days_on_forum'] = 'Days On Forum';
$fastest_users['messperday'] = 'Messages Per Day';
// [/fastest_users]

// [last_online]
$last_online = array();

$last_online['module_name'] = 'Last Online';
$last_online['name'] = 'Username';
$last_online['last_online'] = 'Last Online';
$last_online['Error_message'] = 'Last Visit Mod (<a href=\"http://www.phpbbhacks.com/download/237\">http://www.phpbbhacks.com/download/237</a>) has not been found, please install it before using this module';
// [/last_online]

// [top_posters_month]
$top_posters_month = array();

$top_posters_month['module_name'] = 'Top posting users this month';
// [/top_posters_month]

// [users_by_month]
$users_by_month = array();

$users_by_month['module_name'] = 'Number of new users by month';
$users_by_month['Year'] = 'Year';
$users_by_month['Month_jan'] = 'Jan';
$users_by_month['Month_feb'] = 'Feb';
$users_by_month['Month_mar'] = 'Mar';
$users_by_month['Month_apr'] = 'Apr';
$users_by_month['Month_may'] = 'May';
$users_by_month['Month_jun'] = 'Jun';
$users_by_month['Month_jul'] = 'Jul';
$users_by_month['Month_aug'] = 'Aug';
$users_by_month['Month_sep'] = 'Sep';
$users_by_month['Month_oct'] = 'Oct';
$users_by_month['Month_nov'] = 'Nov';
$users_by_month['Month_dec'] = 'Dec';
// [/users_by_month]

// [top_posters_week]
$top_posters_week = array();

$top_posters_week['module_name'] = 'Top posting users this week';
// [/top_posters_week]

// [most_active_polls]
$most_active_polls = array();

$most_active_polls['module_name'] = 'Most Active Polls';
// [/most_active_polls]

// [most_active_pollstarter]
$most_active_pollstarter = array();

$most_active_pollstarter['module_name'] = 'Most Active Poll Starter';
$most_active_pollstarter['polls'] = 'Polls';
// [/most_active_pollstarter]

// [last_created_posts]
$last_created_posts = array();

$last_created_posts['module_name'] = 'Last Posts';
$last_created_posts['start_date'] = 'Posted';
$last_created_posts['posted_in'] = 'In Subject';
$last_created_posts['post'] = 'Message ID/subject';
$last_created_posts['hekje'] = '#';
$last_created_posts['by'] = 'By';
// [/last_created_posts]

// [last_created_topics]
$last_created_topics = array();

$last_created_topics['module_name'] = 'Last Topics';
$last_created_topics['start_date'] = 'Started';
// [/last_created_topics]

// [posts_by_month]
$posts_by_month = array();

$posts_by_month['module_name'] = 'Number of new posts by month';
$posts_by_month['Year'] = 'Year';
$posts_by_month['Month_jan'] = 'Jan';
$posts_by_month['Month_feb'] = 'Feb';
$posts_by_month['Month_mar'] = 'Mar';
$posts_by_month['Month_apr'] = 'Apr';
$posts_by_month['Month_may'] = 'May';
$posts_by_month['Month_jun'] = 'Jun';
$posts_by_month['Month_jul'] = 'Jul';
$posts_by_month['Month_aug'] = 'Aug';
$posts_by_month['Month_sep'] = 'Sep';
$posts_by_month['Month_oct'] = 'Oct';
$posts_by_month['Month_nov'] = 'Nov';
$posts_by_month['Month_dec'] = 'Dec';
// [/posts_by_month]

// [topics_by_month]
$topics_by_month = array();

$topics_by_month['module_name'] = 'Number of new topics by month';
$topics_by_month['Year'] = 'Year';
$topics_by_month['Month_jan'] = 'Jan';
$topics_by_month['Month_feb'] = 'Feb';
$topics_by_month['Month_mar'] = 'Mar';
$topics_by_month['Month_apr'] = 'Apr';
$topics_by_month['Month_may'] = 'May';
$topics_by_month['Month_jun'] = 'Jun';
$topics_by_month['Month_jul'] = 'Jul';
$topics_by_month['Month_aug'] = 'Aug';
$topics_by_month['Month_sep'] = 'Sep';
$topics_by_month['Month_oct'] = 'Oct';
$topics_by_month['Month_nov'] = 'Nov';
$topics_by_month['Month_dec'] = 'Dec';
// [/topics_by_month]

// [top_attachments]
$top_attachments = array();

$top_attachments['module_name'] = 'Top Downloaded Attachments';
$top_attachments['Filename'] = 'Filename';
$top_attachments['Filecomment'] = 'File Comment';
$top_attachments['Size'] = 'Filesize';
$top_attachments['Downloads'] = 'Downloads';
$top_attachments['Posttime'] = 'Post Time';
$top_attachments['Posted_in_topic'] = 'Posted in Topic';
// Admin Variables
$top_attachments['exclude_images_title'] = 'Exclude Images';
$top_attachments['exclude_images_explain'] = 'If this setting is enabled, images are not considered within the Top Attachments statistics.';
// [/top_attachments]

// [top_avatars]
$top_avatars = array();

$top_avatars['module_name'] = 'Top Gallary Avatars';
$top_avatars['Avatar_image'] = 'Avatar Image';
$top_avatars['Users'] = 'Users';
// [/top_avatars]

// [top_smilies]
$top_smilies = array();

$top_smilies['module_name'] = 'Top Smilies';
$top_smilies['Smilie_image'] = 'Smiley Image';
$top_smilies['Smilie_code'] = 'Smiley Code';
// [/top_smilies]

// [top_shoutbox_poster]
$top_shoutbox_poster = array();

$top_shoutbox_poster['module_name'] = 'Top Shoutbox Posters';
$top_shoutbox_poster['Shouts'] = 'Shouts';
$top_shoutbox_poster['Error_message'] = 'The Fully Integrated Shoutbox Mod have to be installed in order to see the Top Shoutbox Posters.<br>Install the mod (<a href="http://www.phpbbhacks.com/download/1255" target="_blank">http://www.phpbbhacks.com/download/1255</a>) or deactivate this module.';
// [/top_shoutbox_poster]

// [top_words]
$top_words = array();

$top_words['module_name'] = 'Most Used Words';
$top_words['Word'] = 'Word';
$top_words['Count'] = 'Count';
// [/top_words]

// [most_ignored_users]
$most_ignored_users = array();

$most_ignored_users['module_name'] = 'Most Ignored Users';
$most_ignored_users['ignored_by'] = ' users ignored by ';
$most_ignored_users['other'] = ' other users';
$most_ignored_users['Ignores'] = 'Ignored by X Users';
$most_ignored_users['Error_message'] = 'The Ignore Users Mod have to be installed in order to see the Ignored Users.<br>Install the mod (<a href="http://www.phpbbhacks.com/download/789" target="_blank">http://www.phpbbhacks.com/download/789</a>) or deactivate this module.';
// [/most_ignored_users]

// [private_messages]
$private_messages = array();

$private_messages['module_name'] = 'Private Messages';
$private_messages['lastmonth'] = '30 days';
$private_messages['24hours'] = 'Past 24 hours';
$private_messages['7days'] = 'Past Week';
$private_messages['send'] = 'Send';
$private_messages['read'] = 'Read';
$private_messages['new'] = 'New';
$private_messages['saved_in'] = 'Saved Inbox';
$private_messages['saved_out'] = 'Saved Outbox';
$private_messages['unread'] = 'Unread';
// [/private_messages]

// [top_styles]
$top_styles = array();

$top_styles['module_name'] = 'Top Styles';
// [/top_styles]

// [top_languages]
$top_languages = array();

$top_languages['module_name'] = 'Top Languages';
$top_languages['langname'] = 'Language';
// [/top_languages]

// [most_logged_on_users]
$most_logged_on_users = array();

$most_logged_on_users['module_name'] = 'Most Logged On Users';
$most_logged_on_users['totalforumtime'] = 'Boardtime';
$most_logged_on_users['totallogins'] = 'x Logs';
$most_logged_on_users['daysmember'] = 'Registered';
$most_logged_on_users['visitedpages'] = 'Page Hits';
$most_logged_on_users['Error_message'] = 'The Last Visit Mod have to be installed in order to see the Most Logged On Users.<br>Install the mod (<a href="http://www.phpbbhacks.com/download/237" target="_blank">http://www.phpbbhacks.com/download/237</a>) or deactivate this module.';
// [/most_logged_on_users]

// [advanced_karma]
$advanced_karma = array();

$advanced_karma['module_name'] = 'Karma Points';
$advanced_karma['High'] = 'Positive';
$advanced_karma['Low'] = 'Negative';
$advanced_karma['Error_message'] = 'The Advanced Karma Mod have to be installed in order to see the Karma Points.<br>Install the Mod (<a href="http://siava.ru/forum/topic168.html" target="_blank">http://siava.ru/forum/topic168.html</a>) or deactivate this module.';
// [/advanced_karma]

// [Cash]
$Cash = array();

$Cash['module_name'] = 'Cash';
$Cash['username'] = 'User';
$Cash['Error_message'] = 'Cash Mod has not been found, please install it before using this module';
// [/Cash]

// [genders]
$genders = array();

// Genders
$genders['module_name'] = 'Male and Female users';
$genders['Gender'] = 'Gender';
$genders['undefined'] = 'Undefined';
$genders['male'] = '<img src="' . $images['icon_minigender_male'] . '"> Male';
$genders['female'] = '<img src="' . $images['icon_minigender_female'] . '"> Female';
$genders['total'] = 'Total';
$genders['Error_message'] = 'Gender Mod (from Niels <a href="http://phpbbhacks.com/download/182" target="_blank">http://phpbbhacks.com/download/182</a>) has not been found, please install it before using this module';
// [/genders]

// [posts_by_gender_niels]
$posts_by_gender_niels = array();

// Genders
$posts_by_gender_niels['module_name'] = 'Posts by Gender';
$posts_by_gender_niels['Gender'] = 'Gender';
$posts_by_gender_niels['undefined'] = 'Undefined';
$posts_by_gender_niels['male'] = '<img src="' . $images['icon_minigender_male'] . '"> Male';
$posts_by_gender_niels['female'] = '<img src="' . $images['icon_minigender_female'] . '"> Female';
$posts_by_gender_niels['Error_message'] = 'Gender Mod (from Niels <a href="http://phpbbhacks.com/download/182" target="_blank">http://phpbbhacks.com/download/182</a>) has not been found, please install it before using this module';
// [/posts_by_gender_niels]

// [genders_evil]
$genders_evil = array();

// Genders
$genders_evil['module_name'] = 'Male and Female users';
$genders_evil['Gender'] = 'Gender';
$genders_evil['undefined'] = 'Undefined';
$genders_evil['male'] = '<img src="' . $images['gender_m'] . '"> Male';
$genders_evil['female'] = '<img src="' . $images['gender_f'] . '"> Female';
$genders_evil['total'] = 'Total';
$genders_evil['Error_message'] = 'Gender Mod (from Evil <a href="http://www.phpbb.com/phpBB/viewtopic.php?t=467168" target="_blank">http://www.phpbb.com/phpBB/viewtopic.php?t=467168</a>) has not been found, please install it before using this module';
// [/genders_evil]

// [posts_by_gender_evil]
$posts_by_gender_evil = array();

// Genders
$posts_by_gender_evil['module_name'] = 'Posts by Gender';
$posts_by_gender_evil['Gender'] = 'Gender';
$posts_by_gender_evil['undefined'] = 'Undefined';
$posts_by_gender_evil['male'] = '<img src="' . $images['gender_m'] . '"> Male';
$posts_by_gender_evil['female'] = '<img src="' . $images['gender_f'] . '"> Female';
$posts_by_gender_evil['Error_message'] = 'Gender Mod (from Evil <a href="http://www.phpbb.com/phpBB/viewtopic.php?t=467168" target="_blank">http://www.phpbb.com/phpBB/viewtopic.php?t=467168</a>) has not been found, please install it before using this module';
// [/posts_by_gender_evil]

// [age_statistics]
$age_statistics = array();

$age_statistics['module_name'] = 'Age Statistics';
$age_statistics['Users_Age'] = 'No.Users with age';
$age_statistics['Average_Age'] = 'Average Age';
$age_statistics['Youngest_Member'] = 'Youngest Member';
$age_statistics['Youngest_Age'] = 'Youngest Age';
$age_statistics['Oldest_Member'] = 'Oldest Member';
$age_statistics['Oldest_Age'] = 'Oldest Age';
$age_statistics['Error_message'] = '<a href="http://www.phpbbhacks.com/viewhack.php?id=187">Birthday Hack</a> has not been found, please install it before using this module';
// [/age_statistics]

// [age_statistics_terrafrost]
$age_statistics_terrafrost = array();

$age_statistics_terrafrost['module_name'] = 'Age Statistics';
$age_statistics_terrafrost['Users_Age'] = 'No.Users with age';
$age_statistics_terrafrost['Average_Age'] = 'Average Age';
$age_statistics_terrafrost['Youngest_Member'] = 'Youngest Member';
$age_statistics_terrafrost['Youngest_Age'] = 'Youngest Age';
$age_statistics_terrafrost['Oldest_Member'] = 'Oldest Member';
$age_statistics_terrafrost['Oldest_Age'] = 'Oldest Age';
$age_statistics_terrafrost['Error_message'] = '<a href="http://www.phpbb.com/phpBB/viewtopic.php?t=342028">Birthdays mod from terrafrost</a> has not been found, please install it before using this module';
// [/age_statistics_terrafrost]

// [Activity_Arcade_Mod_Stats]
$Activity_Arcade_Mod_Stats = array();

$Activity_Arcade_Mod_Stats['module_name']        = 'Arcade Activity Stats';
$Activity_Arcade_Mod_Stats['best_player']	    = 'Best Player at ';
$Activity_Arcade_Mod_Stats['fav_game']		    = 'Favourite Game at ';
$Activity_Arcade_Mod_Stats['total_games']	    = 'Total Games Avaliable:';
$Activity_Arcade_Mod_Stats['total_games_played'] = 'Total Games Played:';
$Activity_Arcade_Mod_Stats['total_comments']	    = 'Total Comments Left:';
$Activity_Arcade_Mod_Stats['total_ratings']	    = 'Total Ratings:';
$Activity_Arcade_Mod_Stats['total_highscores']   = 'Total Monthly Highscores:';
$Activity_Arcade_Mod_Stats['games']			    = 'Played %t% Times.';
$Activity_Arcade_Mod_Stats['first_places']	    = 'With %t% 1st Places.';
$Activity_Arcade_Mod_Stats['nobody']             = 'Nobody Yet';
$Activity_Arcade_Mod_Stats['no_game']            = 'No games played yet';
// [/Activity_Arcade_Mod_Stats]

// [Arcade_All_Time_High_Score_Players]
$Arcade_All_Time_High_Score_Players = array();

$Arcade_All_Time_High_Score_Players['module_name'] 	= 'Activity Mod [Top '.$core->return_limit.' All Time High Score Players]';
$Arcade_All_Time_High_Score_Players['name'] = 'Username';
$Arcade_All_Time_High_Score_Players['all_time'] = 'All Time Highscores';
// [/Arcade_All_Time_High_Score_Players]

// [Activity_Arcade_Mod_Top_xx_Players]
$Activity_Arcade_Mod_Top_xx_Players = array();

$Activity_Arcade_Mod_Top_xx_Players['module_name'] 	= 'Activity Mod [Top '.$core->return_limit.' Players]';
$Activity_Arcade_Mod_Top_xx_Players['name'] = 'Username';
$Activity_Arcade_Mod_Top_xx_Players['currenthigh'] = 'Current Highscores';
$Activity_Arcade_Mod_Top_xx_Players['all_time'] = 'All Time Highscores';
// [/Activity_Arcade_Mod_Top_xx_Players]

// [Activity_Arcade_Mod_Top_xx_Games]
$Activity_Arcade_Mod_Top_xx_Games = array();

$Activity_Arcade_Mod_Top_xx_Games['module_name'] 	= 'Activity Mod [Top '.$core->return_limit.' Games]';
$Activity_Arcade_Mod_Top_xx_Games['plays'] = 'Plays';
$Activity_Arcade_Mod_Top_xx_Games['game'] = 'Game';
// [/Activity_Arcade_Mod_Top_xx_Games]

// [Activity_Arcade_Mod_xx_Newest_Games]
$Activity_Arcade_Mod_xx_Newest_Games = array();

$Activity_Arcade_Mod_xx_Newest_Games['module_name'] = 'Activity Mod ['.$core->return_limit.' Most Recent Added Games]';
$Activity_Arcade_Mod_xx_Newest_Games['plays'] = 'Plays';
$Activity_Arcade_Mod_xx_Newest_Games['game'] = 'Game';
$Activity_Arcade_Mod_xx_Newest_Games['date'] = 'Installed On';
// [/Activity_Arcade_Mod_xx_Newest_Games]

// [Activity_Arcade_Mod_Top_10_Users_Game_Plays]
$Activity_Arcade_Mod_Top_10_Users_Game_Plays = array();

$Activity_Arcade_Mod_Top_10_Users_Game_Plays['module_name'] 	= 'Activity Mod [Top '.$core->return_limit.' Users (Game Plays)]';
$Activity_Arcade_Mod_Top_10_Users_Game_Plays['played'] = 'Games Played';
// [/Activity_Arcade_Mod_Top_10_Users_Game_Plays]

// [last_created_album_posts]
$last_created_album_posts = array();

$last_created_album_posts['module_name'] = 'Last Album Posts';
$last_created_album_posts['start_date'] = 'Posted';
$last_created_album_posts['posted_in'] = 'Description';
$last_created_album_posts['post'] = 'ID / Title';
$last_created_album_posts['br'] = '<br />';
// [/last_created_album_posts]

// [Top_album_posters_ever]
$Top_album_posters_ever = array();

$Top_album_posters_ever['module_name'] = 'Top album posters';
// [/Top_album_posters_ever]

// [Top_album_posters_this_month]
$Top_album_posters_this_month = array();

$Top_album_posters_this_month['module_name'] = 'Top album posters this month';
// [/Top_album_posters_this_month]

// [Top_album_posters_this_week]
$Top_album_posters_this_week = array();

$Top_album_posters_this_week['module_name'] = 'Top album posters this week';
// [/Top_album_posters_this_week]

// [most_viewed_photos_ever]
$most_viewed_photos_ever = array();

$most_viewed_photos_ever['module_name'] = 'Most Viewed Photos Ever';
$most_viewed_photos_ever['photo'] = 'Photo';
$most_viewed_photos_ever['views'] = 'Views';
$most_viewed_photos_ever['start_date'] = 'Posted';
$most_viewed_photos_ever['desc'] = 'Description';
$most_viewed_photos_ever['br'] = '<br />';
// [/most_viewed_photos_ever]

// [most_viewed_photos_this_month]
$most_viewed_photos_this_month = array();

$most_viewed_photos_this_month['module_name'] = 'Most Viewed Photos this Month';
$most_viewed_photos_this_month['photo'] = 'Photo';
$most_viewed_photos_this_month['views'] = 'Views';
$most_viewed_photos_this_month['start_date'] = 'Posted';
$most_viewed_photos_this_month['desc'] = 'Description';
$most_viewed_photos_this_month['br'] = '<br />';
// [/most_viewed_photos_this_month]

// [most_viewed_photos_this_week]
$most_viewed_photos_this_week = array();

$most_viewed_photos_this_week['module_name'] = 'Most Viewed Photos this Week';
$most_viewed_photos_this_week['photo'] = 'Photo';
$most_viewed_photos_this_week['views'] = 'Views';
$most_viewed_photos_this_week['start_date'] = 'Posted';
$most_viewed_photos_this_week['desc'] = 'Description';
$most_viewed_photos_this_week['br'] = '<br />';
// [/most_viewed_photos_this_week]

// [top_countries_ipcountryflags]
$top_countries_ipcountryflags = array();

$top_countries_ipcountryflags['module_name'] = 'Top Countries';
$top_countries_ipcountryflags['country'] = 'Country';
$top_countries_ipcountryflags['flag'] = 'Flag';
$top_countries_ipcountryflags['users'] = 'Users';
$top_countries_ipcountryflags['Error_message'] = 'IpCountryFlags (<a href="http://www.you3d.net/ip2/index.php" target="_blank">http://www.you3d.net/ip2/index.php</a>) has not been found, please install it before using this module';
// [/top_countries_ipcountryflags]

// [Activity_plus_stats]
$Activity_plus_stats = array();

$Activity_plus_stats['module_name']        = 'Activity Mod Plus';
$Activity_plus_stats['best_player']	    = 'Best Player at ';
$Activity_plus_stats['fav_game']		    = 'Favourite Game at ';
$Activity_plus_stats['total_games']	    = 'Total Games Avaliable:';
$Activity_plus_stats['total_comments']	    = 'Total Comments Left:';
$Activity_plus_stats['total_bets']	    = 'Total Bets Made:';
$Activity_plus_stats['total_challenges']   = 'Total Challenges Sent:';
$Activity_plus_stats['played']			    = 'Played %t% Times.';
$Activity_plus_stats['trophies']	    = 'with %t% Trophies.';
$Activity_plus_stats['nobody'] = 'Nobody';
$Activity_plus_stats['no_game'] = 'No games';
// [/Activity_plus_stats]

// [Activity_Plus_Mod_xx_Newest_Games]
$Activity_Plus_Mod_xx_Newest_Games = array();

$Activity_Plus_Mod_xx_Newest_Games['module_name'] = 'Activity Plus Mod ['.$core->return_limit.' Most Recent Added Games]';
$Activity_Plus_Mod_xx_Newest_Games['plays'] = 'Plays';
$Activity_Plus_Mod_xx_Newest_Games['game'] = 'Game';
$Activity_Plus_Mod_xx_Newest_Games['date'] = 'Installed On';
// [/Activity_Plus_Mod_xx_Newest_Games]

// [Activity_Plus_Mod_Top_xx_Trophies]
$Activity_Plus_Mod_Top_xx_Trophies = array();

$Activity_Plus_Mod_Top_xx_Trophies['module_name'] 	= 'Activity Plus Mod [Top '.$core->return_limit.' Trophies]';
$Activity_Plus_Mod_Top_xx_Trophies['name'] = 'User';
$Activity_Plus_Mod_Top_xx_Trophies['Trophies'] = 'Trophies';
// [/Activity_Plus_Mod_Top_xx_Trophies]

// [Activity_Plus_Mod_Top_xx_Players]
$Activity_Plus_Mod_Top_xx_Players = array();

$Activity_Plus_Mod_Top_xx_Players['module_name'] 	= 'Activity Plus Mod [Top '.$core->return_limit.' Gameplays]';
$Activity_Plus_Mod_Top_xx_Players['plays'] = 'Plays';
$Activity_Plus_Mod_Top_xx_Players['name'] = 'User';
// [/Activity_Plus_Mod_Top_xx_Players]

// [Activity_plus_Mod_Top_xx_Games]
$Activity_plus_Mod_Top_xx_Games = array();

$Activity_plus_Mod_Top_xx_Games['module_name'] 	= 'Activity Plus Mod [Top '.$core->return_limit.' Games]';
$Activity_plus_Mod_Top_xx_Games['plays'] = 'Plays';
$Activity_plus_Mod_Top_xx_Games['game'] = 'Game';
// [/Activity_plus_Mod_Top_xx_Games]

?>