<?php

// [stats_overview]
$stats_overview = array();

$stats_overview['module_name'] = 'Vorhandene Statistiken';

// Admin Specific
$stats_overview['num_columns_title'] = 'Anzahl der Spalten';
$stats_overview['num_columns_explain'] = 'Hier kannst du die Anzahl der Spalten die angezeigt werden sollen, �ndern.';

// [/stats_overview]

// [admin_statistics]
$admin_statistics = array();

// Administrative Statistiken
$admin_statistics['module_name'] = 'Administrative Statistiken';

$admin_statistics['Board_Up_Days'] = 'Tage seit der ersten Inbetriebnahme';
$admin_statistics['Latest_Reg_User'] = 'Letzter registrierter Benutzer';
$admin_statistics['Latest_Reg_User_Date'] = 'Letzter registrierter Benutzer am';
$admin_statistics['Most_Ever_Online'] = 'Meisten User auf einem Mal Online';
$admin_statistics['Most_Ever_Online_Date'] = 'Meisten User auf einem Mal Online am';
$admin_statistics['Disk_usage'] = 'Speicherplatzverbrauch der Attachments';

// [/admin_statistics]

// [most_active_forums]
$most_active_forums = array();

$most_active_forums['module_name'] = 'Die Meisten Aktiven Foren';
// [/most_active_forums]

// [most_active_topics]
$most_active_topics = array();

$most_active_topics['module_name'] = 'Die aktivsten Themen';
// [/most_active_topics]

// [most_viewed_topics]
$most_viewed_topics = array();

$most_viewed_topics['module_name'] = 'Die am h�ufigsten aufgerufenen Themen';
// [/most_viewed_topics]

// [most_active_topicstarter]
$most_active_topicstarter = array();

$most_active_topicstarter['module_name'] = 'Die aktivsten Themenstarter';
// [/most_active_topicstarter]

// [most_interesting_topics]
$most_interesting_topics = array();

$most_interesting_topics['module_name'] = 'Die Meisten Interessanten Themen';
$most_interesting_topics['Rate'] = 'Rate (views/messages)';
// [/most_interesting_topics]

// [least_interesting_topics]
$least_interesting_topics = array();

$least_interesting_topics['module_name'] = 'Wenige Interessante Themen';
$least_interesting_topics['Rate'] = 'Rate (views/messages)';
// [/least_interesting_topics]

// [most_least_interesting_topics]
$most_least_interesting_topics = array();

$most_least_interesting_topics['module_name'] = 'Die Meisten Interessanten Themen&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Wenige Interessante Themen';
$most_least_interesting_topics['Rate'] = 'Rate (views/messages)';
// [/most_least_interesting_topics]

// [top_posters]
$top_posters = array();

$top_posters['module_name'] = 'Die am h�ufigsten postenden Benutzer';
// [/top_posters]

// [fastest_users]
$fastest_users = array();

$fastest_users['module_name'] = 'Schnellste Benutzer';
$fastest_users['days_on_forum'] = 'Tage auf dem Forum';
$fastest_users['messperday'] = 'Anzeigen Pro Tag';
// [/fastest_users]

// [last_online]
$last_online = array();

$last_online['module_name'] = 'Letztes Online';
$last_online['name'] = 'Benutzername';
$last_online['last_online'] = 'Letztes On-line';
$last_online['Error_message'] = 'Last Visit Mod (<a href="http://www.phpbbhacks.com/download/237">http://www.phpbbhacks.com/download/237</a>) ist nicht gefunden worden, es bitte anbringt, bevor man dieses Modul verwendete';
// [/last_online]

// [top_posters_month]
$top_posters_month = array();

$top_posters_month['module_name'] = 'Die am h�ufigsten postenden Benutzer diesen Monat';
// [/top_posters_month]

// [users_by_month]
$users_by_month = array();

$users_by_month['module_name'] = 'Anzahl neuer Benutzer pro Monat';
$users_by_month['Year'] = 'Jahr';
$users_by_month['Month_jan'] = 'Jan';
$users_by_month['Month_feb'] = 'Feb';
$users_by_month['Month_mar'] = 'M�r';
$users_by_month['Month_apr'] = 'Apr';
$users_by_month['Month_may'] = 'Mai';
$users_by_month['Month_jun'] = 'Jun';
$users_by_month['Month_jul'] = 'Jul';
$users_by_month['Month_aug'] = 'Aug';
$users_by_month['Month_sep'] = 'Sep';
$users_by_month['Month_oct'] = 'Okt';
$users_by_month['Month_nov'] = 'Nov';
$users_by_month['Month_dec'] = 'Dez';
// [/users_by_month]

// [top_posters_week]
$top_posters_week = array();

$top_posters_week['module_name'] = 'Die am h�ufigsten postenden Benutzer diese Woche';
// [/top_posters_week]

// [most_active_polls]
$most_active_polls = array();

$most_active_polls['module_name'] = 'Die Meisten Aktiven Abstimmungen';
// [/most_active_polls]

// [most_active_pollstarter]
$most_active_pollstarter = array();

$most_active_pollstarter['module_name'] = 'Der Meiste Aktive Abstimmung-Starter';
$most_active_pollstarter['polls'] = 'Abstimmungen';
// [/most_active_pollstarter]

// [last_created_posts]
$last_created_posts = array();

$last_created_posts['module_name'] = 'Letzte Pfosten';
$last_created_posts['start_date'] = 'Bekanntgegeben';
$last_created_posts['posted_in'] = 'Im Thema';
$last_created_posts['post'] = 'Anzeige ID/thema';
$last_created_posts['hekje'] = '#';
$last_created_posts['by'] = 'Durch';
// [/last_created_posts]

// [last_created_topics]
$last_created_topics = array();

$last_created_topics['module_name'] = 'Letzte Themen';
$last_created_topics['start_date'] = 'Begonnen';
// [/last_created_topics]

// [posts_by_month]
$posts_by_month = array();

$posts_by_month['module_name'] = 'Anzahl neuer Nachrichten pro Monat';
$posts_by_month['Year'] = 'Jahr';
$posts_by_month['Month_jan'] = 'Jan';
$posts_by_month['Month_feb'] = 'Feb';
$posts_by_month['Month_mar'] = 'M�r';
$posts_by_month['Month_apr'] = 'Apr';
$posts_by_month['Month_may'] = 'Mai';
$posts_by_month['Month_jun'] = 'Jun';
$posts_by_month['Month_jul'] = 'Jul';
$posts_by_month['Month_aug'] = 'Aug';
$posts_by_month['Month_sep'] = 'Sep';
$posts_by_month['Month_oct'] = 'Okt';
$posts_by_month['Month_nov'] = 'Nov';
$posts_by_month['Month_dec'] = 'Dez';
// [/posts_by_month]

// [topics_by_month]
$topics_by_month = array();

$topics_by_month['module_name'] = 'Anzahl neuer Themen pro Monat';
$topics_by_month['Year'] = 'Jahr';
$topics_by_month['Month_jan'] = 'Jan';
$topics_by_month['Month_feb'] = 'Feb';
$topics_by_month['Month_mar'] = 'M�r';
$topics_by_month['Month_apr'] = 'Apr';
$topics_by_month['Month_may'] = 'Mai';
$topics_by_month['Month_jun'] = 'Jun';
$topics_by_month['Month_jul'] = 'Jul';
$topics_by_month['Month_aug'] = 'Aug';
$topics_by_month['Month_sep'] = 'Sep';
$topics_by_month['Month_oct'] = 'Okt';
$topics_by_month['Month_nov'] = 'Nov';
$topics_by_month['Month_dec'] = 'Dez';
// [/topics_by_month]

// [top_attachments]
$top_attachments = array();

$top_attachments['module_name'] = 'Die am h�ufigsten heruntergeladenen Dateien';
$top_attachments['Filename'] = 'Dateiname';
$top_attachments['Filecomment'] = 'Dateikommentar';
$top_attachments['Size'] = 'Dateigr��e';
$top_attachments['Downloads'] = 'Downloads';
$top_attachments['Posttime'] = 'Postzeit';
$top_attachments['Posted_in_topic'] = 'Thema';
// Admin Variables
$top_attachments['exclude_images_title'] = 'Bilder nicht ber�cksichtigen';
$top_attachments['exclude_images_explain'] = 'Wenn diese Option eingeschaltet ist, dann werden Bilder in der Statistik nicht ber�cksichtigt.';
// [/top_attachments]

// [top_avatars]
$top_avatars = array();

$top_avatars['module_name'] = 'Top Gallary Avatars';
$top_avatars['Avatar_image'] = 'Avatar Bild';
$top_avatars['Users'] = 'Benutzer';
// [/top_avatars]

// [top_smilies]
$top_smilies = array();

$top_smilies['module_name'] = 'Die meist genutzten Smilies';
$top_smilies['Smilie_image'] = 'Smiley Bild';
$top_smilies['Smilie_code'] = 'Smiley Code';
// [/top_smilies]

// [top_shoutbox_poster]
$top_shoutbox_poster = array();

$top_shoutbox_poster['module_name'] = 'Top Shoutbox Posters';
$top_shoutbox_poster['Shouts'] = 'Shouts';
$top_shoutbox_poster['Error_message'] = 'Der Fully Integrated Shoutbox Mod m�ss angebracht werden, um die Top Shoutbox Posters zu sehen.<br>Bringen Sie den Mod an (<a href="http://www.phpbbhacks.com/download/1255" target="_blank">http://www.phpbbhacks.com/download/1255</a>) oder entaktivieren Sie dieses Modul.';
// [/top_shoutbox_poster]

// [top_words]
$top_words = array();

$top_words['module_name'] = 'Die Meisten Verwendeten W�rter';
$top_words['Word'] = 'Wort';
$top_words['Count'] = 'Z�hlimpuls';
// [/top_words]

// [most_ignored_users]
$most_ignored_users = array();

$most_ignored_users['module_name'] = 'Die meisten ignorierten Benutzer';
$most_ignored_users['ignored_by'] = ' benutzer ignoriert von ';
$most_ignored_users['other'] = ' anderen Benutzern';
$most_ignored_users['Ignores'] = 'Ignoriert durch X Users';
$most_ignored_users['Error_message'] = 'Der Ignore Users Mod m�ss angebracht werden, um die Ignorierten Benutzer zu sehen.<br>Bringen Sie den Mod an (<a href="http://www.phpbbhacks.com/download/789" target="_blank">http://www.phpbbhacks.com/download/789</a>) oder entaktivieren Sie dieses Modul.';
// [/most_ignored_users]

// [private_messages]
$private_messages = array();

$private_messages['module_name'] = 'Private Anzeigen';
$private_messages['lastmonth'] = '30 Tage';
$private_messages['24hours'] = 'Hinter 24 Stunden';
$private_messages['7days'] = 'Hinter Woche';
$private_messages['send'] = 'Send';
$private_messages['read'] = 'Gelesen';
$private_messages['new'] = 'Neu';
$private_messages['saved_in'] = 'Saved Inbox';
$private_messages['saved_out'] = 'Saved Outbox';
$private_messages['unread'] = 'Unread';
// [/private_messages]

// [top_styles]
$top_styles = array();

$top_styles['module_name'] = 'Obere Arten';
// [/top_styles]

// [top_languages]
$top_languages = array();

$top_languages['module_name'] = 'Die meisten verwendeten Sprachen';
$top_languages['langname'] = 'Sprache';
// [/top_languages]

// [most_logged_on_users]
$most_logged_on_users = array();

$most_logged_on_users['module_name'] = 'Die Meisten Angemeldeten Benutzer';
$most_logged_on_users['totalforumtime'] = 'BoardZeit';
$most_logged_on_users['totallogins'] = 'x Logs';
$most_logged_on_users['daysmember'] = 'Registriert';
$most_logged_on_users['visitedpages'] = 'Seite Erfolge';
$most_logged_on_users['Error_message'] = 'Der Last Visit Mod m�ss angebracht werden, um die Die Meisten Angemeldeten Benutzer zu sehen.<br>Bringen Sie den Mod an (<a href="http://www.phpbbhacks.com/download/237" target="_blank">http://www.phpbbhacks.com/download/237</a>) oder entaktivieren Sie dieses Modul.';
// [/most_logged_on_users]

// [advanced_karma]
$advanced_karma = array();

$advanced_karma['module_name'] = 'Karmapunkte';
$advanced_karma['High'] = 'Positiv';
$advanced_karma['Low'] = 'Negativ';
$advanced_karma['Error_message'] = 'Der Advanced Karma Mod m�ssen angebracht werden, um die Karma-Punkte zu sehen.<br>Bringen Sie den Mod an (<a href="http://siava.ru/forum/topic168.html" target="_blank">http://siava.ru/forum/topic168.html</a>) oder entaktivieren Sie dieses Modul.';
// [/advanced_karma]

// [Cash]
$Cash = array();

$Cash['module_name'] = 'Bargeld';
$Cash['username'] = 'Benutzer';
$Cash['Error_message'] = 'Cash Mod ist nicht gefunden worden, es bitte anbringt, bevor man dieses Modul verwendete';
// [/Cash]

// [genders]
$genders = array();

// Administrative Statistiken
$genders['module_name'] = 'M�nnliche und weibliche Benutzer';
$genders['Gender'] = 'Geschlecht';
$genders['undefined'] = 'Unbestimmt';
$genders['male'] = '<img src="' . $images['icon_minigender_male'] . '"> Mann';
$genders['female'] = '<img src="' . $images['icon_minigender_female'] . '"> Frau';
$genders['total'] = 'Gesamtmenge';
$genders['Error_message'] = 'Gender Mod (from Niels <a href="http://phpbbhacks.com/download/182" target="_blank">http://phpbbhacks.com/download/182</a>) ist nicht gefunden worden, es bitte anbringt, bevor man dieses Modul verwendete';
// [/genders]

// [posts_by_gender_niels]
$posts_by_gender_niels = array();

// Administrative Statistiken
$posts_by_gender_niels['module_name'] = 'Pfosten durch Gender';
$posts_by_gender_niels['Gender'] = 'Geschlecht';
$posts_by_gender_niels['undefined'] = 'Unbestimmt';
$posts_by_gender_niels['male'] = '<img src="' . $images['icon_minigender_male'] . '"> Mann';
$posts_by_gender_niels['female'] = '<img src="' . $images['icon_minigender_female'] . '"> Frau';
$posts_by_gender_niels['Error_message'] = 'Gender Mod (from Niels <a href="http://phpbbhacks.com/download/182" target="_blank">http://phpbbhacks.com/download/182</a>) ist nicht gefunden worden, es bitte anbringt, bevor man dieses Modul verwendete';
// [/posts_by_gender_niels]

// [genders_evil]
$genders_evil = array();

// Genders
$genders_evil['module_name'] = 'M�nnliche und weibliche Benutzer';
$genders_evil['Gender'] = 'Geschlecht';
$genders_evil['undefined'] = 'Unbestimmt';
$genders_evil['male'] = '<img src="' . $images['gender_m'] . '"> Mann';
$genders_evil['female'] = '<img src="' . $images['gender_f'] . '"> Frau';
$genders_evil['total'] = 'Gesamtmenge';
$genders_evil['Error_message'] = 'Gender Mod (from Evil <a href="http://www.phpbb.com/phpBB/viewtopic.php?t=467168" target="_blank">http://www.phpbb.com/phpBB/viewtopic.php?t=467168</a>) ist nicht gefunden worden, es bitte anbringt, bevor man dieses Modul verwendete';
// [/genders_evil]

// [posts_by_gender_evil]
$posts_by_gender_evil = array();

// Administrative Statistiken
$posts_by_gender_evil['module_name'] = 'Pfosten durch Gender';
$posts_by_gender_evil['Gender'] = 'Geschlecht';
$posts_by_gender_evil['undefined'] = 'Unbestimmt';
$posts_by_gender_evil['male'] = '<img src="' . $images['gender_m'] . '"> Mann';
$posts_by_gender_evil['female'] = '<img src="' . $images['gender_f'] . '"> Frau';
$posts_by_gender_evil['Error_message'] = 'Gender Mod (from Evil <a href="http://www.phpbb.com/phpBB/viewtopic.php?t=467168" target="_blank">http://www.phpbb.com/phpBB/viewtopic.php?t=467168</a>) ist nicht gefunden worden, es bitte anbringt, bevor man dieses Modul verwendete';
// [/posts_by_gender_evil]

// [age_statistics]
$age_statistics = array();

$age_statistics['module_name'] = 'Alter Statistiken';
$age_statistics['Users_Age'] = 'Zahl der Mitglieder mit Alter';
$age_statistics['Average_Age'] = 'Altersdurchschnitt';
$age_statistics['Youngest_Member'] = 'J�ngstes Mitglied';
$age_statistics['Youngest_Age'] = 'J�ngstes Alter';
$age_statistics['Oldest_Member'] = '�ltestes Mitglied';
$age_statistics['Oldest_Age'] = '�ltestes Alter';
$age_statistics['Error_message'] = '<a href="http://www.phpbbhacks.com/viewhack.php?id=187">Birthday Hack</a> ist nicht gefunden worden, diesen Umb. anbringen, bevor man dieses Modul verwendete';
// [/age_statistics]

// [age_statistics_terrafrost]
$age_statistics_terrafrost = array();

$age_statistics_terrafrost['module_name'] = 'Alter Statistiken';
$age_statistics_terrafrost['Users_Age'] = 'Zahl der Mitglieder mit Alter';
$age_statistics_terrafrost['Average_Age'] = 'Altersdurchschnitt';
$age_statistics_terrafrost['Youngest_Member'] = 'J�ngstes Mitglied';
$age_statistics_terrafrost['Youngest_Age'] = 'J�ngstes Alter';
$age_statistics_terrafrost['Oldest_Member'] = '�ltestes Mitglied';
$age_statistics_terrafrost['Oldest_Age'] = '�ltestes Alter';
$age_statistics_terrafrost['Error_message'] = '<a href="http://www.phpbb.com/phpBB/viewtopic.php?t=342028">Birthdays mod from terrafrost</a> ist nicht gefunden worden, diesen Umb. anbringen, bevor man dieses Modul verwendete';
// [/age_statistics_terrafrost]

// [Activity_Arcade_Mod_Stats]
$Activity_Arcade_Mod_Stats = array();

$Activity_Arcade_Mod_Stats['module_name']        = 'Arcade Activity Stats';
$Activity_Arcade_Mod_Stats['best_player']	    = 'Best Player at ';
$Activity_Arcade_Mod_Stats['fav_game']		    = 'Liebling Spiel an ';
$Activity_Arcade_Mod_Stats['total_games']	    = 'Gesamtspiele Avaliable:';
$Activity_Arcade_Mod_Stats['total_games_played'] = 'Gesamtspiele Gespielt:';
$Activity_Arcade_Mod_Stats['total_comments']	    = 'Gesamtmenge Kommentiert Nach links:';
$Activity_Arcade_Mod_Stats['total_ratings']	    = 'Gesamtbewertungen:';
$Activity_Arcade_Mod_Stats['total_highscores']   = 'Gesamtmonatshighscores:';
$Activity_Arcade_Mod_Stats['games']			    = 'Gespielte Zeiten %t%.';
$Activity_Arcade_Mod_Stats['first_places']	    = 'Mit %t% 1. Pl�tzen.';
$Activity_Arcade_Mod_Stats['nobody']             = 'Niemand Schon';
$Activity_Arcade_Mod_Stats['no_game']            = 'Keine Spiele spielten schon';
// [/Activity_Arcade_Mod_Stats]

// [Arcade_All_Time_High_Score_Players]
$Arcade_All_Time_High_Score_Players = array();

$Arcade_All_Time_High_Score_Players['module_name'] 	= 'Activity Mod [Top '.$core->return_limit.' All Time High Score Players]';
$Arcade_All_Time_High_Score_Players['name'] = 'Benutzername';
$Arcade_All_Time_High_Score_Players['all_time'] = 'Alle Zeit Highscores';
// [/Arcade_All_Time_High_Score_Players]

// [Activity_Arcade_Mod_Top_xx_Players]
$Activity_Arcade_Mod_Top_xx_Players = array();

$Activity_Arcade_Mod_Top_xx_Players['module_name'] 	= 'Activity Mod [Top '.$core->return_limit.' Players]';
$Activity_Arcade_Mod_Top_xx_Players['name'] = 'Benutzername';
$Activity_Arcade_Mod_Top_xx_Players['currenthigh'] = 'Current Highscores';
$Activity_Arcade_Mod_Top_xx_Players['all_time'] = 'Alle Zeit Highscores';
// [/Activity_Arcade_Mod_Top_xx_Players]

// [Activity_Arcade_Mod_Top_xx_Games]
$Activity_Arcade_Mod_Top_xx_Games = array();

$Activity_Arcade_Mod_Top_xx_Games['module_name'] 	= 'T�tigkeit Umb. [Oberseite '.$core->return_limit.' Spiele]';
$Activity_Arcade_Mod_Top_xx_Games['plays'] = 'Gespielt';
$Activity_Arcade_Mod_Top_xx_Games['game'] = 'Spiel';
// [/Activity_Arcade_Mod_Top_xx_Games]

// [Activity_Arcade_Mod_xx_Newest_Games]
$Activity_Arcade_Mod_xx_Newest_Games = array();

$Activity_Arcade_Mod_xx_Newest_Games['module_name'] = 'T�tigkeit Umb. ['.$core->return_limit.' Neueste Addierte Spiele]';
$Activity_Arcade_Mod_xx_Newest_Games['plays'] = 'Gespielt';
$Activity_Arcade_Mod_xx_Newest_Games['game'] = 'Spiel';
$Activity_Arcade_Mod_xx_Newest_Games['date'] = 'Installed On';
// [/Activity_Arcade_Mod_xx_Newest_Games]

// [Activity_Arcade_Mod_Top_10_Users_Game_Plays]
$Activity_Arcade_Mod_Top_10_Users_Game_Plays = array();

$Activity_Arcade_Mod_Top_10_Users_Game_Plays['module_name'] 	= 'T�tigkeit Umb. [ Oberseite '.$core->return_limit.' Benutzer (Spiel-Spiele)';
$Activity_Arcade_Mod_Top_10_Users_Game_Plays['played'] = 'Games Played';
// [/Activity_Arcade_Mod_Top_10_Users_Game_Plays]

// [last_created_album_posts]
$last_created_album_posts = array();

$last_created_album_posts['module_name'] = 'Letzte Album-Pfosten';
$last_created_album_posts['start_date'] = 'Bekanntgegeben';
$last_created_album_posts['posted_in'] = 'Im Thema';
$last_created_album_posts['post'] = 'ID / Titel';
$last_created_album_posts['br'] = '<br />';
// [/last_created_album_posts]

// [Top_album_posters_ever]
$Top_album_posters_ever = array();

$Top_album_posters_ever['module_name'] = 'Obere Albumplakate';
// [/Top_album_posters_ever]

// [Top_album_posters_this_month]
$Top_album_posters_this_month = array();

$Top_album_posters_this_month['module_name'] = 'Obere Albumplakate dieser Monat';
// [/Top_album_posters_this_month]

// [Top_album_posters_this_week]
$Top_album_posters_this_week = array();

$Top_album_posters_this_week['module_name'] = 'Obere Albumplakate diese Woche';
// [/Top_album_posters_this_week]

// [most_viewed_photos_ever]
$most_viewed_photos_ever = array();

$most_viewed_photos_ever['module_name'] = 'Die Meisten Gesehenen Fotos';
$most_viewed_photos_ever['photo'] = 'Foto';
$most_viewed_photos_ever['views'] = 'Ansichten';
$most_viewed_photos_ever['start_date'] = 'Bekanntgegeben';
$most_viewed_photos_ever['desc'] = 'Im Thema';
$most_viewed_photos_ever['br'] = '<br />';
// [/most_viewed_photos_ever]

// [most_viewed_photos_this_month]
$most_viewed_photos_this_month = array();

$most_viewed_photos_this_month['module_name'] = 'Die meisten gesehenen Fotos dieser Monat';
$most_viewed_photos_this_month['photo'] = 'Foto';
$most_viewed_photos_this_month['views'] = 'Ansichten';
$most_viewed_photos_this_month['start_date'] = 'Bekanntgegeben';
$most_viewed_photos_this_month['desc'] = 'Im Thema';
$most_viewed_photos_this_month['br'] = '<br />';
// [/most_viewed_photos_this_month]

// [most_viewed_photos_this_week]
$most_viewed_photos_this_week = array();

$most_viewed_photos_this_week['module_name'] = 'Die meisten gesehenen Fotos diese Woche';
$most_viewed_photos_this_week['photo'] = 'Foto';
$most_viewed_photos_this_week['views'] = 'Ansichten';
$most_viewed_photos_this_week['start_date'] = 'Bekanntgegeben';
$most_viewed_photos_this_week['desc'] = 'Im Thema';
$most_viewed_photos_this_week['br'] = '<br />';
// [/most_viewed_photos_this_week]

// [top_countries_ipcountryflags]
$top_countries_ipcountryflags = array();

$top_countries_ipcountryflags['module_name'] = 'Obere L�nder';
$top_countries_ipcountryflags['country'] = 'Land';
$top_countries_ipcountryflags['flag'] = 'Markierungsfahne';
$top_countries_ipcountryflags['users'] = 'Benutzer';
$top_countries_ipcountryflags['Error_message'] = 'IpCountryFlags <a href="http://www.you3d.net/ip2/index.php" target="_blank">http://www.you3d.net/ip2/index.php</a>) ist nicht gefunden worden, es bitte anbringt, bevor man dieses Modul verwendete';
// [/top_countries_ipcountryflags]

// [Activity_plus_stats]
$Activity_plus_stats = array();

$Activity_plus_stats['module_name']        = 'Activity Mod Plus';
$Activity_plus_stats['best_player']	    = 'Best Player at ';
$Activity_plus_stats['fav_game']		    = 'Liebling Spiel an ';
$Activity_plus_stats['total_games']	    = 'Gesamtspiele Avaliable:';
$Activity_plus_stats['total_comments']	    = 'Gesamtmenge Kommentar:';
$Activity_plus_stats['total_bets']	    = 'Gesamtwetten Gebildet:';
$Activity_plus_stats['total_challenges']   = 'Gesamtherausforderungen Gesendet:';
$Activity_plus_stats['played']			    = 'Gespielte Zeiten %t%.';
$Activity_plus_stats['trophies']	    = 'mit %t% Troph�en.';
$Activity_plus_stats['nobody'] = 'Nobody';
$Activity_plus_stats['no_game'] = 'No games';
// [/Activity_plus_stats]

// [Activity_Plus_Mod_xx_Newest_Games]
$Activity_Plus_Mod_xx_Newest_Games = array();

$Activity_Plus_Mod_xx_Newest_Games['module_name'] = 'T�tigkeit Plus Umb. ['.$core->return_limit.' Neueste Addierte Spiele]';
$Activity_Plus_Mod_xx_Newest_Games['plays'] = 'Gespielt';
$Activity_Plus_Mod_xx_Newest_Games['game'] = 'Spiel';
$Activity_Plus_Mod_xx_Newest_Games['date'] = 'Installed On';
// [/Activity_Plus_Mod_xx_Newest_Games]

// [Activity_Plus_Mod_Top_xx_Trophies]
$Activity_Plus_Mod_Top_xx_Trophies = array();

$Activity_Plus_Mod_Top_xx_Trophies['module_name'] 	= 'T�tigkeit Plus Umb. [Oberseite '.$core->return_limit.' Trophies]';
$Activity_Plus_Mod_Top_xx_Trophies['name'] = 'Name';
$Activity_Plus_Mod_Top_xx_Trophies['Trophies'] = 'Trophies';
// [/Activity_Plus_Mod_Top_xx_Trophies]

// [Activity_Plus_Mod_Top_xx_Players]
$Activity_Plus_Mod_Top_xx_Players = array();

$Activity_Plus_Mod_Top_xx_Players['module_name'] 	= 'T�tigkeit Plus Umb. [Oberseite '.$core->return_limit.' Gespielte Spiele]';
$Activity_Plus_Mod_Top_xx_Players['plays'] = 'Gespielt';
$Activity_Plus_Mod_Top_xx_Players['name'] = 'Name';
// [/Activity_Plus_Mod_Top_xx_Players]

// [Activity_plus_Mod_Top_xx_Games]
$Activity_plus_Mod_Top_xx_Games = array();

$Activity_plus_Mod_Top_xx_Games['module_name'] 	= 'T�tigkeit Plus Umb. [Oberseite '.$core->return_limit.' Spiele]';
$Activity_plus_Mod_Top_xx_Games['plays'] = 'Gespielt';
$Activity_plus_Mod_Top_xx_Games['game'] = 'Spiel';
// [/Activity_plus_Mod_Top_xx_Games]

?>