<?php

// [stats_overview]
$stats_overview = array();

$stats_overview['module_name'] = 'Resumen de estad�sticas';

// Admin Specific
$stats_overview['num_columns_title'] = 'Establece el n�mero de columnas';
$stats_overview['num_columns_explain'] = 'Aqui usted es capaz de establecer el n�mero de columnas a ser mostradas';

// [/stats_overview]

// [admin_statistics]
$admin_statistics = array();

// Administrative Statistics
$admin_statistics['module_name'] = 'Estad�sticas administrativas';

$admin_statistics['Board_Up_Days'] = 'D�as del foro en l�nea';
$admin_statistics['Latest_Reg_User'] = '�ltimo usuario registrado';
$admin_statistics['Latest_Reg_User_Date'] = 'Fecha de registro del �ltimo usuario';
$admin_statistics['Most_Ever_Online'] = 'Mayor catidad de usuarios conectados';
$admin_statistics['Most_Ever_Online_Date'] = 'Fecha de mayor catidad de usuarios conectados';
$admin_statistics['Disk_usage'] = 'Uso del disco';

// [/admin_statistics]

// [most_active_forums]
$most_active_forums = array();

$most_active_forums['module_name'] = 'La mayor�a De los Foros Activos';
// [/most_active_forums]

// [most_active_topics]
$most_active_topics = array();

$most_active_topics['module_name'] = 'La mayor�a De los Asuntos Activos';
// [/most_active_topics]

// [most_viewed_topics]
$most_viewed_topics = array();

$most_viewed_topics['module_name'] = 'La mayor�a De los Asuntos Vistos';
// [/most_viewed_topics]

// [most_active_topicstarter]
$most_active_topicstarter = array();

$most_active_topicstarter['module_name'] = 'La mayor�a Del Topicstarter Activo';
// [/most_active_topicstarter]

// [most_interesting_topics]
$most_interesting_topics = array();

$most_interesting_topics['module_name'] = 'La mayor�a De los Asuntos Interesantes';
$most_interesting_topics['Rate'] = 'Tarifa (opini�nes/mensajes)';
// [/most_interesting_topics]

// [least_interesting_topics]
$least_interesting_topics = array();

$least_interesting_topics['module_name'] = 'Menos Asuntos Interesantes';
$least_interesting_topics['Rate'] = 'Tarifa (opini�nes/mensajes)';
// [/least_interesting_topics]

// [most_least_interesting_topics]
$most_least_interesting_topics = array();

$most_least_interesting_topics['module_name'] = 'La mayor�a De los Asuntos Interesantes&nbsp;-&nbsp;Menos Asuntos Interesantes';
$most_least_interesting_topics['Rate'] = 'Tarifa (opini�nes/mensajes)';
// [/most_least_interesting_topics]

// [top_posters]
$top_posters = array();

$top_posters['module_name'] = 'Carteles Superiores';
// [/top_posters]

// [fastest_users]
$fastest_users = array();

$fastest_users['module_name'] = 'Los Usuarios M�s r�pidos';
$fastest_users['days_on_forum'] = 'D�as En Foro';
$fastest_users['messperday'] = 'Mensajes Por D�a';
// [/fastest_users]

// [last_online]
$last_online = array();

$last_online['module_name'] = 'En l�nea Pasado';
$last_online['name'] = 'Username';
$last_online['last_online'] = 'En l�nea Pasado';
$last_online['Error_message'] = 'Last Visit Mod (<a href="http://www.phpbbhacks.com/download/237">http://www.phpbbhacks.com/download/237</a>) no se ha encontrado, lo instalan por favor antes de usar este m�dulo';
// [/last_online]

// [top_posters_month]
$top_posters_month = array();

$top_posters_month['module_name'] = 'Usuarios de fijaci�n superiores este mes';
// [/top_posters_month]

// [users_by_month]
$users_by_month = array();

$users_by_month['module_name'] = 'N�mero de nuevos usuarios por mes';
$users_by_month['Year'] = 'Year';
$users_by_month['Month_jan'] = 'Jan';
$users_by_month['Month_feb'] = 'Feb';
$users_by_month['Month_mar'] = 'Mar';
$users_by_month['Month_apr'] = 'Apr';
$users_by_month['Month_may'] = 'May';
$users_by_month['Month_jun'] = 'Jun';
$users_by_month['Month_jul'] = 'Jul';
$users_by_month['Month_aug'] = 'Aug';
$users_by_month['Month_sep'] = 'Sep';
$users_by_month['Month_oct'] = 'Oct';
$users_by_month['Month_nov'] = 'Nov';
$users_by_month['Month_dec'] = 'Dec';
// [/users_by_month]

// [top_posters_week]
$top_posters_week = array();

$top_posters_week['module_name'] = 'Usuarios de fijaci�n superiores esta semana';
// [/top_posters_week]

// [most_active_polls]
$most_active_polls = array();

$most_active_polls['module_name'] = 'La mayor�a De las Encuestas Activas';
// [/most_active_polls]

// [most_active_pollstarter]
$most_active_pollstarter = array();

$most_active_pollstarter['module_name'] = 'La mayor�a Del Arrancador Activo De la Encuesta';
$most_active_pollstarter['polls'] = 'Encuestas';
// [/most_active_pollstarter]

// [last_created_posts]
$last_created_posts = array();

$last_created_posts['module_name'] = 'Postes Pasados';
$last_created_posts['start_date'] = 'Fijado';
$last_created_posts['posted_in'] = 'En Tema';
$last_created_posts['post'] = 'Identificaci�n del mensaje/tema';
$last_created_posts['hekje'] = '#';
$last_created_posts['by'] = 'Por';
// [/last_created_posts]

// [last_created_topics]
$last_created_topics = array();

$last_created_topics['module_name'] = 'Asuntos Pasados';
$last_created_topics['start_date'] = 'Comenzado';
// [/last_created_topics]

// [posts_by_month]
$posts_by_month = array();

$posts_by_month['module_name'] = 'N�mero de nuevos postes por mes';
$posts_by_month['Year'] = 'Year';
$posts_by_month['Month_jan'] = 'Jan';
$posts_by_month['Month_feb'] = 'Feb';
$posts_by_month['Month_mar'] = 'Mar';
$posts_by_month['Month_apr'] = 'Apr';
$posts_by_month['Month_may'] = 'May';
$posts_by_month['Month_jun'] = 'Jun';
$posts_by_month['Month_jul'] = 'Jul';
$posts_by_month['Month_aug'] = 'Aug';
$posts_by_month['Month_sep'] = 'Sep';
$posts_by_month['Month_oct'] = 'Oct';
$posts_by_month['Month_nov'] = 'Nov';
$posts_by_month['Month_dec'] = 'Dec';
// [/posts_by_month]

// [topics_by_month]
$topics_by_month = array();

$topics_by_month['module_name'] = 'N�mero de nuevos asuntos por mes';
$topics_by_month['Year'] = 'Year';
$topics_by_month['Month_jan'] = 'Jan';
$topics_by_month['Month_feb'] = 'Feb';
$topics_by_month['Month_mar'] = 'Mar';
$topics_by_month['Month_apr'] = 'Apr';
$topics_by_month['Month_may'] = 'May';
$topics_by_month['Month_jun'] = 'Jun';
$topics_by_month['Month_jul'] = 'Jul';
$topics_by_month['Month_aug'] = 'Aug';
$topics_by_month['Month_sep'] = 'Sep';
$topics_by_month['Month_oct'] = 'Oct';
$topics_by_month['Month_nov'] = 'Nov';
$topics_by_month['Month_dec'] = 'Dec';
// [/topics_by_month]

// [top_attachments]
$top_attachments = array();

$top_attachments['module_name'] = 'Accesorios Descargados Tapa';
$top_attachments['Filename'] = 'Nombre de fichero';
$top_attachments['Filecomment'] = 'Comentario Del Archivo';
$top_attachments['Size'] = 'Filesize';
$top_attachments['Downloads'] = 'Transferencias directas';
$top_attachments['Posttime'] = 'Tiempo Del Poste';
$top_attachments['Posted_in_topic'] = 'Fijado en asunto';
// Admin Variables
$top_attachments['exclude_images_title'] = 'Excluya Las Im�genes';
$top_attachments['exclude_images_explain'] = 'Si se permite este ajuste, las im�genes no se consideran dentro de la estad�stica superior de los accesorios.';
// [/top_attachments]

// [top_avatars]
$top_avatars = array();

$top_avatars['module_name'] = 'Avatars Gallary Superiores';
$top_avatars['Avatar_image'] = 'Imagen Del Avatar';
$top_avatars['Users'] = 'Usuarios';
// [/top_avatars]

// [top_smilies]
$top_smilies = array();

$top_smilies['module_name'] = 'Smilies Superior';
$top_smilies['Smilie_image'] = 'Imagen Del Smiley';
$top_smilies['Smilie_code'] = 'C�digo Del Smiley';
// [/top_smilies]

// [top_shoutbox_poster]
$top_shoutbox_poster = array();

$top_shoutbox_poster['module_name'] = 'Carteles Superiores De Shoutbox';
$top_shoutbox_poster['Shouts'] = 'Gritos';
$top_shoutbox_poster['Error_message'] = 'La MOD completamente integrada de Shoutbox tiene que ser instalada para considerar los carteles superiores de Shoutbox.<br>Instale la MOD (<a href="http://www.phpbbhacks.com/download/1255" target="_blank">http://www.phpbbhacks.com/download/1255</a>) o desactive este m�dulo.';
// [/top_shoutbox_poster]

// [top_words]
$top_words = array();

$top_words['module_name'] = 'La mayor�a De las Palabras Usadas';
$top_words['Word'] = 'Palabra';
$top_words['Count'] = 'Cuenta';
// [/top_words]

// [most_ignored_users]
$most_ignored_users = array();

$most_ignored_users['module_name'] = 'La mayor�a No hicieron caso De Usuarios';
$most_ignored_users['ignored_by'] = ' usuarios no hechos caso cerca ';
$most_ignored_users['other'] = ' otros usuarios';
$most_ignored_users['Ignores'] = 'No hecho caso por X Users';
$most_ignored_users['Error_message'] = 'La MOD de los usuarios del no hacer caso tiene que ser instalada para considerar a los usuarios no hechos caso.<br>Instale la MOD (<a href="http://www.phpbbhacks.com/download/789" target="_blank">http://www.phpbbhacks.com/download/789</a>) o desactive este m�dulo.';
// [/most_ignored_users]

// [private_messages]
$private_messages = array();

$private_messages['module_name'] = 'Mensajes Privados';
$private_messages['lastmonth'] = '30 d�as';
$private_messages['24hours'] = '24 horas';
$private_messages['7days'] = 'Semana';
$private_messages['send'] = 'Env�e';
$private_messages['read'] = 'Le�do';
$private_messages['new'] = 'Nuevo';
$private_messages['saved_in'] = 'Inbox Ahorrado';
$private_messages['saved_out'] = 'Outbox Ahorrado';
$private_messages['unread'] = 'Unread';
// [/private_messages]

// [top_styles]
$top_styles = array();

$top_styles['module_name'] = 'Estilos Superiores';
// [/top_styles]

// [top_languages]
$top_languages = array();

$top_languages['module_name'] = 'Idiomas Superiores';
$top_languages['langname'] = 'Lengua';
// [/top_languages]

// [most_logged_on_users]
$most_logged_on_users = array();

$most_logged_on_users['module_name'] = 'La mayor�a De los Usuarios Entrados';
$most_logged_on_users['totalforumtime'] = 'Boardtime';
$most_logged_on_users['totallogins'] = 'x registros';
$most_logged_on_users['daysmember'] = 'Colocado';
$most_logged_on_users['visitedpages'] = 'Golpes De la P�gina';
$most_logged_on_users['Error_message'] = 'La MOD pasada de la visita tiene que ser instalada para considerar a los usuarios entrados.<br>Instale la MOD (<a href="http://www.phpbbhacks.com/download/237" target="_blank">http://www.phpbbhacks.com/download/237</a>) o desactive este m�dulo.';
// [/most_logged_on_users]

// [Cash]
$Cash = array();

$Cash['module_name'] = 'Efectivo';
$Cash['username'] = 'Usuario';
$Cash['Error_message'] = 'La MOD del efectivo (Cash Mod) no se ha encontrado, lo instala por favor antes de usar este m�dulo';
// [/Cash]

// [genders]
$genders = array();

// Administrative Statistics
$genders['module_name'] = 'Usuarios masculinos y femeninos';
$genders['Gender'] = 'G�nero';
$genders['undefined'] = 'Indefinido';
$genders['male'] = '<img src="' . $images['icon_minigender_male'] . '"> Var�n';
$genders['female'] = '<img src="' . $images['icon_minigender_female'] . '"> Hembra';
$genders['total'] = 'Total';
$genders['Error_message'] = 'Gender Mod (from Niels <a href="http://phpbbhacks.com/download/182" target="_blank">http://phpbbhacks.com/download/182</a>) no se ha encontrado, lo instalan por favor antes de usar este m�dulo';
// [/genders]

// [posts_by_gender_niels]
$posts_by_gender_niels = array();

// Administrative Statistics
$posts_by_gender_niels['module_name'] = 'Postes de Gender';
$posts_by_gender_niels['Gender'] = 'G�nero';
$posts_by_gender_niels['undefined'] = 'Indefinido';
$posts_by_gender_niels['male'] = '<img src="' . $images['icon_minigender_male'] . '"> Var�n';
$posts_by_gender_niels['female'] = '<img src="' . $images['icon_minigender_female'] . '"> Hembra';
$posts_by_gender_niels['Error_message'] = 'Gender Mod (from Niels <a href="http://phpbbhacks.com/download/182" target="_blank">http://phpbbhacks.com/download/182</a>) no se ha encontrado, lo instalan por favor antes de usar este m�dulo';
// [/posts_by_gender_niels]

// [genders_evil]
$genders_evil = array();

// Genders
$genders_evil['module_name'] = 'Usuarios masculinos y femeninos';
$genders_evil['Gender'] = 'G�nero';
$genders_evil['undefined'] = 'Indefinido';
$genders_evil['male'] = '<img src="' . $images['gender_m'] . '"> Var�n';
$genders_evil['female'] = '<img src="' . $images['gender_f'] . '"> Hembra';
$genders_evil['total'] = 'Total';
$genders_evil['Error_message'] = 'Gender Mod (from Evil <a href="http://www.phpbb.com/phpBB/viewtopic.php?t=467168" target="_blank">http://www.phpbb.com/phpBB/viewtopic.php?t=467168</a>) no se ha encontrado, lo instalan por favor antes de usar este m�dulo';
// [/genders_evil]

// [posts_by_gender_evil]
$posts_by_gender_evil = array();

// Administrative Statistics
$posts_by_gender_evil['module_name'] = 'Postes de Gender';
$posts_by_gender_evil['Gender'] = 'G�nero';
$posts_by_gender_evil['undefined'] = 'Indefinido';
$posts_by_gender_evil['male'] = '<img src="' . $images['gender_m'] . '"> Var�n';
$posts_by_gender_evil['female'] = '<img src="' . $images['gender_f'] . '"> Hembra';
$posts_by_gender_evil['Error_message'] = 'Gender Mod (from Evil <a href="http://www.phpbb.com/phpBB/viewtopic.php?t=467168" target="_blank">http://www.phpbb.com/phpBB/viewtopic.php?t=467168</a>) no se ha encontrado, lo instalan por favor antes de usar este m�dulo';
// [/posts_by_gender_evil]

// [age_statistics]
$age_statistics = array();

$age_statistics['module_name'] = 'Age Statistics';
$age_statistics['Users_Age'] = 'N�mero de miembros con edad';
$age_statistics['Average_Age'] = 'Edad Media';
$age_statistics['Youngest_Member'] = 'El Miembro M�s joven';
$age_statistics['Youngest_Age'] = 'La Edad M�s joven';
$age_statistics['Oldest_Member'] = 'El M�s viejo Miembro';
$age_statistics['Oldest_Age'] = 'La M�s vieja Edad';
$age_statistics['Error_message'] = '<a href="http://www.phpbbhacks.com/viewhack.php?id=187">Birthday Hack</a> no se ha encontrado, no instalan esa MOD antes de usar este m�dulo';
// [/age_statistics]

// [age_statistics_terrafrost]
$age_statistics_terrafrost = array();

$age_statistics_terrafrost['module_name'] = 'Age Statistics';
$age_statistics_terrafrost['Users_Age'] = 'N�mero de miembros con edad';
$age_statistics_terrafrost['Average_Age'] = 'Edad Media';
$age_statistics_terrafrost['Youngest_Member'] = 'El Miembro M�s joven';
$age_statistics_terrafrost['Youngest_Age'] = 'La Edad M�s joven';
$age_statistics_terrafrost['Oldest_Member'] = 'El M�s viejo Miembro';
$age_statistics_terrafrost['Oldest_Age'] = 'La M�s vieja Edad';
$age_statistics_terrafrost['Error_message'] = '<a href="http://www.phpbb.com/phpBB/viewtopic.php?t=342028">Birthdays mod from terrafrost</a> no se ha encontrado, no instalan esa MOD antes de usar este m�dulo';
// [/age_statistics_terrafrost]

// [Activity_Arcade_Mod_Stats]
$Activity_Arcade_Mod_Stats = array();

$Activity_Arcade_Mod_Stats['module_name']        = 'Arcade Activity Stats';
$Activity_Arcade_Mod_Stats['best_player']	    = 'Best Player at ';
$Activity_Arcade_Mod_Stats['fav_game']		    = 'Juego del favorito en ';
$Activity_Arcade_Mod_Stats['total_games']	    = 'Juegos Totales Avaliable:';
$Activity_Arcade_Mod_Stats['total_games_played'] = 'Juegos Totales Jugados:';
$Activity_Arcade_Mod_Stats['total_comments']	    = 'El Total Comenta A la izquierda:';
$Activity_Arcade_Mod_Stats['total_ratings']	    = 'Grados Totales:';
$Activity_Arcade_Mod_Stats['total_highscores']   = 'Highscores Mensual Total:';
$Activity_Arcade_Mod_Stats['games']			    = '�pocas Jugadas %t%.';
$Activity_Arcade_Mod_Stats['first_places']	    = 'Con Los 1ros Lugares %t%.';
$Activity_Arcade_Mod_Stats['nobody']             = 'Nadie Todav�a';
$Activity_Arcade_Mod_Stats['no_game']            = 'Ningunos juegos jugados todav�a';
// [/Activity_Arcade_Mod_Stats]

// [Arcade_All_Time_High_Score_Players]
$Arcade_All_Time_High_Score_Players = array();

$Arcade_All_Time_High_Score_Players['module_name'] 	= 'Activity Mod [Top '.$core->return_limit.' All Time High Score Players]';
$Arcade_All_Time_High_Score_Players['name'] = 'Username';
$Arcade_All_Time_High_Score_Players['all_time'] = 'Toda la Hora Highscores';
// [/Arcade_All_Time_High_Score_Players]

// [Activity_Arcade_Mod_Top_xx_Players]
$Activity_Arcade_Mod_Top_xx_Players = array();

$Activity_Arcade_Mod_Top_xx_Players['module_name'] 	= 'Activity Mod [Top '.$core->return_limit.' Players]';
$Activity_Arcade_Mod_Top_xx_Players['name'] = 'Username';
$Activity_Arcade_Mod_Top_xx_Players['currenthigh'] = 'Current Highscores';
$Activity_Arcade_Mod_Top_xx_Players['all_time'] = 'Toda la Hora Highscores';
// [/Activity_Arcade_Mod_Top_xx_Players]

// [Activity_Arcade_Mod_Top_xx_Games]
$Activity_Arcade_Mod_Top_xx_Games = array();

$Activity_Arcade_Mod_Top_xx_Games['module_name'] 	= 'MOD De la Actividad [Juegos De la Tapa '.$core->return_limit.']';
$Activity_Arcade_Mod_Top_xx_Games['plays'] = 'Jugado';
$Activity_Arcade_Mod_Top_xx_Games['game'] = 'Juego';
// [/Activity_Arcade_Mod_Top_xx_Games]

// [Activity_Arcade_Mod_xx_Newest_Games]
$Activity_Arcade_Mod_xx_Newest_Games = array();

$Activity_Arcade_Mod_xx_Newest_Games['module_name'] = 'MOD De la Actividad ['.$core->return_limit.' Juegos Agregados M�s recientes]';
$Activity_Arcade_Mod_xx_Newest_Games['plays'] = 'Jugado';
$Activity_Arcade_Mod_xx_Newest_Games['game'] = 'Juego';
$Activity_Arcade_Mod_xx_Newest_Games['date'] = 'Installed On';
// [/Activity_Arcade_Mod_xx_Newest_Games]

// [Activity_Arcade_Mod_Top_10_Users_Game_Plays]
$Activity_Arcade_Mod_Top_10_Users_Game_Plays = array();

$Activity_Arcade_Mod_Top_10_Users_Game_Plays['module_name'] 	= 'MOD De la Actividad [ Usuarios De la Tapa '.$core->return_limit.' (Juegos Del Juego) ';
$Activity_Arcade_Mod_Top_10_Users_Game_Plays['played'] = 'Games Played';
// [/Activity_Arcade_Mod_Top_10_Users_Game_Plays]

// [last_created_album_posts]
$last_created_album_posts = array();

$last_created_album_posts['module_name'] = 'Postes Pasados Del �lbum';
$last_created_album_posts['start_date'] = 'Fijado';
$last_created_album_posts['posted_in'] = 'Descripci�n';
$last_created_album_posts['post'] = 'ID / T�tulo';
$last_created_album_posts['br'] = '<br />';
// [/last_created_album_posts]

// [Top_album_posters_ever]
$Top_album_posters_ever = array();

$Top_album_posters_ever['module_name'] = 'Carteles superiores del �lbum';
// [/Top_album_posters_ever]

// [Top_album_posters_this_month]
$Top_album_posters_this_month = array();

$Top_album_posters_this_month['module_name'] = 'Carteles superiores del �lbum este mes';
// [/Top_album_posters_this_month]

// [Top_album_posters_this_week]
$Top_album_posters_this_week = array();

$Top_album_posters_this_week['module_name'] = 'Carteles superiores del �lbum esta semana';
// [/Top_album_posters_this_week]

// [most_viewed_photos_ever]
$most_viewed_photos_ever = array();

$most_viewed_photos_ever['module_name'] = 'La mayor�a De las Fotos Vistas Siempre';
$most_viewed_photos_ever['photo'] = 'Foto';
$most_viewed_photos_ever['views'] = 'Opini�nes';
$most_viewed_photos_ever['start_date'] = 'Fijado';
$most_viewed_photos_ever['desc'] = 'Descripci�n';
$most_viewed_photos_ever['br'] = '<br />';
// [/most_viewed_photos_ever]

// [most_viewed_photos_this_month]
$most_viewed_photos_this_month = array();

$most_viewed_photos_this_month['module_name'] = 'La mayor�a de las fotos vistas este mes';
$most_viewed_photos_this_month['photo'] = 'Foto';
$most_viewed_photos_this_month['views'] = 'Opini�nes';
$most_viewed_photos_this_month['start_date'] = 'Fijado';
$most_viewed_photos_this_month['desc'] = 'Descripci�n';
$most_viewed_photos_this_month['br'] = '<br />';
// [/most_viewed_photos_this_month]

// [most_viewed_photos_this_week]
$most_viewed_photos_this_week = array();

$most_viewed_photos_this_week['module_name'] = 'La mayor�a de las fotos vistas esta semana';
$most_viewed_photos_this_week['photo'] = 'Foto';
$most_viewed_photos_this_week['views'] = 'Opini�nes';
$most_viewed_photos_this_week['start_date'] = 'Fijado';
$most_viewed_photos_this_week['desc'] = 'Descripci�n';
$most_viewed_photos_this_week['br'] = '<br />';
// [/most_viewed_photos_this_week]

// [top_countries_ipcountryflags]
$top_countries_ipcountryflags = array();

$top_countries_ipcountryflags['module_name'] = 'Pa�ses Superiores';
$top_countries_ipcountryflags['country'] = 'Pa�s';
$top_countries_ipcountryflags['flag'] = 'Bandera';
$top_countries_ipcountryflags['users'] = 'Usuarios';
$top_countries_ipcountryflags['Error_message'] = 'IpCountryFlags (<a href="http://www.you3d.net/ip2/index.php" target="_blank">http://www.you3d.net/ip2/index.php</a>) no se ha encontrado, lo instalan por favor antes de usar este m�dulo';
// [/top_countries_ipcountryflags]

// [Activity_plus_Mod_Top_xx_Games]
$Activity_plus_Mod_Top_xx_Games = array();

$Activity_plus_Mod_Top_xx_Games['module_name'] 	= 'MOD De la Actividad Plus [Juegos De la Tapa '.$core->return_limit.']';
$Activity_plus_Mod_Top_xx_Games['plays'] = 'Jugado';
$Activity_plus_Mod_Top_xx_Games['game'] = 'Juego';
// [/Activity_plus_Mod_Top_xx_Games]

?>