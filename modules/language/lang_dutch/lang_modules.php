<?php

// [stats_overview]
$stats_overview = array();

$stats_overview['module_name'] = 'Statistics Overview';

// Admin Specific
$stats_overview['num_columns_title'] = 'Bepaal het aantal kolommen';
$stats_overview['num_columns_explain'] = 'Hier kun je het aantal kolommen bepalen';

// [/stats_overview]

// [admin_statistics]
$admin_statistics = array();

// Administrative Statistics
$admin_statistics['module_name'] = 'Administratieve Statistieken';

$admin_statistics['Board_Up_Days'] = 'Board Up Dagen';
$admin_statistics['Latest_Reg_User'] = 'Laatst Geregistreerde Gebruiker';
$admin_statistics['Latest_Reg_User_Date'] = 'Datum Laatst Geregistreerde Gebruiker';
$admin_statistics['Most_Ever_Online'] = 'Meeste gebruikers ooit Online';
$admin_statistics['Most_Ever_Online_Date'] = 'Datum Meeste gebruikers ooit Online';
$admin_statistics['Disk_usage'] = 'Schijf Gebruik';

// [/admin_statistics]

// [most_active_forums]
$most_active_forums = array();

$most_active_forums['module_name'] = 'Meest aktieve Forums';
// [/most_active_forums]

// [most_active_topics]
$most_active_topics = array();

$most_active_topics['module_name'] = 'Meest aktieve Onderwerpen';
// [/most_active_topics]

// [most_viewed_topics]
$most_viewed_topics = array();

$most_viewed_topics['module_name'] = 'Meest bekeken Onderwerpen';
// [/most_viewed_topics]

// [most_active_topicstarter]
$most_active_topicstarter = array();

$most_active_topicstarter['module_name'] = 'Meest aktieve Onderwerpstarter';
// [/most_active_topicstarter]

// [most_interesting_topics]
$most_interesting_topics = array();

$most_interesting_topics['module_name'] = 'Meest Interessante Onderwerpen';
$most_interesting_topics['Rate'] = 'Rating (bekeken/berichten)';
// [/most_interesting_topics]

// [least_interesting_topics]
$least_interesting_topics = array();

$least_interesting_topics['module_name'] = 'Minst interessante onderwerpen';
$least_interesting_topics['Rate'] = 'Rating (bekeken/berichten)';
// [/least_interesting_topics]

// [most_least_interesting_topics]
$most_least_interesting_topics = array();

$most_least_interesting_topics['module_name'] = 'Meest Interessante Onderwerpen&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Minst interessante onderwerpen';
$most_least_interesting_topics['Rate'] = 'Rating (bekeken/berichten)';
// [/most_least_interesting_topics]

// [top_posters]
$top_posters = array();

$top_posters['module_name'] = 'Top Plaatsers';
// [/top_posters]

// [fastest_users]
$fastest_users = array();

$fastest_users['module_name'] = 'Snelste Gebruikers';
$fastest_users['days_on_forum'] = 'Dagen op het Forum';
$fastest_users['messperday'] = 'Berichten per dag';
// [/fastest_users]

// [last_online]
$last_online = array();

$last_online['module_name'] = 'Laatst Online';
$last_online['name'] = 'Gebruikersnaam';
$last_online['last_online'] = 'Laatst Online';
$last_online['Error_message'] = 'Last Visit Mod (<a href="http://www.phpbbhacks.com/download/237">http://www.phpbbhacks.com/download/237</a>) is niet gevonden, installeer die eerst voor je deze module gebruikt.';
// [/last_online]

// [top_posters_month]
$top_posters_month = array();

$top_posters_month['module_name'] = 'Top plaatsers deze maand';
// [/top_posters_month]

// [users_by_month]
$users_by_month = array();

$users_by_month['module_name'] = 'Aantal nieuwe gebruikers per maand';
$users_by_month['Year'] = 'Jaar';
$users_by_month['Month_jan'] = 'Jan';
$users_by_month['Month_feb'] = 'Feb';
$users_by_month['Month_mar'] = 'Maa';
$users_by_month['Month_apr'] = 'Apr';
$users_by_month['Month_may'] = 'Mei';
$users_by_month['Month_jun'] = 'Jun';
$users_by_month['Month_jul'] = 'Jul';
$users_by_month['Month_aug'] = 'Aug';
$users_by_month['Month_sep'] = 'Sep';
$users_by_month['Month_oct'] = 'Okt';
$users_by_month['Month_nov'] = 'Nov';
$users_by_month['Month_dec'] = 'Dec';
// [/users_by_month]

// [top_posters_week]
$top_posters_week = array();

$top_posters_week['module_name'] = 'Top plaatsers deze week';
// [/top_posters_week]

// [most_active_polls]
$most_active_polls = array();

$most_active_polls['module_name'] = 'Meest Aktieve Polls';
// [/most_active_polls]

// [most_active_pollstarter]
$most_active_pollstarter = array();

$most_active_pollstarter['module_name'] = 'Meest Aktieve Poll Starter';
$most_active_pollstarter['polls'] = 'Polls';
// [/most_active_pollstarter]

// [last_created_posts]
$last_created_posts = array();

$last_created_posts['module_name'] = 'Laatste Berichten';
$last_created_posts['start_date'] = 'Gepost op';
$last_created_posts['posted_in'] = 'In Onderwerp';
$last_created_posts['post'] = 'Bericht ID/onderwerp';
$last_created_posts['hekje'] = '#';
$last_created_posts['by'] = 'Door';
// [/last_created_posts]

// [last_created_topics]
$last_created_topics = array();

$last_created_topics['module_name'] = 'Laatste Onderwerpen';
$last_created_topics['start_date'] = 'Gestart op';
// [/last_created_topics]

// [posts_by_month]
$posts_by_month = array();

$posts_by_month['module_name'] = 'Aantal nieuwe berichten per maand';
$posts_by_month['Year'] = 'Jaar';
$posts_by_month['Month_jan'] = 'Jan';
$posts_by_month['Month_feb'] = 'Feb';
$posts_by_month['Month_mar'] = 'Maa';
$posts_by_month['Month_apr'] = 'Apr';
$posts_by_month['Month_may'] = 'Mei';
$posts_by_month['Month_jun'] = 'Jun';
$posts_by_month['Month_jul'] = 'Jul';
$posts_by_month['Month_aug'] = 'Aug';
$posts_by_month['Month_sep'] = 'Sep';
$posts_by_month['Month_oct'] = 'Okt';
$posts_by_month['Month_nov'] = 'Nov';
$posts_by_month['Month_dec'] = 'Dec';
// [/posts_by_month]

// [topics_by_month]
$topics_by_month = array();

$topics_by_month['module_name'] = 'Aantal nieuwe onderwerpen per maand';
$topics_by_month['Year'] = 'Jaar';
$topics_by_month['Month_jan'] = 'Jan';
$topics_by_month['Month_feb'] = 'Feb';
$topics_by_month['Month_mar'] = 'Maa';
$topics_by_month['Month_apr'] = 'Apr';
$topics_by_month['Month_may'] = 'Mei';
$topics_by_month['Month_jun'] = 'Jun';
$topics_by_month['Month_jul'] = 'Jul';
$topics_by_month['Month_aug'] = 'Aug';
$topics_by_month['Month_sep'] = 'Sep';
$topics_by_month['Month_oct'] = 'Okt';
$topics_by_month['Month_nov'] = 'Nov';
$topics_by_month['Month_dec'] = 'Dec';
// [/topics_by_month]

// [top_attachments]
$top_attachments = array();

$top_attachments['module_name'] = 'Top Gedownloade Attachments';
$top_attachments['Filename'] = 'Bestandsnaam';
$top_attachments['Filecomment'] = 'Bestandsbeschrijving';
$top_attachments['Size'] = 'Bestandsgrootte';
$top_attachments['Downloads'] = 'Downloads';
$top_attachments['Posttime'] = 'Post Tijd';
$top_attachments['Posted_in_topic'] = 'Gepost in Onderwerp';
// Admin Variables
$top_attachments['exclude_images_title'] = 'Excludeer Afbeeldingen';
$top_attachments['exclude_images_explain'] = 'Als deze optie aan staat, worden er geen afbeeldingen getoont in Top Attachments statistiek.';
// [/top_attachments]

// [top_avatars]
$top_avatars = array();

$top_avatars['module_name'] = 'Top Gallarij Avatars';
$top_avatars['Avatar_image'] = 'Avatar Afbeelding';
$top_avatars['Users'] = 'Gebruikers';
// [/top_avatars]

// [top_smilies]
$top_smilies = array();

$top_smilies['module_name'] = 'Top Smilies';
$top_smilies['Smilie_image'] = 'Smilie afbeelding';
$top_smilies['Smilie_code'] = 'Smilie Code';
// [/top_smilies]

// [top_shoutbox_poster]
$top_shoutbox_poster = array();

$top_shoutbox_poster['module_name'] = 'Top Shoutbox Posters';
$top_shoutbox_poster['Shouts'] = 'Shouts';
$top_shoutbox_poster['Error_message'] = 'De Fully Integrated Shoutbox Mod moet geinstalleerd zijn om de Top Shoutbox Plaatsers te kunnen bekijken.<br>Installeer de Mod (<a href="http://www.phpbbhacks.com/download/1255" target="_blank">http://www.phpbbhacks.com/download/1255</a>) of deactiveer deze module.';
// [/top_shoutbox_poster]

// [top_words]
$top_words = array();

$top_words['module_name'] = 'Meest gebruikte Woorden';
$top_words['Word'] = 'Woord';
$top_words['Count'] = 'Aantal';
// [/top_words]

// [most_ignored_users]
$most_ignored_users = array();

$most_ignored_users['module_name'] = 'Meest Genegeerde Gebruikers';
$most_ignored_users['ignored_by'] = ' gebruikers genegeerd door ';
$most_ignored_users['other'] = ' andere gebruikers';
$most_ignored_users['Ignores'] = 'Genegeerd door X gebruikers';
$most_ignored_users['Error_message'] = 'De Ignore Users Mod moet geinstalleerd zijn om de genegeerde gebruikers te kunnen bekijken.<br>Installeer de Mod (<a href="http://www.phpbbhacks.com/download/789" target="_blank">http://www.phpbbhacks.com/download/789</a>) of deactiveer deze module.';
// [/most_ignored_users]

// [private_messages]
$private_messages = array();

$private_messages['module_name'] = 'Prive Berichten';
$private_messages['lastmonth'] = 'Laatste 30 dagen';
$private_messages['24hours'] = 'Laatste 24 uur';
$private_messages['7days'] = 'Afgelopen Week';
$private_messages['send'] = 'Verstuurd';
$private_messages['read'] = 'Gelezen';
$private_messages['new'] = 'Nieuw';
$private_messages['saved_in'] = 'Bewaard Inbox';
$private_messages['saved_out'] = 'Bewaard Outbox';
$private_messages['unread'] = 'Ongelezen';
// [/private_messages]

// [top_styles]
$top_styles = array();

$top_styles['module_name'] = 'Top Stijlen';
// [/top_styles]

// [top_languages]
$top_languages = array();

$top_languages['module_name'] = 'Meest gebruikte talen';
$top_languages['langname'] = 'Taal';
// [/top_languages]

// [most_logged_on_users]
$most_logged_on_users = array();

$most_logged_on_users['module_name'] = 'Meest Ingelogde gebruikers';
$most_logged_on_users['totalforumtime'] = 'Forumtijd';
$most_logged_on_users['totallogins'] = 'x Ingelogd';
$most_logged_on_users['daysmember'] = 'Aangemeld';
$most_logged_on_users['visitedpages'] = 'Pagina Hits';
$most_logged_on_users['Error_message'] = 'De Last Visit Mod moet geinstalleerd zijn om de Meest Ingelogde gebruikers te kunnen bekijken.<br>Installeer de Mod (<a href="http://www.phpbbhacks.com/download/237" target="_blank">http://www.phpbbhacks.com/download/237</a>) of deactiveer deze module.';
// [/most_logged_on_users]

// [advanced_karma]
$advanced_karma = array();

$advanced_karma['module_name'] = 'Karma Punten';
$advanced_karma['High'] = 'Positief';
$advanced_karma['Low'] = 'Negatief';
$advanced_karma['Error_message'] = 'De Advanced Karma Mod moet geinstalleerd zijn in om de Karma Punten te kunnen zien.<br>Installeer de Mod (<a href="http://siava.ru/forum/topic168.html" target="_blank">http://siava.ru/forum/topic168.html</a>) of deactiveer deze module.';
// [/advanced_karma]

// [Cash]
$Cash = array();

$Cash['module_name'] = 'Contanten';
$Cash['username'] = 'Gebruikersnaam';
$Cash['Error_message'] = 'Cash Mod is niet gevonden, installeer cash mod eerst voor je deze module gebruikt.';
// [/Cash]

// [genders]
$genders = array();

$genders['module_name'] = 'Mannelijke en Vrouwelijke gebruikers';
$genders['Gender'] = 'Geslacht';
$genders['undefined'] = 'Niet gedefiniŽerd';
$genders['male'] = '<img src="' . $images['icon_minigender_male'] . '"> Man';
$genders['female'] = '<img src="' . $images['icon_minigender_female'] . '"> Vrouw';
$genders['total'] = 'Totaal';
$genders['Error_message'] = 'Gender Mod (from Niels <a href="http://phpbbhacks.com/download/182" target="_blank">http://phpbbhacks.com/download/182</a>) is niet gevonden, installeer cash mod eerst voor je deze module gebruikt.';
// [/genders]

// [posts_by_gender_niels]
$posts_by_gender_niels = array();

$posts_by_gender_niels['module_name'] = 'Berichten per Geslacht';
$posts_by_gender_niels['Gender'] = 'Geslacht';
$posts_by_gender_niels['undefined'] = 'Niet gedefiniŽerd';
$posts_by_gender_niels['male'] = '<img src="' . $images['icon_minigender_male'] . '"> Man';
$posts_by_gender_niels['female'] = '<img src="' . $images['icon_minigender_female'] . '"> Vrouw';
$posts_by_gender_niels['Error_message'] = 'Gender Mod (from Niels <a href="http://phpbbhacks.com/download/182" target="_blank">http://phpbbhacks.com/download/182</a>) is niet gevonden, installeer cash mod eerst voor je deze module gebruikt.';
// [/posts_by_gender_niels]

// [genders_evil]
$genders_evil = array();

// Genders
$genders_evil['module_name'] = 'Mannelijke en Vrouwelijke gebruikers';
$genders_evil['Gender'] = 'Geslacht';
$genders_evil['undefined'] = 'Niet gedefiniŽerd';
$genders_evil['male'] = '<img src="' . $images['gender_m'] . '"> Man';
$genders_evil['female'] = '<img src="' . $images['gender_f'] . '"> Vrouw';
$genders_evil['total'] = 'Totaal';
$genders_evil['Error_message'] = 'Gender Mod (from Evil <a href="http://www.phpbb.com/phpBB/viewtopic.php?t=467168" target="_blank">http://www.phpbb.com/phpBB/viewtopic.php?t=467168</a>) is niet gevonden, installeer cash mod eerst voor je deze module gebruikt.';
// [/genders_evil]

// [posts_by_gender_evil]
$posts_by_gender_evil = array();

$posts_by_gender_evil['module_name'] = 'Berichten per Geslacht';
$posts_by_gender_evil['Gender'] = 'Geslacht';
$posts_by_gender_evil['undefined'] = 'Niet gedefiniŽerd';
$posts_by_gender_evil['male'] = '<img src="' . $images['gender_m'] . '"> Man';
$posts_by_gender_evil['female'] = '<img src="' . $images['gender_f'] . '"> Vrouw';
$posts_by_gender_evil['Error_message'] = 'Gender Mod (from Evil <a href="http://www.phpbb.com/phpBB/viewtopic.php?t=467168" target="_blank">http://www.phpbb.com/phpBB/viewtopic.php?t=467168</a>) is niet gevonden, installeer cash mod eerst voor je deze module gebruikt.';
// [/posts_by_gender_evil]

// [age_statistics]
$age_statistics = array();

$age_statistics['module_name'] = 'Leeftijd Statistieken';
$age_statistics['Users_Age'] = 'Aantal leden met leeftijd';
$age_statistics['Average_Age'] = 'Gemiddelde leeftijd';
$age_statistics['Youngest_Member'] = 'Jongste lid';
$age_statistics['Youngest_Age'] = 'Jongste leeftijd';
$age_statistics['Oldest_Member'] = 'Oudste lid';
$age_statistics['Oldest_Age'] = 'Oudste leeftijd';
$age_statistics['Error_message'] = '<a href="http://www.phpbbhacks.com/viewhack.php?id=187">Birthday Hack</a> is niet gevonden, installeer die mod alvorens deze module te gebruiken';
// [/age_statistics]

// [age_statistics_terrafrost]
$age_statistics_terrafrost = array();

$age_statistics_terrafrost['module_name'] = 'Leeftijd Statistieken';
$age_statistics_terrafrost['Users_Age'] = 'Aantal leden met leeftijd';
$age_statistics_terrafrost['Average_Age'] = 'Gemiddelde leeftijd';
$age_statistics_terrafrost['Youngest_Member'] = 'Jongste lid';
$age_statistics_terrafrost['Youngest_Age'] = 'Jongste leeftijd';
$age_statistics_terrafrost['Oldest_Member'] = 'Oudste lid';
$age_statistics_terrafrost['Oldest_Age'] = 'Oudste leeftijd';
$age_statistics_terrafrost['Error_message'] = '<a href="http://www.phpbb.com/phpBB/viewtopic.php?t=342028">Birthdays mod from terrafrost</a> is niet gevonden, installeer die mod alvorens deze module te gebruiken';
// [/age_statistics_terrafrost]

// [Activity_Arcade_Mod_Stats]
$Activity_Arcade_Mod_Stats = array();

$Activity_Arcade_Mod_Stats['module_name'] 	    = 'Activity Mod Statistieken';
$Activity_Arcade_Mod_Stats['best_player']	    = 'Best Speler op ';
$Activity_Arcade_Mod_Stats['fav_game']		    = 'Favoriete Spel op ';
$Activity_Arcade_Mod_Stats['total_games']	    = 'Totaal Spellen Beschikbaar:';
$Activity_Arcade_Mod_Stats['total_games_played'] = 'Totaal Spellen Gespeeld:';
$Activity_Arcade_Mod_Stats['total_comments']	    = 'Totaal Commentaren:';
$Activity_Arcade_Mod_Stats['total_ratings']	    = 'Totaal Beoordelingen:';
$Activity_Arcade_Mod_Stats['total_highscores']   = 'Totaal Maandelijkse Highscores:';
$Activity_Arcade_Mod_Stats['games']			    = '%t% Keren gespeeld.';
$Activity_Arcade_Mod_Stats['first_places']	    = 'Met %t% 1ste Plaatsen.';
$Activity_Arcade_Mod_Stats['nobody']             = 'Nog Niemand';
$Activity_Arcade_Mod_Stats['no_game']            = 'Nog Geen Spellen Gespeeld';
// [/Activity_Arcade_Mod_Stats]

// [Arcade_All_Time_High_Score_Players]
$Arcade_All_Time_High_Score_Players = array();

$Arcade_All_Time_High_Score_Players['module_name'] 	= 'Activity Mod [Top '.$core->return_limit.' All Time High Score Players]';
$Arcade_All_Time_High_Score_Players['name'] = 'Gebruikersnaam';
$Arcade_All_Time_High_Score_Players['all_time'] = 'Hoogste Scores Aller Tijden';
// [/Arcade_All_Time_High_Score_Players]

// [Activity_Arcade_Mod_Top_xx_Players]
$Activity_Arcade_Mod_Top_xx_Players = array();

$Activity_Arcade_Mod_Top_xx_Players['module_name'] 	= 'Activity Mod [Top '.$core->return_limit.' Players]';
$Activity_Arcade_Mod_Top_xx_Players['name'] = 'Gebruikersnaam';
$Activity_Arcade_Mod_Top_xx_Players['currenthigh'] = 'Current Highscores';
$Activity_Arcade_Mod_Top_xx_Players['all_time'] = 'Hoogste Scores Aller Tijden';
// [/Activity_Arcade_Mod_Top_xx_Players]

// [Activity_Arcade_Mod_Top_xx_Games]
$Activity_Arcade_Mod_Top_xx_Games = array();

$Activity_Arcade_Mod_Top_xx_Games['module_name'] 	= 'Activity Mod [Top '.$core->return_limit.' Spellen]';
$Activity_Arcade_Mod_Top_xx_Games['plays'] = 'x Gespeeld';
$Activity_Arcade_Mod_Top_xx_Games['game'] = 'Spel';
// [/Activity_Arcade_Mod_Top_xx_Games]

// [Activity_Arcade_Mod_xx_Newest_Games]
$Activity_Arcade_Mod_xx_Newest_Games = array();

$Activity_Arcade_Mod_xx_Newest_Games['module_name'] = 'Activity Mod ['.$core->return_limit.' Meest Recent toegevoegde Spellen]';
$Activity_Arcade_Mod_xx_Newest_Games['plays'] = 'x Gespeeld';
$Activity_Arcade_Mod_xx_Newest_Games['game'] = 'Spel';
$Activity_Arcade_Mod_xx_Newest_Games['date'] = 'Installed On';
// [/Activity_Arcade_Mod_xx_Newest_Games]

// [Activity_Arcade_Mod_Top_10_Users_Game_Plays]
$Activity_Arcade_Mod_Top_10_Users_Game_Plays = array();

$Activity_Arcade_Mod_Top_10_Users_Game_Plays['module_name'] 	= 'Activity Mod [Top '.$core->return_limit.' Gebruikerss (Spellen gespeeld)]';
$Activity_Arcade_Mod_Top_10_Users_Game_Plays['played'] = 'Games Played';
// [/Activity_Arcade_Mod_Top_10_Users_Game_Plays]

// [last_created_album_posts]
$last_created_album_posts = array();

$last_created_album_posts['module_name'] = 'Laatste Album Invoegingen';
$last_created_album_posts['start_date'] = 'Gepost op';
$last_created_album_posts['posted_in'] = 'Beschrijving';
$last_created_album_posts['post'] = 'ID / Titel';
$last_created_album_posts['br'] = '<br />';
// [/last_created_album_posts]

// [Top_album_posters_ever]
$Top_album_posters_ever = array();

$Top_album_posters_ever['module_name'] = 'Top album posters';
// [/Top_album_posters_ever]

// [Top_album_posters_this_month]
$Top_album_posters_this_month = array();

$Top_album_posters_this_month['module_name'] = 'Top album posters deze month';
// [/Top_album_posters_this_month]

// [Top_album_posters_this_week]
$Top_album_posters_this_week = array();

$Top_album_posters_this_week['module_name'] = 'Top album posters deze week';
// [/Top_album_posters_this_week]

// [most_viewed_photos_ever]
$most_viewed_photos_ever = array();

$most_viewed_photos_ever['module_name'] = 'Meest bekeken foto\'s ooit';
$most_viewed_photos_ever['photo'] = 'Foto';
$most_viewed_photos_ever['views'] = 'Bekeken';
$most_viewed_photos_ever['start_date'] = 'Gepost';
$most_viewed_photos_ever['desc'] = 'Beschrijving';
$most_viewed_photos_ever['br'] = '<br />';
// [/most_viewed_photos_ever]

// [most_viewed_photos_this_month]
$most_viewed_photos_this_month = array();

$most_viewed_photos_this_month['module_name'] = 'Meest bekeken foto\'s deze maand';
$most_viewed_photos_this_month['photo'] = 'Foto';
$most_viewed_photos_this_month['views'] = 'Bekeken';
$most_viewed_photos_this_month['start_date'] = 'Gepost';
$most_viewed_photos_this_month['desc'] = 'Beschrijving';
$most_viewed_photos_this_month['br'] = '<br />';
// [/most_viewed_photos_this_month]

// [most_viewed_photos_this_week]
$most_viewed_photos_this_week = array();

$most_viewed_photos_this_week['module_name'] = 'Meest bekeken foto\'s deze week';
$most_viewed_photos_this_week['photo'] = 'Foto';
$most_viewed_photos_this_week['views'] = 'Bekeken';
$most_viewed_photos_this_week['start_date'] = 'Gepost';
$most_viewed_photos_this_week['desc'] = 'Beschrijving';
$most_viewed_photos_this_week['br'] = '<br />';
// [/most_viewed_photos_this_week]

// [top_countries_ipcountryflags]
$top_countries_ipcountryflags = array();

$top_countries_ipcountryflags['module_name'] = 'Top Landen';
$top_countries_ipcountryflags['country'] = 'Land';
$top_countries_ipcountryflags['flag'] = 'Vlag';
$top_countries_ipcountryflags['users'] = 'Gebruikers';
$top_countries_ipcountryflags['Error_message'] = 'IpCountryFlags (<a href="http://www.you3d.net/ip2/index.php" target="_blank">http://www.you3d.net/ip2/index.php</a>) is niet gevonden, installeer cash mod eerst voor je deze module gebruikt.';
// [/top_countries_ipcountryflags]

// [Activity_plus_stats]
$Activity_plus_stats = array();

$Activity_plus_stats['module_name']        = 'Activity Mod Plus';
$Activity_plus_stats['best_player']	    = 'Beste speler op ';
$Activity_plus_stats['fav_game']		    = 'Favoriete spel op ';
$Activity_plus_stats['total_games']	    = 'Totaal beschikbare spellen:';
$Activity_plus_stats['total_comments']	    = 'Totaal commentaren:';
$Activity_plus_stats['total_bets']	    = 'Totaal Gemaakte weddenschappen:';
$Activity_plus_stats['total_challenges']   = 'Totaal Verzonden Uitdagingen:';
$Activity_plus_stats['played']			    = '%t% maal gespeeld.';
$Activity_plus_stats['trophies']	    = 'met %t% Trofies.';
$Activity_plus_stats['nobody'] = 'Niemand';
$Activity_plus_stats['no_game'] = 'Geen Spellen';
// [/Activity_plus_stats]

// [Activity_Plus_Mod_xx_Newest_Games]
$Activity_Plus_Mod_xx_Newest_Games = array();

$Activity_Plus_Mod_xx_Newest_Games['module_name'] = 'Activity Plus Mod ['.$core->return_limit.' Meest Recent toegevoegde Spellen]';
$Activity_Plus_Mod_xx_Newest_Games['plays'] = 'x Gespeeld';
$Activity_Plus_Mod_xx_Newest_Games['game'] = 'Spel';
$Activity_Plus_Mod_xx_Newest_Games['date'] = 'Installed On';
// [/Activity_Plus_Mod_xx_Newest_Games]

// [Activity_Plus_Mod_Top_xx_Trophies]
$Activity_Plus_Mod_Top_xx_Trophies = array();

$Activity_Plus_Mod_Top_xx_Trophies['module_name'] 	= 'Activity Plus Mod [Top '.$core->return_limit.' Trophies]';
$Activity_Plus_Mod_Top_xx_Trophies['name'] = 'Naam';
$Activity_Plus_Mod_Top_xx_Trophies['Trophies'] = 'Trophies';
// [/Activity_Plus_Mod_Top_xx_Trophies]

// [Activity_Plus_Mod_Top_xx_Players]
$Activity_Plus_Mod_Top_xx_Players = array();

$Activity_Plus_Mod_Top_xx_Players['module_name'] 	= 'Activity Plus Mod [Top '.$core->return_limit.' Gespeelde Spellen]';
$Activity_Plus_Mod_Top_xx_Players['plays'] = 'x Gespeeld';
$Activity_Plus_Mod_Top_xx_Players['name'] = 'Naam';
// [/Activity_Plus_Mod_Top_xx_Players]

// [Activity_plus_Mod_Top_xx_Games]
$Activity_plus_Mod_Top_xx_Games = array();

$Activity_plus_Mod_Top_xx_Games['module_name'] 	= 'Activity Plus Mod [Top '.$core->return_limit.' Spellen]';
$Activity_plus_Mod_Top_xx_Games['plays'] = 'x Gespeeld';
$Activity_plus_Mod_Top_xx_Games['game'] = 'Spel';
// [/Activity_plus_Mod_Top_xx_Games]

?>