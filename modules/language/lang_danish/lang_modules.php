<?php

// [stats_overview]
$stats_overview = array();

$stats_overview['module_name'] = 'Statistikker oversigt';

// Admin Specific
$stats_overview['num_columns_title'] = 'Indstil antallet af kolonner';
$stats_overview['num_columns_explain'] = 'Her kan du indstille det viste antal kolonner';

// [/stats_overview]

// [admin_statistics]
$admin_statistics = array();

// Administrative Statistics
$admin_statistics['module_name'] = 'Administrative statistikker';

$admin_statistics['Board_Up_Days'] = 'Forum oppetid i dage';
$admin_statistics['Latest_Reg_User'] = 'Senest tilmeldte bruger';
$admin_statistics['Latest_Reg_User_Date'] = 'Dato for senest tilmeldte bruger';
$admin_statistics['Most_Ever_Online'] = 'Flest brugere online nogensinde';
$admin_statistics['Most_Ever_Online_Date'] = 'Dato for flest brugere online nogensinde';
$admin_statistics['Disk_usage'] = 'Diskforbrug';

// [/admin_statistics]

// [most_active_topics]
$most_active_topics = array();

$most_active_topics['module_name'] = 'Most Active Topics';
// [/most_active_topics]

// [most_viewed_topics]
$most_viewed_topics = array();

$most_viewed_topics['module_name'] = 'Most Viewed Topics';
// [/most_viewed_topics]

// [most_active_topicstarter]
$most_active_topicstarter = array();

$most_active_topicstarter['module_name'] = 'Most Active Topicstarter';
// [/most_active_topicstarter]

// [top_posters]
$top_posters = array();

$top_posters['module_name'] = 'Top Posters';
// [/top_posters]

// [fastest_users]
$fastest_users = array();

// by John Hjorth
$fastest_users['module_name'] = 'Hurtigste brugere';
$fastest_users['days_on_forum'] = 'Antal dage p� forum';
$fastest_users['messperday'] = 'Indl�g pr. dag';
// [/fastest_users]

// [last_online]
$last_online = array();

$last_online['module_name'] = 'Last Online';
$last_online['name'] = 'Username';
$last_online['last_online'] = 'Last Online';
$last_online['Error_message'] = 'Last Visit Mod (<a href="http://www.phpbbhacks.com/download/237">http://www.phpbbhacks.com/download/237</a>) has not been found, please install it before using this module';
// [/last_online]

// [users_by_month]
$users_by_month = array();

$users_by_month['module_name'] = 'Number of new users by month';
$users_by_month['Year'] = 'Year';
$users_by_month['Month_jan'] = 'Jan';
$users_by_month['Month_feb'] = 'Feb';
$users_by_month['Month_mar'] = 'Mar';
$users_by_month['Month_apr'] = 'Apr';
$users_by_month['Month_may'] = 'May';
$users_by_month['Month_jun'] = 'Jun';
$users_by_month['Month_jul'] = 'Jul';
$users_by_month['Month_aug'] = 'Aug';
$users_by_month['Month_sep'] = 'Sep';
$users_by_month['Month_oct'] = 'Oct';
$users_by_month['Month_nov'] = 'Nov';
$users_by_month['Month_dec'] = 'Dec';
// [/users_by_month]

// [top_posters_week]
$top_posters_week = array();

$top_posters_week['module_name'] = 'Top posting users this week';
// [/top_posters_week]

// [most_active_polls]
$most_active_polls = array();

$most_active_polls['module_name'] = 'Most Active Polls';
// [/most_active_polls]

// [most_active_pollstarter]
$most_active_pollstarter = array();

$most_active_pollstarter['module_name'] = 'Most Active Poll Starter';
$most_active_pollstarter['polls'] = 'Polls';
// [/most_active_pollstarter]

// [posts_by_month]
$posts_by_month = array();


// [/posts_by_month]

// [topics_by_month]
$topics_by_month = array();

$topics_by_month['module_name'] = 'Number of new topics by month';
$topics_by_month['Year'] = 'Year';
$topics_by_month['Month_jan'] = 'Jan';
$topics_by_month['Month_feb'] = 'Feb';
$topics_by_month['Month_mar'] = 'Mar';
$topics_by_month['Month_apr'] = 'Apr';
$topics_by_month['Month_may'] = 'May';
$topics_by_month['Month_jun'] = 'Jun';
$topics_by_month['Month_jul'] = 'Jul';
$topics_by_month['Month_aug'] = 'Aug';
$topics_by_month['Month_sep'] = 'Sep';
$topics_by_month['Month_oct'] = 'Oct';
$topics_by_month['Month_nov'] = 'Nov';
$topics_by_month['Month_dec'] = 'Dec';
// [/topics_by_month]

// [top_avatars]
$top_avatars = array();

$top_avatars['module_name'] = 'Top Gallary Avatars';
$top_avatars['Avatar_image'] = 'Avatar Image';
$top_avatars['Users'] = 'Users';
// [/top_avatars]

// [top_smilies]
$top_smilies = array();

$top_smilies['module_name'] = 'Top Smilies';
$top_smilies['Smilie_image'] = 'Smiley Image';
$top_smilies['Smilie_code'] = 'Smiley Code';
// [/top_smilies]

// [top_shoutbox_poster]
$top_shoutbox_poster = array();

$top_shoutbox_poster['module_name'] = 'Top Shoutbox Posters';
$top_shoutbox_poster['Shouts'] = 'Shouts';
$top_shoutbox_poster['Error_message'] = 'The Fully Integrated Shoutbox Mod have to be installed in order to see the Top Shoutbox Posters.<br>Install the mod (<a href="http://www.phpbbhacks.com/download/1255" target="_blank">http://www.phpbbhacks.com/download/1255</a>) or deactivate this module.';
// [/top_shoutbox_poster]

// [top_words]
$top_words = array();

$top_words['module_name'] = 'Most Used Words';
$top_words['Word'] = 'Word';
$top_words['Count'] = 'Count';
// [/top_words]

// [most_ignored_users]
$most_ignored_users = array();

$most_ignored_users['module_name'] = 'Most Ignored Users';
$most_ignored_users['ignored_by'] = ' users ignored by ';
$most_ignored_users['other'] = ' other users';
$most_ignored_users['Ignores'] = 'Ignored by X Users';
$most_ignored_users['Error_message'] = 'The Ignore Users Mod have to be installed in order to see the Ignored Users.<br>Install the mod (<a href="http://www.phpbbhacks.com/download/789" target="_blank">http://www.phpbbhacks.com/download/789</a>) or deactivate this module.';
// [/most_ignored_users]

// [private_messages]
$private_messages = array();

$private_messages['module_name'] = 'Private Messages';
$private_messages['lastmonth'] = '30 days';
$private_messages['24hours'] = 'Past 24 hours';
$private_messages['7days'] = 'Past Week';
$private_messages['send'] = 'Send';
$private_messages['read'] = 'Read';
$private_messages['new'] = 'New';
$private_messages['saved_in'] = 'Saved Inbox';
$private_messages['saved_out'] = 'Saved Outbox';
$private_messages['unread'] = 'Unread';
// [/private_messages]

// [top_styles]
$top_styles = array();

$top_styles['module_name'] = 'Top Styles';
// [/top_styles]

// [top_languages]
$top_languages = array();

$top_languages['module_name'] = 'Mest anvendte sprog';
$top_languages['langname'] = 'Sprog';
// [/top_languages]

// [Cash]
$Cash = array();

$Cash['module_name'] = 'Cash';
$Cash['username'] = 'User';
$Cash['Error_message'] = 'Cash Mod has not been found, please install it before using this module';
// [/Cash]

?>