<?php

/***************************************************************************
*                            Arcade_All_Time_High_Score_Players.pak
*                            --------------------
*     begin                :
*     copyright            : (C)      Wicher
*     email                : ---
*
*     $Id: Arcade_All_Time_High_Score_Players.pak, 2006/12/25 18:26:00 Wicher Exp $
*          for Statistics MOD version 3.0.1beta3s and higher
****************************************************************************/
/***************************************************************************
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
****************************************************************************/

if ( !defined('IN_PHPBB') ) 
{ 
   die("Hacking attempt"); 
} 


// 
// Top 10 All Time High Score Players 
// 
$core->start_module(true); 

$core->set_content('statistical'); 

$core->set_view('rows', $core->return_limit); 
$core->set_view('columns', 3); 

$core->define_view('set_columns', array( 
   $core->pre_defined('rank'), 
   'user' => $lang['name'], 
   'alltimehigh' => $lang['all_time']) 
); 

$core->set_header($lang['module_name']); 

$core->assign_defined_view('align_rows', array( 
   'left', 
   'center', 
   'left') 
); 

$core->assign_defined_view('width_rows', array( 
   '10%', 
   '50%', 
   '80%') 
); 


// check if mod installed
$sql = "SELECT at_first_places FROM " . iNA_USER_DATA; 
if (!$result = $db->sql_query($sql))
{
	$there=0;
}
else
{
	$there=1;
}
if ($there == 0)
{
	message_die(GENERAL_MESSAGE, 'This module requires Arcade_MOD_v2.1.2 (<a href="http://www.phpbb-arcade.com/download.php?id=421" target="_blank">http://www.phpbb-arcade.com/download.php?id=421</a>) to be installed.');
}


   $sql = "SELECT DISTINCT u.user_id, u.username, a.first_places, a.at_first_places FROM " . USERS_TABLE . " as u, " . iNA_USER_DATA . " as a
      WHERE u.user_id = a.user_id
      AND a.first_places > -1
      ORDER by a.at_first_places DESC
      LIMIT " . $core->return_limit;

$result = $core->sql_query($sql, 'Couldn\'t retrieve High Score Players'); 
$data = $core->sql_fetchrowset($result); 
$core->set_data($data); 


$core->define_view('set_rows', array( 
   '$core->pre_defined()', 
   '$core->generate_link(append_sid($phpbb_root_path . \'profile.php?mode=viewprofile&u=\' . $core->data(\'user_id\')), $core->data(\'username\'), \'target="_blank"\')', 
   '$core->data(\'at_first_places\')') 
); 

$core->run_module(); 

?>