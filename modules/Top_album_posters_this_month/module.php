<?php
/***************************************************************************
*                            top_album_posters_this_month.pak
*                            --------------------
*     begin                :
*     copyright            : (C)      Wicher
*     email                : ---
*
*     $Id: top_album_posters_this_month.pak, 2007/01/09 16:10:00 Wicher Exp $
*          for Statistics MOD version 4.1.6 and higher
****************************************************************************/
/***************************************************************************
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
****************************************************************************/

if ( !defined('IN_PHPBB') )
{
	die("Hacking attempt");
}

//
// Top album posters this month

$core->start_module(true);

$core->set_content('bars');

$core->set_view('rows', $core->return_limit);
$core->set_view('columns', 5);

$core->define_view('set_columns', array(
	$core->pre_defined('rank'),
	'username' => $lang['Username'],
	'posts' => $lang['Posts'],
	$core->pre_defined('percent'),
	$core->pre_defined('graph'))
);

$content->percentage_sign = TRUE;

$month = array();
$current_time = 0;

$current_time = time();
$year = date('Y', $current_time);
$month [0] = mktime (0,0,0,1,1, $year);
$month [1] = $month [0] + 2678400;
$month [2] = mktime (0,0,0,3,1, $year);
$month [3] = $month [2] + 2678400;
$month [4] = $month [3] + 2592000;
$month [5] = $month [4] + 2678400;
$month [6] = $month [5] + 2592000;
$month [7] = $month [6] + 2678400;
$month [8] = $month [7] + 2678400;
$month [9] = $month [8] + 2592000;
$month [10] = $month [9] + 2678400;
$month [11] = $month [10] + 2592000;
$month [12] = $month [11] + 2592000;
$arr_num = (date('n')-1);
$time_thismonth = $month[$arr_num];

$l_this_month = date('F', $time_thismonth);

$core->set_header($lang['module_name'] . ' [' . $l_this_month . ' ' . date('Y', $time_thismonth) . ']');

$core->assign_defined_view('align_rows', array(
	'left',
	'left',
	'center',
	'center',
	'left')
);

// check if mod installed
$sql = "SELECT pic_user_id, pic_username, pic_time, pic_user_id, pic_cat_id FROM " . ALBUM_TABLE; 
if (!$result = $db->sql_query($sql))
{
	$there=0;
}
else
{
	$there=1;
}
if ($there == 0)
{
	message_die(GENERAL_MESSAGE, 'This module requires Smartor Photo Album (<a href="http://smartor.is-root.com/viewforum.php?f=7" target="_blank">http://smartor.is-root.com/viewforum.php?f=7</a>) or Full Album Pack 1.4.0 (<a href="http://www.mightygorgon.com/viewtopic.php?t=3661" target="_blank">http://www.mightygorgon.com/viewtopic.php?t=3661</a>) to be installed.');
}

// total pics this month
$sql = "SELECT COUNT(p.pic_id) AS count
		FROM " . ALBUM_CAT_TABLE . " AS c
		LEFT JOIN " . ALBUM_TABLE . " AS p ON c.cat_id = p.pic_cat_id 
		WHERE (p.pic_time > '" . intval($time_thismonth) . "') 
		AND (p.pic_user_id <> " . ANONYMOUS . ")";

if (!$result = $db->sql_query($sql))
{
	message_die(GENERAL_ERROR, "Couldn't get total number of pictures for all categories and sub categories", "", __LINE__, __FILE__, $sql);
}

$row = $db->sql_fetchrow($result);
$db->sql_freeresult($result);
$totalpics = intval($row['count']);

// top posters
$sql = "SELECT DISTINCT count(p.pic_user_id) AS pic_count, p.pic_user_id, p.pic_username 
		FROM " . ALBUM_CAT_TABLE . " AS c
		LEFT JOIN " . ALBUM_TABLE . " AS p ON c.cat_id = p.pic_cat_id 
		WHERE (p.pic_time > '" . intval($time_thismonth) . "') 
		AND (p.pic_user_id <> " . ANONYMOUS . ")
		GROUP BY p.pic_user_id
		ORDER BY pic_count DESC
		LIMIT " . $core->return_limit;

if (!$result = $db->sql_query($sql))
{
	message_die(GENERAL_ERROR, "Couldn't get total number of pictures for all categories and sub categories", "", __LINE__, __FILE__, $sql);
}

$topposters = $core->sql_fetchrowset($result);
$db->sql_freeresult($result);

$content->init_math('pic_count', $topposters[0]['pic_count'], $totalpics);
$core->set_data($topposters);

$core->define_view('set_rows', array(
	'$core->pre_defined()',
	'$core->generate_link(append_sid($phpbb_root_path . \'profile.php?mode=viewprofile&u=\' . $core->data(\'pic_user_id\')), $core->data(\'pic_username\'), \'target="_blank"\')',
	'$core->data(\'pic_count\')',
	'$core->pre_defined()',
	'$core->pre_defined()')
);

$core->run_module();

?>