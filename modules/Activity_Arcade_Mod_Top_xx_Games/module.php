<?php

/***************************************************************************
*                            Activity_Arcade_Mod_Top_xx_Games.pak
*                            --------------------
*     begin                :
*     copyright            : (C)      Wicher
*     email                : ---
*
*     $Id: Activity_Arcade_Mod_Top_xx_Games.pak, 2006/11/28 15:40:00 Wicher Exp $
*          for Statistics MOD version 3.0.1beta3s and higher
****************************************************************************/
/***************************************************************************
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
****************************************************************************/

if ( !defined('IN_PHPBB') ) 
{ 
   die("Hacking attempt"); 
} 


// 
// Activity Mod Top 10 Users (Game Plays) 
// 
// true == use db cache 
$core->start_module(true); 

$core->set_content('bars'); 

$core->set_view('rows', $core->return_limit); 
$core->set_view('columns', 5); 

$core->define_view('set_columns', array( 
   $core->pre_defined('rank'), 
   'game' => $lang['game'], 
   'played' => $lang['plays'], 
   $core->pre_defined('percent'), 
   $core->pre_defined('graph')) 
); 

$content->percentage_sign = TRUE; 

$core->set_header($lang['module_name']); 

$core->assign_defined_view('align_rows', array( 
   'left', 
   'left', 
   'center', 
   'center', 
   'left') 
); 


// check if mod installed
$sql = "SELECT at_first_places FROM " . iNA_USER_DATA; 
if (!$result = $db->sql_query($sql))
{
	$there=0;
}
else
{
	$there=1;
}
if ($there == 0)
{
	message_die(GENERAL_MESSAGE, 'This module requires Arcade_MOD_v2.1.2 (<a href="http://www.phpbb-arcade.com/download.php?id=421" target="_blank">http://www.phpbb-arcade.com/download.php?id=421</a>) to be installed.');
}

	$sql = 'SELECT SUM(played) AS total_played
			FROM '. iNA_GAMES;
	$result 	= $core -> sql_query($sql, 'Unable to retrieve totalgames data'); 
	$total = $core -> sql_fetchrow($result);
	$game_total = $total['total_played'];

	$sql = 'SELECT game_id, game_desc, played
			FROM '. iNA_GAMES .'
			ORDER BY played DESC
			LIMIT ' . $core->return_limit;
			

$result = $core->sql_query($sql, 'Unable to retrieve games data'); 
$data = $core->sql_fetchrowset($result); 
$content->init_math('played', $data[0]['played'], $game_total); 
$core->set_data($data); 

$core->define_view('set_rows', array( 
   '$core->pre_defined()', 
   '$core->generate_link(append_sid($phpbb_root_path . \'activity.php?mode=game&id=\' . $core->data(\'game_id\')), $core->data(\'game_desc\'), \'target="_blank"\')', 
   '$core->data(\'played\')', 
   '$core->pre_defined()', 
   '$core->pre_defined()') 
); 

$core->run_module(); 

?>