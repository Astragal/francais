<?php

/***************************************************************************
*                            age.pak
*                            --------------------
*     begin                :
*     copyright            : (C)      Wicher
*     email                : ---
*
*     $Id: age.pak, 2006/10/28 20:00:00 Wicher Exp $
*          for Statistics MOD version 3.0.1beta3s and higher
****************************************************************************/
/***************************************************************************
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
****************************************************************************/

if ( !defined('IN_PHPBB') )
{
	die("Hacking attempt");
}

//
// Age Statistics
//

$core->start_module(true);

$core->set_content('values');

//
// Age Statistics
//

$sql = "SELECT user_next_birthday_greeting FROM " . USERS_TABLE . " ORDER BY user_id DESC LIMIT 1"; 
if (!$result = $db->sql_query($sql))
{
	$there=0;
}
else
{
	$there=1;
}
if ($there == 0)
{
	message_die(GENERAL_MESSAGE, $lang['Error_message']);
}

// Count users who filled in there age
$sql = "SELECT count(*) as pcount
		FROM " . USERS_TABLE . "
		WHERE user_birthday <> 999999";

$result = $db->sql_query($sql);
if (!$result)
{
	message_die(GENERAL_ERROR, "Could not find users.", "",__LINE__, __FILE__, $sql);
}
$usercount = $db->sql_fetchrow($result);
$sum_age = 0;

if ($usercount[pcount])
{
for($i = 0; $i < $usercount[pcount]; $i++)
{
	$sql = "SELECT user_birthday
			FROM " . USERS_TABLE . " u
			WHERE u.user_birthday <> 999999
            LIMIT " . $i . ",1";
	$result = $db->sql_query($sql);
		if ( !$result )
		{
			message_die(GENERAL_ERROR, "Could not find posts.", "",__LINE__, __FILE__, $sql);
		}
	$user_age = $db->sql_fetchrow($result);
   	$poster_age =realdate('Y',(time()/86400))- realdate ('Y',$user_age['user_birthday']);
   	if (date('md')<realdate('md',$user_age['user_birthday'])) $poster_age--;
    $sum_age = $sum_age + $poster_age;
}

// Young!
$sql = "SELECT max(user_birthday) as max
		FROM " . USERS_TABLE . "
   		WHERE user_birthday <> 999999";

$result = $db->sql_query($sql);
if (!$result)
{
	message_die(GENERAL_ERROR, "Could not find posts.", "",__LINE__, __FILE__, $sql);
}
$usermax = $db->sql_fetchrow($result);
$poster_max =realdate('Y',(time()/86400))- realdate ('Y',$usermax['max']);
if (date('md')<realdate('md',$usermax['max'])) $poster_max--;

$sql = "SELECT user_id,username
		FROM " . USERS_TABLE . "
   		WHERE user_birthday =". $usermax['max'];

$result = $db->sql_query($sql);
if ( !$result )
{
	message_die(GENERAL_ERROR, "Could not find posts.", "",__LINE__, __FILE__, $sql);
}
$usermaxid = $db->sql_fetchrow($result);

// Old!
$sql = "SELECT min(user_birthday) as min
		FROM " . USERS_TABLE . "
   		WHERE user_birthday <> 999999";

$result = $db->sql_query($sql);
if ( !$result )
{
	message_die(GENERAL_ERROR, "Could not find posts.", "",__LINE__, __FILE__, $sql);
}
$usermin = $db->sql_fetchrow($result);

$poster_min =realdate('Y',(time()/86400))- realdate ('Y',$usermin['min']);
if (date('md')<realdate('md',$usermin['min'])) $poster_min--;

$sql = "SELECT user_id,username
		FROM " . USERS_TABLE . "
   		WHERE user_birthday =". $usermin['min'];

$result = $db->sql_query($sql);
if ( !$result )
{
	message_die(GENERAL_ERROR, "Could not find users.", "",__LINE__, __FILE__, $sql);
}
$userminid = $db->sql_fetchrow($result);

$usermax = "<a href=". append_sid($phpbb_root_path . 'profile.php?mode=viewprofile&u=' . $usermaxid['user_id']) .">". $usermaxid['username'] ."</a>";
$usermin = "<a href=". append_sid($phpbb_root_path . 'profile.php?mode=viewprofile&u=' . $userminid['user_id']) .">". $userminid['username'] ."</a>";

$statistic_array = array($lang['Users_Age'],$lang['Average_Age'],$lang['Youngest_Member'],$lang['Youngest_Age'],$lang['Oldest_Member'],$lang['Oldest_Age']);
$value_array = array($usercount[pcount],round($sum_age/$usercount[pcount],2),$usermax,$poster_max,$usermin,$poster_min);
}
else
{
$statistic_array = array($lang['Users_Age'],$lang['Average_Age'],$lang['Youngest_Member'],$lang['Youngest_Age'],$lang['Oldest_Member'],$lang['Oldest_Age']);
$value_array = array('0','0','0','0','0','0');
}


$core->set_view('columns', 2);
$core->set_view('num_blocks', 2);
$core->set_view('value_order', 'left_right');
//$core->set_view('value_order', 'up_down');

$core->define_view('set_columns', array(
	'stats' => $lang['Statistic'],
	'value' => $lang['Value'])
);

$core->set_header($lang['module_name']);

$data = $core->assign_defined_view('value_array', array($statistic_array, $value_array));

$core->set_data($data);

$core->define_view('iterate_values', array());

$core->run_module();

?>