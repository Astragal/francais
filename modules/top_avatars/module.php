<?php
/***************************************************************************
*                            top_avatars.pak
*                            --------------------
*     begin                :
*     copyright            : (C)      Wicher
*     email                : ---
*
*     $Id: top_avatars.pak, 2007/01/11 20:11:00 Wicher Exp $
*          for Statistics MOD version 3.0.1beta3s and higher
****************************************************************************/
/***************************************************************************
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
****************************************************************************/

if ( !defined('IN_PHPBB') )
{
	die("Hacking attempt");
}

// true == use db cache
$core->start_module(true);

$core->set_content('bars');

$core->set_view('rows', $core->return_limit);
$core->set_view('columns', 4);

$core->define_view('set_columns', array(
	$core->pre_defined('rank'),
	'image' => $lang['Avatar_image'],
	'posts' => $lang['Users'],
	$core->pre_defined('graph'))
);

$core->set_header($lang['module_name']);

$core->assign_defined_view('align_rows', array(
	'left',
	'left',
	'center',
	'left')
);

$sql = "SELECT SUM(user_avatar) as total_avatar_users FROM " . USERS_TABLE . " 
		WHERE user_avatar <> ''
		AND user_avatar_type = ".USER_AVATAR_GALLERY;

$result = $core->sql_query($sql, 'Unable to retrieve avatar usage data');
$row = $core->sql_fetchrow($result);

$total_avatar_users = $row['total_avatar_users'];

$sql = "SELECT user_avatar, user_avatar_type, COUNT( * ) AS avatarCount 
FROM " . USERS_TABLE . " 
WHERE (user_avatar <> '')
AND user_avatar_type = ".USER_AVATAR_GALLERY."
AND user_id <> ".ANONYMOUS."
GROUP BY user_avatar
ORDER BY avatarCount DESC
LIMIT " . $core->return_limit;

$result = $core->sql_query($sql, 'Unable to retrieve avatar usage data');
$data = $core->sql_fetchrowset($result);
$content->init_math('avatarCount', $data[0]['avatarCount'], $total_avatar_users);

$core->make_global(array(
	'$board_config')
		
);

$core->set_data($data);

$core->define_view('set_rows', array(
	'$core->pre_defined()',
	'$core->generate_image_link($board_config[\'avatar_gallery_path\'] . \'/\' . $core->data(\'user_avatar\'), $core->data(\'user_avatar\'), \'border="0"\')',
	'$core->data(\'avatarCount\')',
	'$core->pre_defined()')
);

$core->run_module();

?>