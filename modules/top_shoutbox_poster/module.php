<?php 
/***************************************************************************
*                            top_shoutbox_poster.pak
*                            --------------------
*     begin                :
*     copyright            : (C)      Wicher
*     email                : ---
*
*     $Id: top_shoutbox_poster.pak, 2006/10/12 20:00:00 Wicher Exp $
*          for Statistics MOD version 3.0.1beta3s and higher
****************************************************************************/
/***************************************************************************
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
****************************************************************************/

if ( !defined('IN_PHPBB') ) 
{ 
   die("Hacking attempt"); 
} 

$core->start_module(true); 

//$core->set_content('statistical'); 


$core->set_content('bars'); 

$core->set_view('rows', $core->return_limit); 
$core->set_view('columns', 5); 

$core->define_view('set_columns', array( 
   $core->pre_defined('rank'), 
   'username' => $lang['Username'], 
   'user_shouts' => $lang['Shouts'], 
   $core->pre_defined('percent'), 
   $core->pre_defined('graph')) 
); 

$content->percentage_sign = TRUE; 

$core->set_header($lang['module_name']); 

$core->assign_defined_view('align_rows', array( 
   'left', 
   'left', 
   'center', 
   'center', 
   'left') 
); 

// check if mod installed
$sql = "SELECT shout_user_id FROM " . SHOUTBOX_TABLE ." ORDER BY shout_user_id DESC LIMIT 1"; 
if (!$result = $db->sql_query($sql))
{
	$there=0;
}
else
{
	$there=1;
}
if ($there == 0)
{
	message_die(GENERAL_MESSAGE, $lang['Error_message']);
}

$sql = "SELECT COUNT(*) as total_shouts FROM " . SHOUTBOX_TABLE . " 
WHERE shout_user_id <> " . ANONYMOUS; 

$result = $core->sql_query($sql, 'Unable to retrieve total shoutbox data'); 
$row = $core->sql_fetchrow($result); 

$total_shouts = $row['total_shouts']; 

$sql = "SELECT shout_user_id, username, COUNT(shout_id) as user_shouts 
   FROM " . SHOUTBOX_TABLE . " , " . USERS_TABLE . " 
   WHERE shout_user_id <> " . ANONYMOUS . " 
      AND shout_user_id = user_id 
   GROUP BY shout_user_id, username 
   ORDER BY user_shouts DESC 
   LIMIT " . $core->return_limit; 

$result = $core->sql_query($sql, 'Unable to retrieve shoutbox data'); 
$data = $core->sql_fetchrowset($result); 
$content->init_math('user_shouts', $data[0]['user_shouts'], $total_shouts); 
$core->set_data($data); 

$core->define_view('set_rows', array( 
   '$core->pre_defined()', 
   '$core->generate_link(append_sid($phpbb_root_path . \'profile.php?mode=viewprofile&u=\' . $core->data(\'shout_user_id\')), $core->data(\'username\'), \'target="_blank"\')', 
   '$core->data(\'user_shouts\')', 
   '$core->pre_defined()', 
   '$core->pre_defined()') 
); 

$core->run_module(); 
?>