<?php
/***************************************************************************
*                            posts_by_gender_evil.pak
*                            --------------------
*     begin                :
*     copyright            : (c) 2006 phpBBModders (based on module by Wicher)
*     email                : ---
*
*     $Id: genders.pak, 2007/01/25 13:37:00 Evil Exp $
*          for Statistics MOD version 3.0.1beta3s and higher
****************************************************************************/
/***************************************************************************
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
****************************************************************************/

if ( !defined('IN_PHPBB') )
{
	die("Hacking attempt");
}

//
// Genders
//

$core->start_module(true);

$core->set_content('values');


if (!file_exists($phpbb_root_path.'templates/' . $theme['template_name'] . '/images/gender_x.gif'))
{
	$there=0;
}
else
{
	$there=1;
}
if ($there == 0)
{
	message_die(GENERAL_MESSAGE, $lang['Error_message']);
}

// male
$male_count = array();
$sql = "SELECT SUM(user_posts) as maleposts
		FROM " . USERS_TABLE . " 
		WHERE user_gender = 1";
$result 	= $db -> sql_query($sql);
$maleresult = $db -> sql_fetchrow($result); 
$total_male = $maleresult['maleposts'];

// female
$female_count = array();
$sql = "SELECT SUM(user_posts) as femaleposts 
		FROM " . USERS_TABLE . " 
		WHERE user_gender = 2";
$result 	= $db -> sql_query($sql);
$femaleresult = $db -> sql_fetchrow($result); 
$total_female = $femaleresult['femaleposts'];

// undefined
$count = array();
$sql = "SELECT SUM(user_posts) as undefinedposts 
		FROM " . USERS_TABLE . " 
		WHERE user_gender = 0";
$result 	= $db -> sql_query($sql);
$countresult = $db -> sql_fetchrow($result); 
$total = $countresult['undefinedposts'];

$gender_array = array($lang['gender_m'], $lang['gender_f'], $lang['gender_x']);
$value_array = array($total_male, $total_female, $total);

$core->set_view('columns', 2);
$core->set_view('num_blocks', 3);
$core->set_view('value_order', 'left_right');
//$core->set_view('value_order', 'up_down');

$core->define_view('set_columns', array(
	'gender' => $lang['gender'],
	'value' => $lang['Posts'])
);

$core->set_header($lang['module_name']);

$data = $core->assign_defined_view('value_array', array($gender_array, $value_array));

$core->set_data($data);

$core->define_view('iterate_values', array());

$core->run_module();

?>