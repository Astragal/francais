<?php 
/***************************************************************************
*                            top_languages.pak
*                            --------------------
*     begin                :
*     copyright            : (C)      Wicher
*     email                : ---
*
*     $Id: top_languages.pak, 2006/10/12 20:00:00 Wicher Exp $
*          for Statistics MOD version 3.0.1beta3s and higher
****************************************************************************/
/***************************************************************************
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
****************************************************************************/

if ( !defined('IN_PHPBB') ) 
{ 
   die("Hacking attempt"); 
} 

// true == use db cache 
$core->start_module(true); 

$core->set_content('bars'); 

$core->set_view('rows', $core->return_limit); 
$core->set_view('columns', 5); 

$core->define_view('set_columns', array( 
   $core->pre_defined('rank'), 
   'langname' => $lang['Language'], 
   'users' => $lang['Users'], 
   $core->pre_defined('percent'), 
   $core->pre_defined('graph')) 
); 

$content->percentage_sign = TRUE; 

$core->set_header($lang['module_name']); 

$core->assign_defined_view('align_rows', array( 
   'left', 
   'left', 
   'center', 
   'center', 
   'left') 
); 

$sql = "SELECT COUNT(*) as total_users 
FROM " . USERS_TABLE . " 
WHERE user_id <> " . ANONYMOUS; 

$result = $core->sql_query($sql, 'Unable to retrieve total users data'); 
$row = $core->sql_fetchrow($result); 

$total_users = $row['total_users']; 

$sql = "SELECT user_lang, COUNT(*) AS used_count 
FROM " . USERS_TABLE . " 
WHERE user_id <> " . ANONYMOUS . " 
AND user_lang <> '' 
GROUP BY user_lang 
ORDER BY used_count DESC 
LIMIT " . $core->return_limit; 

$result = $core->sql_query($sql, 'Unable to retrieve users language data'); 
$data = $core->sql_fetchrowset($result); 

$data[0]['user_lang'] = preg_replace("/^(.*?)_(.*)$/", "\\1 [ \\2 ]", $data[0]['user_lang']); 
$data[0]['user_lang'] = preg_replace("/\[(.*?)_(.*)\]/", "[ \\1 - \\2 ]", $data[0]['user_lang']); 
$data[0]['user_lang'] = ucwords($data[0]['user_lang']); 
$content->init_math('used_count', $data[0]['used_count'], $total_users); 

$core->set_data($data); 

$core->define_view('set_rows', array( 
   '$core->pre_defined()', 
   '$core->data(\'user_lang\')', 
   '$core->data(\'used_count\')', 
   '$core->pre_defined()', 
   '$core->pre_defined()') 
); 

$core->run_module(); 

?>