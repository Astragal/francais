<?php 
/***************************************************************************
*                            top_styles.pak
*                            --------------------
*     begin                :
*     copyright            : (C)      MJ
*     email                : ---
*
*     $Id: top_styles.pak, 2006/10/12 20:00:00 MJ Exp $
*          for Statistics MOD version 3.0.1beta3s and higher
****************************************************************************/
/***************************************************************************
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
****************************************************************************/

if ( !defined('IN_PHPBB') ) 
{ 
   die("Hacking attempt"); 
} 

// true == use db cache 
$core->start_module(true); 

$core->set_content('bars'); 

$core->set_view('rows', $core->return_limit); 
$core->set_view('columns', 5); 

$core->define_view('set_columns', array( 
   $core->pre_defined('rank'), 
   'stylename' => $lang['Style'], 
   'users' => $lang['Users'], 
   $core->pre_defined('percent'), 
   $core->pre_defined('graph')) 
); 

$content->percentage_sign = TRUE; 

$core->set_header($lang['module_name']); 

$core->assign_defined_view('align_rows', array( 
   'left', 
   'left', 
   'center', 
   'center', 
   'left') 
); 

$sql = "SELECT COUNT(*) as total_users 
FROM " . USERS_TABLE . " 
WHERE user_id <> " . ANONYMOUS; 

$result = $core->sql_query($sql, 'Unable to retrieve total users data'); 
$row = $core->sql_fetchrow($result); 

$total_users = $row['total_users']; 

$sql = "SELECT COUNT(u.user_style) AS used_count, t.themes_id, t.style_name 
FROM " . USERS_TABLE . " u, ". THEMES_TABLE ." t 
WHERE u.user_style = t.themes_id 
GROUP BY t.themes_id  
ORDER BY used_count DESC 
LIMIT " . $core->return_limit; 

$result = $core->sql_query($sql, 'Unable to retrieve themes data'); 
$data = $core->sql_fetchrowset($result); 
$content->init_math('used_count', $data[0]['used_count'], $total_users); 
$core->set_data($data); 

$core->define_view('set_rows', array( 
   '$core->pre_defined()', 
   '$core->generate_link(append_sid($phpbb_root_path . \'changestyle.php?s=\' . $core->data(\'themes_id\')), $core->data(\'style_name\'), \'target="_self"\')', 
   '$core->data(\'used_count\')', 
   '$core->pre_defined()', 
   '$core->pre_defined()') 
); 

$core->run_module(); 

?>