<?php
/***************************************************************************
*                            most_active_forums.pak
*                            --------------------
*     begin                :
*     copyright            : (C)      Wicher
*     email                : ---
*
*     $Id: most_active_forums.pak, 2007/01/11 01:40:00 Wicher Exp $
*          for Statistics MOD version 3.0.1beta3s and higher
****************************************************************************/
/***************************************************************************
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
****************************************************************************/

if ( !defined('IN_PHPBB') )
{
	die("Hacking attempt");
}

//
// Most Active Topics
//
$core->start_module(true);

$core->set_content('statistical');

$core->set_view('rows', $core->return_limit);
$core->set_view('columns', 6);

$core->define_view('set_columns', array(
	$core->pre_defined('rank'),
	'posts' => $lang['Posts'],
	'topics' => $lang['Topics'],
	'forum' => $lang['Forum'],
	'cat' => $lang['Category'],
	'desc' => $lang['Forum_desc'])
);

$core->set_header($lang['module_name']);

$core->assign_defined_view('align_rows', array(
	'left',
	'center',
	'center',
	'left',
	'left',
	'left')
);

$core->assign_defined_view('width_rows', array(
	'10%',
	'10%',
	'10%',
	'23%',
	'23%',
	'24%')
);

//FORUMS_TABLE: forum_id  cat_id  forum_name  forum_desc  forum_status  forum_posts  forum_topics 
//CATAGORIES_TABLE: cat_id  cat_title 
$current_time = time();
if ( $userdata['session_logged_in'] )
{
	if ($userdata['user_level'] == ADMIN)
	{
	$sql = 'SELECT f.forum_id, f.cat_id, f.forum_name, f.forum_desc, f.forum_posts, f.forum_topics, c.cat_title 
	FROM ' . FORUMS_TABLE . ' f, ' . CATEGORIES_TABLE . ' c 
	WHERE f.forum_status <> 1 
	AND f.cat_id = c.cat_id 
	ORDER BY f.forum_posts DESC 
	LIMIT ' . $core->return_limit;
	}
	else if ($userdata['user_level'] == MOD)
	{
	$sql = 'SELECT f.forum_id, f.cat_id, f.forum_name, f.forum_desc, f.forum_posts, f.forum_topics, c.cat_title 
	FROM ' . FORUMS_TABLE . ' f, ' . CATEGORIES_TABLE . ' c  
	WHERE f.forum_status <> 1 
	AND f.cat_id = c.cat_id 
	AND (f.auth_view <> 2) 
	AND (f.auth_read <> 2) 
	ORDER BY f.forum_posts DESC 
	LIMIT ' . $core->return_limit;
	}
	else if (($userdata['user_level'] != ADMIN)or($userdata['user_level'] != MOD))
	{
	$sql = 'SELECT f.forum_id, f.cat_id, f.forum_name, f.forum_desc, f.forum_posts, f.forum_topics, c.cat_title 
	FROM ' . FORUMS_TABLE . ' f, ' . CATEGORIES_TABLE . ' c  
	WHERE f.forum_status <> 1 
	AND f.cat_id = c.cat_id 
	AND (f.auth_view < 2) 
	AND (f.auth_read < 2) 
	ORDER BY f.forum_posts DESC 
	LIMIT ' . $core->return_limit;
	}
}
else
{
	$sql = 'SELECT f.forum_id, f.cat_id, f.forum_name, f.forum_desc, f.forum_posts, f.forum_topics, c.cat_title 
	FROM ' . FORUMS_TABLE . ' f, ' . CATEGORIES_TABLE . ' c  
	WHERE f.forum_status <> 1 
	AND f.cat_id = c.cat_id 
	AND (f.auth_view < 1) 
	AND (f.auth_read < 1) 
	ORDER BY f.forum_posts DESC 
	LIMIT ' . $core->return_limit;
}

$result = $core->sql_query($sql, 'Couldn\'t retrieve topic data');
$topic_data = $core->sql_fetchrowset($result);

$core->set_data($topic_data);

//
// Now this one could get a big beast
// We will explain the structure, no fear, but not now. :D
//
$core->define_view('set_rows', array(
	'$core->pre_defined()',
	'$core->data(\'forum_posts\')',
	'$core->data(\'forum_topics\')',
	'$core->generate_link(append_sid($phpbb_root_path . \'viewforum.php?f=\' . $core->data(\'forum_id\')), $core->data(\'forum_name\'), \'target="_top"\')',
	'$core->generate_link(append_sid($phpbb_root_path . \'index.php?c=\' . $core->data(\'cat_id\')), $core->data(\'cat_title\'), \'target="_top"\')',
	'$core->data(\'forum_desc\')'
	)
);

$core->run_module();

?>