<?php 
/***************************************************************************
*                            top_countries_ipcountryflags .pak
*                            --------------------
*     begin                :
*     copyright            : (C)      Wicher
*     email                : ---
*
*     $Id: top_countries_ipcountryflags .pak, 2007/01/26 15:41:00 Wicher Exp $
*          for Statistics MOD version 3.0.1beta3s and higher
****************************************************************************/
/***************************************************************************
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
****************************************************************************/

if ( !defined('IN_PHPBB') ) 
{ 
   die("Hacking attempt"); 
} 

// check if mod installed
$sql = "SELECT iso3661_1 FROM " . CF_ISO_TABLE ." LIMIT 1"; 
if (!$result = $db->sql_query($sql))
{
	$there=0;
}
else
{
	$there=1;
}
if ($there == 0)
{
	message_die(GENERAL_MESSAGE, $lang['Error_message']);
}

// true == use db cache 
$core->start_module(true); 

$core->set_content('bars'); 

$core->set_view('rows', $core->return_limit); 
$core->set_view('columns', 6); 

$core->define_view('set_columns', array( 
   $core->pre_defined('rank'), 
   'country' => $lang['country'], 
   'flag' => $lang['flag'],
   'users' => $lang['users'], 
   $core->pre_defined('percent'), 
   $core->pre_defined('graph')) 
); 

$content->percentage_sign = TRUE; 

$core->set_header($lang['module_name']); 

$core->assign_defined_view('align_rows', array( 
   'left', 
   'center', 
   'center', 
   'center', 
   'center', 
   'left') 
); 

$sql = "SELECT COUNT(*) as total_users 
FROM " . USERS_TABLE . " 
WHERE user_id <> " . ANONYMOUS . "
AND user_cf_iso3661_1 <> 'wo'"; 

$result = $core->sql_query($sql, 'Unable to retrieve total users data'); 
$row = $core->sql_fetchrow($result); 

$total_users = $row['total_users']; 

if ( $userdata['session_logged_in'] )
{
	include('./language/lang_'.$userdata['user_lang'].'/lang_extend_ip_cf.'.$phpEx);
}
else
{
	include('./language/lang_'.$board_config['default_lang'].'/lang_extend_ip_cf.'.$phpEx);
}

$sql = "SELECT DISTINCT(user_cf_iso3661_1), COUNT(user_cf_iso3661_1) AS user_count
FROM " . USERS_TABLE . " 
WHERE user_id <> " . ANONYMOUS . " 
AND user_cf_iso3661_1 <> 'wo' 
GROUP BY user_cf_iso3661_1 
ORDER BY user_count DESC 
LIMIT " . $core->return_limit; 

$result = $core->sql_query($sql, 'Unable to retrieve users language data'); 
$country_data = $core->sql_fetchrowset($result);

$data = array();
for ($i = 0; $i < $core->return_limit; $i++)
{
	if ($country_data[$i]['user_cf_iso3661_1'])
	{
		$data[] = array('country' => $lang['IP2Country'][$country_data[$i]['user_cf_iso3661_1']],
						 'flag' => '<img src="images/flags/small/'.$country_data[$i]['user_cf_iso3661_1'].'.png" width="14" height="9" alt="' . $lang['IP2Country'][$country_data[$i]['user_cf_iso3661_1']] . '" title="' . $lang['IP2Country'][$country_data[$i]['user_cf_iso3661_1']] . '" border="0" />',
						 'usercount' => $country_data[$i]['user_count']
						 );
	}
}

$content->init_math('usercount', $data[0]['usercount'], $total_users); 

$core->set_data($data); 

$core->define_view('set_rows', array( 
   '$core->pre_defined()', 
   '$core->data(\'country\')', 
   '$core->data(\'flag\')', 
   '$core->data(\'usercount\')', 
   '$core->pre_defined()', 
   '$core->pre_defined()') 
); 

$core->run_module(); 

?>