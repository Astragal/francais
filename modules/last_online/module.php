<?php

/***************************************************************************
*                            lastonline.pak
*                            --------------------
*     begin                :
*     copyright            : (C)      Wicher
*     email                : ---
*
*     $Id: lastonline.pak, 2006/12/08 11:39:00 Wicher Exp $
*          for Statistics MOD version 3.0.1beta3s and higher
****************************************************************************/
/***************************************************************************
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
****************************************************************************/

if ( !defined('IN_PHPBB') ) 
{ 
   die("Hacking attempt"); 
} 


// 
// Last Online 
// 
$core->start_module(true); 

$core->set_content('statistical'); 

$sql = "SELECT user_totaltime FROM " . USERS_TABLE . " ORDER BY user_totaltime DESC	LIMIT 1"; 
if (!$result = $db->sql_query($sql))
{
	$there=0;
}
else
{
	$there=1;
}
if ($there == 0)
{
	message_die(GENERAL_MESSAGE, $lang['Error_message']);
}

$core->set_view('rows', $core->return_limit); 
$core->set_view('columns', 3); 

$core->define_view('set_columns', array( 
   $core->pre_defined('rank'), 
   'date' => $lang['last_online'], 
   'user' => $lang['name']) 
); 

$core->set_header($lang['module_name']); 

$core->assign_defined_view('align_rows', array( 
   'left', 
   'center', 
   'left') 
); 

$core->assign_defined_view('width_rows', array( 
   '10%', 
   '50%', 
   '80%') 
); 



$sql = 'SELECT user_id, username, FROM_UNIXTIME(user_lastvisit + '.$userdata['user_timezone'].' * 3600) as lastvisit 
FROM ' . USERS_TABLE . ' 
WHERE user_id <> ' . ANONYMOUS . ' 
ORDER BY user_lastvisit DESC 
LIMIT ' . $core->return_limit; 

$result = $core->sql_query($sql, 'Couldn\'t retrieve topic data'); 
$topic_data = $core->sql_fetchrowset($result); 
$core->set_data($topic_data); 


// 
// Now this one could get a big beast 
// We will explain the structure, no fear, but not now. :D 
// 

$core->define_view('set_rows', array( 
   '$core->pre_defined()', 
   '$core->data(\'lastvisit\')', 
   '$core->generate_link(append_sid($phpbb_root_path . \'profile.php?mode=viewprofile&u=\' . $core->data(\'user_id\')), $core->data(\'username\'), \'target="_blank"\')') 
); 

//   '$core->create_date('.$board_config['default_dateformat'].', data(\'user_lastvisit\'), '.$board_config['board_timezone'].')', 


$core->run_module(); 

?>