<?php

/***************************************************************************
*                            age_statistics_terrafrost.pak
*                            --------------------
*     begin                :
*     copyright            : (C)      Wicher
*     email                : ---
*
*     $Id: age_statistics_terrafrost.pak, 2007/02/25 20:00:00 Wicher Exp $
*          for Statistics MOD version 3.0.1beta3s and higher
****************************************************************************/
/***************************************************************************
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
****************************************************************************/

if ( !defined('IN_PHPBB') )
{
	die("Hacking attempt");
}

//
// Age Statistics
//

$core->start_module(true);

$core->set_content('values');

//
// Age Statistics TerraFrost
//


if (!$board_config['bday_show'])
{
	$there=0;
}
else
{
	$there=1;
}
if ($there == 0)
{
	message_die(GENERAL_MESSAGE, $lang['Error_message']);
}

// Count users who filled in there age
$sql = "SELECT count(*) as pcount
		FROM " . USERS_TABLE . "
		WHERE user_birthday <> 999999";

$result = $db->sql_query($sql);
if (!$result)
{
	message_die(GENERAL_ERROR, "Could not find users.", "",__LINE__, __FILE__, $sql);
}
$usercount = $db->sql_fetchrow($result);
$sum_age = 0;

if ($usercount[pcount])
{
for($i = 0; $i < $usercount[pcount]; $i++)
{
	$sql = "SELECT user_birthday
			FROM " . USERS_TABLE . " u
			WHERE u.user_birthday <> 999999
            LIMIT " . $i . ",1";
	$result = $db->sql_query($sql);
		if ( !$result )
		{
			message_die(GENERAL_ERROR, "Could not find posts.", "",__LINE__, __FILE__, $sql);
		}
	$user_age = $db->sql_fetchrow($result);
   	$poster_age =realdate('Y',(time()/86400))- realdate ('Y',$user_age['user_birthday']);
   	if (date('md')<realdate('md',$user_age['user_birthday'])) $poster_age--;
    $sum_age = $sum_age + $poster_age;
}

// Young!
$sql = "SELECT max(user_birthday) as max
		FROM " . USERS_TABLE . "
   		WHERE user_birthday <> 999999";

$result = $db->sql_query($sql);
if (!$result)
{
	message_die(GENERAL_ERROR, "Could not find posts.", "",__LINE__, __FILE__, $sql);
}
$usermax = $db->sql_fetchrow($result);
$poster_max =realdate('Y',(time()/86400))- realdate ('Y',$usermax['max']);
if (date('md')<realdate('md',$usermax['max'])) $poster_max--;

$sql = "SELECT user_id,username
		FROM " . USERS_TABLE . "
   		WHERE user_birthday =". $usermax['max'];

$result = $db->sql_query($sql);
if ( !$result )
{
	message_die(GENERAL_ERROR, "Could not find posts.", "",__LINE__, __FILE__, $sql);
}
$usermaxid = $db->sql_fetchrow($result);

// Old!
$sql = "SELECT min(user_birthday) as min
		FROM " . USERS_TABLE . "
   		WHERE user_birthday <> 999999";

$result = $db->sql_query($sql);
if ( !$result )
{
	message_die(GENERAL_ERROR, "Could not find posts.", "",__LINE__, __FILE__, $sql);
}
$usermin = $db->sql_fetchrow($result);

$poster_min =realdate('Y',(time()/86400))- realdate ('Y',$usermin['min']);
if (date('md')<realdate('md',$usermin['min'])) $poster_min--;

$sql = "SELECT user_id,username
		FROM " . USERS_TABLE . "
   		WHERE user_birthday =". $usermin['min'];

$result = $db->sql_query($sql);
if ( !$result )
{
	message_die(GENERAL_ERROR, "Could not find users.", "",__LINE__, __FILE__, $sql);
}
$userminid = $db->sql_fetchrow($result);

$usermax = "<a href=". append_sid($phpbb_root_path . 'profile.php?mode=viewprofile&u=' . $usermaxid['user_id']) .">". $usermaxid['username'] ."</a>";
$usermin = "<a href=". append_sid($phpbb_root_path . 'profile.php?mode=viewprofile&u=' . $userminid['user_id']) .">". $userminid['username'] ."</a>";

$statistic_array = array($lang['Users_Age'],$lang['Average_Age'],$lang['Youngest_Member'],$lang['Youngest_Age'],$lang['Oldest_Member'],$lang['Oldest_Age']);
$value_array = array($usercount[pcount],round($sum_age/$usercount[pcount],2),$usermax,$poster_max,$usermin,$poster_min);
}
else
{
$statistic_array = array($lang['Users_Age'],$lang['Average_Age'],$lang['Youngest_Member'],$lang['Youngest_Age'],$lang['Oldest_Member'],$lang['Oldest_Age']);
$value_array = array('0','0','0','0','0','0');
}


$core->set_view('columns', 2);
$core->set_view('num_blocks', 2);
$core->set_view('value_order', 'left_right');
//$core->set_view('value_order', 'up_down');

$core->define_view('set_columns', array(
	'stats' => $lang['Statistic'],
	'value' => $lang['Value'])
);

$core->set_header($lang['module_name']);

$data = $core->assign_defined_view('value_array', array($statistic_array, $value_array));

$core->set_data($data);

$core->define_view('iterate_values', array());

$core->run_module();

// Add function mkrealdate for Birthday MOD
// the originate php "mktime()", does not work proberly on all OS, especially when going back in time
// before year 1970 (year 0), this function "mkrealtime()", has a mutch larger valid date range,
// from 1901 - 2099. it returns a "like" UNIX timestamp divided by 86400, so
// calculation from the originate php date and mktime is easy.
// mkrealdate, returns the number of day (with sign) from 1.1.1970.

function mkrealdate($day,$month,$birth_year)
{
	// range check months
	if ($month<1 || $month>12) return "error";
	// range check days
	switch ($month)
	{
		case 1: if ($day>31) return "error";break;
		case 2: if ($day>29) return "error";
			$epoch=$epoch+31;break;
		case 3: if ($day>31) return "error";
			$epoch=$epoch+59;break;
		case 4: if ($day>30) return "error" ;
			$epoch=$epoch+90;break;
		case 5: if ($day>31) return "error";
			$epoch=$epoch+120;break;
		case 6: if ($day>30) return "error";
			$epoch=$epoch+151;break;
		case 7: if ($day>31) return "error";
			$epoch=$epoch+181;break;
		case 8: if ($day>31) return "error";
			$epoch=$epoch+212;break;
		case 9: if ($day>30) return "error";
			$epoch=$epoch+243;break;
		case 10: if ($day>31) return "error";
			$epoch=$epoch+273;break;
		case 11: if ($day>30) return "error";
			$epoch=$epoch+304;break;
		case 12: if ($day>31) return "error";
			$epoch=$epoch+334;break;
	}
	$epoch=$epoch+$day;
	$epoch_Y=sqrt(($birth_year-1970)*($birth_year-1970));
	$leapyear=round((($epoch_Y+2) / 4)-.5);
	if (($epoch_Y+2)%4==0)
	{// curent year is leapyear
		$leapyear--;
		if ($birth_year >1970 && $month>=3) $epoch=$epoch+1;
		if ($birth_year <1970 && $month<3) $epoch=$epoch-1;
	} else if ($month==2 && $day>28) return "error";//only 28 days in feb.
	//year
	if ($birth_year>1970)
		$epoch=$epoch+$epoch_Y*365-1+$leapyear;
	else
		$epoch=$epoch-$epoch_Y*365-1-$leapyear;
	return $epoch;
}

// functions borrowed from Birtday hack by Niels
// Add function realdate for Birthday MOD
// the originate php "date()", does not work proberly on all OS, especially when going back in time
// before year 1970 (year 0), this function "realdate()", has a mutch larger valid date range,
// from 1901 - 2099. it returns a "like" UNIX date format (only date, related letters may be used, due to the fact that
// the given date value should already be divided by 86400 - leaving no time information left)
// a input like a UNIX timestamp divided by 86400 is expected, so
// calculation from the originate php date and mktime is easy.
// e.g. realdate ("m d Y", 3) returns the string "1 3 1970"

// UNIX users should replace this function with the below code, since this should be faster
//
//function realdate($date_syntax="Ymd",$date=0) 
//{ return create_date($date_syntax,$date*86400+1,0); }

function realdate($date_syntax="Ymd",$date=0)
{
	global $lang;
	$i=2;
	if ($date>=0)
	{
	 	return create_date($date_syntax,$date*86400+1,0);
	} else
	{
		$year= -(date%1461);
		$days = $date + $year*1461;
		while ($days<0)
		{
			$year--;
			$days+=365;
			if ($i++==3)
			{
				$i=0;
				$days++;
			}
		}
	}
	$leap_year = ($i==0) ? TRUE : FALSE;
	$months_array = ($i==0) ?
		array (0,31,60,91,121,152,182,213,244,274,305,335,366) :
		array (0,31,59,90,120,151,181,212,243,273,304,334,365);
	for ($month=1;$month<12;$month++)
	{
		if ($days<$months_array[$month]) break;
	}

	$day=$days-$months_array[$month-1]+1;
	//you may gain speed performance by remove som of the below entry's if they are not needed/used
	return strtr ($date_syntax, array(
		'a' => '',
		'A' => '',
		'\\d' => 'd',
		'd' => ($day>9) ? $day : '0'.$day,
		'\\D' => 'D',
		'D' => $lang['day_short'][($date-3)%7],
		'\\F' => 'F',
		'F' => $lang['month_long'][$month-1],
		'g' => '',
		'G' => '',
		'H' => '',
		'h' => '',
		'i' => '',
		'I' => '',
		'\\j' => 'j',
		'j' => $day,
		'\\l' => 'l',
		'l' => $lang['day_long'][($date-3)%7],
		'\\L' => 'L',
		'L' => $leap_year,
		'\\m' => 'm',
		'm' => ($month>9) ? $month : '0'.$month,
		'\\M' => 'M',
		'M' => $lang['month_short'][$month-1],
		'\\n' => 'n',
		'n' => $month,
		'O' => '',
		's' => '',
		'S' => '',
		'\\t' => 't',
		't' => $months_array[$month]-$months_array[$month-1],
		'w' => '',
		'\\y' => 'y',
		'y' => ($year>29) ? $year-30 : $year+70,
		'\\Y' => 'Y',
		'Y' => $year+1970,
		'\\z' => 'z',
		'z' => $days,
		'\\W' => '',
		'W' => '') );
}
// End add - Birthday MOD

?>