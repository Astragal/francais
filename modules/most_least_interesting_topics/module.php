<?php

/***************************************************************************
*                            most_least_interesting_topics.pak
*                            --------------------
*     begin                :
*     copyright            : (C)      Wicher
*     email                : ---
*
*     $Id: most_least_interesting_topics.pak, 2007/01/22 18:05:00 Wicher Exp $
*          for Statistics MOD version 3.0.1beta3s and higher
****************************************************************************/
/***************************************************************************
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
****************************************************************************/

if ( !defined('IN_PHPBB') ) 
{ 
   die("Hacking attempt"); 
} 


$core->start_module(true); 

$core->set_content('statistical'); 

$core->set_view('rows', $core->return_limit); 
$core->set_view('columns', 7); 


$core->define_view('set_columns', array( 
   $core->pre_defined('rank'), 
	'rate' => $lang['Rate'],
	'topic' => $lang['Topic'],
   'empty' => '&nbsp;',
   $core->pre_defined('rank'), 
	'rate2' => $lang['Rate'],
	'topic2' => $lang['Topic'])
); 

$core->set_header($lang['module_name']); 

$core->assign_defined_view('align_rows', array( 
   'left', 
   'center', 
   'left',
   'center', 
   'left', 
   'center', 
   'left') 
); 

$core->assign_defined_view('width_rows', array( 
   '', 
   '', 
   '30%',
   '', 
   '', 
   '', 
   '30%') 
); 



$current_time = time();
// ------------------------- BEGIN MOST-------------------------
if ( $userdata['session_logged_in'] )
{
	if ($userdata['user_level'] == ADMIN)
	{
	$sql = 'SELECT topic_id, forum_id, topic_title, topic_replies, topic_views, round(topic_views/(topic_replies + 1),2) AS k
	FROM ' . TOPICS_TABLE . ' 
	ORDER BY k DESC 
	LIMIT ' . $core->return_limit;
	}
	else if ($userdata['user_level'] == MOD)
	{
	$sql = 'SELECT t.topic_id, t.forum_id, t.topic_title, t.topic_replies, t.topic_views, round(t.topic_views/(t.topic_replies + 1),2) AS k
	FROM ' . TOPICS_TABLE . ' t, ' . FORUMS_TABLE . ' f  
	WHERE (t.topic_status <> 2) 
	AND (t.topic_time < '.$current_time.')
	AND (t.topic_replies > 0) 
	AND (t.forum_id = f.forum_id) 
	AND (f.auth_view <> 2) 
	AND (f.auth_read <> 2) 
	ORDER BY k DESC 
	LIMIT ' . $core->return_limit;
	}
	else if (($userdata['user_level'] != ADMIN)or($userdata['user_level'] != MOD))
	{
	$sql = 'SELECT t.topic_id, t.forum_id, t.topic_title, t.topic_replies, t.topic_views, round(t.topic_views/(t.topic_replies + 1),2) AS k
	FROM ' . TOPICS_TABLE . ' t, ' . FORUMS_TABLE . ' f  
	WHERE (t.topic_status <> 2) 
	AND (t.topic_time < '.$current_time.')
	AND (t.topic_replies > 0) 
	AND (t.forum_id = f.forum_id) 
	AND (f.auth_view < 2) 
	AND (f.auth_read < 2) 
	ORDER BY k DESC 
	LIMIT ' . $core->return_limit;
	}
}
else
{
	$sql = 'SELECT t.topic_id, t.forum_id, t.topic_title, t.topic_replies, t.topic_views, round(t.topic_views/(t.topic_replies + 1),2) AS k
	FROM ' . TOPICS_TABLE . ' t, ' . FORUMS_TABLE . ' f  
	WHERE (t.topic_status <> 2) 
	AND (t.topic_time < '.$current_time.')
	AND (t.topic_replies > 0) 
	AND (t.forum_id = f.forum_id) 
	AND (f.auth_view < 1) 
	AND (f.auth_read < 1) 
	ORDER BY k DESC 
	LIMIT ' . $core->return_limit;
}

$result = $core->sql_query($sql, 'Shit, that didn\'t work');

$most_count = $core->sql_numrows($result);
$most_data = $core->sql_fetchrowset($result);

// ------------------------- END MOST-------------------------

// ------------------------- BEGIN LEAST-------------------------
if ( $userdata['session_logged_in'] )
{
	if ($userdata['user_level'] == ADMIN)
	{
	$sql = 'SELECT topic_id, forum_id, topic_title, topic_replies, topic_views, round(topic_views/(topic_replies + 1),2) AS k
	FROM ' . TOPICS_TABLE . ' 
	ORDER BY k ASC 
	LIMIT ' . $core->return_limit;
	}
	else if ($userdata['user_level'] == MOD)
	{
	$sql = 'SELECT t.topic_id, t.forum_id, t.topic_title, t.topic_replies, t.topic_views, t.topic_views/(t.topic_replies + 1) AS k
	FROM ' . TOPICS_TABLE . ' t, ' . FORUMS_TABLE . ' f  
	WHERE (t.topic_status <> 2) 
	AND (t.topic_time < '.$current_time.')
	AND (t.topic_replies > 0) 
	AND (t.forum_id = f.forum_id) 
	AND (f.auth_view <> 2) 
	AND (f.auth_read <> 2) 
	ORDER BY k ASC 
	LIMIT ' . $core->return_limit;
	}
	else if (($userdata['user_level'] != ADMIN)or($userdata['user_level'] != MOD))
	{
	$sql = 'SELECT t.topic_id, t.forum_id, t.topic_title, t.topic_replies, t.topic_views, round(t.topic_views/(t.topic_replies + 1),2) AS k
	FROM ' . TOPICS_TABLE . ' t, ' . FORUMS_TABLE . ' f  
	WHERE (t.topic_status <> 2) 
	AND (t.topic_time < '.$current_time.')
	AND (t.topic_replies > 0) 
	AND (t.forum_id = f.forum_id) 
	AND (f.auth_view < 2) 
	AND (f.auth_read < 2) 
	ORDER BY k ASC 
	LIMIT ' . $core->return_limit;
	}
}
else
{
	$sql = 'SELECT t.topic_id, t.forum_id, t.topic_title, t.topic_replies, t.topic_views, round(t.topic_views/(t.topic_replies + 1),2) AS k
	FROM ' . TOPICS_TABLE . ' t, ' . FORUMS_TABLE . ' f  
	WHERE (t.topic_status <> 2) 
	AND (t.topic_time < '.$current_time.')
	AND (t.topic_replies > 0) 
	AND (t.forum_id = f.forum_id) 
	AND (f.auth_view < 1) 
	AND (f.auth_read < 1) 
	ORDER BY k ASC 
	LIMIT ' . $core->return_limit;
}

$result = $core->sql_query($sql, 'Shit, that didn\'t work');

$least_count = $core->sql_numrows($result);
$least_data = $core->sql_fetchrowset($result);

// ------------------------- END LEAST-------------------------

if ($most_count >= $least_count)
{
	$topiccount = $most_count;
}
else
{
	$topiccount = $least_count;
}

$topicdata = array();
for ($i = 0; $i < $topiccount; $i++)
{
	$topicdata[] = array('topicid1' => $most_data[$i]['topic_id'],
						 'topic_rate1' => $most_data[$i]['k'],
						 'topictitle1' => $most_data[$i]['topic_title'],
						 'divider' => '<img src="images/divider_left.gif"><img src="images/divider_mid.gif" width="80%" height="20"><img src="images/divider_right.gif">',
						 'topicid2' => $least_data[$i]['topic_id'],
						 'topic_rate2' => $least_data[$i]['k'],
						 'topictitle2' => $least_data[$i]['topic_title']
						 );
}

$core->set_data($topicdata); 


// 
// Now this one could get a big beast 
// We will explain the structure, no fear, but not now. :D 
// 
$core->define_view('set_rows', array( 
	'$core->pre_defined()',
	'$core->data(\'topic_rate1\')',
	'$core->generate_link(append_sid(\'viewtopic.php?t=\' . $core->data(\'topicid1\')), $core->data(\'topictitle1\'), \'target="_blank"\')',
   '$core->data(\'divider\')', 
	'$core->pre_defined()',
	'$core->data(\'topic_rate2\')',
	'$core->generate_link(append_sid(\'viewtopic.php?t=\' . $core->data(\'topicid2\')), $core->data(\'topictitle2\'), \'target="_blank"\')')
); 



$core->run_module(); 

?>