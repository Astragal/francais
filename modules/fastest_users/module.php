<?php
/***************************************************************************
*                            fastest_users.pak
*                            --------------------
*     begin                :
*     copyright            : (C)      Wicher
*     email                : ---
*
*     $Id: fastest_users.pak, 2006/10/12 20:00:00 Wicher Exp $
*          for Statistics MOD version 3.0.1beta3s and higher
****************************************************************************/
/***************************************************************************
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
****************************************************************************/

if ( !defined('IN_PHPBB') )
{
	die("Hacking attempt");
}

// true == use db cache
$core->start_module(true);

$core->set_content('bars');

$core->set_view('columns', 5);

$core->define_view('set_columns', array(
	$core->pre_defined('rank'),
	'username' => $lang['Username'],
	'dof' => $lang['days_on_forum'],
	'messperday' => $lang['messperday'],
	$core->pre_defined('graph'))
);

$content->percentage_sign = TRUE;

$core->set_header($lang['module_name']);

$core->assign_defined_view('align_rows', array(
	'left',
	'left',
	'center',
	'center',
	'left')
);

$sql = "SELECT SUM(user_posts) as total_posts FROM " . USERS_TABLE . " WHERE user_id <> " . ANONYMOUS;
$result = $core->sql_query($sql, 'Unable to retrieve users totalposts (fastest_users module)');
$row = $core->sql_fetchrow($result);
$total_posts = $row['total_posts'];


$currect_time = time();
$sql ="SELECT user_id, username, user_posts, user_regdate, 
(user_posts/(($currect_time - user_regdate)/ 86400)) rate, 
ROUND(($currect_time - user_regdate)/ 86400) time_on_forum
FROM " . USERS_TABLE .
" WHERE (user_id <> " .ANONYMOUS . " ) AND (user_posts > 0) ORDER BY rate DESC LIMIT " . $core->return_limit;

$result = $core->sql_query($sql, 'Unable to retrieve users data (fastest_users module)');
$user_count = $core->sql_numrows($result);
$user_data = $core->sql_fetchrowset($result);

$core->set_data($user_data);
$content->init_math('user_posts', $total_posts, 'time_on_forum');

$core->define_view('set_rows', array(
	'$core->pre_defined()',
	'$core->generate_link(append_sid($phpbb_root_path . \'profile.php?mode=viewprofile&u=\' . $core->data(\'user_id\')), $core->data(\'username\'), \'target="_blank"\')',
	'$core->data(\'time_on_forum\')',
	'round($core->data(\'rate\'), 2)',
	'$core->pre_defined()')
);

$core->run_module();

?>