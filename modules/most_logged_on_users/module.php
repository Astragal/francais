<?php
/***************************************************************************
*                            most_logged_on_users.pak
*                            --------------------
*     begin                :
*     copyright            : (C)      Wicher
*     email                : ---
*
*     $Id: most_logged_on_users.pak, 2006/11/14 09:00:00 Wicher Exp $
*          for Statistics MOD version 3.0.1beta3s and higher
****************************************************************************/
/***************************************************************************
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
****************************************************************************/

if ( !defined('IN_PHPBB') )
{
	die("Hacking attempt");
}

// true == use db cache
$core->start_module(true);

$core->set_content('bars');

$core->set_view('rows', $core->return_limit);
$core->set_view('columns', 8);

$core->define_view('set_columns', array(
	$core->pre_defined('rank'),
	'username' => $lang['Username'],
	'daysmember' => $lang['daysmember'],
	'totalforumtime' => $lang['totalforumtime'],
	'totallogins' => $lang['totallogins'],
	'visitedpages' => $lang['visitedpages'],
	$core->pre_defined('percent'),
	$core->pre_defined('graph')
	)
);

$content->percentage_sign = TRUE;

$core->set_header($lang['module_name']);

$core->assign_defined_view('align_rows', array(
	'left',
	'left',
	'center',
	'center',
	'center',
	'center',
	'center',
	'left')
);

// check if mod installed
$sql = "SELECT user_totaltime FROM " . USERS_TABLE . " ORDER BY user_totaltime DESC	LIMIT 1"; 
if (!$result = $db->sql_query($sql))
{
	$there=0;
}
else
{
	$there=1;
}
if ($there == 0)
{
	message_die(GENERAL_MESSAGE, $lang['Error_message']);
}

$sql = "SELECT SUM(user_totaltime) as total_time FROM " . USERS_TABLE . "
WHERE user_id <> " . ANONYMOUS;

if ( !($result = $stat_db->sql_query($sql)) )
{
	message_die(GENERAL_ERROR, 'Couldn\'t retrieve users data', '', __LINE__, __FILE__, $sql);
}

$row = $stat_db->sql_fetchrow($result);
$total_time = $row['total_time'];

$sql = 'SELECT user_id, username, user_totaltime, user_regdate, user_totallogon, user_dateformat, user_timezone, user_totalpages 
FROM ' . USERS_TABLE . ' 
WHERE (user_id <> ' . ANONYMOUS . ' ) AND (user_totaltime > 0) 
ORDER BY user_totaltime DESC LIMIT ' . $core->return_limit;

if ( !($result = $stat_db->sql_query($sql)) )
{
	message_die(GENERAL_ERROR, 'Couldn\'t retrieve users data', '', __LINE__, __FILE__, $sql);
}

$user_count = $core->sql_numrows($result);
$data = $core->sql_fetchrowset($result);

$content->init_math('user_totaltime', $data[0]['user_totaltime'], $total_time);
$core->set_data($data);

$core->define_view('set_rows', array(
	'$core->pre_defined()',
	'$core->generate_link(append_sid($phpbb_root_path . \'profile.php?mode=viewprofile&u=\' . $core->data(\'user_id\')), $core->data(\'username\'), \'target="_blank"\')',
	'create_date($core->data(\'user_dateformat\'), $core->data(\'user_regdate\'), $core->data(\'user_timezone\'))', 
	'make_hour($core->data(\'user_totaltime\'))',
	'$core->data(\'user_totallogon\')',
	'$core->data(\'user_totalpages\')',
	'$core->pre_defined()',
	'$core->pre_defined()'
));


$core->run_module();

?>