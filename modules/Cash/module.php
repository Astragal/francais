<?php

/***************************************************************************
*                            cashmod.pak
*                            --------------------
*     begin                :
*     copyright            : (C)      Wicher
*     email                : ---
*
*     $Id: cashmod.pak, 2006/10/12 15:07:00 Wicher Exp $
*          for Statistics MOD version 3.0.1beta3s and higher
****************************************************************************/
/***************************************************************************
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
****************************************************************************/

if ( !defined('IN_PHPBB') ) 
{ 
   die("Hacking attempt"); 
} 


// 
// Cash 
// 
$core->start_module(true); 

$core->set_content('statistical'); 
$core->set_view('rows', $core->return_limit); 

// check if mod installed
$sql = "SELECT cash_dbfield FROM " . $table_prefix . "cash ORDER BY cash_id DESC LIMIT 1"; 
if (!$result = $db->sql_query($sql))
{
	$there=0;
}
else
{
	$there=1;
}
if ($there == 0)
{
	message_die(GENERAL_MESSAGE, $lang['Error_message']);
}

$currencie_count = array();
$sql = "SELECT cash_dbfield, cash_name 
		FROM " . $table_prefix . "cash
		ORDER BY cash_order DESC";
$curresult = $core->sql_query($sql, 'Couldn\'t retrieve currencies'); 
do
{
	$currencies[] = $row;
}
while ($row = $db->sql_fetchrow($curresult));
$total_cur = count($currencies);

$core->set_view('columns', $total_cur+1); 

if ($total_cur == 2)
{
$core->define_view('set_columns', array( 
   $core->pre_defined('rank'), 
   'user' => $lang['username'],
   'currenc' => $currencies[1]['cash_name'])
   ); 

$core->set_header($lang['module_name']); 

$core->assign_defined_view('align_rows', array( 
   'left', 
   'center',
   'center') 
); 

$core->assign_defined_view('width_rows', array( 
   '10%', 
   '20%',
   '') 
); 

$sql = 'SELECT  user_id, username, '.$currencies[1]['cash_dbfield'].' as cur 
		FROM ' . USERS_TABLE . ' 
		WHERE user_id <> -1 
		ORDER BY '. $currencies[1]['cash_dbfield'] . ' DESC 
		LIMIT ' . $core->return_limit; 

$result = $core->sql_query($sql, 'Couldn\'t retrieve currencies'); 
$money = $core->sql_fetchrowset($result); 
$core->set_data($money); 

$core->define_view('set_rows', array( 
   '$core->pre_defined()', 
   '$core->generate_link(append_sid($phpbb_root_path . \'profile.php?mode=viewprofile&u=\' . $core->data(\'user_id\')), $core->data(\'username\'), \'target="_blank"\')',
   '$core->data(\'cur\')')
   ); 
}

if ($total_cur == 3)
{
$core->define_view('set_columns', array( 
   $core->pre_defined('rank'), 
   'user' => $lang['username'],
   'currenc' => $currencies[1]['cash_name'],
   'currenc2' => $currencies[2]['cash_name'])
   ); 

$core->set_header($lang['module_name']); 

$core->assign_defined_view('align_rows', array( 
   'left', 
   'center',
   'center',
   'center') 
); 

$core->assign_defined_view('width_rows', array( 
   '10%', 
   '20%',
   '35%',
   '35%') 
); 

$sql = 'SELECT  user_id, username, '.$currencies[1]['cash_dbfield'].' as cur, '.$currencies[2]['cash_dbfield'].' as cur2 
		FROM ' . USERS_TABLE . ' 
		WHERE user_id <> -1 
		ORDER BY '. $currencies[1]['cash_dbfield'] . ' DESC 
		LIMIT ' . $core->return_limit; 

$result = $core->sql_query($sql, 'Couldn\'t retrieve currencies'); 
$money = $core->sql_fetchrowset($result); 
$core->set_data($money); 

$core->define_view('set_rows', array( 
   '$core->pre_defined()', 
   '$core->generate_link(append_sid($phpbb_root_path . \'profile.php?mode=viewprofile&u=\' . $core->data(\'user_id\')), $core->data(\'username\'), \'target="_blank"\')',
   '$core->data(\'cur\')',
   '$core->data(\'cur2\')')
   ); 
}

if ($total_cur == 4)
{
$core->define_view('set_columns', array( 
   $core->pre_defined('rank'), 
   'user' => $lang['username'],
   'currenc' => $currencies[1]['cash_name'],
   'currenc2' => $currencies[2]['cash_name'],
   'currenc3' => $currencies[3]['cash_name'])
   ); 

$core->set_header($lang['module_name']); 

$core->assign_defined_view('align_rows', array( 
   'left', 
   'center',
   'center',
   'center',
   'center') 
); 

$core->assign_defined_view('width_rows', array( 
   '10%', 
   '15%',
   '25%',
   '25%',
   '25%') 
); 

$sql = 'SELECT  user_id, username, '.$currencies[1]['cash_dbfield'].' as cur, '.$currencies[2]['cash_dbfield'].' as cur2, '.$currencies[3]['cash_dbfield'].' as cur3 
		FROM ' . USERS_TABLE . ' 
		WHERE user_id <> -1 
		ORDER BY '. $currencies[1]['cash_dbfield'] . ' DESC 
		LIMIT ' . $core->return_limit; 

$result = $core->sql_query($sql, 'Couldn\'t retrieve currencies'); 
$money = $core->sql_fetchrowset($result); 
$core->set_data($money); 

$core->define_view('set_rows', array( 
   '$core->pre_defined()', 
   '$core->generate_link(append_sid($phpbb_root_path . \'profile.php?mode=viewprofile&u=\' . $core->data(\'user_id\')), $core->data(\'username\'), \'target="_blank"\')',
   '$core->data(\'cur\')',
   '$core->data(\'cur2\')',
   '$core->data(\'cur3\')')
   ); 
}

if ($total_cur == 5)
{
$core->define_view('set_columns', array( 
   $core->pre_defined('rank'), 
   'user' => $lang['username'],
   'currenc' => $currencies[1]['cash_name'],
   'currenc2' => $currencies[2]['cash_name'],
   'currenc3' => $currencies[3]['cash_name'],
   'currenc4' => $currencies[4]['cash_name'])
   ); 

$core->set_header($lang['module_name']); 

$core->assign_defined_view('align_rows', array( 
   'left', 
   'center',
   'center',
   'center',
   'center',
   'center') 
); 

$core->assign_defined_view('width_rows', array( 
   '10%', 
   '10%',
   '20%',
   '20%',
   '20%',
   '20%') 
); 

$sql = 'SELECT  user_id, username, '.$currencies[1]['cash_dbfield'].' as cur, '.$currencies[2]['cash_dbfield'].' as cur2, '.$currencies[3]['cash_dbfield'].' as cur3, '.$currencies[4]['cash_dbfield'].' as cur4 
		FROM ' . USERS_TABLE . ' 
		WHERE user_id <> -1 
		ORDER BY '. $currencies[1]['cash_dbfield'] . ' DESC 
		LIMIT ' . $core->return_limit; 

$result = $core->sql_query($sql, 'Couldn\'t retrieve currencies'); 
$money = $core->sql_fetchrowset($result); 
$core->set_data($money); 

$core->define_view('set_rows', array( 
   '$core->pre_defined()', 
   '$core->generate_link(append_sid($phpbb_root_path . \'profile.php?mode=viewprofile&u=\' . $core->data(\'user_id\')), $core->data(\'username\'), \'target="_blank"\')',
   '$core->data(\'cur\')',
   '$core->data(\'cur2\')',
   '$core->data(\'cur3\')',
   '$core->data(\'cur4\')')
   ); 
}

if ($total_cur == 6)
{
$core->define_view('set_columns', array( 
   $core->pre_defined('rank'), 
   'user' => $lang['username'],
   'currenc' => $currencies[1]['cash_name'],
   'currenc2' => $currencies[2]['cash_name'],
   'currenc3' => $currencies[3]['cash_name'],
   'currenc4' => $currencies[4]['cash_name'],
   'currenc5' => $currencies[4]['cash_name'])
   ); 

$core->set_header($lang['module_name']); 

$core->assign_defined_view('align_rows', array( 
   'left', 
   'center',
   'center',
   'center',
   'center',
   'center',
   'center') 
); 

$core->assign_defined_view('width_rows', array( 
   '10%', 
   '10%',
   '16%',
   '16%',
   '16%',
   '16%',
   '16%') 
); 

$sql = 'SELECT  user_id, username, '.$currencies[1]['cash_dbfield'].' as cur, '.$currencies[2]['cash_dbfield'].' as cur2, '.$currencies[3]['cash_dbfield'].' as cur3, '.$currencies[4]['cash_dbfield'].' as cur4, '.$currencies[5]['cash_dbfield'].' as cur5 
		FROM ' . USERS_TABLE . ' 
		WHERE user_id <> -1 
		ORDER BY '. $currencies[1]['cash_dbfield'] . ' DESC 
		LIMIT ' . $core->return_limit; 

$result = $core->sql_query($sql, 'Couldn\'t retrieve currencies'); 
$money = $core->sql_fetchrowset($result); 
$core->set_data($money); 

$core->define_view('set_rows', array( 
   '$core->pre_defined()', 
   '$core->generate_link(append_sid($phpbb_root_path . \'profile.php?mode=viewprofile&u=\' . $core->data(\'user_id\')), $core->data(\'username\'), \'target="_blank"\')',
   '$core->data(\'cur\')',
   '$core->data(\'cur2\')',
   '$core->data(\'cur3\')',
   '$core->data(\'cur4\')',
   '$core->data(\'cur5\')')
   ); 
}


$core->run_module(); 

?>