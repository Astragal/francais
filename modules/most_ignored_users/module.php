<?php
/***************************************************************************
*                            most_ignored_users.pak
*                            --------------------
*     begin                :
*     copyright            : (C)      Wicher
*     email                : ---
*
*     $Id: most_ignored_users.pak, 2006/10/12 20:00:00 Wicher Exp $
*          for Statistics MOD version 3.0.1beta3s and higher
****************************************************************************/
/***************************************************************************
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
****************************************************************************/

if ( !defined('IN_PHPBB') )
{
	die("Hacking attempt");
}

// true == use db cache
$core->start_module(true);

$core->set_content('statistical');

$core->set_view('rows', $core->return_limit);
$core->set_view('columns', 3);

$core->define_view('set_columns', array(
	$core->pre_defined('rank'),
	'username' => $lang['Username'],
	'ignores' => $lang['Ignores'])
);

$content->percentage_sign = TRUE;


$core->assign_defined_view('align_rows', array(
	'left',
	'left',
	'center')
);

// check if mod installed
$sql = "SELECT user_id FROM " . IGNORE_TABLE ." ORDER BY user_id DESC LIMIT 1"; 
if (!$result = $db->sql_query($sql))
{
	$there=0;
}
else
{
	$there=1;
}
if ($there == 0)
{
	message_die(GENERAL_MESSAGE, $lang['Error_message']);
}

$sql = "SELECT i.user_id, i.user_ignore, u.username FROM phpbb_ignore i, " . USERS_TABLE . " u 
		WHERE i.user_ignore = u.user_id
		ORDER BY i.user_ignore DESC";
$result = $core->sql_query($sql, 'Unable to retrieve number ignored users');
if ($row = $core->sql_fetchrow($result))
{
	do
	{
		$postrow[] = $row;
	}
	while ($row = $core->sql_fetchrow($result));
}
$total_ignoring_users = count($postrow);

if ($total_ignoring_users)
{
$count_ignoredby = 1;
for($i = 0; $i < count($postrow); $i++)
{
	$i_check = $i+1;
	if ($postrow[$i_check]['user_ignore'] == $postrow[$i]['user_ignore'])
	{
		$count_ignoredby++;
	}
	else
	{
		$postrowalmostfinal[]=array_merge(array("ignored_count"=>$count_ignoredby),$postrow[$i]);
		$count_ignoredby = 1;
	}
}

$igcount = array();
for($i = 0; $i < count($postrowalmostfinal); $i++)
{
	$igcount[] = $postrowalmostfinal[$i]['ignored_count'];
}
array_multisort($igcount, SORT_DESC, $postrowalmostfinal);

$totalusers = count($postrowalmostfinal);
if ($totalusers > $core->return_limit)
{
	for($f = 0; $f < $core->return_limit; $f++)
	{
		$postrowfinal[] = $postrowalmostfinal[$f];
	}
}
else
{
	for($f = 0; $f < $totalusers; $f++)
	{
		$postrowfinal[] = $postrowalmostfinal[$f];
	}
}
}
$core->set_header($lang['module_name'].' - '.$totalusers.$lang['ignored_by'].$total_ignoring_users.$lang['other']);

$core->set_data($postrowfinal);


$core->define_view('set_rows', array(
	'$core->pre_defined()',
	'$core->generate_link(append_sid($phpbb_root_path . \'profile.php?mode=viewprofile&u=\' . $core->data(\'user_ignore\')), $core->data(\'username\'), \'target="_blank"\')',
	'$core->data(\'ignored_count\')')
);

$core->run_module();

?>