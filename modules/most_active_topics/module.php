<?php
/***************************************************************************
*                            most_active_topics.pak
*                            --------------------
*     begin                :
*     copyright            : (C)      Wicher
*     email                : ---
*
*     $Id: most_active_topics.pak, 2007/01/11 01:40:00 Wicher Exp $
*          for Statistics MOD version 3.0.1beta3s and higher
****************************************************************************/
/***************************************************************************
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
****************************************************************************/

if ( !defined('IN_PHPBB') )
{
	die("Hacking attempt");
}

//
// Most Active Topics
//
$core->start_module(true);

$core->set_content('statistical');

$core->set_view('rows', $core->return_limit);
$core->set_view('columns', 3);

$core->define_view('set_columns', array(
	$core->pre_defined('rank'),
	'replies' => $lang['Replies'],
	'topic' => $lang['Topic'])
);

$core->set_header($lang['module_name']);

$core->assign_defined_view('align_rows', array(
	'left',
	'center',
	'left')
);

$core->assign_defined_view('width_rows', array(
	'',
	'20%',
	'')
);

$current_time = time();
if ( $userdata['session_logged_in'] )
{
	if ($userdata['user_level'] == ADMIN)
	{
	$sql = 'SELECT topic_id, topic_title, topic_replies 
	FROM ' . TOPICS_TABLE . ' 
	ORDER BY topic_replies DESC 
	LIMIT ' . $core->return_limit;
	}
	else if ($userdata['user_level'] == MOD)
	{
	$sql = 'SELECT t.topic_id, t.topic_title, t.topic_replies 
	FROM ' . TOPICS_TABLE . ' t, ' . FORUMS_TABLE . ' f  
	WHERE (t.topic_status <> 2) 
	AND (t.topic_time < '.$current_time.')
	AND (t.topic_replies > 0) 
	AND (t.forum_id = f.forum_id) 
	AND (f.auth_view <> 2) 
	AND (f.auth_read <> 2) 
	ORDER BY t.topic_replies DESC 
	LIMIT ' . $core->return_limit;
	}
	else if (($userdata['user_level'] != ADMIN)or($userdata['user_level'] != MOD))
	{
	$sql = 'SELECT t.topic_id, t.topic_title, t.topic_replies 
	FROM ' . TOPICS_TABLE . ' t, ' . FORUMS_TABLE . ' f  
	WHERE (t.topic_status <> 2) 
	AND (t.topic_time < '.$current_time.')
	AND (t.topic_replies > 0) 
	AND (t.forum_id = f.forum_id) 
	AND (f.auth_view < 2) 
	AND (f.auth_read < 2) 
	ORDER BY t.topic_replies DESC 
	LIMIT ' . $core->return_limit;
	}
}
else
{
	$sql = 'SELECT t.topic_id, t.topic_title, t.topic_replies, f.forum_id 
	FROM ' . TOPICS_TABLE . ' t, ' . FORUMS_TABLE . ' f  
	WHERE (t.topic_status <> 2) 
	AND (t.topic_time < '.$current_time.')
	AND (t.topic_replies > 0) 
	AND (t.forum_id = f.forum_id) 
	AND (f.auth_view < 1) 
	AND (f.auth_read < 1) 
	ORDER BY t.topic_replies DESC 
	LIMIT ' . $core->return_limit;
}

$result = $core->sql_query($sql, 'Couldn\'t retrieve topic data');
$topic_data = $core->sql_fetchrowset($result);

$core->set_data($topic_data);

//
// Now this one could get a big beast
// We will explain the structure, no fear, but not now. :D
//
$core->define_view('set_rows', array(
	'$core->pre_defined()',
	'$core->data(\'topic_replies\')',
	'$core->generate_link(append_sid($phpbb_root_path . \'viewtopic.php?t=\' . $core->data(\'topic_id\')), $core->data(\'topic_title\'), \'target="_blank"\')'
	)
);

$core->run_module();

?>