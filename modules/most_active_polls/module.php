<?php
/***************************************************************************
*                            most_active_polls.pak
*                            --------------------
*     begin                :
*     copyright            : (C)      Wicher
*     email                : ---
*
*     $Id: most_active_polls.pak, 2007/01/11 01:40:00 Wicher Exp $
*          for Statistics MOD version 3.0.1beta3s and higher
****************************************************************************/
/***************************************************************************
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
****************************************************************************/

if ( !defined('IN_PHPBB') )
{
	die("Hacking attempt");
}

// Most Active Polls
$core->start_module(true);

$core->set_content('statistical');

$core->set_view('rows', $core->return_limit);
$core->set_view('columns', 3);

$core->define_view('set_columns', array(
	$core->pre_defined('rank'),
	'votes' => $lang['Total_votes'],
	'topic' => $lang['Topic'])
);

$core->set_header($lang['module_name']);

$core->assign_defined_view('align_rows', array(
	'left',
	'center',
	'left')
);

$core->assign_defined_view('width_rows', array(
	'',
	'20%',
	'')
);


$current_time = time();
if ( $userdata['session_logged_in'] )
{
	if ($userdata['user_level'] == ADMIN)
	{
	$sql = 'SELECT SUM(a.vote_result) AS num_votes, b.topic_id, t.forum_id, t.topic_title 
	FROM ' . VOTE_RESULTS_TABLE . ' a, ' . VOTE_DESC_TABLE . ' b, ' . TOPICS_TABLE . ' t 
	WHERE a.vote_id = b.vote_id 
	AND b.topic_id = t.topic_id 
	GROUP BY a.vote_id 
	ORDER BY num_votes DESC 
	LIMIT ' . $core->return_limit;
	}
	else if ($userdata['user_level'] == MOD)
	{
	$sql = 'SELECT SUM(a.vote_result) AS num_votes, b.topic_id, t.forum_id, t.topic_title 
	FROM ' . VOTE_RESULTS_TABLE . ' a, ' . VOTE_DESC_TABLE . ' b, ' . TOPICS_TABLE . ' t, ' . FORUMS_TABLE . ' f 
	WHERE (t.forum_id = f.forum_id) 
	AND (t.topic_id = b.topic_id) 
	AND (t.topic_status <> 2) 
	AND (t.topic_time < '.$current_time.')
	AND (f.auth_view <> 2) 
	AND (f.auth_read <> 2) 
	AND (a.vote_id = b.vote_id) 
	GROUP BY a.vote_id 
	ORDER BY num_votes DESC 
	LIMIT ' . $core->return_limit;
	}
	else if (($userdata['user_level'] != ADMIN)or($userdata['user_level'] != MOD))
	{
	$sql = 'SELECT SUM(a.vote_result) AS num_votes, b.topic_id, t.forum_id, t.topic_title 
	FROM ' . VOTE_RESULTS_TABLE . ' a, ' . VOTE_DESC_TABLE . ' b, ' . TOPICS_TABLE . ' t, ' . FORUMS_TABLE . ' f 
	WHERE (t.forum_id = f.forum_id) 
	AND (t.topic_id = b.topic_id) 
	AND (t.topic_status <> 2) 
	AND (t.topic_time < '.$current_time.')
	AND (f.auth_view < 2) 
	AND (f.auth_read < 2) 
	AND (a.vote_id = b.vote_id) 
	GROUP BY a.vote_id 
	ORDER BY num_votes DESC 
	LIMIT ' . $core->return_limit;
	}
}
else
{
	$sql = 'SELECT SUM(a.vote_result) AS num_votes, b.topic_id, t.forum_id, t.topic_title 
	FROM ' . FORUMS_TABLE . ' f, ' . TOPICS_TABLE . ' t, ' . VOTE_RESULTS_TABLE . ' a, ' . VOTE_DESC_TABLE . ' b 
	WHERE (t.forum_id = f.forum_id) 
	AND (t.topic_id = b.topic_id) 
	AND (t.topic_status <> 2) 
	AND (t.topic_time < '.$current_time.')
	AND (f.auth_view < 1) 
	AND (f.auth_read < 1) 
	AND (a.vote_id = b.vote_id) 
	GROUP BY a.vote_id 
	ORDER BY num_votes DESC 
	LIMIT ' . $core->return_limit;
}

$result = $core->sql_query($sql, 'Couldn\'t retrieve topic data');
$poll_data = $core->sql_fetchrowset($result);

$core->set_data($poll_data);

//
// Now this one could get a big beast
// We will explain the structure, no fear, but not now. :D
//
$core->define_view('set_rows', array(
	'$core->pre_defined()',
	'$core->data(\'num_votes\')',
	'$core->generate_link(append_sid($phpbb_root_path . \'viewtopic.php?t=\' . $core->data(\'topic_id\')), $core->data(\'topic_title\'), \'target="_blank"\')'
	));

$core->run_module();

?>