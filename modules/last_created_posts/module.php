<?php
/***************************************************************************
*                            last_created_posts.pak
*                            --------------------
*     begin                :
*     copyright            : (C)      Wicher
*     email                : ---
*
*     $Id: last_created_posts.pak, 2007/01/11 01:40:00 Wicher Exp $
*          for Statistics MOD version 3.0.1beta3s and higher
****************************************************************************/
/***************************************************************************
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
****************************************************************************/

if ( !defined('IN_PHPBB') )
{
	die("Hacking attempt");
}

//
// Last Created Posts
//

$core->start_module(true);

$core->set_content('statistical');

$core->set_view('rows', $core->return_limit);
$core->set_view('columns', 5);

$core->define_view('set_columns', array(
	$core->pre_defined('rank'),
	'post' => $lang['post'],
	'start_date' => $lang['start_date'],
	'door' => $lang['by'],
	'in' => $lang['posted_in'])
);

$core->set_header($lang['module_name']);

$core->assign_defined_view('align_rows', array(
	'left',
	'center',
	'left',
	'left',
	'left')
);

$core->assign_defined_view('width_rows', array(
	'5%',
	'20%',
	'20%',
	'',
	'')
);



if ( $userdata['session_logged_in'] )
{
	if ($userdata['user_level'] == ADMIN)
	{
	$sql = 'SELECT p.post_id, p.post_time, pt.post_subject, t.topic_id, t.topic_title, u.username, u.user_timezone, u.user_dateformat 
	FROM ' . POSTS_TABLE . ' p, ' . POSTS_TEXT_TABLE . ' pt, ' . TOPICS_TABLE . ' t, ' . USERS_TABLE . ' u  
	WHERE p.post_id = topic_last_post_id
	AND p.poster_id = u.user_id
	AND pt.post_id = p.post_id
	ORDER BY p.post_time DESC 
	LIMIT ' . $core->return_limit;
	}
	else if ($userdata['user_level'] == MOD)
	{
	$sql = 'SELECT t.topic_id, t.topic_title, p.post_id, p.post_time, pt.post_subject, u.username, u.user_timezone, u.user_dateformat 
	FROM ' . TOPICS_TABLE . ' t, ' . POSTS_TABLE . ' p, ' . POSTS_TEXT_TABLE . ' pt, ' . FORUMS_TABLE . ' f, ' . USERS_TABLE . ' u  
	WHERE p.post_id = topic_last_post_id
	AND p.poster_id = u.user_id
	AND pt.post_id = p.post_id
	AND (t.topic_status <> 2) 
	AND (t.topic_time < '.$current_time.')
	AND (t.forum_id = f.forum_id) 
	AND (f.auth_view <> 2) 
	AND (f.auth_read <> 2) 
	ORDER BY p.post_time DESC 
	LIMIT ' . $core->return_limit;
	}
	else if (($userdata['user_level'] != ADMIN)or($userdata['user_level'] != MOD))
	{
	$sql = 'SELECT t.topic_id, t.topic_title, p.post_id, p.post_time, pt.post_subject, u.username, u.user_timezone, u.user_dateformat 
	FROM ' . TOPICS_TABLE . ' t, ' . POSTS_TABLE . ' p, ' . POSTS_TEXT_TABLE . ' pt, ' . FORUMS_TABLE . ' f, ' . USERS_TABLE . ' u  
	WHERE p.post_id = topic_last_post_id
	AND p.poster_id = u.user_id
	AND pt.post_id = p.post_id
	AND (t.topic_status <> 2) 
	AND (t.topic_time < '.$current_time.')
	AND (t.forum_id = f.forum_id) 
	AND (f.auth_view < 2) 
	AND (f.auth_read < 2) 
	ORDER BY p.post_time DESC 
	LIMIT ' . $core->return_limit;
	}
}
else
{
	$sql = 'SELECT t.topic_id, t.topic_title, p.post_id, p.post_time, pt.post_subject, u.username, u.user_timezone, u.user_dateformat, f.forum_id 
	FROM ' . TOPICS_TABLE . ' t, ' . POSTS_TABLE . ' p, ' . POSTS_TEXT_TABLE . ' pt, ' . FORUMS_TABLE . ' f, ' . USERS_TABLE . ' u  
	WHERE p.post_id = topic_last_post_id
	AND p.poster_id = u.user_id
	AND pt.post_id = p.post_id
	AND (t.topic_status <> 2) 
	AND (t.topic_time < '.$current_time.')
	AND (t.forum_id = f.forum_id) 
	AND (f.auth_view < 1) 
	AND (f.auth_read < 1) 
	ORDER BY p.post_time DESC 
	LIMIT ' . $core->return_limit;
}

$result = $core->sql_query($sql, 'Couldn\'t retrieve post data');
$post_data = $core->sql_fetchrowset($result);

$core->set_data($post_data);

//
// Now this one could get a big beast
// We will explain the structure, no fear, but not now. :D
//
$core->define_view('set_rows', array(
	'$core->pre_defined()',
	'$core->generate_link(append_sid($phpbb_root_path . \'viewtopic.php?p=\' . $core->data(\'post_id\').$lang["hekje"].$core->data(\'post_id\')), $core->data(\'post_id\'), \'target="_blank"\')." ".$core->generate_link(append_sid($phpbb_root_path . \'viewtopic.php?p=\' . $core->data(\'post_id\').$lang["hekje"].$core->data(\'post_id\')), $core->data(\'post_subject\'), \'target="_blank"\')',
	'create_date($core->data(\'user_dateformat\'), $core->data(\'post_time\'), $core->data(\'user_timezone\'))', 
	'$core->data(\'username\')',
	'$core->generate_link(append_sid($phpbb_root_path . \'viewtopic.php?t=\' . $core->data(\'topic_id\')), $core->data(\'topic_title\'), \'target="_blank"\')')
);

$core->run_module();

?>