<!-- view:values -->

<table border="0" cellpadding="0" cellspacing="0" class="forumline" width="100%"> 
	<tr> 
		<td class="catHead" colspan="{NUM_COLUMNS}"> 
			<table width="100%">
				<tr>
					<td align="center" width="{TOP_WIDTH}"><span class="gensmall"><a href="#pagetop">{L_TOP}</a></span></td>
					<td align="center"><span class="cattitle">{MODULE_NAME}</span></td>
				</tr>
			</table
		</td> 
	</tr> 
	<tr>
		<td>
			<table cellpadding="1" cellspacing="1" width="100%">
				<tr>    
					<!-- BEGIN column -->
					<th colspan="1" class=<!-- IF column.FIRST_COLUMN -->"thCornerL"<!-- ELSEIF column.LAST_COLUMN -->"thCornerR"<!-- ELSE -->"thTop"<!-- ENDIF --> align="center" width="{column.WIDTH}%"><strong>{column.VALUE}</strong></th>
					<!-- END column -->
				</tr> 
				<!-- BEGIN row -->
				<tr>
					<!-- BEGIN row_column -->
					<td class=<!-- IF row.row_column.ROW is even -->"row2"<!-- ELSE -->"row1"<!-- ENDIF --> align="{row.row_column.ALIGN}" width="{row.row_column.WIDTH}%"><span class="gensmall">{row.row_column.VALUE}</span></td>
					<!-- END row_column -->
				</tr> 
				<!-- END row -->
			</table>
		</td>
	</tr>
</table> 
