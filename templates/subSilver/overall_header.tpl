<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html dir="{S_CONTENT_DIRECTION}" xmlns:fb="http://ogp.me/ns/fb#" xmlns:g="https://plus.google.com/" xmlns:gcse="https://cse.google.com/">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta http-equiv="Content-Type" content="text/html; charset={S_CONTENT_ENCODING}">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta name="keywords" content="linguistics, languages, forum, french, foreignisms" lang="en">
<meta name="description" content="Discussion board on the French language" lang="en">
<meta name="keywords" content="linguistique, langues, forums, fran�ais, x�nismes" lang="fr">
<meta name="description" content="Forum de discussion sur la langue fran�aise" lang="fr">
<meta name="keywords" content="linguistica, lingue, forum, francese, forestierismi" lang="it">
<meta name="description" content="Forum di discussione sulla lingua francese" lang="it">
<meta property="og:title" content="{PAGE_TITLE} :: {SITENAME}" >
<meta property="og:image" content="{SITE_URL}templates/subSilver/images/logo_phpBB_og.gif" >
<link rel="alternate" type="application/rss+xml" title="Flux RSS" href="rss.php">
{META}
{NAV_LINKS}
{CANONICAL_LINK}
<title>{PAGE_TITLE} :: {SITENAME}</title>
<link rel="icon" href="images/favicon.ico">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet" href="templates/subSilver/subSilver-0.0.2.min.css?v={ASSETS_VERSION}">
<link type="text/css" rel="stylesheet" href="templates/subSilver/common.min.css?v={ASSETS_VERSION}">
<!-- BEGIN switch_bfcache_refresh -->
<script type="text/javascript" src="/bfrefresh.min.js"></script>
<!-- END switch_bfcache_refresh -->
<!-- BEGIN switch_alt_css -->
<link type="text/css" rel="stylesheet" href="templates/subSilver/alt.min.css?v={ASSETS_VERSION}">
<!-- END switch_alt_css -->
<!-- BEGIN switch_enable_pm_popup -->
<script language="Javascript" type="text/javascript">
<!--
	if ( {PRIVATE_MESSAGE_NEW_FLAG} )
	{
		window.open('{U_PRIVATEMSGS_POPUP}', '_phpbbprivmsg', 'HEIGHT=225,resizable=yes,WIDTH=400');
	}
//-->
</script>
<!-- END switch_enable_pm_popup -->

<script type="text/javascript">
<!--
window.___gcfg = { lang: '{L_SEARCH_INTERFACE_FULL}' };
//-->
</script>
<script src="http://www.google.com/jsapi?key={GOOGLE_API_KEY}" type="text/javascript"></script>
<!-- BEGIN switch_change_style -->
<style type="text/css">{GLOBAL_FONT_SIZE_CSS}{POST_FONT_SIZE_CSS}{CONTENT_MAX_WIDTH_CSS}</style>
<!-- END switch_change_style -->
<script>
	(function() {
		var cx = '{GOOGLE_CSE_KEY}';
		var gcse = document.createElement('script');
		gcse.type = 'text/javascript';
		gcse.async = true;
        gcse.src = 'https://cse.google.com/cse.js?cx=' + cx + '&language={L_SEARCH_INTERFACE}&encoding={S_CONTENT_ENCODING}';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(gcse, s);
	})();
</script>
{GOOGLE_ADSENSE_SCRIPT}
</head>
<body bgcolor="{T_BODY_BGCOLOR}" text="{T_BODY_TEXT}" link="{T_BODY_LINK}" vlink="{T_BODY_VLINK}">
<!-- BEGIN switch_admin_announcement -->
<div class="bodyline" style="padding:8px"><img src="templates/subSilver/images/announcement.png" style="margin-right:10px" alt="Annonce"><span class="gen" style="color:#069">{ADMIN_ANNOUNCEMENT}</span></div>
<!-- END switch_admin_announcement -->
<a name="top"></a>
<table width="100%" cellspacing="0" cellpadding="10" border="0" align="center">
	<tr> 
		<td class="bodyline"><table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tr> 
				<td style="vertical-align: top;"><a href="{U_INDEX}"><img src="{LOGO}" border="0" alt="{L_INDEX}" vspace="1" /></a></td>
				<td align="center" width="100%" valign="middle"><span class="maintitle">{SITENAME}</span><br /><span class="gen">{SITE_DESCRIPTION}<br />&nbsp; </span>
					<div>
						<a href="{U_FAQ}" class="mainmenu"><img src="templates/subSilver/images/icon_mini_faq.gif" width="12" height="13" border="0" alt="{L_FAQ}" hspace="3"/>{L_FAQ}</a>
						<a href="{U_MEMBERLIST}" class="mainmenu"><img src="templates/subSilver/images/icon_mini_members.gif" width="12" height="13" border="0" alt="{L_MEMBERLIST}" hspace="3"/>{L_MEMBERLIST}</a>
						<a href="{U_GROUP_CP}" class="mainmenu"><img src="templates/subSilver/images/icon_mini_groups.gif" width="12" height="13" border="0" alt="{L_USERGROUPS}" hspace="3"/>{L_USERGROUPS}</a>
						<a href="notifications.php" class="mainmenu"><img src="templates/subSilver/images/icon_mini_feed.gif" width="12" height="13" border="0" alt="Param�trer les notifications de nouveaux messages" hspace="3"/>Notifications</a>
						<!-- BEGIN switch_user_logged_out -->
						<a href="{U_REGISTER}" class="mainmenu"><img src="templates/subSilver/images/icon_mini_register.gif" width="12" height="13" border="0" alt="{L_REGISTER}" hspace="3"/>{L_REGISTER}</a>
						<!-- END switch_user_logged_out -->
						<div id="mainmenusearch">
							<gcse:searchbox-only resultsUrl="{U_GSEARCH}" enableAutoComplete="true" enableHistory="true" enableOrderBy="true" linkTarget="_self" safeSearch="off"><a href="{U_GSEARCH}" class="mainmenu"><img src="templates/subSilver/images/icon_mini_search.gif" width="12" height="13" border="0" alt="{L_SEARCH}" hspace="3">{L_SEARCH}</a></gcse:searchbox-only>
						</div>
					</div>
					<div>
						<a href="{U_PROFILE}" class="mainmenu"><img src="templates/subSilver/images/icon_mini_profile.gif" width="12" height="13" border="0" alt="{L_PROFILE}" hspace="3"/>{L_PROFILE}</a>
						<!-- BEGIN switch_user_logged_in -->
						<a href="{U_BOOKMARKS}" class="mainmenu"><img src="templates/subSilver/images/icon_mini_bookmark.gif" width="12" height="13" border="0" alt="{L_BOOKMARKS}" hspace="3"/>{L_BOOKMARKS}</a>
						<!-- END switch_user_logged_in -->
						<a href="dons.php" class="mainmenu"><img src="templates/subSilver/images/icon_mini_donate.png" border="0" hspace="3"/>Dons</a>
						<a href="boutique.php" class="mainmenu"><img src="templates/subSilver/images/icon_mini_cart.png" border="0" hspace="3"/>Boutique</a>
						<a href="{U_PRIVATEMSGS}" class="mainmenu"><img src="templates/subSilver/images/icon_mini_message.gif" width="12" height="13" border="0" hspace="3"/>{PRIVATE_MESSAGE_INFO}</a>
						<a href="{U_LOGIN_LOGOUT}" class="mainmenu"><img src="templates/subSilver/images/icon_mini_login.gif" width="12" height="13" border="0" alt="{L_LOGIN_LOGOUT}" hspace="3"/>{L_LOGIN_LOGOUT}</a>
					</div>
				</td>
			</tr>
		</table>

		<br />

