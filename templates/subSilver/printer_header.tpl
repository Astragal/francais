<!-- DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" -->
<!-- <html xmlns="http://www.w3.org/1999/xhtml"> -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html dir="{S_CONTENT_DIRECTION}">
<head>
<meta http-equiv="Content-Type" content="text/html; charset={S_CONTENT_ENCODING}"  />
<meta http-equiv="Content-Style-Type" content="text/css" />
<style type="text/css">
<!--
-->
</style>
{META}
<title>{SITENAME} :: {PAGE_TITLE}</title>
<link rel="icon" href="images/favicon.ico">
<style type="text/css">
<!--
@import url('//brick.a.ssl.fastly.net/Linux+Libertine:400,400i,700,700i:f');
body, .postbody, .quote, .gen, .genmed { 
	background-color: white;
	font-family: 'Linux Libertine', {T_FONTFACE1}, Georgia, Serif !important;
	font-size: 10pt !important;
	line-height: normal !important;
	-webkit-font-feature-settings: "kern", "liga";
	-moz-font-feature-settings: "kern", "liga";
	-o-font-feature-settings: "kern", "liga";
	font-feature-settings: "kern", "liga";
    text-rendering: optimizeLegibility;
}

/* The largest text used in the index page title and toptic title etc. */
.maintitle	{
			font-weight: bold; font-size: 22pt; font-family: 'Linux Libertine', {T_FONTFACE1}, Georgia, Serif;
			text-decoration: none; line-height : 120%; color : {T_BODY_TEXT};
}
a.maintitle:hover { text-decoration: underline; }

/* General text */
.gen { font-size : {T_FONTSIZE3}pt; }
.genmed { font-size : {T_FONTSIZE2}pt; }
.gensmall { font-size : {T_FONTSIZE1}pt; }
.gen,.genmed,.gensmall { color : black; }
a.gen,a.genmed,a.gensmall { color: black; text-decoration: none; }
a.gen:hover,a.genmed:hover,a.gensmall:hover	{ color: {T_BODY_HLINK}; text-decoration: underline; }

/* Forum category titles */
.cattitle		{ font-weight: bold; font-size: {T_FONTSIZE3}pt ; letter-spacing: 1pt; color : black}
a.cattitle		{ text-decoration: none; color : black; }
a.cattitle:hover{ text-decoration: underline; }

/* Forum title: Text and link to the forums used in: index.php */
.forumlink		{ font-weight: bold; font-size: {T_FONTSIZE3}pt; color : black; }
a.forumlink 	{ text-decoration: none; color : black; }
a.forumlink:hover{ text-decoration: underline; color : black; }

/* Used for the navigation text, (Page 1,2,3 etc) and the navigation bar when in a forum */
.nav			{ font-weight: bold; font-size: {T_FONTSIZE2}pt; color : black;}
a.nav			{ text-decoration: none; color : black; }
a.nav:hover		{ text-decoration: underline; }

/* titles for the topics: could specify viewed link colour too */
.topictitle			{ font-weight: bold; font-size: {T_FONTSIZE2}pt; color : black; }
a.topictitle:link   { text-decoration: none; color : black; }
a.topictitle:visited { text-decoration: none; color : black; }
a.topictitle:hover	{ text-decoration: underline; color : {T_BODY_HLINK}; }

/* Name of poster in viewmsg.php and viewtopic.php and other places */
.name			{ font-size : {T_FONTSIZE2}pt; color : black;}

/* Location, number of posts, post date etc */
.postdetails		{ font-size : {T_FONTSIZE1}pt; color : black; }

/* The content of the posts (body of text) */
.postbody { font-size : {T_FONTSIZE3}pt; line-height: 18pt}
a.postlink:link	{ text-decoration: none; color : black }
a.postlink:visited { text-decoration: none; color : black; }
a.postlink:hover { text-decoration: underline; color : {T_BODY_HLINK}}

a[href^="http://"]:after, a[href^="https://"]:after
{
	content:" {" attr(href) "}";
	font-size:90%
}
a[href^="javascript:"]:after, a[href^="#"]:after, .genmed a:after
{
	content:""
}

/* Quote & Code blocks */
.code { 
	font-family: {T_FONTFACE3}; font-size: {T_FONTSIZE2}pt; color: black;
	background-color: white; border: black; border-style: solid;
	border-left-width: 1pt; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt
}

.quote {
	font-family: {T_FONTFACE1}; font-size: {T_FONTSIZE2}pt; color: black; line-height: 125%;
	background-color: white; border: black; border-style: solid;
	border-left-width: 1pt; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt
}

/* Sizes */
.size1 {
	font-size: 1pt;
}
.size2 {
	font-size: 2pt;
}
.size3 {
	font-size: 3pt;
}
.size4 {
	font-size: 4pt;
}
.size5 {
	font-size: 5pt;
}
.size6 {
	font-size: 6pt;
}
.size7 {
	font-size: 7pt;
}
.size8 {
	font-size: 8pt;
}
.size9 {
	font-size: 9pt;
}
.size10 {
	font-size: 10pt;
}
.size11 {
	font-size: 10pt;
}
.size12 {
	font-size: 10pt;
}
.size13 {
	font-size: 10pt;
}
.size14 {
	font-size: 10pt;
}
.size15 {
	font-size: 10pt;
}
.size16 {
	font-size: 10pt;
}
.size17 {
	font-size: 10pt;
}
.size18 {
	font-size: 10pt;
}
.size19 {
	font-size: 10pt;
}
.size20 {
	font-size: 10pt;
}

/* Copyright and bottom info */
.copyright		{ font-size: {T_FONTSIZE1}pt; font-family: {T_FONTFACE1}; color: black; letter-spacing: -1pt;}
a.copyright		{ color: black; text-decoration: none;}
a.copyright:hover { color: {T_BODY_TEXT}; text-decoration: underline;}
.copyright { display: none;}


/* The text input fields background colour */
input.post, textarea.post, select {
	background-color : white;
}

input { text-indent : 2pt; }

/* This is the line in the posting page which shows the rollover
  help line. This is actually a text box, but if set to be the same
  colour as the background no one will know ;)
*/
.helpline { background-color: #00ffff; border-style: none; }

@media print
{
	.noprint { display: none; }
}


/* Import the fancy styles for IE only (NS4.x doesn't use the @import function) */
@import url("templates/subSilver/formIE.css"); 
-->
</style>
</head>

<body bgcolor="white" text="black" link="black" vlink="black">
<span class="gen"><a name="top"></a></span>
