<div><a href="{U_INDEX}" class="nav">{L_INDEX}</a>
    <h1>Notifications</h1>
    <p>Vous pouvez recevoir des notifications lorsque de nouveaux messages sont publi�s sur le forum.<br>
        Vous avez le choix entre plusieurs types de notifications.</p>
    <h2><img src="templates/subSilver/images/emails.png">�tre notifi� par courriel</h2>
    <h3>�tre notifi� pour un sujet</h3>
    <p>Ouvrez le sujet et cliquez sur ��Surveiller ce sujet��. Pour surveiller automatiquement les sujets o� vous avez
        particip�, cocher ��Toujours m'avertir des r�ponses�� dans votre profil.</p>
    <h3>�tre notifi� pour tous les sujets d'un forum</h3>
    <p>Ouvrez le forum concern� et cliquez sur ��Surveiller ce forum�� en bas de la page.</p>
    <h3>Recevoir un r�sum� des messages r�cents</h3>
    <p>Abonnez-vous � la liste de diffusion suivante pour recevoir un r�sum� quotidien�:
        <a href="http://eepurl.com/cVCPeb">http://eepurl.com/cVCPeb</a>
    </p>
    <p>Ou cliquez sur <a href="digests.php">R�sum�s</a> (un compte sur ce forum est requis).</p>
    <h2><img class="mrs" src="templates/subSilver/images/feed.png">S'abonner au flux RSS</h2>
    <p>Le flux RSS du forum <i>Fran�ais notre belle langue</i> contient, entre autres, les titres, contenus et liens des
        messages les plus r�cents.</p>
    <p>L'URL du flux est�: <a href="rss.php">http://www.achyra.org/francais/rss.php</a></p>
    <h3>Comment s'abonner au flux�?</h3>
    <p>Copier l'URL du flux dans votre <a href="https://fr.wikipedia.org/wiki/Agr%C3%A9gateur#Logiciels">agr�gateur
            RSS</a>
        (RSSOwl, par exemple). Vous pouvez aussi utiliser un agr�gateur RSS en ligne, comme Netvibes.<br>
        Le logiciel interrogera le serveur pour r�cup�rer les derniers messages publi�s sur le forum.</p>
</div>
