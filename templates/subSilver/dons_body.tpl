<div>
    <a href="{U_INDEX}" class="nav">{L_INDEX}</a>
    <h1>Faire un don pour le forum <i>Fran�ais notre belle langue</i></h1>
    <p>Si vous le souhaitez, vous pouvez faire un don pour nous aider � financer l'h�bergement de ce forum.<br>Le don peut �tre fait avec
        <i>PayPal</i> ou <i>Le pot commun</i>.<span id="cochon"></span>
    </p>
    <div class="box">
        <img src="templates/subSilver/images/logo_paypal.png" alt="PayPal" style="margin-bottom:20px;">
        <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" title="Faire un don avec�� PayPal��">
            <input type="hidden" name="cmd" value="_s-xclick">
            <input type="hidden" name="hosted_button_id" value="UZ9DDJNGJPF58">
            <input type="image" src="https://www.paypalobjects.com/fr_FR/FR/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal, le r�flexe s�curit� pour payer en ligne">
            <img alt="" border="0" src="https://www.paypalobjects.com/fr_FR/i/scr/pixel.gif" width="1" height="1">
        </form>
    </div>
    <div class="box">
        <a href="https://www.lepotcommun.fr/pot/5lnzdn3p" title="Faire un don avec�� Le pot commun��"><img src="templates/subSilver/images/logo_lepotcommun.png" alt="Faire un don avec Le pot commun"></h2>
        </a>
    </div>
</div>
<img src="templates/subSilver/images/cochon.png" style="display:none;">