var __g_c = (function (doc) {
    'use strict';

    const COOKIES = {
            global_font_size: 'global_fontSize',
            post_font_size: 'post_fontSize',
            expire: 31536000, // 1 an = 365 * 24 * 60 * 60 = 31536000
            content_max_width: 'content_maxWidth',
            path: '/francais',
        },
        DEFAULT_FONT_SIZE = 16, // px
        DEFAULT_POST_FONT_SIZE = 12, // px
        DEFAULT_POST_LINE_HEIGHT = 18; // px

    var rootElement = doc.documentElement;

    /*
     Consentement aux cookies
     https://cookieconsent.insites.com
     https://www.cnil.fr/fr/cookies-traceurs-que-dit-la-loi
     */
    (function () {
        var cookieconsentName = 'cookieconsent',
            cookieconsentStatusName = cookieconsentName + '_status';

        if (docCookies.hasItem(cookieconsentName)) {
            if (!docCookies.hasItem(cookieconsentStatusName)) {
                docCookies.setItem(cookieconsentStatusName, 'dismiss', COOKIES.expire);
            }
            docCookies.removeItem(cookieconsentName);
        }
        else if (!docCookies.hasItem(cookieconsentStatusName)) {
            loadScript('//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js');
            loadCSS('//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css');

            window.addEventListener("load", function () {
                window.cookieconsent.initialise({
                    palette: {
                        popup: {
                            background: "#EFEFEF",
                            text: "#006699"
                        },
                        button: {
                            background: "#006699",
                            text: "#FFA34F"
                        }
                    },
                    theme: "edgeless",
                    content: {
                        message: "En poursuivant votre navigation sur ce site, vous acceptez l'utilisation de cookies.",
                        dismiss: "J'accepte",
                        link: "En savoir plus",
                        href: "http://www.allaboutcookies.org/fr/"
                    },
                    cookie: {
                        path: COOKIES.path
                    }
                });

                docCookies.setItem(cookieconsentName, 1);
            });
        }
    }());

    /*
     S'il y a un bloqueur de publicit�...
     */
    (function () {
        if (typeof __g_adblock === 'undefined' && !docCookies.hasItem('adblockMsg')) {
            var div = doc.createElement('div');

            div.innerHTML =
                'Cher visiteur, il semble que vous utilisez un bloqueur de publicit�. L\'h�bergement de ce forum est financ� par les dons et la publicit�. Si vous souhaitez soutenir <i>Fran�ais notre belle langue</i>, vous avez la possibilit� de faire un don ou d\'acheter des produits chez nos partenaires. Pour cela, visitez les pages <a href="http://www.achyra.org/francais/dons.php">Faire un don</a> et <a href="http://www.achyra.org/francais/boutique.php">Boutique</a>. Merci et bonne visite.<hr>Cliquez sur le bouton suivant pour ne plus afficher ce message. <input type="button" class="button1" value="J\'ai compris" />';

            div.className = 'adblock';

            var btn = div.querySelector('input');
            btn.addEventListener('click', function () {
                docCookies.setItem('adblockMsg', '', COOKIES.expire);
                this.value = 'Ce message ne s\'affichera plus�!';
            });

            doc.body.appendChild(div);
        }
    }());

    function loadScript(src) {
        var el = doc.createElement('script');
        el.type = 'text/javascript';
        el.src = src;
        doc.head.appendChild(el);
    }

    function loadCSS(href) {
        var el = doc.createElement('link');
        el.type = 'text/css';
        el.rel = 'stylesheet';
        el.href = href;
        doc.head.appendChild(el);
    }

    // http://perfectionkills.com/instanceof-considered-harmful-or-how-to-write-a-robust-isarray/
    function isString(obj) {
        return Object.prototype.toString.call(obj) === '[object String]';
    }

    function isNumber(obj) {
        return Object.prototype.toString.call(obj) === '[object Number]';
    }

    function getGlobalFontSize() {
        return parseInt(window.getComputedStyle(rootElement).fontSize);
    }

    function setGlobalFontSize(size) {
        rootElement.style.fontSize = size + 'px';
        if (size != DEFAULT_FONT_SIZE) {
            docCookies.setItem(COOKIES.global_font_size, size, COOKIES.expire);
        }
        else {
            docCookies.removeItem(COOKIES.global_font_size);
        }
    }

    function getPostFontSize() {
        var size = docCookies.getItem(COOKIES.post_font_size);

        // si aucun cookie ou cookie incorrect, on retourne la taille par d�faut
        if (!isString(size) || !size.match(/^(\d+)$/)) {
            return DEFAULT_POST_FONT_SIZE;
        }

        return parseInt(size);
    }

    function setPostFontSize(size) {
        var fontSize = size / DEFAULT_FONT_SIZE, // rem
            lineHeight = (DEFAULT_POST_LINE_HEIGHT + size - DEFAULT_POST_FONT_SIZE) / DEFAULT_FONT_SIZE; // rem

        var elems = doc.querySelectorAll('span.postbody');
        for (var i = 0; i < elems.length; i++) {
            elems[i].style = 'font-size:' + fontSize + 'rem;line-height:' + lineHeight + 'rem;';
        }

        if (size != DEFAULT_POST_FONT_SIZE) {
            docCookies.setItem(COOKIES.post_font_size, size, COOKIES.expire);
        }
        else {
            docCookies.removeItem(COOKIES.post_font_size);
        }
    }

    function setContentMaxWidth(width) {
        var elTable = doc.querySelector('body > table');

        width = parseInt(width);

        if (width > 1) {
            docCookies.setItem(COOKIES.content_max_width, width, COOKIES.expire);
            elTable.style = 'margin:0 auto;max-width:' + width;
        }
        else {
            docCookies.removeItem(COOKIES.content_max_width);
            elTable.style = 'max-width:100%;';
        }
    }

    function a2a_getShareButtons(size) {
        return '<div class="a2a_kit a2a_kit_size_' + size + ' a2a_default_style">' +
            '<a class="a2a_button_facebook"></a>' +
            '<a class="a2a_button_twitter"></a>' +
            '<a class="a2a_button_google_plus"></a>' +
            '<a class="a2a_button_pinterest"></a>' +
            '<a class="a2a_dd" href="https://www.addtoany.com/share"></a>' +
            '</div>';
    }

    return {
        showInfoBox: function (message) {
            var infoBox = document.getElementById('infoBox');
            infoBox.innerHTML = '<p>' + message + '</p>';
            infoBox.style.display = 'block';
        },

        increaseGlobalFontSize: function () {
            setGlobalFontSize(getGlobalFontSize() + 1);
            return false;
        },

        decreaseGlobalFontSize: function () {
            setGlobalFontSize(getGlobalFontSize() - 1);
            return false;
        },

        resetGlobalFontSize: function () {
            setGlobalFontSize(DEFAULT_FONT_SIZE);
            return false;
        },

        increasePostFontSize: function () {
            setPostFontSize(getPostFontSize() + 1);
            return false;
        },

        decreasePostFontSize: function () {
            setPostFontSize(getPostFontSize() - 1);
            return false;
        },

        resetPostFontSize: function () {
            setPostFontSize(DEFAULT_POST_FONT_SIZE);
            return false;
        },

        toggleContentMaxWidth: function (width) {
            var cookie = docCookies.getItem(COOKIES.content_max_width);

            if (width === undefined) {
                // ondblclick
                width = prompt('Entrez une taille (en pixels)�:', '1000');
            }
            else if (cookie !== null || !isNumber(width)) {
                // remise � z�ro
                width = '';
            }

            setContentMaxWidth(width);
            return false;
        },

        share: function (el, postID) {
            if (el.dataset.a2a !== undefined) {
                return false;
            }

            window.a2a_config = window.a2a_config || {};

            var url = 'http://www.achyra.org/francais/';

            if (isNumber(postID)) {
                url += 'viewtopic.php?p=' + postID + '#' + postID;
                var parentTd = el.parentNode.parentNode;
                parentTd.insertAdjacentHTML('beforebegin', '<tr><td colspan="2">' + a2a_getShareButtons(20) + '</td></tr>');
                window.a2a_config.linkname = 'Fran�ais notre belle langue\n' +
                    parentTd.textContent.match(/Sujet du message : (?:Re: )?\s*(.+)\s*/)[1];
            }
            else {
                el.insertAdjacentHTML('afterend', a2a_getShareButtons(32));
                window.a2a_config.linkname = 'Fran�ais notre belle langue\nLieu de discussion consacr� � la langue fran�aise';
            }

            window.a2a_config.linkurl = url;
            if (!window.a2a_init) {
                loadScript('//static.addtoany.com/menu/page.js');
            }
            else {
                a2a_init('page');
            }

            el.dataset.a2a = '';

            return false;
        },

        toggleAds: function () {
            if (docCookies.getItem('noAds') != 1) {
                docCookies.setItem('noAds', 1, COOKIES.expire);
            } else {
                docCookies.removeItem('noAds');
            }

            doc.location.reload();
            return false;
        }
    };
})(document);
