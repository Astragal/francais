	<h1>{L_PAGE_TITLE}</h1>

	<p>{L_ADMIN_EXPLAIN}</p>

	<script type="text/javascript"> 

	<!-- Hide Javascript from validators

	function validate_fields (site_url_field, custom_ss_field, date_format, html_encoding, text_encoding, users_per_page, send_key, summary_date)
	{
		
		if (validate_site_url (site_url_field) == false)
		{
			return false;
		}
		if (validate_custom_template (custom_ss_field) == false)
		{
			return false;
		}
		if (validate_field_not_blank (date_format) == false)
		{
			return false;
		}
		if (validate_field_not_blank (html_encoding) == false)
		{
			return false;
		}
		if (validate_field_not_blank (text_encoding) == false)
		{
			return false;
		}
		if (validate_numeric (users_per_page) == false)
		{
			return false;
		}
		if (validate_field_not_blank (send_key) == false)
		{
			return false;
		}
		if (validate_field_not_blank (summary_date) == false)
		{
			return false;
		}
		return true;
	}
	
	function validate_site_url (field)
	{
		if (((field.value.indexOf("http://") == 0) || (field.value.indexOf("https://") == 0)) && (field.value.length >7) && (field.value.lastIndexOf("/",(field.value.length-1)) == (field.value.length-1)))
		{
			return true;
		}
		else
		{
			alert("{L_INVALID_SITE_URL}");
			return false;
		}
	}
		
	function validate_field_not_blank (field)
	{
		if (field.value.length != 0)
		{
			return true;
		}
		else
		{
			alert(field.name + ' {L_MAY_NOT_BE_BLANK}');
			return false;
		}
	}
		
	function validate_custom_template (field)
	{
		if (field.value.indexOf("templates/") == 0)
		{
			return true;
		}
		else
		{
			alert("{L_INVALID_TEMPLATE_NAME}");
			return false;
		}
	}
		
	function validate_numeric (field)
	{
		if (isNaN(field.value))
		{
			alert(field.name + ' {L_NOT_A_NUMBER}');
			return false;
		}
		else if (field.value < 0)
		{
			alert(field.name + ' {L_NOT_NEGATIVE}');
			return false;
		}
		else
		{
			return true;
		}
	}
		
	function enable_disable_radio_controls (checkbox, radio_1, radio_2)
	{
		// This disables the radio buttons for an associated checkbox if the checkbox is unchecked
		
		if (checkbox.checked == true)
		{
			radio_1.disabled = false;
			radio_2.disabled = false;
		}
		else
		{
			radio_1.disabled = true;
			radio_2.disabled = true;
		}
	}
	
	function enable_disable_text_box (checkbox, textbox)
	{
		// This disables the radio buttons for an associated checkbox if the checkbox is unchecked
		
		if (checkbox.checked == true)
		{
			textbox.disabled = false;
		}
		else
		{
			textbox.disabled = true;
		}
	}
	
	// End hiding Javascript -->
	</script>

      <form name="digest_settings_form" action="{S_POST_ACTION}" method="post" onsubmit="return validate_fields (site_url, custom_ss_path, date_format, html_encoding, text_encoding, users_per_page, send_key, summary_date);">
        <input type="hidden" name="digest_settings" value="1" />

        <table width="99%" cellpadding="4" cellspacing="1" border="0" align="center" class="forumline"> 
          <tr>
            <th class="thHead" colspan="2">{L_DIGEST_SETTING_DEFAULTS}</th>
          </tr>
          <tr>
            <td class="row1"><span class="gen"><label for="site_url">{L_SITE_URL}</label></span></td>
            <td class="row2">
              <input type="text" name="site_url" id="site_url" value="{SITE_URL}" size="60" maxlength="255" />
            </td>
          </tr>
          <tr>
            <td class="row1"><span class="gen"><label for="autosubscribe">{L_AUTOSUBSCRIBE_USERS}</label></span></td>
            <td class="row2">
              <input type="checkbox" name="autosubscribe" id="autosubscribe"{AUTOSUBSCRIBED_CHECKED} onclick="enable_disable_radio_controls(this, autosubscribedaily1, autosubscribedaily2);"/>
            </td>
          </tr>
          <tr>
            <td class="row1"><span class="gen">{L_AUTOSUBSCRIBE_DAILY_USERS}</span></td>
            <td class="row2">
              <input type="radio" name="autosubscribedaily" id="autosubscribedaily1" {AUTOSUBSCRIBE_DAILY_CHECKED} {ENABLE_DISABLE} value="DAY" /><span class="gen"><label for="autosubscribedaily1">{L_DAILY}</label></span><br />
              <input type="radio" name="autosubscribedaily" id="autosubscribedaily2" {AUTOSUBSCRIBE_WEEKLY_CHECKED} {ENABLE_DISABLE} value="WEEK" /><span class="gen"><label for="autosubscribedaily2">{L_WEEKLY}</label></span><br />
            </td>
          </tr>
          <tr>
            <td class="row1"><span class="gen"><label for="day_of_week">{L_WEEKLY_DAY_OF_WEEK}</label></span></td>
            <td class="row2">
              <select name="day_of_week" id="day_of_week">
                <option value="0"{SUNDAY_SELECTED}>{L_SUNDAY}</option>
                <option value="1"{MONDAY_SELECTED}>{L_MONDAY}</option>
                <option value="2"{TUESDAY_SELECTED}>{L_TUESDAY}</option>
                <option value="3"{WEDNESDAY_SELECTED}>{L_WEDNESDAY}</option>
                <option value="4"{THURSDAY_SELECTED}>{L_THURSDAY}</option>
                <option value="5"{FRIDAY_SELECTED}>{L_FRIDAY}</option>
                <option value="6"{SATURDAY_SELECTED}>{L_SATURDAY}</option>
              </select>
            </td>
          </tr>
          <tr>
            <td class="row1"><span class="gen"><label for="use_custom_ss">{L_USE_CUSTOM_STYLESHEET}</label></span></td>
            <td class="row2">
              <input type="checkbox" name="use_custom_ss" id="use_custom_ss"{USE_CUSTOM_STYLESHEET_CHECKED} onclick="enable_disable_text_box(this, custom_ss_path);"/>
            </td>
          </tr>
          <tr>
            <td class="row1"><span class="gen"><label for="custom_ss_path">{L_CUSTOM_STYLESHEET_PATH}</label></span></td>
            <td class="row2">
              <input type="text" name="custom_ss_path" id="custom_ss_path" value="{CUSTOM_STYLESHEET_PATH}" size="60" maxlength="255" {TEXTBOX_ENABLE_DISABLE}/>
            </td>
          </tr>
          <tr>
            <td class="row1"><span class="gen"><label for="date_format">{L_DIGEST_DATE_FORMAT}</label></span></td>
            <td class="row2">
              <input type="text" name="date_format" id="date_format" value="{DIGEST_DATE_FORMAT}" size="20" />
            </td>
          </tr>
          <tr>
            <td class="row1"><span class="gen"><label for="use_encodings">{L_USE_ENCODINGS}</label></span></td>
            <td class="row2">
              <input type="checkbox" name="use_encodings" id="use_encodings" onclick="enable_disable_text_box(this, html_encoding); enable_disable_text_box(this, text_encoding);" />
            </td>
          </tr>
          <tr>
            <td class="row1"><span class="gen"><label for="html_encoding">{L_HTML_ENCODING}</label></span></td>
            <td class="row2">
              <input type="text" name="html_encoding" id="html_encoding" value="{HTML_ENCODING}" disabled="disabled" size="20" />
            </td>
          </tr>
          <tr>
            <td class="row1"><span class="gen"><label for="text_encoding">{L_TEXT_ENCODING}</label></span></td>
            <td class="row2">
              <input type="text" name="text_encoding" id="text_encoding" value="{TEXT_ENCODING}" disabled="disabled" size="20" />
            </td>
          </tr>
          <tr>
            <td class="row1"><span class="gen"><label for="users_per_page">{L_USERS_PER_PAGE}</label></span></td>
            <td class="row2">
              <input type="text" name="users_per_page" id="users_per_page" value="{USERS_PER_PAGE}" size="3" />
            </td>
          </tr>
          <tr>
            <td class="row1"><span class="gen"><label for="req_send_key">{L_REQUIRE_SEND_KEY}</label></span></td>
            <td class="row2">
              <input type="checkbox" name="req_send_key" id="req_send_key"{REQUIRE_SEND_KEY} onclick="enable_disable_text_box(this, send_key);" />
            </td>
          </tr>
          <tr>
            <td class="row1"><span class="gen"><label for="send_key">{L_SEND_KEY}</label></span></td>
            <td class="row2">
              <input type="text" name="send_key" id="send_key" value="{SEND_KEY}" size="60" maxlength="255" {ENABLE_DISABLE_KEY} />
            </td>
          </tr>
          <tr>
            <td class="row1"><span class="gen"><label for="show_summary">{L_SHOW_SUMMARY}</label></span></td>
            <td class="row2">
              <input type="checkbox" name="show_summary" id="show_summary"{SHOW_SUMMARY} onclick="enable_disable_radio_controls(this, sformat1, sformat2); enable_disable_text_box (this, summary_date);" />
            </td>
          </tr>
          <tr>
            <td class="row1"><span class="gen">{L_SUMMARY_FORMAT}</span></td>
            <td class="row2">
              <input type="radio" name="sformat" id="sformat1" {SUMMARY_HTML_CHECKED} value="HTML"{SUMMARY_HTML_ENABLED} {SUMMARY_HTML_HIDDEN} /> <span class="gen"><label for="sformat1">{L_SUMMARY_HTML}</label></span><br />
              <input type="radio" name="sformat" id="sformat2" {SUMMARY_TEXT_CHECKED} value="TEXT"{SUMMARY_TEXT_ENABLED} {SUMMARY_TEXT_HIDDEN} /> <span class="gen"><label for="sformat2">{L_SUMMARY_TEXT}</label></span>
            </td>
          </tr>
          <tr>
            <td class="row1"><span class="gen"><label for="summary_date">{L_SUMMARY_DATE_DISPLAY_FORMAT}</label></span></td>
            <td class="row2">
              <input type="text" name="summary_date" id="summary_date" value="{SUMMARY_DATE_DISPLAY_FORMAT}" {ENABLE_DISABLE_SUMMARY_DATE} size="20" />
            </td>
          </tr>
          <tr>
			<td colspan="2" align="center" class="catBottom" height="28"><span class="gen"><button type="submit" class="mainoption">{L_SUBMIT2}</button>&nbsp;<button type="reset" class="liteoption">{L_RESET}</button></span></td>
          </tr>
        </table>
	  </form>
	  <br />
	  
      <form name="digest_defaults_form" action="{S_POST_ACTION}" method="post">
        <input type="hidden" name="digest_defaults" value="1" />

        <table width="99%" cellpadding="4" cellspacing="1" border="0" align="center" class="forumline">
          <tr>
            <th class="thHead" colspan="2">{L_DIGEST_USER_DEFAULTS}</th>
          </tr>
          <tr>
            <td class="row1"><span class="gen">{L_DIGEST_TYPE}</span></td>
            <td class="row2">
              <input type="radio" name="digest_type" id="digest_type1" {DAY_CHECKED} value="DAY" /><span class="gen"><label for="digest_type1">{L_DAILY}</label></span><br />
              <input type="radio" name="digest_type" id="digest_type2" {WEEK_CHECKED} value="WEEK" /><span class="gen"><label for="digest_type2">{L_WEEKLY}</label></span><br />
            </td>
          </tr>
          <tr>
            <td class="row1"><span class="gen">{L_FORMAT}</span></td>
            <td class="row2">
              <input type="radio" name="format" id="format1" {HTML_CHECKED} value="HTML" /> <span class="gen"><label for="format1">{L_HTML_ENH}</label></span><br />
              <input type="radio" name="format" id="format2" {PLAIN_HTML_CHECKED} value="PHTM" /> <span class="gen"><label for="format2">{L_HTML_PLAIN}</label></span><br />
              <input type="radio" name="format" id="format3" {TEXT_CHECKED} value="TEXT" /> <span class="gen"><label for="format3">{L_TEXT}</label></span>
            </td>
          </tr>
          <tr>
            <td class="row1"><span class="gen">{L_SHOW_TEXT}</span></td>
            <td class="row2">
              <input type="radio" name="show_text" id="show_text1" {SHOW_TEXT_YES_CHECKED} value="YES" /> <span class="gen"><label for="show_text1">{L_YES}</label></span>
              <input type="radio" name="show_text" id="show_text2" {SHOW_TEXT_NO_CHECKED} value="NO" /> <span class="gen"><label for="show_text2">{L_NO}</label></span>
            </td>
          </tr>
          <tr>
            <td class="row1"><span class="gen">{L_SHOW_MINE}</span></td>
            <td class="row2">
              <input type="radio" name="show_mine" id="show_mine1" {SHOW_MINE_YES_CHECKED} value="YES" /> <span class="gen"><label for="show_mine1">{L_YES}</label></span>
              <input type="radio" name="show_mine" id="show_mine2" {SHOW_MINE_NO_CHECKED} value="NO" /> <span class="gen"><label for="show_mine2">{L_NO}</label></span>
            </td>
          </tr>
          <tr>
            <td class="row1"><span class="gen">{L_NEW_ONLY}</span></td>
            <td class="row2">
              <input type="radio" name="new_only" id="new_only1" {NEW_ONLY_YES_CHECKED} value="TRUE" /> <span class="gen"><label for="new_only1">{L_YES}</label></span>
              <input type="radio" name="new_only" id="new_only2" {NEW_ONLY_NO_CHECKED} value="FALSE" /> <span class="gen"><label for="new_only2">{L_NO}</label></span>
            </td>
          </tr>
          <tr>
            <td class="row1"><span class="gen">{L_SEND_ON_NO_MESSAGES}</span></td>
            <td class="row2">
              <input type="radio" name="send_on_no_messages" id="send_on_no_messages1" {SEND_ON_NO_MESSAGES_YES_CHECKED} value="YES" /> <span class="gen"><label for="send_on_no_messages1">{L_YES}</label></span>
              <input type="radio" name="send_on_no_messages" id="send_on_no_messages2" {SEND_ON_NO_MESSAGES_NO_CHECKED} value="NO" /> <span class="gen"><label for="send_on_no_messages2">{L_NO}</label></span>
            </td>
          </tr>
		  <tr>
			<td class="row1"><span class="gen">{L_PRIVATE_MGS_IN_FEED}</span></td>
			<td class="row2" width="550px;">
			  <input type="radio" name="show_pms" id="pms1" value="YES" {SHOW_PMS_YES_CHECKED} /> <span class="gen"><label for="pms1">{L_YES}</label></span>
			  <input type="radio" name="show_pms" id="pms2" value="NO" {SHOW_PMS_NO_CHECKED}/> <span class="gen"><label for="pms2">{L_NO}</label></span>
			</td>
		  </tr>
          <tr>
            <td class="row1"><span class="gen"><label for="send_hour">{L_SEND_HOUR}</label></span></td>
            <td class="row2">
              <select name="send_hour" id="send_hour">
				<option value="R"{RANDOM_SELECTED}>{L_RANDOM}</option>
                <option value="0"{MIDNIGHT_SELECTED}>{L_MIDNIGHT}</option>
                <option value="1"{1AM_SELECTED}>{L_1AM}</option>
                <option value="2"{2AM_SELECTED}>{L_2AM}</option>
                <option value="3"{3AM_SELECTED}>{L_3AM}</option>
                <option value="4"{4AM_SELECTED}>{L_4AM}</option>
                <option value="5"{5AM_SELECTED}>{L_5AM}</option>
                <option value="6"{6AM_SELECTED}>{L_6AM}</option>
                <option value="7"{7AM_SELECTED}>{L_7AM}</option>
                <option value="8"{8AM_SELECTED}>{L_8AM}</option>
                <option value="9"{9AM_SELECTED}>{L_9AM}</option>
                <option value="10"{10AM_SELECTED}>{L_10AM}</option>
                <option value="11"{11AM_SELECTED}>{L_11AM}</option>
                <option value="12"{12PM_SELECTED}>{L_12PM}</option>
                <option value="13"{1PM_SELECTED}>{L_1PM}</option>
                <option value="14"{2PM_SELECTED}>{L_2PM}</option>
                <option value="15"{3PM_SELECTED}>{L_3PM}</option>
                <option value="16"{4PM_SELECTED}>{L_4PM}</option>
                <option value="17"{5PM_SELECTED}>{L_5PM}</option>
                <option value="18"{6PM_SELECTED}>{L_6PM}</option>
                <option value="19"{7PM_SELECTED}>{L_7PM}</option>
                <option value="20"{8PM_SELECTED}>{L_8PM}</option>
                <option value="21"{9PM_SELECTED}>{L_9PM}</option>
                <option value="22"{10PM_SELECTED}>{L_10PM}</option>
                <option value="23"{11PM_SELECTED}>{L_11PM}</option>
              </select>
            </td>
          </tr>
          <tr>
            <td class="row1"><span class="gen"><label for="text_length">{L_TEXT_LENGTH}</label></span></td>
            <td class="row2">
              <select name="text_length" id="text_length">
                <option value="50"{50_SELECTED}>{L_50}</option>
                <option value="100"{100_SELECTED}>{L_100}</option>
                <option value="150"{150_SELECTED}>{L_150}</option>
                <option value="300"{300_SELECTED}>{L_300}</option>
                <option value="600"{600_SELECTED}>{L_600}</option>
                <option value="32000"{MAX_SELECTED}>{L_MAX}</option>
              </select>
            </td>
          </tr>
          <tr>
            <td colspan="2" valign="top" class="row2"><span class="gensmall">{L_ADMIN_FORUM_SELECTION_EXPLAIN}</span></td>
          </tr>
          <tr>
			<td colspan="2" align="center" class="catBottom" height="28"><span class="gen"><button type="submit" class="mainoption">{L_SUBMIT}</button>&nbsp;<button type="reset" class="liteoption">{L_RESET}</button></span></td>
          </tr>
        </table>
	</form>
	  
	<h1>{L_VERSION_INFORMATION}</h1>

	<p style="text-align:left">{VERSION_INFO}</p>

	<div style="text-align:center" class="copyright">Cr�� par <a href="{U_DIGEST_PAGE_URL}" class="copyright" target="_blank">phpBB Digests, version {DIGEST_VERSION}</a></div>
