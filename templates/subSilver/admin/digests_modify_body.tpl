	<h1>{L_PAGE_TITLE}</h1>

	<p>{L_ADMIN_EXPLAIN}</p>

	<script type="text/javascript"> 
	<!-- Hide Javascript from validators
	
	function select_unselect_all ()
	{
		// Depending on whether selectall is checked or unchecked, will select or unselect all rows presented
		// on the current page of the admin interface
		var select_unselect = document.admin.selectall.checked;
		var input_tags=document.getElementsByTagName("input");
		for(i=0;i<input_tags.length;i++)
		{
			tag_object = input_tags.item(i);
			tag_id = tag_object.id;
			if(tag_id.substr(0,3) == "id_")
			{
				tag_object.checked = select_unselect;
			}
		}
	}
	
	function update_delete_all ()
	{
		// Depending on whether selectall is checked or unchecked, will make all rows presented
		// on the current page of the admin interface either update or delete
		var update_deleted = document.admin.updatedelete.checked;
		var select_tags = document.getElementsByTagName("select");
		for(i=0;i<select_tags.length;i++)
		{
			tag_object = select_tags.item(i);
			tag_id = tag_object.id;
			if(tag_id.substr(0,7) == "action_")
			{
				for(j=0;j<tag_object.length;j++)
				{
					if (update_deleted == false)
					{
						if (tag_object.options[j].value == 'UPD')
						{
							tag_object.options[j].selected = true;
						}
						else 
						{
							tag_object.options[j].selected = false;
						}
					}
					else
					{
						if (tag_object.options[j].value == 'UPD')
						{
							tag_object.options[j].selected = false;
						}
						else 
						{
							tag_object.options[j].selected = true;
						}
					}
				}
			}
		}
	}
	
	function prune_digest_rows ()
	{
		// This function removes from the HTML DOM all rows on the current page that the administrator sees in the list of digest subscribers
		// that are unselected. This minimizes the data sent to the web server for processing.
		
		// Hide and collapse the form so the admin is not alarmed by behind the scenes manipulations
        document.getElementById('admin_form').style.visibility = 'hidden';
		
        var x = document.getElementById('digest_table');
		i = 0;
		num_rows = x.rows.length;
		while (i < num_rows)
		{
			if (x.rows[i].id.indexOf('digestrow_') == 0)
			{
				// Examine the select cell's checkbox property
				if (x.rows[i].cells[0].firstChild.checked == false)
				{
					x.deleteRow(i);
					num_rows = x.rows.length;
				}
				else
				{
					i++;
				}
			}
			else
			{
				i++;
			}
		}
	}
	
    // End hiding Javascript -->
    </script>

    <form name="admin" id="admin_form" action="{S_MODIFY_URL}" method="post" onsubmit="prune_digest_rows();" style="visibility:inherit">
		<table id="digest_table" width="99%" cellpadding="4" cellspacing="1" border="0" align="center" class="forumline">
		  <tr>
			<th class="thHead" colspan="13">
			{L_SELECT_SUBSCRIBE}
			</th>
		  </tr>
		  <tr>
			<td class="row2" style="text-align:center" id="h1"><span class="gen">{L_SELECT}</span></td>
			<td class="row2" style="text-align:center" id="h2"><span class="gen">{L_ACTION}</span></td>
			<td class="row2" style="text-align:center" id="h3"><span class="gen">{L_USERNAME}</span></td>
			<td class="row2" style="text-align:center" id="h4"><span class="gen">{L_DIGEST_TYPE_SHORT}</span></td>
			<td class="row2" style="text-align:center" id="h5"><span class="gen">{L_FORMAT_SHORT}</span></td>
			<td class="row2" style="text-align:center" id="h6"><span class="gen">{L_SHOW_TEXT_SHORT}</span></td>
			<td class="row2" style="text-align:center" id="h7"><span class="gen">{L_SHOW_MINE_SHORT}</span></td>
			<td class="row2" style="text-align:center" id="h8"><span class="gen">{L_SHOW_NEW_ONLY_SHORT}</span></td>
			<td class="row2" style="text-align:center" id="h9"><span class="gen">{L_SEND_ON_NO_MESSAGES_SHORT}</span></td>
			<td class="row2" style="text-align:center" id="h10"><span class="gen">{L_PRIVATE_MGS_IN_FEED_SHORT}</span></td>
			<td class="row2" style="text-align:center" id="h11"><span class="gen">{L_SEND_HOUR_SHORT}</span></td>
			<td class="row2" style="text-align:center" id="h12"><span class="gen">{L_TEXT_LENGTH_SHORT}</span></td>
			<td class="row2" style="text-align:center" id="h13"><span class="gen">{L_FORUM_SUBSCRIPTIONS}</span></td>
		  </tr>
		  <!-- BEGIN digestrow -->
		  <tr id="digestrow_{digestrow.ID}">
			<td class="row1" style="text-align:center" headers="h1"><input type="checkbox" name="id_{digestrow.ID}" id="id_{digestrow.ID}" /></td>
			<td class="row1" style="text-align:center" headers="h2">
				<span class="gen">
					<select name="action_{digestrow.ID}" id="action_{digestrow.ID}">
						<option selected="selected" value="UPD">{digestrow.L_UPDATE}</option>
						<option value="DEL">{digestrow.L_DELETE}</option>
					</select>
				</span>
			</td>
			<td class="row1" style="text-align:center" headers="h3"><span class="gen">{digestrow.USERNAME}</span></td>
			<td class="row1" style="text-align:center" headers="h4">
				<span class="gen">
					<select name="type_{digestrow.ID}">
						<option{digestrow.DAILY_SELECTED} value="DAY">{digestrow.L_DAILY}</option>
						<option{digestrow.WEEKLY_SELECTED} value="WEEK">{digestrow.L_WEEKLY}</option>
					</select>
				</span>
			</td>		
			<td class="row1" style="text-align:center" headers="h5">
				<span class="gen">
					<select name="format_{digestrow.ID}">
						<option{digestrow.HTML_ENH_SELECTED} value="HTML">{digestrow.L_HTML_ENH}</option>
						<option{digestrow.HTML_PLAIN_SELECTED} value="PHTM">{digestrow.L_HTML_PLAIN}</option>
						<option{digestrow.TEXT_SELECTED} value="TEXT">{digestrow.L_TEXT}</option>
					</select>
				</span>
			</td>		
			<td class="row1" style="text-align:center" headers="h6">
				<span class="gen">
					<select name="excerpt_{digestrow.ID}">
						<option{digestrow.EXCERPT_YES_SELECTED} value="YES">{digestrow.L_YES}</option>
						<option{digestrow.EXCERPT_NO_SELECTED} value="NO">{digestrow.L_NO}</option>
					</select>
				</span>
			</td>		
			<td class="row1" style="text-align:center" headers="h7">
				<span class="gen">
					<select name="mine_{digestrow.ID}">
						<option{digestrow.MINE_YES_SELECTED} value="YES">{digestrow.L_YES}</option>
						<option{digestrow.MINE_NO_SELECTED} value="NO">{digestrow.L_NO}</option>
					</select>
				</span>
			</td>		
			<td class="row1" style="text-align:center" headers="h8">
				<span class="gen">
					<select name="new_{digestrow.ID}">
						<option{digestrow.NEW_YES_SELECTED} value="TRUE">{digestrow.L_YES}</option>
						<option{digestrow.NEW_NO_SELECTED} value="FALSE">{digestrow.L_NO}</option>
					</select>
				</span>
			</td>		
			<td class="row1" style="text-align:center" headers="h9">
				<span class="gen">
					<select name="only_if_msgs_{digestrow.ID}">
						<option{digestrow.ONLY_IF_MSGS_YES_SELECTED} value="YES">{digestrow.L_YES}</option>
						<option{digestrow.ONLY_IF_MSGS_NO_SELECTED} value="NO">{digestrow.L_NO}</option>
					</select>
				</span>
			</td>		
			<td class="row1" style="text-align:center" headers="h10">
				<span class="gen">
					<select name="pms_{digestrow.ID}">
						<option{digestrow.PMS_YES_SELECTED} value="YES">{digestrow.L_YES}</option>
						<option{digestrow.PMS_NO_SELECTED} value="NO">{digestrow.L_NO}</option>
					</select>
				</span>
			</td>		
			<td class="row1" style="text-align:center" headers="h11">
				<select name="send_hour_{digestrow.ID}">
					<option value="0" {digestrow.MIDNIGHT_SELECTED}>{digestrow.L_MIDNIGHT}</option>
					<option value="1" {digestrow.1AM_SELECTED}>{digestrow.L_1AM}</option>
					<option value="2" {digestrow.2AM_SELECTED}>{digestrow.L_2AM}</option>
					<option value="3" {digestrow.3AM_SELECTED}>{digestrow.L_3AM}</option>
					<option value="4" {digestrow.4AM_SELECTED}>{digestrow.L_4AM}</option>
					<option value="5" {digestrow.5AM_SELECTED}>{digestrow.L_5AM}</option>
					<option value="6" {digestrow.6AM_SELECTED}>{digestrow.L_6AM}</option>
					<option value="7" {digestrow.7AM_SELECTED}>{digestrow.L_7AM}</option>
					<option value="8" {digestrow.8AM_SELECTED}>{digestrow.L_8AM}</option>
					<option value="9" {digestrow.9AM_SELECTED}>{digestrow.L_9AM}</option>
					<option value="10" {digestrow.10AM_SELECTED}>{digestrow.L_10AM}</option>
					<option value="11" {digestrow.11AM_SELECTED}>{digestrow.L_11AM}</option>
					<option value="12" {digestrow.12PM_SELECTED}>{digestrow.L_12PM}</option>
					<option value="13" {digestrow.1PM_SELECTED}>{digestrow.L_1PM}</option>
					<option value="14" {digestrow.2PM_SELECTED}>{digestrow.L_2PM}</option>
					<option value="15" {digestrow.3PM_SELECTED}>{digestrow.L_3PM}</option>
					<option value="16" {digestrow.4PM_SELECTED}>{digestrow.L_4PM}</option>
					<option value="17" {digestrow.5PM_SELECTED}>{digestrow.L_5PM}</option>
					<option value="18" {digestrow.6PM_SELECTED}>{digestrow.L_6PM}</option>
					<option value="19" {digestrow.7PM_SELECTED}>{digestrow.L_7PM}</option>
					<option value="20" {digestrow.8PM_SELECTED}>{digestrow.L_8PM}</option>
					<option value="21" {digestrow.9PM_SELECTED}>{digestrow.L_9PM}</option>
					<option value="22" {digestrow.10PM_SELECTED}>{digestrow.L_10PM}</option>
					<option value="23" {digestrow.11PM_SELECTED}>{digestrow.L_11PM}</option>
				</select>
			</td>		
			<td class="row1" style="text-align:center" headers="h12">
				<span class="gen">
					<select name="text_length_{digestrow.ID}">
						<option value="50" {digestrow.50_SELECTED}>{digestrow.L_50}</option>
						<option value="100" {digestrow.100_SELECTED}>{digestrow.L_100}</option>
						<option value="150" {digestrow.150_SELECTED}>{digestrow.L_150}</option>
						<option value="300" {digestrow.300_SELECTED}>{digestrow.L_300}</option>
						<option value="600" {digestrow.600_SELECTED}>{digestrow.L_600}</option>
						<option value="32000" {digestrow.MAX_SELECTED}>{digestrow.L_MAX}</option>
					</select>
				</span>
			</td>
			<td class="row1" style="text-align:center" headers="h13">
				<span class="gen">{digestrow.SUBSCRIPTIONS}</span>
			</td>
		  </tr>
		  <!-- END digestrow -->
		  <tr>
			<td colspan="13" class="row2" style="text-align:left">
				<span class="gen">
					<input type="checkbox" id="selectall" onclick="select_unselect_all();" />&nbsp;<label for="selectall"><b>{L_SELECT_UNSELECT_ALL}</b></label>&nbsp;&nbsp;
					<input type="checkbox" id="updatedelete" onclick="update_delete_all();" />&nbsp;<label for="updatedelete"><b>{L_UPDATE_DELETE_ALL}</b></label>
				</span>
			</td>
		  </tr>
		  <tr>
			<td colspan="13" style="text-align:center" class="catBottom" height="28"><span class="gen"><button type="submit" class="mainoption">{L_GLOBAL_SUBMIT}</button>&nbsp;<button type="reset" class="liteoption">{L_RESET}</button></span></td>
		  </tr>
		</table>
		
		<!-- BEGIN switch_pagination -->
		<table width="99%" cellspacing="0" cellpadding="0" border="0">
		  <tr>
			<td colspan="2" class="nav" style="text-align:left"><span class="gen"><br />{L_PAGE_NUMBER}</span></td>
			<td colspan="9" class="nav" style="text-align:right"><span class="gen">&nbsp;</span></td>
			<td colspan="2" class="nav" style="text-align:right"><span class="gen"><br />{L_PAGINATION}</span></td>
		  </tr>
		</table>
		<!-- END switch_pagination -->
	</form>
	
	<div style="text-align:center" class="copyright">Cr�� par <a href="{U_DIGEST_PAGE_URL}" class="copyright" target="_blank">phpBB Digests, version {DIGEST_VERSION}</a></div>
