{FELICITATIONS}
<!-- BEGIN privmsg_extensions -->
<table border="0" cellspacing="0" cellpadding="0" align="center" width="100%">
  <tr>
	<td valign="top" align="center" width="100%">
	  <table height="40" cellspacing="2" cellpadding="2" border="0">
		<tr valign="middle">
		  <td>{INBOX_IMG}</td>
		  <td><span class="cattitle">{INBOX_LINK}&nbsp;&nbsp;</span></td>
		  <td>{SENTBOX_IMG}</td>
		  <td><span class="cattitle">{SENTBOX_LINK}&nbsp;&nbsp;</span></td>
		  <td>{OUTBOX_IMG}</td>
		  <td><span class="cattitle">{OUTBOX_LINK}&nbsp;&nbsp;</span></td>
		  <td>{SAVEBOX_IMG}</td>
		  <td><span class="cattitle">{SAVEBOX_LINK}&nbsp;&nbsp;</span></td>
		</tr>
	  </table>
	</td>
  </tr>
</table>

<br clear="all" />
<!-- END privmsg_extensions -->

<form action="{S_POST_ACTION}" method="post" name="post" onsubmit="return checkForm(this)">

{POST_PREVIEW_BOX}
{ERROR_BOX}
{WARN_BOX}

<table width="100%" cellspacing="2" cellpadding="2" border="0" align="center">
	<tr>
		<td align="left"><span  class="nav"><a href="{U_INDEX}" class="nav">{L_INDEX}</a>
		<!-- BEGIN switch_not_privmsg -->
		-> <a href="{U_VIEW_FORUM}" class="nav">{FORUM_NAME}</a></span></td>
		<!-- END switch_not_privmsg -->
	</tr>
</table>

<table border="0" cellpadding="3" cellspacing="1" width="100%" class="forumline">
	<tr>
		<th class="thHead" colspan="2" height="25"><b>{L_POST_A}</b></th>
	</tr>
	<!-- BEGIN switch_username_select -->
	<tr>
		<td class="row1"><span class="gen"><b>{L_USERNAME}</b></span></td>
		<td class="row2"><span class="genmed"><input type="text" class="post" tabindex="1" name="username" size="25" maxlength="25" value="{USERNAME}" /></span></td>
	</tr>
	<!-- END switch_username_select -->
	<!-- BEGIN switch_privmsg -->
	<tr>
		<td class="row1"><span class="gen"><b>{L_USERNAME}</b></span></td>
		<td class="row2"><span class="genmed"><input type="text"  class="post" name="username" maxlength="25" size="25" tabindex="1" value="{USERNAME}" />&nbsp;<input type="submit" name="usersubmit" value="{L_FIND_USERNAME}" class="liteoption" onClick="window.open('{U_SEARCH_USER}', '_phpbbsearch', 'HEIGHT=250,resizable=yes,WIDTH=400');return false;" /></span></td>
	</tr>
	<!-- END switch_privmsg -->
	<tr>
	  <td class="row1" width="22%"><span class="gen"><b>{L_SUBJECT}</b></span></td>
	  <td class="row2" width="78%"> <span class="gen">
		<input type="text" name="subject" size="45" maxlength="60" style="width:600px" tabindex="2" class="post" value="{SUBJECT}" />
		</span> </td>
	</tr>
	<tr> 
	  <td class="row1" valign="top"> 
		<table width="100%" border="0" cellspacing="0" cellpadding="1">
		  <tr>
			<td><span class="gen"><b>{L_MESSAGE_BODY}</b></span> </td>
		  </tr>
		  <tr>
			<td valign="middle" align="center"> <br />
			  <table width="100" border="0" cellspacing="0" cellpadding="5">
				<tr align="center">
				  <td colspan="{S_SMILIES_COLSPAN}" class="gensmall"><b>{L_EMOTICONS}</b></td>
				</tr>
				<!-- BEGIN smilies_row -->
				<tr align="center" valign="middle">
				  <!-- BEGIN smilies_col -->
				  <td><a href="javascript:emoticon('{smilies_row.smilies_col.SMILEY_CODE}')"><img src="{smilies_row.smilies_col.SMILEY_IMG}" border="0" alt="{smilies_row.smilies_col.SMILEY_DESC}" title="{smilies_row.smilies_col.SMILEY_DESC}" /></a></td>
				  <!-- END smilies_col -->
				</tr>
				<!-- END smilies_row -->
				<!-- BEGIN switch_smilies_extra -->
				<tr align="center">
				  <td colspan="{S_SMILIES_COLSPAN}"><span  class="nav"><a href="{U_MORE_SMILIES}" onclick="window.open('{U_MORE_SMILIES}', '_phpbbsmilies', 'HEIGHT=525,resizable=yes,scrollbars=yes,WIDTH=570');return false;" target="_phpbbsmilies" class="nav">{L_MORE_SMILIES}</a></span></td>
				</tr>
				<!-- END switch_smilies_extra -->
			  </table>
			</td>
		  </tr>
		</table>
	  </td>
		<td class="row2" valign="top"><span class="gen"> <span class="genmed"> </span>
		<table class="tblpost" border="0" cellspacing="0" cellpadding="2">
		  <noscript><tr><td colspan="13" class="gen" style="color:red">Vous devez activer le JavaScript pour pouvoir utiliser les boutons ci-dessous.</td></tr></noscript>
		  <tr class="rowbbcode" valign="middle">
			<td><span class="genmed">
			  <input type="button" class="button" accesskey="b" name="addbbcode0" value="B" style="font-weight:bold; width: 30px" onClick="bbstyle(0)" onMouseOver="helpline('b')" />
			  </span></td>
			<td><span class="genmed">
			  <input type="button" class="button" accesskey="i" name="addbbcode2" value="i" style="font-style:italic; width: 30px" onClick="bbstyle(2)" onMouseOver="helpline('i')" />
			  </span></td>
			<td><span class="genmed">
			  <input type="button" class="button" name="addbbcode4" value="u" style="text-decoration: underline; width: 30px" onClick="bbstyle(4)" onMouseOver="helpline('u')" />
			  </span></td>
			<td><span class="genmed">
			  <input type="button" class="button" accesskey="u" name="addbbcode24" value="u" style="width:30px;text-decoration:underline solid red" onClick="bbstyle(24)" onMouseOver="helpline('_u_color')" />
			  </span></td>
			<td><span class="genmed">
			  <input type="button" class="button" accesskey="q" name="addbbcode6" value="Citer" style="width: 50px" onClick="bbstyle(6)" onMouseOver="helpline('q')" />
			  </span></td>
			<td><span class="genmed">
			  <input type="button" class="button" accesskey="c" name="addbbcode8" value="Code" style="width: 50px" onClick="bbstyle(8)" onMouseOver="helpline('c')" />
			  </span></td>
			<td><span class="genmed">
			  <input type="button" class="button" accesskey="l" name="addbbcode10" value="List" style="width: 40px" onClick="bbstyle(10)" onMouseOver="helpline('l')" />
			  </span></td>
			<td><span class="genmed">
			  <input type="button" class="button" accesskey="o" name="addbbcode12" value="List=" style="width: 45px" onClick="bbstyle(12)" onMouseOver="helpline('o')" />
			  </span></td>
			<td><span class="genmed">
			  <input type="button" class="button" accesskey="p" name="addbbcode14" value="Img" style="width: 40px"  onClick="bbstyle(14)" onMouseOver="helpline('p')" />
			  </span></td>
			<td><span class="genmed">
			  <input type="button" class="button" accesskey="w" name="addbbcode16" value="URL" style="text-decoration: underline; width: 40px" onClick="bbstyle(16)" onMouseOver="helpline('w')" />
			  </span></td>
			<td><span class="genmed">
			  <input type="button" class="button" name="addbbcode18" value="b" style="text-decoration: line-through; width: 30px" onClick="bbstyle(18)" onMouseOver="helpline('_s')" />
			  </span></td>
			<td><span class="genmed">
			  <input type="button" class="button" name="addbbcode20" value="X&#8322;" style="width: 30px" onClick="bbstyle(20)" onMouseOver="helpline('_sub')" />
			  </span></td>
			<td><span class="genmed">
			  <input type="button" class="button" name="addbbcode22" value="X&#178;" style="width: 30px" onClick="bbstyle(22)" onMouseOver="helpline('_sup')" />
			  </span></td>
		  </tr>
		  <tr>
			<td colspan="13">
			  <table width="600px" border="0" cellspacing="0" cellpadding="0">
				<tr>
				  <td><span class="genmed">
						  <select name="addbbcode100" onChange="bbfontstyle('[color=' + this.form.addbbcode100.options[this.form.addbbcode100.selectedIndex].value + ']', '[/color]');this.selectedIndex=0;" onMouseOver="helpline('s')">
					  <option style="color:black; background-color: {T_TD_COLOR1}" value="{T_FONTCOLOR1}" class="genmed">{L_COLOR_DEFAULT}</option>
					  <option style="color:darkred; background-color: {T_TD_COLOR1}" value="darkred" class="genmed">{L_COLOR_DARK_RED}</option>
					  <option style="color:red; background-color: {T_TD_COLOR1}" value="red" class="genmed">{L_COLOR_RED}</option>
					  <option style="color:orange; background-color: {T_TD_COLOR1}" value="orange" class="genmed">{L_COLOR_ORANGE}</option>
					  <option style="color:brown; background-color: {T_TD_COLOR1}" value="brown" class="genmed">{L_COLOR_BROWN}</option>
					  <option style="color:yellow; background-color: {T_TD_COLOR1}" value="yellow" class="genmed">{L_COLOR_YELLOW}</option>
					  <option style="color:green; background-color: {T_TD_COLOR1}" value="green" class="genmed">{L_COLOR_GREEN}</option>
					  <option style="color:olive; background-color: {T_TD_COLOR1}" value="olive" class="genmed">{L_COLOR_OLIVE}</option>
					  <option style="color:cyan; background-color: {T_TD_COLOR1}" value="cyan" class="genmed">{L_COLOR_CYAN}</option>
					  <option style="color:blue; background-color: {T_TD_COLOR1}" value="blue" class="genmed">{L_COLOR_BLUE}</option>
					  <option style="color:darkblue; background-color: {T_TD_COLOR1}" value="darkblue" class="genmed">{L_COLOR_DARK_BLUE}</option>
					  <option style="color:indigo; background-color: {T_TD_COLOR1}" value="indigo" class="genmed">{L_COLOR_INDIGO}</option>
					  <option style="color:violet; background-color: {T_TD_COLOR1}" value="violet" class="genmed">{L_COLOR_VIOLET}</option>
					  <option style="color:white; background-color: {T_TD_COLOR1}" value="white" class="genmed">{L_COLOR_WHITE}</option>
					  <option style="color:black; background-color: {T_TD_COLOR1}" value="black" class="genmed">{L_COLOR_BLACK}</option>
						  </select>
						  <select name="addbbcode102" selected onChange="bbfontstyle('[size=' + this.form.addbbcode102.options[this.form.addbbcode102.selectedIndex].value + ']', '[/size]');this.selectedIndex=0;" onMouseOver="helpline('f')">
					  <option value="0" class="genmed">{L_FONT_SIZE}</option>
					  <option value="7" class="genmed">{L_FONT_TINY}</option>
					  <option value="9" class="genmed">{L_FONT_SMALL}</option>
					  <option value="12" class="genmed">{L_FONT_NORMAL}</option>

					  <option value="18" class="genmed">{L_FONT_LARGE}</option>
					  <option  value="24" class="genmed">{L_FONT_HUGE}</option>
						  </select>
						  <select name="addbbcode106" onChange="bbfontstyle('[u=' + this.form.addbbcode106.options[this.form.addbbcode106.selectedIndex].value + ']', '[/u]');this.selectedIndex=0;" onMouseOver="helpline('_u_color')">
					  <option style="color:black; background-color: {T_TD_COLOR1}" value="{T_FONTCOLOR1}" class="genmed">Souligner</option>
					  <option style="color:darkred; background-color: {T_TD_COLOR1}" value="darkred" class="genmed">{L_COLOR_DARK_RED}</option>
					  <option style="color:red; background-color: {T_TD_COLOR1}" value="red" class="genmed">{L_COLOR_RED}</option>
					  <option style="color:orange; background-color: {T_TD_COLOR1}" value="orange" class="genmed">{L_COLOR_ORANGE}</option>
					  <option style="color:brown; background-color: {T_TD_COLOR1}" value="brown" class="genmed">{L_COLOR_BROWN}</option>
					  <option style="color:yellow; background-color: {T_TD_COLOR1}" value="yellow" class="genmed">{L_COLOR_YELLOW}</option>
					  <option style="color:green; background-color: {T_TD_COLOR1}" value="green" class="genmed">{L_COLOR_GREEN}</option>
					  <option style="color:olive; background-color: {T_TD_COLOR1}" value="olive" class="genmed">{L_COLOR_OLIVE}</option>
					  <option style="color:cyan; background-color: {T_TD_COLOR1}" value="cyan" class="genmed">{L_COLOR_CYAN}</option>
					  <option style="color:blue; background-color: {T_TD_COLOR1}" value="blue" class="genmed">{L_COLOR_BLUE}</option>
					  <option style="color:darkblue; background-color: {T_TD_COLOR1}" value="darkblue" class="genmed">{L_COLOR_DARK_BLUE}</option>
					  <option style="color:indigo; background-color: {T_TD_COLOR1}" value="indigo" class="genmed">{L_COLOR_INDIGO}</option>
					  <option style="color:violet; background-color: {T_TD_COLOR1}" value="violet" class="genmed">{L_COLOR_VIOLET}</option>
					  <option style="color:white; background-color: {T_TD_COLOR1}" value="white" class="genmed">{L_COLOR_WHITE}</option>
					  <option style="color:black; background-color: {T_TD_COLOR1}" value="black" class="genmed">{L_COLOR_BLACK}</option>
						  </select>
						  <select name="addbbcode104" selected onChange="bbfontstyle('[indent=' + this.form.addbbcode104.options[this.form.addbbcode104.selectedIndex].value + ']', '[/indent]');this.selectedIndex=0;" onMouseOver="helpline('_indent')">
					  <option value="0" class="genmed">Indenter</option>
					  <option value="10" class="genmed">{L_FONT_TINY}</option>
					  <option value="20" class="genmed">{L_FONT_SMALL}</option>
					  <option value="40" class="genmed">{L_FONT_NORMAL}</option>
					  <option value="80" class="genmed">{L_FONT_LARGE}</option>
					  <option value="160" class="genmed">{L_FONT_HUGE}</option>
						  </select>
					</span></td>
				  <td nowrap="nowrap" align="right"><span class="gensmall"><a href="javascript:bbstyle(-1)" class="genmed" onMouseOver="helpline('a')">{L_BBCODE_CLOSE_TAGS}</a></span></td>
				</tr>
			  </table>
			</td>
		  </tr>
		  <tr>
			<td colspan="13"> <span class="gensmall" id="helpbox">{L_STYLES_TIP}</span></td>
		  </tr>
          <!-- BEGIN switch_spChars -->
          <tr id="spChars"></tr>
          <!-- END switch_spChars -->
		  <tr>
			<td colspan="13"><span class="gen">
			  <textarea name="message" rows="15" cols="35" wrap="virtual" tabindex="3" class="post" onselect="storeCaret(this);" onclick="storeCaret(this);" onkeyup="storeCaret(this);">{MESSAGE}</textarea>
					<!-- BEGIN switch_new_member -->
					<p style="text-align: justify; margin-top: 5px; padding: 9px; background-color: #FAFAFA; border: 2px solid #069;"><strong class="gen" style="font-size: 1.125rem; line-height: 1.4em; color: green; font-style: italic;">Cher nouvel utilisateur, il nous serait agr�able de vous compter longtemps parmi nous, aussi nous vous invitons � participer aux conversations que vous engagez ou, du moins, � donner � la fin votre avis sur les r�ponses � vos questions, ainsi qu'il est de mise sur ce forum.</strong></p>
					<!-- END switch_new_member --></span></td>
		  </tr>
		</table>
		</span></td>
	</tr>
	<!-- BEGIN switch_altMarkup -->
	<tr id="altMarkup">
		<td class="row1">
			<a href="#" class="gensmall">Afficher l'aide</a>
			<div id="altMarkup_help_1" class="genmed" style="display: none">
				<p><strong>Comment retirer ces boutons bizarres ?</strong>
				<span>Vous pouvez les d�sactiver dans la page de votre <a href="{U_PROFILE}" class="mainmenu">profil</a>.</span></p>
				<p><strong>� quoi servent-ils ?</strong>
				<span>Veuillez visiter la <a href="{U_FAQ}#f8" class="mainmenu">FAQ</a> pour plus d'informations.</span></p>
			</div>
		</td>
		<td class="row2">
			<table>
				<tr>
					<td>
						<input type="button" class="mainoption" value="* > [ ]">
						<input type="button" class="mainoption" value="* > [ ] + pr�visualisation">
						<input type="button" class="mainoption" value="* > [ ] + envoyer">
						<input type="submit" name="preview" class="mainoption" value="{L_PREVIEW}">
						<input type="submit" name="post" class="mainoption" value="{L_SUBMIT}">
					</td>
				</tr>
				<tr id="altMarkup_help_2" style="display: none">
					<td>
{ALTMARKUP_HELP_TABLE}
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<!-- END switch_altMarkup -->
	<tr>
	  <td class="row1" valign="top"><span class="gen"><b>{L_OPTIONS}</b></span><br /><span class="gensmall">{HTML_STATUS}<br />{BBCODE_STATUS}<br />{SMILIES_STATUS}</span></td>
	  <td class="row2"><span class="gen"> </span>
		<table cellspacing="0" cellpadding="1" border="0">
		  <!-- BEGIN switch_html_checkbox -->
		  <tr>
			<td>
			  <input type="checkbox" name="disable_html" {S_HTML_CHECKED} />
			</td>
			<td><span class="gen">{L_DISABLE_HTML}</span></td>
		  </tr>
		  <!-- END switch_html_checkbox -->
		  <!-- BEGIN switch_bbcode_checkbox -->
		  <tr>
			<td>
			  <input type="checkbox" name="disable_bbcode" {S_BBCODE_CHECKED} />
			</td>
			<td><span class="gen">{L_DISABLE_BBCODE}</span></td>
		  </tr>
		  <!-- END switch_bbcode_checkbox -->
		  <!-- BEGIN switch_smilies_checkbox -->
		  <tr>
			<td>
			  <input type="checkbox" name="disable_smilies" {S_SMILIES_CHECKED} />
			</td>
			<td><span class="gen">{L_DISABLE_SMILIES}</span></td>
		  </tr>
		  <!-- END switch_smilies_checkbox -->
		  <!-- BEGIN switch_signature_checkbox -->
		  <tr>
			<td>
			  <input type="checkbox" name="attach_sig" {S_SIGNATURE_CHECKED} />
			</td>
			<td><span class="gen">{L_ATTACH_SIGNATURE}</span></td>
		  </tr>
		  <!-- END switch_signature_checkbox -->
		  <!-- BEGIN switch_bookmark_checkbox -->
		  <tr> 
			<td> 
			  <input type="checkbox" name="setbm" {S_SETBM_CHECKED} />
			</td>
			<td><span class="gen">{L_SET_BOOKMARK}</span></td>
		  </tr>
		  <!-- END switch_bookmark_checkbox -->

		  <!-- BEGIN switch_notify_checkbox -->
		  <tr>
			<td>
			  <input type="checkbox" name="notify" {S_NOTIFY_CHECKED} />
			</td>
			<td><span class="gen">{L_NOTIFY_ON_REPLY}</span></td>
		  </tr>
		  <!-- END switch_notify_checkbox -->
		  <!-- BEGIN switch_delete_checkbox -->
		  <tr>
			<td>
			  <input type="checkbox" name="delete" />
			</td>
			<td><span class="gen">{L_DELETE_POST}</span></td>
		  </tr>
		  <!-- END switch_delete_checkbox -->
		  <!-- BEGIN switch_type_toggle -->
		  <tr>
			<td></td>
			<td><span class="gen">{S_TYPE_TOGGLE}</span></td>
		  </tr>
		  <!-- END switch_type_toggle -->
		</table>
	  </td>
	</tr>
	{POLLBOX}

	<!-- BEGIN switch_confirm -->
	<tr>
		<td class="row1" colspan="2" align="center"><span class="gensmall">{L_CONFIRM_CODE_IMPAIRED}</span><br /><br />{CONFIRM_IMG}<br /><br /></td>
	</tr>
	<tr>
	  <td class="row1"><span class="gen">{L_CONFIRM_CODE}: * </span><br /><span class="gensmall">{L_CONFIRM_CODE_EXPLAIN}</span></td>
	  <td class="row2"><input type="text" class="post" style="width: 200px" name="confirm_code" size="8" maxlength="8" value="" /></td>
	</tr>
	<!-- END switch_confirm -->

	<tr>
	  <td class="catBottom" colspan="2" align="center" height="28"> {S_HIDDEN_FORM_FIELDS}<input type="submit" tabindex="5" name="preview" class="mainoption" value="{L_PREVIEW}" />&nbsp;<input type="submit" accesskey="s" tabindex="6" name="post" class="mainoption" value="{L_SUBMIT}" /></td>
	</tr>
  </table>

  <table width="100%" cellspacing="2" border="0" align="center" cellpadding="2">
	<tr>
	  <td align="right" valign="top"><span class="gensmall">{S_TIMEZONE}</span></td>
	</tr>
  </table>
</form>

<table width="100%" cellspacing="2" border="0" align="center">
  <tr>
	<td valign="top" align="right">{JUMPBOX}</td>
  </tr>
</table>

{TOPIC_REVIEW_BOX}

<!-- Variables -->
<script type="text/javascript">
	// bbCode control by
	// subBlue design
	// www.subBlue.com

	// Helpline messages
	b_help = "{L_BBCODE_B_HELP}";
	i_help = "{L_BBCODE_I_HELP}";
	u_help = "{L_BBCODE_U_HELP}";
	q_help = "{L_BBCODE_Q_HELP}";
	c_help = "{L_BBCODE_C_HELP}";
	l_help = "{L_BBCODE_L_HELP}";
	o_help = "{L_BBCODE_O_HELP}";
	p_help = "{L_BBCODE_P_HELP}";
	w_help = "{L_BBCODE_W_HELP}";
	a_help = "{L_BBCODE_A_HELP}";
	s_help = "{L_BBCODE_S_HELP}";
	f_help = "{L_BBCODE_F_HELP}";
	_s_help = "Texte barr� (au sens o� celui-ci n'est plus d'actualit� ou n'est plus pertinent) : [s]barr�[/s] &#10132; <s>barr�</s>";
	_sub_help = "Texte en indice : H[sub]2[/sub]O &#10132; H<sub>2</sub>O";
	_sup_help = "Texte en exposant : M[sup]me[/sup] &#10132; M<sup>me</sup>";
	_indent_help = "Indenter le texte : [indent=40]texte indent�[/indent]";
	_u_color_help = "Texte soulign� avec la couleur de soulignement de votre choix (alt+u) :<br>J'ai [u=red]manger[/u] ou J'ai [u=#FF000]manger[/u] &#10132; J'ai <span style=\"border-bottom:1px solid red\">manger</span>";

	// Define the bbCode tags
	bbcode = new Array();
	bbtags = new Array('[b]','[/b]','[i]','[/i]','[u]','[/u]','[quote]','[/quote]','[code]','[/code]','[list]','[/list]','[list=]','[/list]','[img]','[/img]','[url]','[/url]','[s]','[/s]','[sub]','[/sub]','[sup]','[/sup]','[u=red]','[/u]');
	imageTag = false;

	var _l_emptyMessage = "{L_EMPTY_MESSAGE}";
</script>

<!-- Functions -->
<script type="text/javascript" src="templates/subSilver/posting.min.js?v={ASSETS_VERSION}"></script>

<script type="text/javascript">
	<!-- BEGIN switch_spChars -->
	__ext.SpChars('�,�,�,�,�,�,�,�,�,�,�,�,&#339;,&#338;,\u00DF,\u1E9E,�&nbsp;&nbsp;�,\u2039&nbsp;&nbsp;\u203A,&ndash;,&mdash;,\u2E2E,&nbsp;');
	<!-- END switch_spChars -->
	<!-- BEGIN switch_altMarkup -->
	__ext.AltMarkup();
	<!-- END switch_altMarkup -->
</script>
