<div style="margin-top: 10px;" align="center">
{AD_OVERALL_FOOTER-CENTRE}
    <a href="#" class="tooltip" onclick="return __g_c.increaseGlobalFontSize();"><img src="templates/subSilver/images/increase_font_size.png"><div><p>Augmenter la taille de texte</p><p>Ceci affectera toutes les pages du forum.</p></div></a><a href="#" class="tooltip" onclick="return __g_c.decreaseGlobalFontSize();"><img src="templates/subSilver/images/decrease_font_size.png"><div><p>Diminuer la taille de texte</p><p>Ceci affectera toutes les pages du forum.</p></div></a><a href="#" class="tooltip" onclick="return __g_c.resetGlobalFontSize();"><img src="templates/subSilver/images/reset_font_size.png"><div><p>R�tablir la taille de texte</p></div></a><a href="#" class="tooltip" onclick="return __g_c.toggleContentMaxWidth(1000);" ondblclick="return __g_c.toggleContentMaxWidth();"><img src="templates/subSilver/images/max-width.png"><div><p>R�duire la largeur du contenu</p><p>Le forum sera affich� avec une largeur fixe. Le texte ne prendra plus toute la largeur de l'�cran. Cela pourrait am�liorer le confort de lecture si vous utilisez un �cran large.</p><p>Recliquez sur l'ic�ne pour annuler le redimensionnement.</p><p>Double-cliquez pour entrer une valeur personnalis�e.</p></div></a><a href="#" class="tooltip" onclick="return __g_c.share(this);"><img src="templates/subSilver/images/share.png"><div><p>Partager l'adresse de ce forum</p></div></a>
</div>

<!-- TouchBB Alert -->
<script type="text/javascript">
var aProds=new Array('iPhone','iPod');for(i=0;i<2;i++){if(navigator.userAgent.indexOf(aProds[i]+';')!=-1){var cValue='';if(document.cookie.length>0){cStart=document.cookie.indexOf('A2FLI=');if(cStart!=-1){cStart+=6;cEnd=document.cookie.indexOf(';',cStart);if(cEnd==-1)cEnd=document.cookie.length;cValue=document.cookie.substring(cStart,cEnd)}}if(cValue!='Z5D3'){var exdate=new Date();exdate.setDate(exdate.getDate()+3);document.cookie='A2FLI=Z5D3;expires='+exdate.toUTCString();if(confirm('Abbiamo rilevato che stai usando un '+aProds[i]+'. Vuoi provare un\'applicazione che ti permetta di accedere pi� agevolmente al forum?'))location.href='http://itunes.apple.com/it/app/touchbb-lite/id332458253?mt=8';}break}}
</script>
<div class="gensmall links-footer">
    {ADMIN_LINK}
    <span><img src="templates/subSilver/images/icon_mini_digest.gif"><a href="https://docs.google.com/forms/d/e/1FAIpQLSeHh3BTjFBdxOqwEWSSU2E6Qvr91LmPvkr2GJ6-Ljdav8qIpg/viewform?usp=sf_link" target="_blank">Nous contacter</a></span>
    <span><img src="templates/subSilver/images/icon_mini_feed.gif"><a href="http://www.achyra.org/francais/rss.php" target="_blank">Flux RSS</a></span>
    <!-- BEGIN switch_user_logged_in -->
    <span><a href="{U_STAT_HEADERLINK}" onclick="__g_c.showInfoBox('<p>Chargement des statistiques du forum...</p><p>Le chargement de la page prend au moins 30 secondes.</p><p>Veuillez patienter.</p>')"><img src="templates/subSilver/images/icon_mini_statistics.png" width="12" height="13" border="0" alt="Statistics" hspace="3"/>{L_STAT_HEADERLINK}</a></span>
    <!-- END switch_user_logged_in -->
    <span><a href="dons.php"><img src="templates/subSilver/images/icon_mini_donate.png" border="0" hspace="3"/>Faire un don</a></span>
    <span><img src="templates/subSilver/images/icon_mini_ads.png"><a href="#" onclick="return __g_c.toggleAds();">{SHOW_HIDE_ADS}</a></span>
</div>

<div align="center"><span class="copyright">
<!--
	We request you retain the full copyright notice below including the link to www.phpbb.com.
	This not only gives respect to the large amount of time given freely by the developers
	but also helps build interest, traffic and use of phpBB 2.0. If you cannot (for good
	reason) retain the full copyright we request you at least leave in place the
	Powered by phpBB line, with phpBB linked to www.phpbb.com. If you refuse
	to include even this then support on our forums may be affected.

	The phpBB Group : 2002
// -->
D�velopp� par <a href="http://www.phpbb.com/" target="_phpbb" class="copyright">phpBB</a> &copy; 2001, 2005 phpBB Group<br />{TRANSLATION_INFO}<br />

Prot�g� par Anti-Spam ACP</span></div>
<div style="margin-top: 10px;" align="center">
	<a href="http://www.langue-francaise.org/" title="Visiter le site de l'association DLF (D�fense de la langue fran�aise)" target="_blank"><img src="/francais/images/logo_dlf.gif" alt="Logo de l'association D�fense de la langue fran�aise" style="border: 1px solid black;"></a>
</div>{X_INDEX_FOOTER}
		</td>
	</tr>
</table>

<script type="text/javascript" src="templates/subSilver/cookies.min.js?v={ASSETS_VERSION}"></script>
<script type="text/javascript" src="templates/subSilver/ads.js?"></script>
<script async type="text/javascript" src="templates/subSilver/common.min.js?v={ASSETS_VERSION}"></script>

<p id="infoBox" class="infoBox gen" style="display: none"></p>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script type="text/javascript" async="async" src="https://www.googletagmanager.com/gtag/js?id=UA-4130151-1"></script>
<script type="text/javascript">
/* <![CDATA[ */
window._gaq = window._gaq || [];
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-4130151-1');
/* ]]> */
</script>
</body>
</html>
