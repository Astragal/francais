(function (doc) {
    'use strict';

    var searchInterface = window.searchInterface ? window.searchInterface : "en";
    var prevnext = {
        en: ["Previous", "Next"],
        fr: ["Précédent", "Suivant"],
        it: ["Indietro", "Avanti"]
    }[searchInterface];
    var sitenameSearchPattern = window.sitenameSearchPattern ? new RegExp(window.sitenameSearchPattern) : null;
    var topicSearchPattern = window.topicSearchPattern ? new RegExp(window.topicSearchPattern) : null;

    function onSearchComplete() {
        var links = doc.querySelectorAll('a.gs-title');
        for (var i = 0; i < links.length; i++) {
            var link = links[i];

            if (link !== null) {
                if (topicSearchPattern) {
                    link.innerHTML = link.innerHTML.replace(topicSearchPattern, '');
                }
                if (sitenameSearchPattern) {
                    link.innerHTML = link.innerHTML.replace(sitenameSearchPattern, '');
                }

                var q = google.search.cse.element
                    .getElement('gsearch')
                    .getInputQuery();

                link.href = link.href.replace(/(\/viewtopic\.php\?.+)/,
                    '$1&highlight=' + encodeURIComponent(q).replace(/%20/g, '+') + '&ie=utf-8');
                link.removeAttribute('data-cturl');
                link.removeAttribute('data-ctorig');
            }
        }

        var currentPage = doc.querySelector('.gsc-cursor-current-page');
        var prev = createLink('\u25C4 ' + prevnext[0], currentPage.previousSibling);
        var next = createLink(prevnext[1] + ' \u25BA', currentPage.nextSibling);
        var parent = currentPage.parentNode;

        parent.appendChild(prev);
        parent.appendChild(next);
    }

    function createLink(text, targetElement) {
        var css = 'font-variant: small-caps;text-decoration: none;';
        var elm = doc.createElement('div');
        elm.textContent = text;
        elm.className = 'gsc-cursor-page';
        if (targetElement === null) {
            css += 'color: #CECECE; cursor: not-allowed;'
        } else {
            elm.addEventListener('click', function () {
                targetElement.click();
            });
        }
        elm.style.cssText = css;

        return elm;
    }

    // Watching the "new results" event
    new MutationObserver(function (mutations) {
        for (var i = 0; i < mutations.length; ++i) {
            if (mutations[i].target.classList.contains('gsc-results')) {
                onSearchComplete();
                break;
            }
        }
    }).observe(doc.querySelector('#cse'), {
        subtree: true,
        attributes: false,
        childList: true,
        characterData: false
    });
})(document);
