<?xml version="1.0" ?>
<rss version="2.0" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:atom="http://www.w3.org/2005/Atom">
	<channel>
		<docs>http://cyber.law.harvard.edu/rss/rss.html</docs>
		<title>{BOARD_TITLE}</title>
		<link>{BOARD_URL}</link>
		<description>{BOARD_DESCRIPTION}</description>
		<language>fr-fr</language>
		<managingEditor>{BOARD_MANAGING_EDITOR} ({BOARD_TITLE})</managingEditor>
		<webMaster>{BOARD_WEBMASTER} ({BOARD_TITLE})</webMaster>
		<lastBuildDate>{BUILD_DATE}</lastBuildDate>
		<atom:link href="{REQUEST_URI}" rel="self" type="application/rss+xml" />
		<!-- BEGIN post_item -->
		<item>
			<title>{post_item.FORUM_NAME} :: {post_item.TOPIC_TITLE}</title>
			<link>{post_item.POST_URL}</link>
			<guid>{post_item.POST_URL}</guid>
			<pubDate>{post_item.POST_PUBDATE}</pubDate>
			<dc:creator>{post_item.AUTHOR_PLAIN}</dc:creator>
			<description>{L_AUTHOR}: {post_item.AUTHOR}&lt;br /&gt;
				{post_item.POST_SUBJECT}
				{L_POSTED}: {post_item.POST_TIME}&lt;br /&gt;
				{L_TOPIC_REPLIES}: {post_item.TOPIC_REPLIES}&lt;br /&gt;&lt;br /&gt;
				&lt;span class="postbody"&gt;{post_item.POST_TEXT}
				{post_item.USER_SIG}&lt;/span&gt;&lt;br /&gt;
			</description>
		</item>
		<!-- END post_item -->
	</channel>
</rss>