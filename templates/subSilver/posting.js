/**
 * --charset iso-8859-1
 */

// ****************************************************************************
// BEGIN: JavaScript from posting_body.tpl

// Startup variables
var imageTag = false;
var theSelection = false;

// Check for Browser & Platform for PC & IE specific bits
// More details from: http://www.mozilla.org/docs/web-developer/sniffer/browser_type.html
var clientPC = navigator.userAgent.toLowerCase(); // Get client info
var clientVer = parseInt(navigator.appVersion); // Get browser version

var is_ie = ((clientPC.indexOf("msie") != -1) && (clientPC.indexOf("opera") == -1));
var is_nav = ((clientPC.indexOf('mozilla')!=-1) && (clientPC.indexOf('spoofer')==-1)
&& (clientPC.indexOf('compatible') == -1) && (clientPC.indexOf('opera')==-1)
&& (clientPC.indexOf('webtv')==-1) && (clientPC.indexOf('hotjava')==-1));
var is_moz = 0;

var is_win = ((clientPC.indexOf("win")!=-1) || (clientPC.indexOf("16bit") != -1));
var is_mac = (clientPC.indexOf("mac")!=-1);

// Shows the help messages in the helpline window
function helpline(help) {
    document.getElementById('helpbox').innerHTML = eval(help + "_help");
}


// Replacement for arrayname.length property
function getarraysize(thearray) {
    for (i = 0; i < thearray.length; i++) {
        if ((thearray[i] == "undefined") || (thearray[i] == "") || (thearray[i] == null))
            return i;
    }
    return thearray.length;
}

// Replacement for arrayname.push(value) not implemented in IE until version 5.5
// Appends element to the array
function arraypush(thearray,value) {
    thearray[ getarraysize(thearray) ] = value;
}

// Replacement for arrayname.pop() not implemented in IE until version 5.5
// Removes and returns the last element of an array
function arraypop(thearray) {
    thearraysize = getarraysize(thearray);
    retval = thearray[thearraysize - 1];
    delete thearray[thearraysize - 1];
    return retval;
}


function checkForm() {

    formErrors = false;

    if (document.post.message.value.length < 2) {
        formErrors = _l_emptyMessage;
    }

    if (formErrors) {
        alert(formErrors);
        return false;
    } else {
        bbstyle(-1);
        //formObj.preview.disabled = true;
        //formObj.submit.disabled = true;
        return true;
    }
}

// include the insert smilies fixes for Mozilla
function emoticon(text) {
    emoticon_insert(document.post.message, text);
}

function emoticon_insert(txtarea, text)
{
    text = ' ' + text + ' ';
    if (txtarea.createTextRange && txtarea.caretPos)
    {
        var caretPos = txtarea.caretPos;
        var baseHeight;
        if ( is_ie ) {
            baseHeight = document.selection.createRange().duplicate().boundingHeight;
        }
        if (baseHeight != txtarea.caretPos.boundingHeight) {
            txtarea.focus();
            storeCaret(txtarea);
        }
        caretPos.text = caretPos.text.charAt(caretPos.text.length - 1) == ' ' ? caretPos.text + text + ' ' : caretPos.text + text;
        txtarea.focus();
    }
    else if ( (txtarea.selectionEnd | txtarea.selectionEnd == 0) && (txtarea.selectionStart | txtarea.selectionStart == 0) )
    {
        mozInsert(txtarea, text, "");
    }
    else
    {
        txtarea.value  += text;
        txtarea.focus();
    }
}

// insert smilies fixes for Mozilla
function mozInsert(txtarea, openTag, closeTag) {
    var scrollTop = ( typeof(txtarea.scrollTop) == 'number' ? txtarea.scrollTop : -1 );
    if (txtarea.selectionEnd > txtarea.value.length) {
        txtarea.selectionEnd = txtarea.value.length;
    }

    var startPos = txtarea.selectionStart;
    var endPos = txtarea.selectionEnd + openTag.length;
    txtarea.value = txtarea.value.slice(0, startPos) + openTag + txtarea.value.slice(startPos);
    txtarea.value = txtarea.value.slice(0, endPos) + closeTag + txtarea.value.slice(endPos);
    txtarea.selectionStart = startPos + openTag.length;
    txtarea.selectionEnd = endPos;
    txtarea.focus();
    if (scrollTop >= 0) {
        txtarea.scrollTop = scrollTop;
    }
}

function bbfontstyle(bbopen, bbclose) {
    var txtarea = document.post.message;

    if ((clientVer >= 4) && is_ie && is_win) {
        theSelection = document.selection.createRange().text;
        if (!theSelection) {
            txtarea.value += bbopen + bbclose;
            txtarea.focus();
            return;
        }
        document.selection.createRange().text = bbopen + theSelection + bbclose;
        txtarea.focus();
        return;
    }
    else if (txtarea.selectionEnd && (txtarea.selectionEnd - txtarea.selectionStart > 0))
    {
        mozWrap(txtarea, bbopen, bbclose);
        return;
    }
    else
    {
        txtarea.value += bbopen + bbclose;
        txtarea.focus();
    }
    storeCaret(txtarea);
}


function bbstyle(bbnumber) {
    var txtarea = document.post.message;

    txtarea.focus();
    donotinsert = false;
    theSelection = false;
    bblast = 0;

    if (bbnumber == -1) { // Close all open tags & default button names
        while (bbcode[0]) {
            butnumber = arraypop(bbcode) - 1;
            txtarea.value += bbtags[butnumber + 1];
            buttext = eval('document.post.addbbcode' + butnumber + '.value');
            eval('document.post.addbbcode' + butnumber + '.value ="' + buttext.substr(0,(buttext.length - 1)) + '"');
        }
        imageTag = false; // All tags are closed including image tags :D
        txtarea.focus();
        return;
    }

    if ((clientVer >= 4) && is_ie && is_win)
    {
        theSelection = document.selection.createRange().text; // Get text selection
        if (theSelection) {
            // Add tags around selection
            document.selection.createRange().text = bbtags[bbnumber] + theSelection + bbtags[bbnumber+1];
            txtarea.focus();
            theSelection = '';
            return;
        }
    }
    else if (txtarea.selectionEnd && (txtarea.selectionEnd - txtarea.selectionStart > 0))
    {
        mozWrap(txtarea, bbtags[bbnumber], bbtags[bbnumber+1]);
        return;
    }

    // Find last occurance of an open tag the same as the one just clicked
    for (i = 0; i < bbcode.length; i++) {
        if (bbcode[i] == bbnumber+1) {
            bblast = i;
            donotinsert = true;
        }
    }

    if (donotinsert) {		// Close all open tags up to the one just clicked & default button names
        while (bbcode[bblast]) {
            butnumber = arraypop(bbcode) - 1;
            txtarea.value += bbtags[butnumber + 1];
            buttext = eval('document.post.addbbcode' + butnumber + '.value');
            eval('document.post.addbbcode' + butnumber + '.value ="' + buttext.substr(0,(buttext.length - 1)) + '"');
            imageTag = false;
        }
        txtarea.focus();
        return;
    } else { // Open tags

        if (imageTag && (bbnumber != 14)) {		// Close image tag before adding another
            txtarea.value += bbtags[15];
            lastValue = arraypop(bbcode) - 1;	// Remove the close image tag from the list
            document.post.addbbcode14.value = "Img";	// Return button back to normal state
            imageTag = false;
        }

        // Open tag
        txtarea.value += bbtags[bbnumber];
        if ((bbnumber == 14) && (imageTag == false)) imageTag = 1; // Check to stop additional tags after an unclosed image tag
        arraypush(bbcode,bbnumber+1);
        eval('document.post.addbbcode'+bbnumber+'.value += "*"');
        txtarea.focus();
        return;
    }
    storeCaret(txtarea);
}

// From http://www.massless.org/mozedit/
function mozWrap(txtarea, open, close)
{
    var selLength = txtarea.textLength;
    var selStart = txtarea.selectionStart;
    var selEnd = txtarea.selectionEnd;
    if (selEnd == 1 || selEnd == 2)
        selEnd = selLength;

    var s1 = (txtarea.value).substring(0,selStart);
    var s2 = (txtarea.value).substring(selStart, selEnd);
    var s3 = (txtarea.value).substring(selEnd, selLength);
    txtarea.value = s1 + open + s2 + close + s3;
    return;
}

// Insert at Claret position. Code from
// http://www.faqts.com/knowledge_base/view.phtml/aid/1052/fid/130
function storeCaret(textEl) {
    if (textEl.createTextRange) textEl.caretPos = document.selection.createRange().duplicate();
}

// END: JavaScript from posting_body.tpl
// ****************************************************************************

var __ext = (function (doc) {
    'use strict';

    if (typeof document.querySelector === "undefined") {
        return false;
    }

    var $patterns = [
        [/([^=]|^)"([^"]+)"([^]]|$)/gm, '$1��$2��$3'],          // guillemets fran�ais � �
        [/\*\*(?!\s)(.+?)(?!\s)\*\*/g, '[b]$1[/b]'],                        // **gras**
        [/''(?!\s)(.+?)(?!\s)''/g, '[i]$1[/i]'],                            // ''italique''
        [/(^|[^-])---(?!-)/gm, '$1\u2014'],                     // cadratin
        [/(^|[^-])--(?!-)/gm, '$1\u2013'],                      // demi-cadratin
        [/,,(?!\s)(.+?)(?!\s),,/g, '[sub]$1\[/sub]'],                       // ,,indice,,
        [/\^\^(?!\s)(.+?)(?!\s)\^\^/g, '[sup]$1\[/sup]'],                   // ^^exposant^^
        [/\^(?!\s)([^ ]+)\b/g, '[sup]$1\[/sup]'],               // exposant, ex. XX^e si�cle
        [/@@(?!\s)(.+?)(?!\s)@@(https?:\/\/[^\s]+)/gi, '[url=$2]$1[/url]'], // @@description de l'URL@@http://example.com
        [/(https?:\/\/[^\s]+)@@(?!\s)(.+?)(?!\s)@@/gi, '[url=$1]$2[/url]']  // http://example.com@@description de l'URL@@
    ];

    var textarea = doc.querySelector('table.tblpost textarea');
    var tblPost = doc.querySelector('table.tblpost tbody');
    var inputPrev = doc.querySelector('input[name="preview"]');
    var inputPost = doc.querySelector('input[name="post"]');

    function stringInsertAtCarret(el, val) {
        if (el.selectionStart || el.selectionStart === 0) {
            var startPos = el.selectionStart;
            var endPos = el.selectionEnd;
            el.value = el.value.substring(0, startPos) +
                val + el.value.substring(endPos, el.value.length);
        } else {
            el.value += val;
        }
    }

    return {
        SpChars: function (chars) {
            function onClick(e) {
                e.preventDefault();
                stringInsertAtCarret(textarea, e.target.textContent);
            }

            var tblSpChars = doc.getElementById('spChars');

            var html = '';
            var split = chars.split(/\h*,\h*/);
            split.forEach(function (ch) {
                html += '<a href="#">' + ch + '</a>';
            });
            tblSpChars.innerHTML = '<td colspan="13">' + html + '</td>';

            tblSpChars.addEventListener("click", onClick, false);

            var tr = document.querySelector('table.tblpost > tbody > tr:nth-child(4)');
            tr.parentNode.insertBefore(tblSpChars, tr);
        },

        AltMarkup: function () {
            function altMarkup() {
                var txt = textarea.value;
                $patterns.forEach(function (e) {
                    txt = txt.replace(e[0], e[1]);
                });
                textarea.value = txt;
            }

            var elAltMarkup = doc.getElementById('altMarkup');
            var showHideHelp = elAltMarkup.querySelector('td.row1 a');
            var buttons = elAltMarkup.querySelectorAll('td.row2 input');
            var btnAltMarkup = buttons[0];
            var btnAltMarkupPrev = buttons[1];
            var btnAltMarkupPost = buttons[2];
            var helpContent1 = doc.getElementById('altMarkup_help_1');
            var helpContent2 = doc.getElementById('altMarkup_help_2');

            var tr = doc.createElement('tr');
            var td = tr.appendChild(doc.createElement('td'));
            td.colSpan = 13;
            td.style = 'text-align: center; padding-top: 0px;';

            //
            // �v�nements
            //
            btnAltMarkup.addEventListener('click', altMarkup, false);
            btnAltMarkupPrev.addEventListener('click', function () {

                altMarkup();
                inputPrev.click();
            }, false);
            btnAltMarkupPost.addEventListener('click', function () {
                altMarkup();
                inputPost.click();
            }, false);
            showHideHelp.addEventListener('click', function (e) {
                e.preventDefault();
                if (helpContent1.style.display == 'none')
                {
                    helpContent1.style.display = '';
                    helpContent2.style.display = '';
                    showHideHelp.textContent = 'Masquer l\'aide';
                }
                else
                {
                    helpContent1.style.display = 'none';
                    helpContent2.style.display = 'none';
                    showHideHelp.textContent = 'Afficher l\'aide';
                }
            });
        }
    };
})(document);
