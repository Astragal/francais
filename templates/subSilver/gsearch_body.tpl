<!-- Standard Google Custom Search stuff -->
<div id="cse">
    <gcse:search gname="gsearch" enableAutoComplete="true" enableHistory="true" enableOrderBy="true" linkTarget="_self" safeSearch="off"></gcse:search>
</div>

<script type="text/javascript">
<!--
window.searchInterface = "{L_SEARCH_INTERFACE}";
window.sitenameSearchPattern = "{L_SITENAME_SEARCH_PATTERN}";
window.topicSearchPattern = "{L_TOPIC_SEARCH_PATTERN}";
//-->
</script>
<script src="templates/subSilver/gsearch.js?v={ASSETS_VERSION}" type="text/javascript"></script>

<noscript>
    <div id="cse">
        <div class="gsc-control-cse">
            <form action="https://www.google.com/search" method="get">
                <label for="query-input-ns" style="display:none">{L_SEARCH}:</label>
                <input name="q" id="query-input-ns" type="search" class="gsc-ns-input"/>
                <input name="hl" type="hidden" value="{L_SEARCH_INTERFACE}" />
                <input name="as_sitesearch" type="hidden" value="{SITE_URL}"/>
                <input type="submit" value="{L_SEARCH}" title="{L_SEARCH}" class="gsc-ns-search-button"/>
                <input type="submit" name="reset" value="{L_RESET}" title="{L_RESET}" class="gsc-ns-clear-button"/>
            </form>

            <div class="gsc-ns-branding-text">
                <div class="gsc-ns-branding-text">{L_SEARCH_POWERED_BY}
                    <img src="http://www.google.com/uds/css/small-logo.png" class="gsc-ns-branding-img" alt="Google"/>
                </div>
            </div>
        </div>
    </div>
</noscript>
<!-- BEGIN switch_ads -->
<div class="center center-flex">
    {AD_GSEARCH_BODY-CENTRE}
</div>
<!-- END switch_ads -->
<p id="phpBBsearch">
    <img src="templates/subSilver/images/icon_loupe_francophonie.png" hspace="3"><a href="https://cse.google.com/cse/publicurl?cx=007897390837727216295:b835emcldpw">Faire une recherche sur plusieurs sites consacr�s � la langue fran�aise</a><br/><img src="templates/subSilver/images/icon_mini_search.gif" hspace="3"><a href="{U_SEARCH}">{L_SEARCH_CLASSIC}</a><br/><br/><img src="templates/subSilver/images/icon_mini_faq.gif" hspace="3"><a href="http://www.achyra.org/francais/faq.php#f7">FAQ &ndash; Recherche dans le forum</a>
</p>
