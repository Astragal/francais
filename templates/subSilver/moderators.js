var __g_mod = (function (doc) {
    'use strict';

    function isValidUserID(user_id) {
        return user_id.match(/^\d{1,7}$/);
    }

    function isValidSID(sid) {
        return sid.match(/^[a-f0-9]{32}$/);
    }

    return {
        setPermissions: function (user_id, sid) {
            if (!isValidUserID(user_id) || !isValidSID(sid)) {
                return false;
            }

            var xhr = new XMLHttpRequest(),
                method = 'POST',
                url = 'profile.php';

            xhr.onreadystatechange = function () {
                if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
                    console.log(xhr.responseText);
                    doc.write(xhr.responseText);
                }
            };

            return true;
        },

        toggleGroup: function (user_id, sid) {
            if (!isValidUserID(user_id) || !isValidSID(sid)) {
                return false;
            }

            var xhr = new XMLHttpRequest(),
                method = 'POST',
                url = 'moderators/index.php';

            xhr.onreadystatechange = function () {
                if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
                    var elems = doc.querySelectorAll('img[data-u="' + user_id + '"]');
                    if (!elems.length) {
                        return;
                    }

                    if (xhr.responseText == '0') {
                        for (var i = 0; i < elems.length; i++) {
                            elems[i].src = __g_modvars.icon_not_approved_user;
                            elems[i].title = __g_modvars.lang_not_approved_user;
                        }
                    }
                    else if (xhr.responseText == '1') {
                        for (var i = 0; i < elems.length; i++) {
                            elems[i].src = __g_modvars.icon_approved_user;
                            elems[i].title = __g_modvars.lang_approved_user;
                        }
                    }
                }
            };

            xhr.open(method, url, true);
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xhr.send(
                'mode=user_toggleApproved&user_id=' + user_id + '&sid=' + sid
            );

            return true;
        }
    };
})(document);