<?php
/***************************************************************************
 *                                gsearch.php
 *                            -------------------
 *   begin                : Saturday, Feb 13, 2001
 *   copyright            : (C) 2001 The phpBB Group
 *   email                : support@phpbb.com
 *
 *   $Id: search.php,v 1.72.2.19 2006/02/05 15:59:48 grahamje Exp $
 *
 *
 ***************************************************************************/

/***************************************************************************
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

define('IN_PHPBB', true);
$phpbb_root_path = './';
include($phpbb_root_path . 'extension.inc');
include($phpbb_root_path . 'common.'.$phpEx);

global $show_ads;

//
// Start session management
//
$userdata = session_pagestart($user_ip, PAGE_SEARCH);
init_userprefs($userdata);
//
// End session management
//

//
// Output the basic page
//
$page_title = $lang['Search'];
$bfcache_refresh = true;
include($phpbb_root_path . 'includes/page_header.'.$phpEx);

$template->set_filenames(array(
	'body' => 'gsearch_body.tpl'
	)
);

make_jumpbox('viewforum.'.$phpEx);

$l_search_interface = strtolower(substr($board_config['default_lang'], 0, 2));
$sitename_search_pattern = '^(<b>)?' . $board_config['sitename'] . '(<\/b>)? :: ';
$topic_search_pattern = $sitename_search_pattern . '(<b>)?' . $lang['View_topic'] . '(<\/b>)? - | :: (<b>)?' . $board_config['sitename'] . '(<\/b>)?$';

$template->assign_vars(array(
	'L_SEARCH_CLASSIC' => $lang['Search_classic'],
	'L_SEARCH_LOADING' => $lang['Search_loading'],
	'L_SEARCH_NEXT' => $lang['Search_next_result_page'],
	'L_SEARCH_PREVIOUS' => $lang['Search_previous_result_page'],
	'L_SEARCH_NO_RESULTS' => $lang['Search_no_results'],
	'L_SEARCH_POWERED_BY' => $lang['Search_powered_by'],
	'L_RESET' => $lang['Reset'],
    'L_SEARCH_INTERFACE' => $l_search_interface,
    'L_SITENAME_SEARCH_PATTERN' => $sitename_search_pattern,
    'L_TOPIC_SEARCH_PATTERN' => $topic_search_pattern
	)
);

// Advertising
if ($show_ads)
{
    global $ads;
    $template->assign_vars(array(
            'AD_GSEARCH_BODY-CENTRE' => $ads->getAd('gsearch_body-centre'))
    );
}

$template->pparse('body');

include($phpbb_root_path . 'includes/page_tail.'.$phpEx);
?>